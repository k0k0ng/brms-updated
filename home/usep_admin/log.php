<?php
  require('../session/usep_admin.php'); // Secure Connection
  require('../database/usep_admin_database_query.php'); // Database Query
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>    

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->
    <link rel="stylesheet" href="../css/table_Design.css"> <!-- COSTUMIZE TABLE COLOR -->
  
    <script>
      // OnKeyRelease (Load Related Purok)
      function load_logs(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {   
            document.getElementById("responsecontainer1").innerHTML = this.responseText;
          }
        };

        xmlhttp.open("GET", "../ajax/loadAjax_logBarangayHistory.php?&str="+str, true);
        xmlhttp.send();        
      }
    </script>

    <script>

      // Document Ready
      $(document).ready(function(){
        load_logs(''); // Load Logs
      });
    </script>

    <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <br>

    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


          <!--Search-->
          <div class="input-group">
            <!-- don't give a name === doesn't send on POST/GET -->
            <span class="input-group-addon"><b>Search 15 Most Recent Logs</b></span>
            <input type="text" onkeyup="load_logs(this.value)" class="form-control image-preview-filename" placeholder="Barangay or Admin or History" autofocus>  
          </div><!-- /input-group image-preview [TO HERE]--> 

          <br>

          <div id="table_Value1" class="table table-hover table-mc-light-blue">          
            <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th>Barangay</th>
                  <th>Admin</th>
                  <th>History</th> 
                  <th>Date</th>
                  <th>Time</th>
                </tr>
              </thead>                    
              <tbody id="responsecontainer1" style="cursor: pointer;"></tbody>
            </table>
          </div>       

        </div>
      </div>
    </div>

  </body>

</html>