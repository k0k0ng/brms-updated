<?php
  require('../session/usep_admin.php'); // Secure Connection    
  require('../session/usep_admin_barangay_purok.php'); // Secure Connection (Barangay Purok Value is_set)
  require('../database/usep_admin_database_query.php'); // Database Query
?>

<?php
  if($_SERVER["REQUEST_METHOD"] == "POST" && ($_POST["submit"] === "Add New Admin")){
   
    $brgy_id = $_SESSION['brms_usep_admin_barangay_purok_brgy_id'];
    $first_name = $_POST["first_name"];
    $middle_name = $_POST["middle_name"];
    $last_name = $_POST["last_name"];
    $username = $_POST["username"];
    $password = $_POST["password"];
    $access_role = $_POST["access_level"];

    //unset($_POST); // Unset Post
    
    // Success
    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> insert_admin_info($brgy_id, $first_name, $middle_name, $last_name,$username,$password,$access_role);

    // If Exist
    if($result == -1){
      echo "<script type='text/javascript'>alert('Admin already exist!'); location='barangay_purok.php';</script>";  
    }
    // New Admin Added
    else if($result == 1){       
      echo "<script type='text/javascript'>alert('Admin successfully added!'); location='barangay_purok.php';</script>";      
    }        
    // Error
    else{
      echo "<script type='text/javascript'>alert('Username already exist!')</script>";
    }        
  }
?>

<?php
  if($_SERVER["REQUEST_METHOD"] == "POST" && ($_POST["submit"] === "Update Admin")){
   
    $brgy_id = $_SESSION['brms_usep_admin_barangay_purok_brgy_id'];
    $info_id = $_POST["info_id"];
    $first_name = $_POST["first_name"];
    $middle_name = $_POST["middle_name"];
    $last_name = $_POST["last_name"];
    $username = $_POST["username"];
    $password = $_POST["password"];
    $access_role = $_POST["access_level"];

    unset($_POST); // Unset Post

    // Success
    $query = new database_query(); // Database Query (initialize connection)     
    
    // Success
    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> update_admin_info($brgy_id,$info_id,$first_name,$middle_name,$last_name,$username,$password,$access_role);

    // If Exist
    if($result == -1){
      echo "<script type='text/javascript'>alert('Admin already exist!');</script>";  
    }
    // Admin Updated
    else if($result == 1){       
      echo "<script type='text/javascript'>alert('Admin successfully updated!'); location='barangay_purok.php';</script>";      
    }        
    // Error
    else{
      echo "<script type='text/javascript'>alert('Username already exist!')</script>";
    }        
  }
?>

<?php
  if($_SERVER["REQUEST_METHOD"] == "POST" && ($_POST["submit"] === "Delete Admin")){
   
    $info_id = $_POST["info_id"];

    unset($_POST); // Unset Post

    // Database
    $query = new database_query(); // Database Query (initialize connection)     
    
    // Execute Query
    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> delete_account($info_id);

    // If Success
    if($result == 1){       
      echo "<script type='text/javascript'>alert('Admin deleted successfully!'); location='barangay_purok.php';</script>";      
    }        
    // Error
    else{
      echo "<script type='text/javascript'>alert('Error!')</script>";
    }        
  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">    

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->
    <link rel="stylesheet" href="../css/table_Design.css"> <!-- COSTUMIZE TABLE COLOR -->
    
    <script>
      // On Submit
      function doCheckDelete(){

        // Confirmation
        if (confirm("Are you sure to delete this admin?") == true) {
          return true;
        } else {
          return false;
        }
        
      }
    </script>

    <script>
      // On Submit
      function doCheckUpdate(){

        var message = ((document.getElementById("new_admin").value === "Update Admin") ? "update" : " add");
        
        // Confirmation
        if (confirm("Are you sure to "+ message +" this admin?") == true) {
          return true;
        } else {
          return false;
        }
        
      }
    </script>

    <script>
      var valid = "glyphicon glyphicon-ok"; // VALID
      var invalid = "glyphicon glyphicon-remove"; // INVALID
    </script>
    
    <script>
      // Check if Valid Admin
      function valid_admin(){
        
        if(document.getElementById("valid_pass").className === valid && document.getElementById("valid_user").className === valid
          && (document.getElementById("password").value !== '' && document.getElementById("confirm") !== '')){
          document.getElementById("new_admin").disabled = false; 
        }else{
          document.getElementById("new_admin").disabled = true; 
        }
      }
    </script>

    <script>
      // Check if username exist
      function check_username(username){

        if(username.length == 0){
          document.getElementById("valid_user").className = invalid;
          valid_admin(); // Check if Valid (Submit Button) 
        }
        else{
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
          
          if (this.readyState == 4 && this.status == 200) {
              // Username Found
              if(this.responseText == 1){
                document.getElementById("valid_user").className = invalid;            
              }
              // Username Not Found
              else{                
                document.getElementById("valid_user").className = valid;                
              }
              valid_admin(); // Check if Valid (Submit Button)               
            }
          };          

          xmlhttp.open("GET", "../ajax/checkAjax_admin.php?username=" + username, true);
          xmlhttp.send();
        }        
      }
    </script>

    <script>
      // OnKeyRelease (Check if password match)
      function verify(){
        var password = document.getElementById("password").value;
        var confirm = document.getElementById("confirm").value;        

        if(password === confirm){
          document.getElementById("valid_pass").className = valid;
        }else{
          document.getElementById("valid_pass").className = invalid;
        }

        valid_admin(); // Check if Valid (Submit Button)
      }
    </script>  

    <script>
        // Document Ready
        $( document ).ready(function() {  
          loadAccessLevel(); // Load Access Level
        });
    </script>

    <script>
      // Update Admin
      function update(){ 
          var info_id = "<?php echo $_SESSION['brms_usep_admin_add_info_id']; ?>";
          var first_name = "<?php echo $_SESSION['brms_usep_admin_add_first_name']; ?>";
          var middle_name = "<?php echo $_SESSION['brms_usep_admin_add_middle_name']; ?>";
          var last_name = "<?php echo $_SESSION['brms_usep_admin_add_last_name']; ?>";
          var role = "<?php echo $_SESSION['brms_usep_admin_add_role']; ?>";
          var username = "<?php echo $_SESSION['brms_usep_admin_add_username']; ?>";
          var password = "<?php echo $_SESSION['brms_usep_admin_add_password']; ?>";
          var submit_value = "Update Admin";
          
          if(info_id !== ''){
            document.getElementById("info_id").value = info_id;
            document.getElementById("first_name").value = first_name;
            document.getElementById("middle_name").value = middle_name;
            document.getElementById("last_name").value = last_name;
            document.getElementById("new_admin").value = submit_value;
            document.getElementById("username").value = username;
            //document.getElementById("password").value = password;
            //document.getElementById("confirm").value = password;
            $("#access_level_list").val(role); // Role
            document.getElementById("valid_user").className = valid; // Valid Username
            
            document.getElementById("username").readOnly = true;
            document.getElementById("del_admin").style.display = 'block';
          
            document.getElementById("valid_pass").className = valid;

            document.getElementById("new_admin").disabled = true;

          }else{
            var select = document.getElementById('access_level_list');
            select.removeChild(select.lastChild);
          }
      }
    </script>

    <script>
      // Load Access Level Role
      function loadAccessLevel(){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) { 

            var data = this.responseText.split(",");        

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#access_level_list').append("<option>"+data[i]+"</option>")
              }
            }
              update(); // Load Update
          }
        };
        
        xmlhttp.open("GET", "../ajax/loadAjax_access_level.php?", true); // Get access levels
        xmlhttp.send();
      }
    </script>

    <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container container-profile"> <!-- Profile Divider -->  
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
          <div class="panel panel-default">

            <div class="panel-heading">
              <h4>Barangay <?php echo $_SESSION['brms_usep_admin_barangay_purok_brgy_name'];?> (Grant Access)</h4>
            </div>

            <div class="panel-body">             
              
              <br>

              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">
                  <!--FML Name-->
                  <div class="input-group" style="display: none;">
                    <span class="input-group-addon">Info ID</span>
                    <input id="info_id" type="text" class="form-control" name="info_id" placeholder="Info ID">
                  </div>
              
                  <div class="input-group">
                    <span class="input-group-addon">First Name</span>
                    <input id="first_name" type="text" class="form-control" name="first_name" placeholder="First Name" required>
                  </div>
              <br>
                  <div class="input-group">
                    <span class="input-group-addon">Middle Name</span>
                    <input id="middle_name" type="text" class="form-control" name="middle_name" placeholder="Middle Name" required>
                  </div>
              <br>
                  <div class="input-group">
                    <span class="input-group-addon">Last Name</span>
                    <input id="last_name" type="text" class="form-control" name="last_name" placeholder="Last Name" required>
                  </div>
           
              <br>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-star"></i> Access Level</span>
                    <select id="access_level_list" name="access_level" class="form-control">
                    </select>
                  </div>
              <br>
                  <div class="input-group">
                    <span class="input-group-addon">Username</span>
                    <input id="username" type="text" onkeyup="check_username(this.value)" class="form-control" name="username" placeholder="Username" required>
                    <span class="input-group-addon"><i id="valid_user" class="glyphicon glyphicon-remove"/></i></span>
                  </div>
              <br>
                  <div class="input-group">
                    <span class="input-group-addon">Password</span>
                    <input id="password" type="password" onkeyup="verify()" class="form-control" name="password" placeholder="Password">
                  </div>
              <br>
                  <div class="input-group">
                    <span class="input-group-addon">Confirm Password</span>
                    <input id="confirm" type="password" onkeyup="verify()" class="form-control" name="confirm" placeholder="Confirm Password">
                    <span class="input-group-addon"><i id="valid_pass" class="glyphicon glyphicon-remove"/></i></span>
                  </div>

              <br>
                  <div class="input-group" style="float: right;">                    
                    <input name="submit" type="submit" onclick="return doCheckUpdate()" id="new_admin" value="Add New Admin" class="btn btn-primary" style="float: right; margin-left:10px; width:125px;">
                    <input name="submit" type="submit" onclick="return doCheckDelete()" id="del_admin" value="Delete Admin" class="btn btn-danger" style="float: right; margin-left:10px; display: none; width:125px;">
                    <input name="submit" type="button" onclick="window.location.href='barangay_purok.php';" value="Cancel" class="btn btn-info" style="float: right; width:125px;">
                  </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </body>

</html>