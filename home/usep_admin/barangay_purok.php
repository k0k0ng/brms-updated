<?php  
  require('../session/usep_admin.php'); // Secure Connection    
  require('../session/usep_admin_barangay_purok.php'); // Secure Connection (Barangay Purok Value is_set)
  require('../database/usep_admin_database_query.php'); // Database Query
?>

<?php
  if($_SERVER["REQUEST_METHOD"] == "POST" && ($_POST["submit"] === "Update Name")){
    $brgy_name = $_POST["brgy_name"]; // Barangay Name
    $brgy_id = $_SESSION['brms_usep_admin_barangay_purok_brgy_id']; // Barangay ID

    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> update_brgy_name($brgy_id,$brgy_name);

    if($result == 1){
      $_SESSION['brms_usep_admin_barangay_purok_brgy_name'] = $_POST["brgy_name"];
      echo "<script type='text/javascript'> 
              alert('Barangay $brgy_name successfully updated!');
              location = 'barangay_purok.php';
            </script>";
    }else{
      echo "<script type='text/javascript'> alert('Barangay $brgy_name already exist!'); </script>";
    }
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <title>BRMS - Barangay Record Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/custom2test.css">    

  <link rel="javascript" src="../js/jquery.js">
  <link rel="javascript" src="../js/jquery.min.js">

  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->

  <script>
      // On Submit
      function doCheck(){

        if (confirm("Are you sure to update this barangay?") == true) {
            return true;
          } else {
            return false;
          }
        
      }
    </script>

  <script>
      // OnKeyRelease (Check if Barangay Exist)
      function checkBarangay(brgy_name){

        var valid = "glyphicon glyphicon-ok";
        var invalid = "glyphicon glyphicon-remove";
        var city = "Davao City"; // DEFAULT CITY

        if(brgy_name.length == 0){           
          document.getElementById("valid_brgy").className = invalid;
          document.getElementById("update_brgy").disabled = true;
        }else{          
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) { 

              // Barangay Found
              if(this.responseText === 'true'){
                document.getElementById("valid_brgy").className = invalid;
                document.getElementById("update_brgy").disabled = true;               
              }
              // Barangay Not Found
              else{                
                document.getElementById("valid_brgy").className = valid;
                document.getElementById("update_brgy").disabled = false;
              }                
            }
          };
          
          xmlhttp.open("GET", "../ajax/checkAjax_barangay.php?brgy_name=" + brgy_name +"&city=" + city, true);
          xmlhttp.send();
        }
      }
    </script>

    <script>
      // OnKeyRelease (Load Related Purok)
      function checkPurokTable(purok_name){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {   
            document.getElementById("responsecontainer").innerHTML = this.responseText;
          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_usep_admin_barangay_purok_brgy_id']; ?>"; // Barangay id
        xmlhttp.open("GET", "../ajax/loadAjax_barangay_purok_table.php?brgy_id="+brgy_id+"&purok_name="+purok_name, true);
        xmlhttp.send();        
      }
    </script>

    <script>
      // OnKeyRelease (Load Related Purok)
      function checkAdminable(name){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {   
            document.getElementById("responsecontainer2").innerHTML = this.responseText;
          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_usep_admin_barangay_purok_brgy_id']; ?>"; // Barangay id
        xmlhttp.open("GET", "../ajax/loadAjax_barangay_admin_table2.php?brgy_id="+brgy_id+"&name="+name, true);
        xmlhttp.send();        
      }
    </script>

    <script>
      // DocumentReady
      $( document ).ready(function() {
        checkAdminable(''); // Load Admin

        $("#table_Value").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var info_id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value

          window.location.href="../session/usep_admin_add.php?info_id="+info_id; // Update Sesssion (@session/usep_admin_update_session_barangay_purok.php)

        });
      });
    </script>

    <!--for navigation bar-->

    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container"> <!-- Profile Divider -->  
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">         
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4>Barangay <?php echo $_SESSION['brms_usep_admin_barangay_purok_brgy_name']; ?> (Admins)</h4>
            </div>

              <div class="panel-body">
                <div class="row">
                  <form method="post" role="form" autocomplete="off" action="">
                    <div class="col-xs-10 col-sm-10 col-md-9 col-lg-9">
                      <div class="input-group">
                        <span class="input-group-addon">Barangay Name</span>                  
                        <input id="brgy_name" type="text" onkeyup="checkBarangay(this.value)" class="form-control" name="brgy_name" placeholder="Barangay Name" value="<?php echo $_SESSION['brms_usep_admin_barangay_purok_brgy_name']; ?>" required>
                        <span class="input-group-addon"><i id="valid_brgy" class="glyphicon glyphicon-remove"/></i></span>
                      </div>
                    </div>

                    <div class="col-xs-2 col-sm-2 col-md-3 col-lg-3">
                      <input id="update_brgy" name="submit" type="submit" onclick="return doCheck()" value="Update Name" class="btn btn-primary" style="float: right;" disabled>
                    </div>
                  </form>

                  <br>
                  <br>
                  <br>

                  <form method="post" role="form" autocomplete="off" action="">
                    <div class="col-xs-9 col-sm-10 col-md-9 col-lg-9">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>                  
                        <input id="purok_name" type="text" onkeyup="checkAdminable(this.value)" class="form-control" name="purok_name" placeholder="Search" required autofocus>
                      </div>
                    </div>
                    <div class="col-xs-3 col-sm-2 col-md-3 col-lg-3">
                      <input id="submit_admin" name="submit" onclick="window.location.href='../session/usep_admin_add_unset.php';" type="button" value="Grant Access" class="btn btn-primary" style="float: right;">
                    </div>
                  </form>
                  
                </div>

                <br>

                <form method="post" role="form" autocomplete="off" action="">
                  <div id="table_Value" class="table-responsive"> 
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Admin Name</th>
                          <th>Role</th>
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer2"></tbody>
                    </table>
                  </div>
                </form>

              </div>

            </div>
          </div>
        </div>
      </div>      

  </body>

</html>