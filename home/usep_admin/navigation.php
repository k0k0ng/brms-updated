<div class="example3">
  <nav class="navbar navbar-inverse navbar-static-top" style="background-color: #7a0404;">
    <div class="container">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>        
        <a href="admin_home.php"><img src="../brgy_image/brms_logo.png" style="height: 60px;margin-top: 10px;"></a>
      </div>

      <div id="navbar3" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="background-color: #7a0404;">USEP Admin <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" style="padding: 0px; border: 0px;" >
              <li>
                <div class="container-fluid" style="padding: 0px;">
                  <div class="row">
                    <div class="col-md-12">   
                      <a href="change_password.php" class="list-group-item list-group-item-action">Change Password</a>
                      <a href="../session/usep_admin_logout.php" class="list-group-item list-group-item-action">Log out</a>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>

          <li><a href="admin_home.php">Home</a></li>   
          <li><a href="log.php">Logs</a></li>
        </ul>
      </div> 

    </div>
  </nav>
</div>