<?php
  require('../session/usep_admin.php'); // Secure Connection
  require('../database/usep_admin_database_query.php'); // Database Query
?>

<?php
  if($_SERVER["REQUEST_METHOD"] == "POST" && ($_POST["submit"] === "Add Barangay")){
    $query = new database_query(); // Database Query (initialize connection)
    
    $brgy_name = $_POST["brgy_name"]; // Barabgay Name
    $city = $_POST["city"]; // City
    unset($_POST); // Unset Post

    // Success
    if ($query -> insert_barangay($brgy_name, $city) == 1) {
        echo "<script type='text/javascript'>alert('Barangay $brgy_name successfully added!'); location='admin_home.php';</script>";
    }        
    // Error
    else{
      echo "<script type='text/javascript'>alert('Barangay $brgy_name already exist!')</script>";
    }
  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>    

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->
    <link rel="stylesheet" href="../css/table_Design.css"> <!-- COSTUMIZE TABLE COLOR -->

    <script>
      // On Submit
      function doCheck(){

        var brgy_name = document.getElementById("brgy_name").value; // Get Barangay Name
        document.getElementById("brgy_name").value = brgy_name.replace(/\s/g, ""); // Remove WhiteSpace
        
        // Check if Empty
        if (brgy_name) {

          // Confirmation
          if (confirm("Are you sure to add this barangay?") == true) {
            return true;
          } else {
            return false;
          }
          
        } else{
          alert("Invalid barangay name!");
          return false;
        }
        
      }
    </script>

    <script>
      // OnKeyRelease (Check if Barangay Exist)
      function checkBarangay(brgy_name){

        checkBarangayTable(brgy_name); // Barangay Table (List of Registered Barangay)

        var valid = "glyphicon glyphicon-ok";
        var invalid = "glyphicon glyphicon-remove";
        var city = "Davao City"; // DEFAULT CITY

        if(brgy_name.length == 0){           
          document.getElementById("valid_brgy").className = invalid;
          document.getElementById("submit_brgy").disabled = true;
        }else{          
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) { 

              // Barangay Found
              if(this.responseText == 1){
                document.getElementById("valid_brgy").className = invalid;
                document.getElementById("submit_brgy").disabled = true;               
              }
              else {                
                document.getElementById("valid_brgy").className = valid;
                document.getElementById("submit_brgy").disabled = false;
              }                
            }
          };
          
          xmlhttp.open("GET", "../ajax/checkAjax_barangay.php?brgy_name=" + brgy_name, true);
          xmlhttp.send();
        }
      }
    </script>

    <script>
      // OnKeyRelease
      function checkBarangayTable(brgy_name){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {   
            document.getElementById("responsecontainer").innerHTML = this.responseText; 
          }
        };
        
        xmlhttp.open("GET", "../ajax/loadAjax_barangay_purokCount_table.php?brgy_name="+brgy_name, true);
        xmlhttp.send();
        
      }
    </script>

    <script>
      // DocumentReady
      $( document ).ready(function() {
        checkBarangayTable(''); // Load Barangays

        // On Click Table
        $("#table_Value").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var brgy_id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var brgy_name = currentRow.find("td:eq(1)").text(); // get current row 2nd TD value
          var registered_purok = currentRow.find("td:eq(2)").text(); // get current row 3rd TD value

          window.location.href="../session/usep_admin_update_session_barangay_purok.php?brgy_id="+brgy_id+"&brgy_name="+brgy_name+"&registered_purok="+registered_purok; // Update Sesssion (@session/usep_admin_update_session_barangay_purok.php)
        });
        
      });
    </script>

    <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!--SEARCH BAR-->
    <div class="container">    
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4>Registered Barangays (Davao City)</h4>
            </div>

            <div class="panel-body">

              <form method="post" role="form" autocomplete="off" action="">

                <div class="row">
                  <div class="col-xs-9 col-sm-9 col-md-9 col-lg-10">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>                  
                      <input id="brgy_name" type="text" onkeyup="checkBarangay(this.value)" class="form-control" name="brgy_name" placeholder="Barangay Name" required autofocus>
                      <span class="input-group-addon"><i id="valid_brgy" class="glyphicon glyphicon-remove"/></i></span>
                    </div>
                  </div>

                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
                    <input id="submit_brgy" name="submit" type="submit" onclick="return doCheck()" value="Add Barangay" class="btn btn-primary" style="float: right;">
                  </div>
                </div>

                <br>

                <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>                  
                  <input id="city" type="text" class="form-control" name="city" placeholder="City" value="Davao City" readonly>
                  <span class="input-group-addon"><i class="glyphicon glyphicon-ok"/></i></span>
                </div> 

              </form>

              <br>

              <div id="table_Value" class="table-responsive">          
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Barangay Name</th>
                      <th>Admins</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer" style="cursor: pointer;"></tbody>
                </table>
              </div>       

            </div>
            
          </div>

        </div>
      </div>
    </div>

  </body>

</html>