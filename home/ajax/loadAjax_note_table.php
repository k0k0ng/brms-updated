<?php
	
	include '../database/database.php'; // Database Connection

	// This will load transaction

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$info_id = $_REQUEST["info_id"]; // Barangay id
		$str = $_REQUEST["str"]; // Input Value

		$sql = "SELECT 
					(CASE WHEN note.info_id IN (SELECT id FROM user_info) 
							THEN 
							    (
								    SELECT CONCAT(last_name ,', ', first_name ,' ', middle_name) 
									FROM user_info 
									WHERE id = note.info_id AND brgy_id = $brgy_id
								)
							ELSE
							    (
								    SELECT business.name 
									FROM business
									INNER JOIN purok ON purok.id = business.purok_id 
									WHERE business.id = note.info_id AND purok.brgy_id = $brgy_id
								)
					END) AS 'Citizen/Business',	

					date_note AS 'Date Noted',
					LEFT(note, 200) AS 'Notes'
				FROM note
				LEFT OUTER JOIN user_info ON user_info.id = note.info_id
				LEFT OUTER JOIN business ON business.id = note.info_id
				WHERE note.admin_id = '$info_id'
					AND (CONCAT(user_info.last_name ,', ', user_info.first_name ,' ', user_info.middle_name) LIKE '%$str%'
					OR business.name LIKE '%$str%'
					OR note.note LIKE '%$str%')
				ORDER BY note.id DESC 
				LIMIT 10";
					
		$result = $conn->query($sql);
		
		if ($result->num_rows > 0) {
			echo "<tbody>";				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr><td>" . $row['Citizen/Business'] .
						"</td><td>" . date("F d, Y", strtotime($row['Date Noted'])) .
						"</td><td>" . date("h:i:s A", strtotime($row['Date Noted'])) . 
						"</td><td style=\"max-width: 300px\">" . $row['Notes'] . 
					  "</td></tr>";
			}
			echo "</tbody>";
		}else{
			echo "";
		}

		mysqli_close($conn);
	}
?>