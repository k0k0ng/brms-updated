<?php
	
	include '../database/database.php'; // Database Connection

	// This will get citizen's id , first_name , last_name
	
	if ($_SERVER["REQUEST_METHOD"] == "GET"){

		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$q = $_REQUEST["q"];
		$brgy_id = $_REQUEST["brgy_id"];	
			
		$sql = "SELECT 
					(CASE WHEN user_info.id IS NOT NULL THEN user_info.id ELSE '' END) AS id,
					(CASE WHEN user_info.first_name IS NOT NULL THEN user_info.first_name ELSE '' END) AS first_name,
					(CASE WHEN user_info.middle_name IS NOT NULL THEN user_info.middle_name ELSE '' END) AS middle_name,
					(CASE WHEN user_info.last_name IS NOT NULL THEN user_info.last_name ELSE '' END) AS last_name,
					(CASE WHEN account.username IS NOT NULL THEN account.username ELSE '' END) AS username,
					(CASE WHEN account.password IS NOT NULL THEN account.password ELSE '' END) AS password,
					(CASE WHEN access_level.role IS NOT NULL THEN access_level.role ELSE '' END) AS role
				FROM user_info
				LEFT JOIN account ON user_info.id = account.info_id
				LEFT JOIN access_level ON access_level.id = account.access_id
				WHERE user_info.brgy_id = $brgy_id
				AND (
						CONCAT(user_info.last_name ,', ', user_info.first_name ,' ', user_info.middle_name) LIKE '%$q%'
					)
				LIMIT 1";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo $row['id'] . ',' . 
				$row['first_name'] . ',' . 
				$row['middle_name'] . ',' . 
				$row['last_name'] . ',' .
				$row['username'] . ',' . 
				$row['password'] . ',' . 
				$row['role'];
			}
		}
		else{
			echo "";
		}
		mysqli_close($conn);
	}
?>