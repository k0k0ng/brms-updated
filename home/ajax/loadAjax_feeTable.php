<?php
	
	include '../database/database.php'; // Database Connection

	// This will load fee list

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$type = $_REQUEST["type"]; // User info id

		$sql = "SELECT * FROM fee WHERE brgy_id = $brgy_id AND type LIKE '%$type%' ORDER BY type LIMIT 3";

		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {
			echo "<tbody>";				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr class=\"selectedRow\"><td>" . $row['type'] . "</td><td style=\"display:none;\">" . $row['amount'] . "</td></tr>";
			}
			echo "</tbody>";
		}else{
			echo "";
		}
		
		mysqli_close($conn);
	}
?>