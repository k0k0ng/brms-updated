<?php
	
	include '../database/database.php'; // Database Connection

	// This will load transaction with admin

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$info_id = $_REQUEST["info_id"]; // User info id
		$str = $_REQUEST["str"]; // Input Value

		$sql = "SELECT
					transaction.id,
					CONCAT(user_info.last_name ,', ', user_info.first_name ,' ', user_info.middle_name) AS name,
					transaction.date_transact AS date_transact, 
					transaction.type AS type 
				FROM transaction 
				INNER JOIN user_info ON user_info.id = transaction.info_id
				WHERE user_info.brgy_id = $brgy_id AND transaction.admin_id = $info_id 
				AND (transaction.type LIKE '%$str%' OR CONCAT(user_info.last_name ,', ', user_info.first_name ,' ', user_info.middle_name) LIKE '%$str%') 
				ORDER BY transaction.id DESC LIMIT 10";
					
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {
			echo "<tbody>";				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr class=\"selectedRow\"><td style=\"display:none\">" . $row['id'] .
						"</td><td>" . $row['name'] .
						"</td><td>" . date("F d, Y", strtotime($row['date_transact'])) .
						"</td><td>" . date("h:i:s A", strtotime($row['date_transact'])) . 
						"</td><td>" . $row['type'] . 
					  "</td></tr>";
			}
			echo "</tbody>";
		}else{
			echo "";
		}

		mysqli_close($conn);
	}
?>