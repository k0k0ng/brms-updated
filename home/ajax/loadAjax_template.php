<?php
	include '../database/database.php'; // Database Connection

	// This will load certification template

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
		
		$header = $_REQUEST["header"];
		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id

		$sql = "SELECT * FROM certification
				WHERE brgy_id = $brgy_id AND header LIKE '%$header%'					
				ORDER BY header
				LIMIT 10";
					
		$result = $conn->query($sql);			
			
		if ($result->num_rows > 0) {
			echo "<tbody>";

			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr class=\"selectedRow\">
							<td style=\"display:none;\">" . $row['id'] . 
							"</td><td>" . $row['header']."</td><td>".
								"<textarea rows='5' type='textarea' class='form-control' readonly>" 
									. $row['paragraph1'] . 
								"</textarea>".
							"</td><td>" . 
								"<textarea rows='5' type='textarea' class='form-control' readonly>" 
									. $row['paragraph2'] . 
								"</textarea>" .
							"</td></tr>";
			}


			echo "</tbody>";
		}
		else{
			echo "";
		}

		mysqli_close($conn);
	}
?>