<?php
	session_start(); 
	include '../database/database.php'; // Database Connection

	// This will load minor

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		echo getPurokTable($_REQUEST["purok_name"]);
	}
  
	function getPurokTable($purok_name) {
      $database = new Database(); // Create Database Connection
      $conn = $database -> get_Connection(); // Get Database Connection
      
      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID

      $sql =  "SELECT
              purok.name AS 'Purok Name',
              (SELECT COUNT(*) FROM user_info 
                WHERE (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0)<18 AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'Minor',
              (SELECT COUNT(*) FROM user_info WHERE purok_id = purok.id AND brgy_id = $brgy_id) AS 'Population'
              FROM user_info
              RIGHT OUTER JOIN purok
              ON purok.id = user_info.purok_id
              WHERE purok.brgy_id = $brgy_id
              AND purok.name LIKE '%$purok_name%'
              GROUP BY purok.id
              ORDER BY purok.id, purok.name";

      $result = $conn->query($sql);
      
      $return = "<tbody>";
      if ($result->num_rows > 0) {

        // output data of each row
        while($row = $result->fetch_assoc()) {
          $return .= "<tr nobr=\"true\"><td>" . $row['Purok Name'] . "</td><td>" . $row['Minor']. "</td><td>" . $row['Population'] . "</td></tr>";
        }
      }
      $return .= "</tbody>";
      mysqli_close($conn);

      return $return;
    }
?>