<?php
	
	include '../database/database.php'; // Database Connection

	// This will get all fee type

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$collection_id = $_REQUEST["collection_id"]; // Collection ID

		$sql = "SELECT * FROM fee_type WHERE collection_id = $collection_id 
				AND brgy_id = $brgy_id ORDER BY type";
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo $row['type'] . ",";
			}
		}else{
			echo "";
		}
	}
?>