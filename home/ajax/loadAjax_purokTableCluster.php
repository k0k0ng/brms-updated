<?php 
	
	include '../database/database.php'; // Database Connection

	// This will load citizens with purok

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
			
		$brgy_id = $_REQUEST["brgy_id"];
		$purok_id = $_REQUEST["purok_id"];
		$q = $_REQUEST["q"];

		$sql = "SELECT * FROM user_info WHERE purok_id = '$purok_id' AND brgy_id = $brgy_id AND (first_name LIKE '%$q%' OR middle_name LIKE '%$q%' OR last_name LIKE '%$q%' OR address LIKE '%$q%') ORDER BY last_name LIMIT 10";
		$result = $conn->query($sql);
			
		echo "<tbody>";
		if ($result->num_rows > 0) {

			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr class=\"selectedRow\"><td style=\"display:none;\">" . $row['id'] . "</td><td>" . $row['first_name'] . "</td><td>" . $row['middle_name']. "</td><td>" . $row['last_name']. "</td><td>" . $row['address'] . "</td></tr>";
			}
		}
		echo "</tbody>";

		mysqli_close($conn);
	}
?>