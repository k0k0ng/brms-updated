<?php
	
	include '../database/database.php'; // Database Connection

	// This will load cititens

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
		
		$q = $_REQUEST["q"];
		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$purok_id = $_REQUEST["purok_id"]; // Purok id

		$sql = "SELECT 
					user_info.id AS id,
					user_info.first_name AS first_name,
					user_info.last_name AS last_name,
					user_info.middle_name AS middle_name,
					purok.name AS purok_name
				FROM user_info 
				INNER JOIN purok ON user_info.purok_id = purok.id
				WHERE user_info.brgy_id = $brgy_id AND user_info.purok_id = $purok_id AND (user_info.first_name LIKE '%$q%'  OR user_info.last_name LIKE '%$q%'  OR user_info.middle_name LIKE '%$q%'  OR user_info.purok_id LIKE '%$q%') ORDER BY purok.name, user_info.last_name LIMIT 5";
				
		$result = $conn->query($sql);			
		
		if ($result->num_rows > 0) {
			echo "<tbody>";

			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr class=\"selectedRow\"><td style=\"display:none;\">" . $row['id'] . "</td><td>" . $row['first_name'] . "</td><td>" . $row['middle_name']. "</td><td>" . $row['last_name']. "</td><td>" . $row['purok_name'] . "</td></tr>";
			}

			echo "</tbody>";
		}
		else{
			echo "";
		}

		mysqli_close($conn);
	}
?>