<?php 

	include '../database/database.php'; // Database Connection

	// This will display registered barangay and number of admins
	
	if ($_SERVER["REQUEST_METHOD"] == "GET"){

		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
		
		$brgy_name = $_REQUEST["brgy_name"]; // Barangay Name	

		$sql = "SELECT barangay_info.id AS id, barangay_info.brgy_name AS name, COUNT(IF(account.access_id IN(2,3), 1, null)) AS count
				FROM barangay_info				
				LEFT OUTER JOIN user_info ON barangay_info.id = user_info.brgy_id
				LEFT OUTER JOIN account ON user_info.id = account.info_id				
				WHERE barangay_info.brgy_name LIKE '%$brgy_name%' AND barangay_info.id !=2
				GROUP BY barangay_info.id
				ORDER BY barangay_info.brgy_name";	

		$result = $conn->query($sql);
			
		if($result->num_rows > 0){
			// Output Table
			echo "<tbody>";
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					echo "<tr class=\"selectedRow\"><td style=\"display:none;\">" . $row['id'] . "</td><td>" . $row['name'] . "</td><td>" . $row['count'] . "</td></tr>";
				}
			}
			echo "</tbody>";
			     		
		}else{
			echo ""; // Output Nothing
		}

		mysqli_close($conn);
	}
?>