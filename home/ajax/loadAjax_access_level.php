<?php
	
	include '../database/database.php'; // Database Connection

	// This will get access level

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$sql = "SELECT * FROM access_level WHERE id IN (2,3)"; // Query
		$result = $conn->query($sql); // Result
			
		if ($result->num_rows > 0) {				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo $row['role'] . ",";
			}
		}else{
			echo "";
		}

		mysqli_close($conn); // Close Connection
	}
?>