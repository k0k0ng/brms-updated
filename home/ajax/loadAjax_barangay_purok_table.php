<?php 

	include '../database/database.php'; // Database Connection

	// This will display registered purok
	
	if ($_SERVER["REQUEST_METHOD"] == "GET"){

		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
		
		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$name = $_REQUEST["purok_name"]; // Purok name

		$sql = "SELECT * FROM purok WHERE brgy_id = '$brgy_id' AND name LIKE '%$name%' ORDER BY name LIMIT 10";

		$result = $conn->query($sql);
			
		if($result->num_rows > 0){
			// Output Table
			echo "<tbody>";
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					echo "<tr class=\"selectedRow\"><td style=\"display:none;\">" 
					. $row['brgy_id'] . "</td><td style=\"display:none;\">" 
					. $row['purok_id'] . "</td><td>"
					. $row['name'] . "</td><td>" 
					. select_citizen_count_admin($conn,$row['brgy_id'],$row['purok_id']) . "</td></tr>";
				}
			}
			echo "</tbody>";
			     		
		}else{			
			echo ""; // Output Nothing
		}
		mysqli_close($conn);		
	}

	// Population count per purok
	function select_citizen_count_admin($conn,$brgy_id,$purok_id){
		$sql = "SELECT COUNT(*) AS count FROM user_info WHERE brgy_id = $brgy_id AND purok_id = $purok_id";
		$result = $conn->query($sql);

		if($result->num_rows > 0){
        		// output data of each row
			while($row = $result->fetch_assoc()) {
				return $row['count'];
			}
		}else{ 
			return 0;
		}      
	}
?>