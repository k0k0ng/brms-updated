<?php
	
	include '../database/database.php'; // Database Connection

	// This will load fee

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		function selectDatabase() {
			
			$database = new Database(); // Create Database Connection
			$conn = $database -> get_Connection(); // Get Database Connection

			$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
			$info_id = $_REQUEST["info_id"]; // User info id
			$str = $_REQUEST["str"]; // Input Value
			$collection_id = $_REQUEST["collection_id"]; // Collection id

			$sql = "SELECT 
						fee_collection.date_paid AS date_paid, 
						fee_type.type AS type, 
						fee_collection.amount_paid AS amount_paid,
						fee_collection.receipt_no AS receipt_no
					FROM fee_collection
					INNER JOIN fee_type ON fee_collection.fee_type_id = fee_type.id
					WHERE fee_type.brgy_id = $brgy_id 
					AND fee_collection.info_id = $info_id 
					AND fee_type.type LIKE '%$str%'
					AND fee_type.collection_id = $collection_id 
					ORDER BY fee_collection.id DESC LIMIT 10";

			$result = $conn->query($sql);
			
			if ($result->num_rows > 0) {
				echo "<tbody>";				
				// output data of each row
				while($row = $result->fetch_assoc()) {
					echo "<tr><td>" . date("F d, Y", strtotime($row['date_paid'])) . "</td><td>" 
						. date("h:i:s A", strtotime($row['date_paid'])) . "</td><td>" 
						. $row['amount_paid'] . "</td><td>" 
						. $row['type'] . "</td><td>"
						. $row['receipt_no'] . "</td></tr>";
				}
				echo "</tbody>";
			}else{
				echo "";
			}
			mysqli_close($conn);
		}

		selectDatabase(); // Execut e
	}
?>