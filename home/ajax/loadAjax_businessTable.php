<?php
	include '../database/database.php'; // Database Connection

	// This will load business

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
		
		$q = $_REQUEST["q"];
		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id

		$sql = "SELECT 
					business.id AS 'Business ID',
					business.name AS 'Business Name',
					CONCAT(user_info.last_name, ', ', user_info.first_name, ' ', user_info.middle_name) AS 'Owner Name',
					business.address AS 'Address',
					purok.name AS 'Purok'

				FROM business
				INNER JOIN user_info ON business.info_id = user_info.id
				RIGHT OUTER JOIN purok ON purok.id = business.purok_id
				WHERE purok.brgy_id = $brgy_id 
				AND 
				(
					CONCAT(user_info.last_name, ', ', user_info.first_name, ' ', user_info.middle_name) LIKE '%$q%'
				OR 
					business.name LIKE '%$q%'
				)
				ORDER BY business.name, user_info.last_name
				LIMIT 10";
				
		$result = $conn->query($sql);			
		
		if ($result->num_rows > 0) {
			echo "<tbody>";
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr class=\"selectedRow\">
						<td style=\"display:none;\">" . $row['Business ID'] . 
							"</td><td>" . $row['Business Name']. 
							"</td><td>" . $row['Owner Name']. 
							"</td><td>" . $row['Address']. 
							"</td><td>" . $row['Purok'] . "</td></tr>";
			}
				echo "</tbody>";
		}
		else{
			echo "";
		}
		mysqli_close($conn);
	}
?>