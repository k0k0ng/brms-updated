<?php

	include '../database/database.php'; // Database Connection

	// This will check if barangay exist
	
	if ($_SERVER["REQUEST_METHOD"] == "GET"){

		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
		
		$brgy_name = $_REQUEST["brgy_name"]; // Get Barangay Name From HTTP Request
		
		$sql = "SELECT * FROM barangay_info WHERE brgy_name = '$brgy_name'"; // Query
		$result = $conn->query($sql); // Result
			
		if($result->num_rows > 0){
			echo 1; // Barangay Found
		}else{
			echo 0; // Barangay Not Found
		}

		mysqli_close($conn); // Close Connection
	}
?>