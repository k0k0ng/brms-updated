<?php
	
	include '../database/database.php'; // Database Connection

	// This will load logs with all admin

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$str = $_REQUEST["str"]; // Input Value

		$sql = "SELECT
					barangay_info.brgy_name AS 'Barangay', 
					CONCAT(user_info.last_name, ', ', user_info.first_name, ' ', user_info.middle_name) AS 'Admin',
					log.history AS 'History',
					log.date_time AS 'Date'
				FROM user_info
				INNER JOIN log ON user_info.id = log.info_id
				INNER JOIN barangay_info ON barangay_info.id = user_info.brgy_id
				WHERE (CONCAT(user_info.last_name, ', ', user_info.first_name, ' ', user_info.middle_name) LIKE '%$str%'
				OR log.history LIKE '%$str%'
				OR barangay_info.brgy_name LIKE '%$str%')
				ORDER BY log.id DESC
				LIMIT 15";
					
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			echo "<tbody>";				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr><td>" . $row['Barangay'] .
						"</td><td>" . $row['Admin'] .
						"</td><td>" . $row['History'] .
						"</td><td>" . date("F d, Y", strtotime($row['Date'])) .
						"</td><td>" . date("h:i:s A", strtotime($row['Date'])) . 
					  "</td></tr>";
			}
			echo "</tbody>";
		}else{
			echo "";
		}

		mysqli_close($conn);
	}
?>