<?php
	
	include '../database/database.php'; // Database Connection

	// This will load transaction (business)

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$bus_id = $_REQUEST["bus_id"]; // Business id
		$str = $_REQUEST["str"]; // Input Value

		$sql = "SELECT
					transaction.id,
					transaction.date_transact AS date_transact, 
					transaction.type AS type,
					transaction.receipt_no
				FROM transaction 
				INNER JOIN user_info ON transaction.info_id = user_info.id
				INNER JOIN transaction_business ON transaction_business.transaction_id = transaction.id
				WHERE user_info.brgy_id = $brgy_id
				AND transaction_business.business_id = $bus_id
				AND transaction.type LIKE '%$str%'
				ORDER BY transaction.id DESC
				LIMIT 10";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			echo "<tbody>";				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo 	"<tr class=\"selectedRow\"><td style=\"display:none\">" . $row['id'] . 
							"</td><td>" . date("F d, Y", strtotime($row['date_transact'])) . 
							"</td><td>" . date("h:i:s A", strtotime($row['date_transact'])) . 
							"</td><td>" . $row['type'] . 
							"</td><td>" . $row['receipt_no'] . 
						"</td></tr>";
			}
			echo "</tbody>";
		}else{
			echo "";
		}

		mysqli_close($conn);
	}
?>