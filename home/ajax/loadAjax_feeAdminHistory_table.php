<?php
	
	include '../database/database.php'; // Database Connection

	// This will load fee with all admin

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$str = $_REQUEST["str"]; // Input Value

		$sql = "SELECT
					(SELECT CONCAT(last_name ,', ', first_name ,' ', middle_name) 
					FROM user_info WHERE id = fee_collection.admin_id) AS admin,
					CONCAT(user_info.last_name ,', ', user_info.first_name ,' ', user_info.middle_name) AS citizen, 
					fee_collection.date_paid AS date_paid, 
					fee_type.type AS type, 
					fee_collection.amount_paid AS amount_paid
				FROM fee_collection
				INNER JOIN fee_type ON fee_collection.fee_type_id = fee_type.id
				INNER JOIN user_info ON user_info.id = fee_collection.info_id
				WHERE user_info.brgy_id = $brgy_id
				AND (fee_type.type LIKE '%$str%' OR CONCAT(user_info.last_name ,', ', user_info.first_name ,' ', user_info.middle_name) LIKE '%$str%'
				OR (SELECT CONCAT(last_name ,', ', first_name ,' ', middle_name) 
					FROM user_info WHERE id = fee_collection.admin_id) LIKE '%$str%'
				OR CONCAT(user_info.last_name ,', ', user_info.first_name ,' ', user_info.middle_name) LIKE '%$str%')
				ORDER BY fee_collection.id DESC LIMIT 10";

		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {
			echo "<tbody>";				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr><td>" . $row['admin'] .
						"</td><td>" . $row['citizen'] .  
						"</td><td>" . date("F d, Y", strtotime($row['date_paid'])) . 
						"</td><td>" . date("h:i:s A", strtotime($row['date_paid'])) . 
						"</td><td>" . $row['amount_paid'] . 
						"</td><td>" . $row['type'] . "</td></tr>";
			}
			echo "</tbody>";
		}else{
			echo "";
		}

		mysqli_close($conn);
	}
?>