<?php 
	
	include '../database/database.php'; // Database Connection

	// This will load clustered purok including purok leader
	if ($_SERVER["REQUEST_METHOD"] == "GET"){

		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$q = $_REQUEST["q"];
		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id

		$sql = "SELECT 
					user_info.id AS id, 
					purok.id AS purok_id, 
					purok.name AS purok_name, 
					CONCAT(user_info.last_name , ', ', user_info.first_name ,' ' , user_info.middle_name) AS name,
					(SELECT COUNT(*) FROM user_info WHERE brgy_id=$brgy_id AND purok_id = purok.id) AS population
				FROM purok LEFT OUTER JOIN user_info ON purok.purok_leader = user_info.id 
				WHERE purok.brgy_id = $brgy_id AND (purok.name LIKE '%$q%' OR user_info.last_name LIKE '%$q%' OR user_info.first_name LIKE '%$q%' OR user_info.middle_name LIKE '%$q%')
				ORDER BY purok.id, user_info.last_name";

		$result = $conn->query($sql);
		
		echo "<tbody>";
		if ($result->num_rows > 0) {

			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr class=\"selectedRow\"><td style=\"display:none;\">"
				. $row['id'] . "</td><td style=\"display:none;\">" 
				. $row['purok_id'] . "</td><td>" 
				. $row['purok_name'] . "</td><td>" 
				. $row['name'] . "</td><td>"
				. $row['population'] . "</td><td>"
				. "<button class=\"btnView btnViewBackground\">View Purok Information</button>" . "</td><td>"
				. "<button class=\"btnUpdate btnUpdteBackground\">Update Purok Information</button>" . "</td></tr>";
			}
		}
		echo "</tbody>";			

		mysqli_close($conn);
	}
?>