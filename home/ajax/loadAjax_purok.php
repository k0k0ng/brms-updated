<?php
	
	include '../database/database.php'; // Database Connection

	// This will get all purok name

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id

		$sql = "SELECT * FROM purok WHERE brgy_id = $brgy_id ORDER BY id";
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {
				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo $row['name'] . ",";
			}
		}else{
			echo "";
		}
	}
?>