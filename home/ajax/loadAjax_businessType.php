<?php
	
	include '../database/database.php'; // Database Connection

	// This will get all business type

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$sql = "SELECT * FROM business_type";
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {
				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo $row['type'] . ",";
			}
		}else{
			echo "";
		}	
	}
?>