<?php
	
	include '../database/database.php'; // Database Connection

	// This will load complaint with all admin

	if ($_SERVER["REQUEST_METHOD"] == "GET"){		
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$str = $_REQUEST["str"]; // Input Value

		$sql = "SELECT
					complaint.id,
					(SELECT CONCAT(last_name ,', ', first_name ,' ', middle_name)
				FROM user_info WHERE id = complaint.admin_id) AS 'Admin',
					complaint.complainants AS 'Complainant',
					CONCAT(user_info.first_name, ' ' , user_info.middle_name, ' ', user_info.last_name) AS 'Complainee',
					complaint.date_file AS 'Date Transacted',					
					complaint.action_taken AS 'Action'
				FROM complaint
				INNER JOIN user_info ON user_info.id = complaint.complainee_id
				WHERE user_info.brgy_id = $brgy_id
				AND (
					(SELECT CONCAT(last_name ,', ', first_name ,' ', middle_name)
					FROM user_info WHERE id = complaint.admin_id) LIKE '%$str%'
					OR
					CONCAT(user_info.first_name, ' ' , user_info.middle_name, ' ', user_info.last_name) LIKE '%$str%'
					OR
					complaint.complainants LIKE '%$str%'
					OR
					complaint.action_taken LIKE '%$str%'
					)
				ORDER BY complaint.id DESC
				LIMIT 10";

		$result = $conn->query($sql);
		echo mysqli_error($conn);
		
		if ($result->num_rows > 0) {
			echo "<tbody>";				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo  "<tr class=\"selectedRow\"><td style=\"display:none\">" . $row['id'] .
						"</td><td>" . $row['Admin'] .
						"</td><td>" . $row['Complainant'] .
						"</td><td>" . $row['Complainee'] .
						"</td><td>" . date("F d, Y", strtotime($row['Date Transacted'])) .
						"</td><td>" . date("h:i:s A", strtotime($row['Date Transacted'])) . 
						"</td><td>" . $row['Action'] . 
					  "</td></tr>";
			}
			echo "</tbody>";
		}else{
			echo "";
		}
		
		mysqli_close($conn);
	}
?>