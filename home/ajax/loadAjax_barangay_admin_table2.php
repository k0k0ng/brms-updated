<?php 

	include '../database/database.php'; // Database Connection

	// This will display barangay admin
	
	if ($_SERVER["REQUEST_METHOD"] == "GET"){

		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
		
		$brgy_id = $_REQUEST["brgy_id"]; // Barangay ID
		$name = $_REQUEST["name"]; // Barangay ID

		$sql = "SELECT user_info.id, user_info.first_name AS first_name, user_info.last_name AS last_name, user_info.middle_name AS middle_name, access_level.role AS role
				FROM user_info 
				INNER JOIN account ON user_info.id = account.info_id
				INNER JOIN access_level ON access_level.id = account.access_id
				WHERE user_info.brgy_id = $brgy_id AND (account.access_id IN (2,3,5))
				AND (user_info.first_name LIKE '%$name%' OR user_info.last_name LIKE '%$name%' OR user_info.middle_name LIKE '%$name%')
				ORDER BY access_level.id, user_info.last_name
				LIMIT 10";

		$result = $conn->query($sql); // Result
			
		if($result->num_rows > 0){
			// Output Table
			echo "<tbody>";
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					echo "<tr class=\"selectedRow\"><td style=\"display:none;\">" 
					. $row['id'] . "</td><td>" 
					. $row['last_name'] . ', ' . $row['first_name'] . ' ' . $row['middle_name'] . "</td><td>"					
					. $row['role'] . "</td></tr>";
				}
			}
			echo "</tbody>";
			     		
		}else{
			// Output Nothing
			echo "";
		}
		mysqli_close($conn);		
	}
?>