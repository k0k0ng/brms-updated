<?php
	session_start(); 
	include '../database/database.php'; // Database Connection

	// This will load residence type

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		echo getPurokTable($_REQUEST["purok_name"]);
	}
  
	function getPurokTable($purok_name) {
      $database = new Database(); // Create Database Connection
      $conn = $database -> get_Connection(); // Get Database Connection
      
      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID

      $sql = "SELECT
                purok.name AS 'Purok Name',
                (SELECT COUNT(*) FROM user_info WHERE user_info.residence_type = 'Owned' AND purok_id = purok.id AND  brgy_id = $brgy_id) AS 'Owned',
                (SELECT COUNT(*) FROM user_info WHERE user_info.residence_type = 'Leased' AND purok_id = purok.id AND  brgy_id = $brgy_id) AS 'Leased',
                (SELECT COUNT(*) FROM user_info WHERE user_info.residence_type = 'Rented' AND purok_id = purok.id AND  brgy_id = $brgy_id) AS 'Rented',
                (SELECT COUNT(*) FROM user_info WHERE user_info.residence_type = 'Boarder' AND purok_id = purok.id AND  brgy_id = $brgy_id) AS 'Boarder',
                (SELECT COUNT(*) FROM user_info WHERE user_info.residence_type = 'Otherwise' AND purok_id = purok.id AND  brgy_id = $brgy_id) AS 'Otherwise'

              FROM user_info
              RIGHT OUTER JOIN purok
              ON purok.id = user_info.purok_id
              WHERE purok.brgy_id = $brgy_id
              AND purok.name LIKE '%$purok_name%'
              GROUP BY purok.id
              ORDER BY purok.id, purok.name";

      $result = $conn->query($sql);
      
      $return = "<tbody>";
      if ($result->num_rows > 0) {

        // output data of each row
        while($row = $result->fetch_assoc()) {
          $return .= "<tr><td>" . $row['Purok Name'] .
                    "</td><td>" . $row['Owned'] .  
                    "</td><td>" . $row['Leased'] . 
                    "</td><td>" . $row['Rented'] . 
                    "</td><td>" . $row['Boarder'] . 
                    "</td><td>" . $row['Otherwise'] . "</td></tr>";
        }
      }
      $return .= "</tbody>";
      mysqli_close($conn);

      return $return;
    }
?>