<?php
	
	include '../database/database.php'; // Database Connection

	// This will load business

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
		
		$q = $_REQUEST["q"];
		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id

		$sql = "SELECT 
					id AS 'Business ID', 
					CONCAT(name, ', ' , address) AS 'Business Name' 
				FROM business 
				WHERE brgy_id = $brgy_id 
				AND CONCAT(name, ', ' , address) LIKE '%$q%' LIMIT 3";

		$result = $conn->query($sql);			
			
		if ($result->num_rows > 0) {
			echo "<tbody>";

			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo 	"<tr class=\"selectedRow\"><td style=\"display:none;\">" 
							. $row['Business ID'] . "</td><td>" 
							. $row['Business Name'] . "</td></tr>";
			}

			echo "</tbody>";
		}
		else{
			echo "";
		}

		mysqli_close($conn);
	}
?>