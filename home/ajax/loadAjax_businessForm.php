<?php
	
	include '../database/database.php'; // Database Connection

	// This will get all business form

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$sql = "SELECT * FROM business_form"; // Query
		$result = $conn->query($sql); // Resuult
			
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo $row['form'] . ",";
			}
		}else{
			echo "";
		}
		mysqli_close($conn); // Close Connection
	}
?>