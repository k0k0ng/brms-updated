<?php
	session_start(); 
	include '../database/database.php'; // Database Connection

	// This will load blood type

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		echo getPurokTable($_REQUEST["purok_name"]);
	}

	function getPurokTable($purok_name) {
      $database = new Database(); // Create Database Connection
      $conn = $database -> get_Connection(); // Get Database Connection
      
      $brgy_id = $_SESSION['brms_brgyId'];  // Barangay ID

      $sql =  "SELECT
                purok.name AS 'Purok Name',
                (SELECT COUNT(*) FROM user_info WHERE user_info.blood_type = 'A+' AND purok_id = purok.id  AND brgy_id = $brgy_id) AS 'A+',
                (SELECT COUNT(*) FROM user_info WHERE user_info.blood_type = 'A-' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'A-',
                (SELECT COUNT(*) FROM user_info WHERE user_info.blood_type = 'B+' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'B+',
                (SELECT COUNT(*) FROM user_info WHERE user_info.blood_type = 'B-' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'B-',
                (SELECT COUNT(*) FROM user_info WHERE user_info.blood_type = 'AB+' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'AB+',
                (SELECT COUNT(*) FROM user_info WHERE user_info.blood_type = 'AB-' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'AB-',
                (SELECT COUNT(*) FROM user_info WHERE user_info.blood_type = 'O+' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'O+',
                (SELECT COUNT(*) FROM user_info WHERE user_info.blood_type = 'O-' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'O-',
                (SELECT COUNT(*) FROM user_info WHERE user_info.blood_type = 'Unknown' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'Unknown'
              FROM user_info
              RIGHT OUTER JOIN purok
              ON purok.id = user_info.purok_id
              WHERE purok.brgy_id = $brgy_id
              AND purok.name LIKE '%$purok_name%'
              GROUP BY purok.id
              ORDER BY purok.id, purok.name";

      $result = $conn->query($sql);
      
      $return = "<tbody>";
      if ($result->num_rows > 0) {

        // output data of each row
        while($row = $result->fetch_assoc()) {
          $return .= "<tr nobr=\"true\"><td colspan=\"2\">" . $row['Purok Name'] .
                      "</td><td>" . $row['A+']. 
                      "</td><td>" . $row['A-'].
                      "</td><td>" . $row['B+'].
                      "</td><td>" . $row['B-'].
                      "</td><td>" . $row['AB+'].
                      "</td><td>" . $row['AB-'].
                      "</td><td>" . $row['O+'].
                      "</td><td>" . $row['O-']. 
                      "</td><td>" . $row['Unknown']. 
                      "</td></tr>";
        }
      }
      $return .= "</tbody>";
      mysqli_close($conn);

      return $return;
    }
?>