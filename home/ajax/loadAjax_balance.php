<?php
	
	include '../database/database.php'; // Database Connection

	// This will get Remaining Balance

	if ($_SERVER["REQUEST_METHOD"] == "GET"){		
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$info_id = $_REQUEST["info_id"]; // Info Id
		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$type = $_REQUEST["type"]; // Fee Type
		$id = getFeeId($conn, $brgy_id, $type); // Get Fee ID

		echo getFeeAmount($conn, $brgy_id, $type) - getFeeCollection($conn, $brgy_id, $id, $info_id); // Total Payment - Total Paid Amount

		mysqli_close($conn); // Close Connection
	}

	// Get Fee Collection
	function getFeeCollection($conn, $brgy_id, $fee_type_id, $info_id){
		$sql = "SELECT SUM(fee_collection.amount_paid) AS amount 
				FROM fee_collection 
				INNER JOIN fee_type ON fee_collection.fee_type_id = fee_type.id
				WHERE fee_type.brgy_id = $brgy_id AND fee_type_id = $fee_type_id AND fee_collection.info_id = $info_id";
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				return $row['amount'];			
			}
		}else{
			return 0;
		}
	}

	// Get Fee ID
	function getFeeId($conn, $brgy_id, $type){
		$sql = "SELECT * FROM fee_type WHERE brgy_id = $brgy_id AND type = '$type'";
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				return $row['id'];			
			}
		}else{
			return 0;
		}
	}

	// Get Fee Amount
	function getFeeAmount($conn, $brgy_id, $type){
		$sql = "SELECT * FROM fee_type WHERE brgy_id = $brgy_id AND type = '$type'";
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {				
			// output data of each row
			while($row = $result->fetch_assoc()) {

				// Monthly
				if($row['period'] === 'Monthly'){	
					return $row['amount'] * getMonthCount($row['start']);
				}
				// Yearly
				else if ($row['period'] === 'Yearly'){
					return $row['amount'] * getYearCount($row['start']);
				}
				// Otherwise
				else{
					return -1;
				}
			}
		}else{
			return 0;
		}
	}

	// Get Year Count
	function getYearCount($date) { // Y-m-d format

		$d1 = date('Y-m-d'); // Current Date
		$d2 = date('Y-m-d', strtotime($date)); // Starting Date

		$datetime1 = date_create($d1);
		$datetime2 = date_create($d2);
		$interval = date_diff($datetime1, $datetime2);

		return ($interval->format('%y') + 1); // Advanced 1
  	}

	// Get Month Count
	function getMonthCount($date) { // Y-m-d format

		$d1 = date('Y-m-d'); // Current Date
		$d2 = date('Y-m-d', strtotime($date)); // Starting Date

		$datetime1 = date_create($d1);
		$datetime2 = date_create($d2);
		$interval = date_diff($datetime1, $datetime2);

		return ($interval->format('%y') * 12) + ($interval->format('%m') + 1); // Advanced 1
  	}

?>