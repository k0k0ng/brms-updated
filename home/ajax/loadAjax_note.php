<?php
	
	include '../database/database.php'; // Database Connection

	// This will load note

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$info_id = $_REQUEST["info_id"]; // User info id			
		$type = $_REQUEST["type"]; // Input Value
		$str = $_REQUEST["str"]; // String
			
		$sql = "SELECT 
					date_note AS 'Date Note',
					LEFT(note, 100) AS 'Note'
				FROM note
				WHERE info_id = $info_id
				AND type = '$type'
				AND note LIKE '%$str%'
				ORDER BY note.id DESC
				LIMIT 10";

		$result = $conn->query($sql);
		echo mysqli_error($conn);
			
		if ($result->num_rows > 0) {
			echo "<tbody>";				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr><td>" . date("F d, Y", strtotime($row['Date Note'])) .
						"</td><td>" . date("h:i:s A", strtotime($row['Date Note'])) . 
						"</td><td style=\"max-width: 200px\">" . $row['Note'] . 
					  "</td></tr>";
			}
			echo "</tbody>";
		}else{
			echo "";
		}

		mysqli_close($conn);
	}
?>