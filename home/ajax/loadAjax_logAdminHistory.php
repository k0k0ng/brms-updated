<?php
	
	include '../database/database.php'; // Database Connection

	// This will load logs with admin

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
			
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id
		$str = $_REQUEST["str"]; // Input Value

		$sql = "SELECT 
					CONCAT(user_info.last_name, ', ', user_info.first_name, ' ', user_info.middle_name) AS 'Admin',
					log.history AS 'History',
					log.date_time AS 'Date'
				FROM user_info
				INNER JOIN log ON user_info.id = log.info_id
				WHERE user_info.brgy_id = $brgy_id
				AND (CONCAT(user_info.last_name, ', ', user_info.first_name, ' ', user_info.middle_name) LIKE '%$str%'
				OR log.history LIKE '%$str%')
				ORDER BY log.id DESC
				LIMIT 10";
					
		$result = $conn->query($sql);
			
		if ($result->num_rows > 0) {
			echo "<tbody>";				
			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr><td>" . $row['Admin'] .
						"</td><td>" . $row['History'] .
						"</td><td>" . date("F d, Y", strtotime($row['Date'])) .
						"</td><td>" . date("h:i:s A", strtotime($row['Date'])) . 
					  "</td></tr>";
			}
			echo "</tbody>";
		}else{
			echo "";
		}

		mysqli_close($conn);
	}
?>