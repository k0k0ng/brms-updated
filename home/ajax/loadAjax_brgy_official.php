<?php
	
	include '../database/database.php'; // Database Connection

	// This will load barangay official

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		
		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection
		
		$q = $_REQUEST["q"];
		$brgy_id = $_REQUEST["brgy_id"]; // Barangay id

		$sql = "SELECT * FROM user_info WHERE brgy_id = $brgy_id AND CONCAT(first_name ,' ', middle_name ,' ', last_name) LIKE '%$q%' LIMIT 3";

		$result = $conn->query($sql);			
			
		if ($result->num_rows > 0) {
			echo "<tbody>";

			// output data of each row
			while($row = $result->fetch_assoc()) {
				echo "<tr class=\"selectedRow\"><td style=\"display:none;\">" . $row['id'] . "</td><td>" . $row['first_name'] . " " . $row['middle_name']. " " . $row['last_name'] . "</td></tr>";
			}

			echo "</tbody>";
		}
		else{
			echo "";
		}

		mysqli_close($conn);
	}
?>