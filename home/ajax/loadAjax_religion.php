<?php
	session_start(); 
	include '../database/database.php'; // Database Connection

	// This will load religion

	if ($_SERVER["REQUEST_METHOD"] == "GET"){
		echo getPurokTable($_REQUEST["purok_name"]);
	}
  
	function getPurokTable($purok_name) {
      $database = new Database(); // Create Database Connection
      $conn = $database -> get_Connection(); // Get Database Connection
      
      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID

      $sql = "SELECT
                purok.name AS 'Purok Name',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Roman Catholic' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'A',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Islam' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'B',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Protestant' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'C',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Iglesia ni Cristo' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'D',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Jesus Miracle Crusade International Ministry' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'E',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Members Church of God International' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'F',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Most Holy Church of God in Christ Jesus' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'G',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Philippine Independent Church' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'H',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Apostolic Catholic Church' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'I',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Orthodoxy' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'J',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'The Kingdom of Jesus Christ the Name Above Every Name' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'K',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Judaism' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'L',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Hinduism' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'M',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Atheism' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'N',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Others' AND purok_id = purok.id AND brgy_id = $brgy_id) AS 'O'

              FROM user_info
              RIGHT OUTER JOIN purok
              ON purok.id = user_info.purok_id
              WHERE purok.brgy_id = $brgy_id
              AND purok.name LIKE '%$purok_name%'
              GROUP BY purok.id
              ORDER BY purok.id, purok.name";              

      $result = $conn->query($sql);
      
      $return = "<tbody>";
      if ($result->num_rows > 0) {

        // output data of each row
        while($row = $result->fetch_assoc()) {
          $return .= "<tr nobr=\"true\">
                                  <td colspan=\"2\">" . $row['Purok Name'] .
                                  "</td><td>" . $row['A'] . 
                                  "</td><td>" . $row['B'] . 
                                  "</td><td>" . $row['C'] . 
                                  "</td><td>" . $row['D'] . 
                                  "</td><td>" . $row['E'] .
                                  "</td><td>" . $row['F'] . 
                                  "</td><td>" . $row['G'] .
                                  "</td><td>" . $row['H'] . 
                                  "</td><td>" . $row['I'] .
                                  "</td><td>" . $row['J'] . 
                                  "</td><td>" . $row['K'] .
                                  "</td><td>" . $row['L'] . 
                                  "</td><td>" . $row['M'] .
                                  "</td><td>" . $row['N'] .     
                                  "</td><td>" . $row['O'] .                             
                      "</td></tr>";
        }
      }
      $return .= "</tbody>";
      mysqli_close($conn);

      return $return;
    }
?>