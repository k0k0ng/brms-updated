<?php
  require('../session/brgy_captain.php'); // Secure Connection
?> 

<!DOCTYPE html>
<html lang="en">

<head>

  <title>BRMS - Barangay Record Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/custom2.css">    

  <link rel="javascript" src="../js/jquery.js">
  <link rel="javascript" src="../js/jquery.min.js">

  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->
  <link rel="stylesheet" href="../css/table_Design.css"> <!-- COSTUMIZE TABLE COLOR -->

  <!--Website Tab Icon-->
  <link rel="icon" type="image/png" href="../images/logo.png"/>

  <script>
      var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID
    </script>

    <script>
      // OnKeyRelease (Load Related Purok)
      function checkAdminTable(name){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {   
            document.getElementById("responsecontainer1").innerHTML = this.responseText;
          }
        };

        xmlhttp.open("GET", "../ajax/loadAjax_barangay_admin_table.php?brgy_id="+brgy_id+"&name="+name, true);
        xmlhttp.send();        
      }
    </script>

    <script>
      // Load Note
      function load_logHistory(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer6").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_logAdminHistory.php?brgy_id="+brgy_id+"&str="+str, true);
          xmlhttp.send();    
        }      
    </script>
  
    <script>
      // Load Note
      function load_noteHistory(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer5").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_noteAdminHistory.php?brgy_id="+brgy_id+"&str="+str, true);
          xmlhttp.send();    
        }      
    </script>

    <script>
      // Load Complaint
      function load_complaintHistory(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer4").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_complaintAdminHistory_table.php?brgy_id="+brgy_id+"&str="+str, true);
          xmlhttp.send();    
        }      
    </script>

    <script>
      // Load Fee
      function load_feeHistory(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer3").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_feeAdminHistory_table.php?brgy_id="+brgy_id+"&str="+str, true);
          xmlhttp.send();    
        }      
      </script>

    <script>
      // Load Transaction
      function load_transactionHistory(str){          
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {   
            document.getElementById("responsecontainer2").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_transactionAdminHistory_table.php?brgy_id="+brgy_id+"&str="+str, true);
        xmlhttp.send();       
      }      
    </script>   

    <script>

      // Document Ready
      $(document).ready(function(){
        checkAdminTable(''); // Load Admins
        load_transactionHistory(''); // Load Tracnsaction History
        load_feeHistory(''); // Load Fee History
        load_complaintHistory(''); // Load Complaint History
        load_noteHistory(''); // Load Note History
        load_logHistory(''); // Load Log History

        // On Click Table
        $("#table_Value1").on('click','.btnView',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var info_id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID
              
          window.location.href="../session/brgy_captain_update_session_client.php?info_id="+info_id+"&brgy_id="+brgy_id; // Update Sesssion (CLIENT)
        });

        // On Click Table
        $("#table_Value1").on('click','.btnUpdate',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var name = currentRow.find("td:eq(1)").text(); // get current row 1st TD value
              
          window.location.href="update_admin.php?name="+name; // Update Sesssion (CLIENT)
        });

        // On Click Table
        $("#table_Value2").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value

          window.open("../pdf_transaction/"+id+".pdf");
        });


        // On Click Table
        $("#table_Value4").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value

          window.open("../pdf_complaint/"+id+".pdf");
        });
        
      });
    </script>

    <script>
      $(window).on("load resize ", function() {
        var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
        $('.tbl-header').css({'padding-right':scrollWidth});
      }).resize();      
    </script>

    <!--for navigation bar-->

    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>


    <div class="container">
      <br>
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#admins">Admins</a></li>
        <li><a data-toggle="tab" href="#transaction_history">Transactions</a></li>
        <li><a data-toggle="tab" href="#fee_history">Fees</a></li>
        <li><a data-toggle="tab" href="#complaint_history">Complaints</a></li>
        <li><a data-toggle="tab" href="#note_history">Notes</a></li>
        <li><a data-toggle="tab" href="#log_history">Logs</a></li>      
      </ul>

      <div class="tab-content">
        <div id="admins" class="tab-pane fade in active">
          <br>
          <div class="row">
            <div class="">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4>Barangay Admins</h4>
                </div>
                <div class="panel-body">

                  <div class="row">
                    <form method="post" role="form" autocomplete="off" action="">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon">Search  </span>                  
                          <input id="purok_name" type="text" onkeyup="checkAdminTable(this.value)" class="form-control" name="purok_name" placeholder="Admin" required autofocus>
                        </div>
                      </div>
                    </form>
                  </div>

                  <br>

                  <div id="table_Value1" class="table-responsive"> 
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Admin Name</th>
                          <th>Role</th>
                          <th colspan="2">Action</th> 
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer1"></tbody>
                    </table>
                  </div>   

                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="transaction_history" class="tab-pane fade">
          <br>
          <div class="row">
            <div class="">
              
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4>Transaction History</h4>
                </div>
                <div class="panel-body">

                  <div class="row">
                    <form method="post" role="form" autocomplete="off" action="">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon">Search 10 Most Recent Transactions</span>                  
                          <input id="purok_name" type="text" onkeyup="load_transactionHistory(this.value)" class="form-control" name="purok_name" placeholder="Admin or Citizen or Purpose" required autofocus>
                        </div>
                      </div>
                    </form>
                  </div>

                  <br>

                  <div id="table_Value2" class="table-responsive"> 
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Admin</th>
                          <th>Citizen</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Purpose</th> 
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer2"></tbody>
                    </table>
                  </div>   

                </div>
              </div>

            </div>
          </div>
        </div>
        <div id="fee_history" class="tab-pane fade">
          <br>
          <div class="row">
            <div class="">
              
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4>Fee History</h4>
                </div>
                <div class="panel-body">

                  <div class="row">
                    <form method="post" role="form" autocomplete="off" action="">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon">Search 10 Most Recent Fees</span>                  
                          <input id="purok_name" type="text" onkeyup="load_feeHistory(this.value)" class="form-control" name="purok_name" placeholder="Admin or Citizen or Fee Type" required autofocus>
                        </div>
                      </div>
                    </form>
                  </div>

                  <br>

                  <div id="table_Value3" class="table-responsive"> 
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Admin</th>
                          <th>Citizen</th>
                          <th>Date</th>
                          <th>Time</th>                          
                          <th>Amount Paid</th>
                          <th>Fee Type</th>
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer3"></tbody>
                    </table>
                  </div>   

                </div>
              </div>

            </div>
          </div>
        </div>
        
        <div id="complaint_history" class="tab-pane fade">
          <br>
          <div class="row">
            <div class="">
              
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4>Complaint History</h4>
                </div>
                <div class="panel-body">

                  <div class="row">
                    <form method="post" role="form" autocomplete="off" action="">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon">Search 10 Most Recent Complaints</span>                  
                          <input id="purok_name" type="text" onkeyup="load_complaintHistory(this.value)" class="form-control" name="purok_name" placeholder="Admin or Complainant or Complainee or Actions" required autofocus>
                        </div>
                      </div>
                    </form>
                  </div>

                  <br>

                  <div id="table_Value4" class="table-responsive"> 
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Admin</th>
                          <th>Complainant</th>
                          <th>Complainee</th>
                          <th>Date</th>
                          <th>Time</th> 
                          <th>Actions</th>
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer4"></tbody>
                    </table>
                  </div>   

                </div>
              </div>
              
            </div>
          </div>
        </div>

        <div id="note_history" class="tab-pane fade">
          <br>
          <div class="row">
            <div class="">
              
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4>Note History</h4>
                </div>
                <div class="panel-body">

                  <div class="row">
                    <form method="post" role="form" autocomplete="off" action="">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon">Search 10 Most Recent Notes</span>                  
                          <input id="purok_name" type="text" onkeyup="load_noteHistory(this.value)" class="form-control" name="purok_name" placeholder="Admin or Note" required autofocus>
                        </div>
                      </div>
                    </form>
                  </div>

                  <br>

                  <div id="table_Value5" class="table-responsive"> 
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Admin</th>
                          <th>Citizen / Business</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Note</th>  
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer5"></tbody>
                    </table>
                  </div>   

                </div>
              </div>
              
            </div>
          </div>
        </div>

        <div id="log_history" class="tab-pane fade">
          <br>
          <div class="row">
            <div class="">
              
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4>Log History</h4>
                </div>
                <div class="panel-body">

                  <div class="row">
                    <form method="post" role="form" autocomplete="off" action="">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="input-group">
                          <span class="input-group-addon">Search 10 Most Recent Logs</span>                  
                          <input id="purok_name" type="text" onkeyup="load_logHistory(this.value)" class="form-control" name="purok_name" placeholder="Admin or History" required autofocus>
                        </div>
                      </div>
                    </form>
                  </div>

                  <br>

                  <div id="table_Value6" class="table-responsive"> 
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Admin</th>
                          <th>History</th> 
                          <th>Date</th>
                          <th>Time</th>
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer6"></tbody>
                    </table>
                  </div>   

                </div>
              </div>
              
            </div>
          </div>
        </div>

      </div>
    </div>

  </body>

</html>