<?php
	require('library/php-excel-reader/excel_reader2.php');
	require('library/SpreadsheetReader.php');
	require('../../database/database.php'); // Database Connection

	// Insert Log
	function insert_log($info_id,$history){

		$database = new Database(); // New Conenction
		$conn = $database->get_Connection(); // Get Database Connection

		$sql = "INSERT INTO log (info_id,history,date_time) 
				VALUES ($info_id,'$history',NOW())";

		$result = $conn->query($sql);
		mysqli_close($conn);
	}

	if(isset($_POST['Submit'])){

		$database = new Database(); // Create Database Connection
		$conn = $database -> get_Connection(); // Get Database Connection

		session_start();
		insert_log($_SESSION['brms_userId'],"Upload Excel File ");

		set_time_limit(0); // No Time Limit On Execution Time	

		$mimes = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel'];		
		
		if(in_array($_FILES["file"]["type"],$mimes)){

			ini_set('display_errors', 1);
			ini_set('display_startup_errors', 1);
			error_reporting(E_ALL);
			
			$uploadFilePath = 'uploads/'.basename($_FILES['file']['name']);
			move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath);

			$Reader = new SpreadsheetReader($uploadFilePath);

			$totalSheet = count($Reader->sheets()); // Total Sheets

			$new_entry = 0;
			$duplicate_entry = 0;
			$is_1st_row = 0;

			/* For Loop for all sheets */
			for($i=0;$i<$totalSheet;$i++){

				$Reader->ChangeSheet($i);
				foreach ($Reader as $Row)
		        {	
		        	if ($Row !== null){
			        	if($is_1st_row != 0){
							/* Check If sheet not emprt */
							$brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 				
					        $first_name = isset($Row[0]) ? $Row[0] : '';					
							$middle_name = isset($Row[1]) ? $Row[1] : '';
							$last_name = isset($Row[2]) ? $Row[2] : '';	
							$gender = isset($Row[3]) ? $Row[3] : '';
							$birthdate = isset($Row[4]) ? $Row[4] : '';
							$address = isset($Row[5]) ? $Row[5] : '';
							$purok_name = isset($Row[6]) ? $Row[6] : '';
							$religion = isset($Row[7]) ? $Row[7] : '';
							$status = isset($Row[8]) ? $Row[8] : '';
							$blood_type = isset($Row[9]) ? $Row[9] : '';
							$education = isset($Row[10]) ? $Row[10] : '';
							$occupation = isset($Row[11]) ? $Row[11] : '';
							$mobile = isset($Row[12]) ? $Row[12] : '';
							$telephone = isset($Row[13]) ? $Row[13] : '';
							$is_voter = isset($Row[14]) ? $Row[14] : '';
							$voter_id = isset($Row[15]) ? $Row[15] : '';
							$barangay_id = isset($Row[16]) ? $Row[16] : '';
							$residence_type = isset($Row[17]) ? $Row[17] : '';
							$cur_employed = isset($Row[18]) ? $Row[18] : '';
							$pwd = isset($Row[19]) ? $Row[19] : '';
							$cur_enrolled = isset($Row[20]) ? $Row[20] : '';
							$senior_citizen = isset($Row[21]) ? $Row[21] : '';
							$deceased = isset($Row[22]) ? $Row[22] : '';		
							
							// Chech Purok if Exist
							if(check_purok($conn,$purok_name,$brgy_id)==1){
								$purok_id = get_purok_id($conn,$purok_name,$brgy_id); // Get Purok ID
							}else{
								// Insert Purok
								if(insert_purok($conn,$brgy_id,$purok_name)==1){
									$purok_id = get_purok_id($conn,$purok_name,$brgy_id); // Get Purok ID
								}else{
									$purok_id = 0;
								}
							}

							if(check_user($conn,$first_name,$middle_name,$last_name,$brgy_id)==0){
								// Insert User Info
								if(insert_unser_info(
										$conn,
										$brgy_id,				
										$first_name,				
										$middle_name,
										$last_name,
										$gender,
										date("Y-m-d", strtotime($birthdate)),
										$address,
										$purok_id,
										$religion,
										$status,
										$blood_type,
										$education,
										$occupation,
										$mobile,
										$telephone,
										$is_voter,
										$voter_id,
										$barangay_id,
										$residence_type,
										$cur_employed,
										$pwd,
										$cur_enrolled,
										$senior_citizen,
										$deceased
								)==1){
									$new_entry = ++$new_entry; // New Entry
								}
							}else{
								$duplicate_entry = ++$duplicate_entry;
							}	

						}
						$is_1st_row = ++$is_1st_row;
					}				
		        }
			}

		}else {
			die("<script type='text/javascript'>
					location = '../upload_excel.php';
					alert('Sorry, File type is not allowed. Only EXCEL file.');
				</script>");
		}	
		mysqli_close($conn);
		echo "<script type='text/javascript'>
					location = '../upload_excel.php';
					alert('New entry: $new_entry \\nDuplicate entry: $duplicate_entry'); 
			  </script>"; 
	}

	// Insert Unser Info
	function insert_unser_info(
		$conn,
		$brgy_id,				
		$first_name,				
		$middle_name,
		$last_name,
		$gender,
		$birthdate,
		$address,
		$purok_id,
		$religion,
		$status,
		$blood_type,
		$education,
		$occupation,
		$mobile,
		$telephone,
		$is_voter,
		$voter_id,
		$barangay_id,
		$residence_type,
		$cur_employed,
		$pwd,
		$cur_enrolled,
		$senior_citizen,
		$deceased
	){

	  	$sql = "INSERT INTO user_info (
	  				brgy_id,				
					first_name,				
					middle_name,
					last_name,
					gender,
					birthdate,
					address,
					purok_id,
					religion,
					status,
					blood_type,
					education,
					occupation,
					cell_no, 
			  		tell_no,
					is_voter,
					voter_id,
					citizen_brgy_id,
					residence_type,
					cur_employed,
					pwd,
					cur_enrolled,
					senior_citizen,
					deceased			  		
	  			) 
	  			VALUES
	  			(
	  				 $brgy_id,				
					'$first_name',				
					'$middle_name',
					'$last_name',
					'$gender',
					'$birthdate',
					'$address',
					 $purok_id,
					'$religion',
					'$status',
					'$blood_type',
					'$education',
					'$occupation',
					'$mobile',
					'$telephone',
					'$is_voter',
					'$voter_id',
					'$barangay_id',
					'$residence_type',
					'$cur_employed',
					'$pwd',
					'$cur_enrolled',
					'$senior_citizen',
					'$deceased'
	  			)";

	  		if(mysqli_query($conn, $sql)){	    	
				return 1; // Success
		    }else{
		    	return 0; // Error
		    }
	}

	// Check if user exist
	function check_user($conn,$first_name,$middle_name,$last_name,$brgy_id) {				
		$sql = "SELECT * FROM user_info 
			    WHERE first_name = '$first_name' AND middle_name = '$middle_name' AND last_name = '$last_name' AND brgy_id = $brgy_id";
		
		$result = $conn->query($sql);

		if($result->num_rows > 0){
			return 1; // Found
		}else{
			return 0; // No FOunt
		}
	}

	// Check if purok exist
	function check_purok($conn,$purok_name,$brgy_id) {				
		$sql = "SELECT * FROM purok WHERE name = '$purok_name' AND brgy_id = $brgy_id";
		
		$result = $conn->query($sql);
			
		if($result->num_rows > 0){
			return 1; // Found
		}else{
			return 0; // No FOunt
		}
	}

	// Get Last User Info ID
	function get_info_id($conn) {			
		$sql = "SELECT * FROM user_info ORDER BY id DESC LIMIT 1";

		$result = $conn->query($sql);		
		if ($result->num_rows > 0) {

			// output data of each row
			while($row = $result->fetch_assoc()) {
				return $row['id'];
			}

		}else{
			return 0; // No Found
		}
	}

	// Get purok ID
	function get_purok_id($conn,$purok_name,$brgy_id) {			
		$sql = "SELECT * FROM purok WHERE name = '$purok_name' AND brgy_id = $brgy_id";

		$result = $conn->query($sql);		
		if ($result->num_rows > 0) {

			// output data of each row
			while($row = $result->fetch_assoc()) {
				return $row['id'];
			}

		}else{
			return 0; // No Found
		}
	}

	// Insert Purok
	function insert_purok($conn,$brgy_id,$purok_name){
		$sql = "INSERT INTO purok (brgy_id, name) VALUES ($brgy_id,'$purok_name')";	

		if($purok_name === ''){
			return 0;
		}

		if(mysqli_query($conn, $sql)){
      		return 1;
		}else{
			return 0;
		}
	}
	
?>