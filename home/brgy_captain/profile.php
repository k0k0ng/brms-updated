<?php
  require('../session/brgy_captain.php'); // Secure Connection
  require('../session/brgy_admin_profile.php'); // Secure Profile Connection
  require('../session/brgy_admin_user_image.php'); // Get Image Src
?>

<?php 
  function getAge($date) { // Y-m-d format
    return intval(substr(date('Ymd') - date('Ymd', strtotime($date)), 0, -4));
  }
?>

  <!DOCTYPE html>
    <html lang="en">

    <head>
      <title>BRMS - Barangay Record Management System</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/custom2test.css">
      <link rel="stylesheet" href="../css/profile.css"> <!-- PROFILE -->
      <link rel="javascript" src="../js/jquery.js">
      <link rel="javascript" src="../js/jquery.min.js">

      <!--Website Tab Icon-->
      <link rel="icon" type="image/png" href="../images/logo.png"/>

      <script src="../js/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>

      <script>
        var info_id = "<?php echo $_SESSION['brms_SESSION_userId']; ?>";
        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>";
      </script>

      <script>
        // Load Note
        function load_log(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer5").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_log_table.php?info_id="+info_id+"&brgy_id="+brgy_id+"&str="+str, true);
          xmlhttp.send();    
        }      
      </script>

      <script>
        // Load Note
        function load_note(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer4").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_note_table.php?info_id="+info_id+"&brgy_id="+brgy_id+"&str="+str, true);
          xmlhttp.send();    
        }      
      </script>
      
      <script>
      // Load Complaint
      function load_complaint(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer3").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_complaintAdmin_table.php?info_id="+info_id+"&brgy_id="+brgy_id+"&str="+str, true);
          xmlhttp.send();    
        }      
      </script>

      <script>
      // Load Fee
      function load_fee(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer2").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_feeAdmin_table.php?info_id="+info_id+"&brgy_id="+brgy_id+"&str="+str, true);
          xmlhttp.send();    
        }      
      </script>
   
      <script>
      // Load Transaction
      function load_transaction(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer1").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_transactionAdmin_table.php?info_id="+info_id+"&brgy_id="+brgy_id+"&str="+str, true);
          xmlhttp.send();    
        }      
      </script>    

      <script>
        // Document Ready      
        $(document).ready(function(){
          load_transaction('');
          load_fee('');
          load_complaint('');
          load_note('');
          load_log('');

          // On Click Table
          $("#table_Value").on('click','.selectedRow',function(){
            // get the current row
            var currentRow=$(this).closest("tr");          
            var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value

            window.open("../pdf_transaction/"+id+".pdf");
          });

          // On Click Table
          $("#table_Value1").on('click','.selectedRow',function(){
            // get the current row
            var currentRow=$(this).closest("tr");          
            var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value

            window.open("../pdf_complaint/"+id+".pdf");
          });

        });      
      </script>    

      <!--for navigation bar-->

      <script>
        $.get("navigation.php", function(data){
          $("#nav-placeholder").replaceWith(data);
        });
      </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
          <div class="user-image">          
            <img src="<?php echo $src; ?>" alt="profile photo" style="width: 100px; margin: 0px auto;display: block;padding-top: 30px;" />          
            <br>
            <h3><?php echo $_SESSION['brms_SESSION_userLname'] . ', ' .$_SESSION['brms_SESSION_userFname'] . ' ' . $_SESSION['brms_SESSION_userMname'];?></h3>
            <a href="edit_profile.php" style="font-size: 12px; display: inline; vertical-align: bottom;">Edit profile</a>
            <br><br>
          </div>
        </div>

        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#profile">Profile</a></li>
            <li><a data-toggle="tab" href="#transaction">Transactions</a></li>
            <li><a data-toggle="tab" href="#fee">Fees</a></li>
            <li><a data-toggle="tab" href="#complaint">Complaints</a></li>
            <li><a data-toggle="tab" href="#note">Notes</a></li>
            <li><a data-toggle="tab" href="#log">Logs</a></li>
          </ul>
          <div class="tab-content">
            <div id="profile" class="tab-pane fade in active">
              <div class="user-body">
                <ul class="container-fluid details list-unstyled" >
                  <br>
                  <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                      <li><p><span class="glyphicon glyphicon-home" style="width:40px;"></span><b>Purok: </b><?php echo $_SESSION['brms_SESSION_userPurokName']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-road" style="width:40px;"></span><b>Address: </b><?php echo $_SESSION['brms_SESSION_userAddress']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-home" style="width:40px;"></span><b>Residence Type: </b><?php echo $_SESSION['brms_SESSION_userResidenceType']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-calendar" style="width:40px;"></span><b>Birthdate: </b><?php echo date("F d, Y", strtotime($_SESSION['brms_SESSION_userBdate'])); ?></p></li>
                      <li><p><span class="glyphicon glyphicon-calendar" style="width:40px;"></span><b>Age: </b><?php echo getAge($_SESSION['brms_SESSION_userBdate']); ?></p></li>
                      <li><p><span class="glyphicon glyphicon-user" style="width:40px;"></span><b>Gender: </b><?php echo $_SESSION['brms_SESSION_userGender']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-heart" style="width:40px;"></span><b>Civil Status: </b><?php echo $_SESSION['brms_SESSION_userStatus']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-tint" style="width:40px;"></span><b>Blood Type: </b><?php echo $_SESSION['brms_SESSION_userBlood_type']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-briefcase" style="width:40px;"></span><b>Occupation: </b><?php echo $_SESSION['brms_SESSION_userOccupation']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-check" style="width:40px;"></span><b>Currently Employed: </b><?php echo $_SESSION['brms_SESSION_userCurEmployed']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-check" style="width:40px;"></span><b>PWD: </b><?php echo $_SESSION['brms_SESSION_userPwd']; ?></p></li>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                      <li><p><span class="glyphicon glyphicon-education" style="width:40px;"></span><b>Education: </b><?php echo $_SESSION['brms_SESSION_userEducation']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-check" style="width:40px;"></span><b>Currently Enrolled: </b><?php echo $_SESSION['brms_SESSION_userCurEnrolled']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-check" style="width:40px;"></span><b>Senior Citizen Member: </b><?php echo $_SESSION['brms_SESSION_userSeniorCitizen']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-check" style="width:40px;"></span><b>Deceased: </b><?php echo $_SESSION['brms_SESSION_userDeceased']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-check" style="width:40px;"></span><b>Religion: </b><?php echo $_SESSION['brms_SESSION_userReligion']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-earphone" style="width:40px;"></span><b>Cell No: </b><?php echo $_SESSION['brms_SESSION_userCell']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-phone-alt" style="width:40px;"></span><b>Tell No: </b><?php echo $_SESSION['brms_SESSION_userTell']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-check" style="width:40px;"></span><b>Registered Voter: </b><?php echo $_SESSION['brms_SESSION_userIsVoter']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-check" style="width:40px;"></span><b>Voter's ID: </b><?php echo $_SESSION['brms_SESSION_userVoterId']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-check" style="width:40px;"></span><b>Barangay ID: </b><?php echo $_SESSION['brms_SESSION_userCitizenBrgyId']; ?></p></li>
                    </div>
                  </div>
                </ul>
              </div>
            </div>

            <div id="transaction" class="tab-pane fade ">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  
                  <br>
                  <div class="input-group">
                    <span class="input-group-addon">Search 10 Most Recent Transactions</span>
                    <input id="search" type="text" onkeyup="load_transaction(this.value)" class="form-control" name="search" placeholder="Citizen or Purpose" required>
                  </div>
                  <br>

                  <div id="table_Value" class="table-responsive">       
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Citizen</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Purpose</th>                     
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer1">
                      </tbody>
                    </table>
                  </div>
                </div>                
              </div>
              <br>
            </div>

            <div id="fee" class="tab-pane fade ">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  
                  <br>
                  <!--Date-->
                  <div class="input-group">
                    <span class="input-group-addon">Search 10 Most Recent Fees</span>
                    <input id="search" onkeyup="load_fee(this.value)" type="text" class="form-control" name="search" placeholder="Citizen or Fee Type" required>
                  </div>
                  <br>

                  <div id="" class="">          
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Citizen</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Amount Paid</th>
                          <th>Fee Type</th>                          
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer2">
                      </tbody>
                    </table>
                  </div>
                </div>                                
              </div>
            </div>

            <div id="complaint" class="tab-pane fade ">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  
                  <br>
                  <!--Date-->
                  <div class="input-group">
                    <span class="input-group-addon">Search 10 Most Recent Complaints</span>
                    <input id="search" onkeyup="load_complaint(this.value)" type="text" class="form-control" name="search" placeholder="Complainant or Complainee or Actions" required>
                  </div>
                  <br>

                  <div id="table_Value1" class="table-responsive">           
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Complainant</th>
                          <th>Complainee</th>
                          <th>Date</th>
                          <th>Time</th> 
                          <th>Actions</th>
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer3">
                      </tbody>
                    </table>
                  </div>
                </div>                                
              </div>
            </div>

            <div id="note" class="tab-pane fade ">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  
                  <br>
                  <!--Date-->
                  <div class="input-group">
                    <span class="input-group-addon">Search 10 Most Recent Notes</span>
                    <input id="search" onkeyup="load_note(this.value)" type="text" class="form-control" name="search" placeholder="Citizen / Business or Note" required>
                  </div>
                  <br>

                  <div id="" class="">          
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Citizen / Business</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Note</th>  
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer4">
                      </tbody>
                    </table>
                  </div>
                </div>                                
              </div>
            </div>

            <div id="log" class="tab-pane fade ">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  
                  <br>
                  <!--Date-->
                  <div class="input-group">
                    <span class="input-group-addon">Search 10 Most Recent Logs</span>
                    <input id="search" onkeyup="load_log(this.value)" type="text" class="form-control" name="search" placeholder="History" required>
                  </div>
                  <br>

                  <div id="" class="">          
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>                        
                          <th>History</th>
                          <th>Date</th>
                          <th>Time</th>  
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer5">
                      </tbody>
                    </table>
                  </div>
                </div>                                
              </div>
            </div>

          </div>
          <br>
        </div>

      </div>
    </div>


</body>
</html>