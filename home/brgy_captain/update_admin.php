<?php
  require('../session/brgy_captain.php'); // Secure Connection
  require('../database/brgy_captain_database_query.php'); // Database Query
?> 

<?php 
  if (!empty($_GET)) {
    $name = $_GET["name"];
  }else{
    header('Location: admin_home.php');
  }
?>

<?php 
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Add Admin") {
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 
    $user_id = $_POST["user_id"];
    $access_level = $_POST["access_level"];
    $username = $_POST["username"];
    $password = $_POST["password"];

    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> add_admin($brgy_id,$user_id,$access_level,$username,$password); // Add Admin

    // Success
    if($result == 1){

      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Add New Admin " 
                          . $_POST["last_name"] . ", "
                          . $_POST["first_name"] . " "
                          . $_POST["middle_name"]); // Insert Log 

      echo "<script type='text/javascript'>
                alert('Admin Successfully Added!');
                location = 'admin_home.php';
            </script>"; 
    }
    // Error
    else{
      echo "<script type='text/javascript'>
                alert('Error!');
            </script>"; 
    }
  }
?>

<?php 
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Update Admin") {    
    $user_id = $_POST["user_id"];
    $access_level = $_POST["access_level"];
    $password = $_POST["password"];

    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> update_admin($user_id,$access_level,$password); // Update Admin

    // Success
    if($result == 1){

      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Update Admin " 
                          . $_POST["last_name"] . ", "
                          . $_POST["first_name"] . " "
                          . $_POST["middle_name"]); // Insert Log 

      echo "<script type='text/javascript'>
                alert('Admin Successfully Updated!');
                location = 'admin_home.php';
            </script>"; 
    }
    // Error
    else{
      echo "<script type='text/javascript'>
                alert('Error!');
            </script>"; 
    }

  }
?>

<?php 
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Delete Admin") {    
    $user_id = $_POST["user_id"];
    
    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> delete_account($user_id); // Update Admin

    // Success
    if($result == 1){
      echo "<script type='text/javascript'>
                alert('Admin successfully deleted!');
                location = 'admin_home.php';
            </script>"; 
    }
    // Error
    else{
      echo "<script type='text/javascript'>
                alert('Error!');
            </script>"; 
    }

  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">
    
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
      // On Submit
      function doCheckUpdate(){

        var message = document.getElementById("submit_admin").value === 'Update Admin' ? 'update' : 'add';
        
        if (confirm("Are you sure to " + message + " this admin?") == true) {
          return true;
        } else {
          return false;
        }

      }
    </script>

    <script>
      // On Submit
      function doCheckDelete(){
        
        if (confirm("Are you sure to delete this Admin?") == true) {
          return true;
        } else {
          return false;
        }

      }
    </script>
    
    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script>
      var valid = "glyphicon glyphicon-ok"; // VALID
      var invalid = "glyphicon glyphicon-remove"; // INVALID
    </script>

    <script>
      var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID
    </script>

    <script>
      // Check if Valid Admin
      function valid_admin(){
        if(document.getElementById("valid_user").className === valid && document.getElementById("valid_pass").className === valid
            && (document.getElementById("password").value !== '' && document.getElementById("password_confirm").value !== '')){
          document.getElementById("submit_admin").disabled = false;
        }else{
          document.getElementById("submit_admin").disabled = true; 
        }
      }
    </script>

    <script>
      // Check if username exist
      function check_username(username){

        if(username.length == 0){
          document.getElementById("valid_user").className = invalid;
          valid_admin(); // Check if Valid (Submit Button) 
        }
        else{
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
          
          if (this.readyState == 4 && this.status == 200) {
              // Username Found
              if(this.responseText == 1){
                document.getElementById("valid_user").className = invalid;            
              }
              // Username Not Found
              else{                
                document.getElementById("valid_user").className = valid;                
              }
              valid_admin(); // Check if Valid (Submit Button)               
            }
          };          

          xmlhttp.open("GET", "../ajax/checkAjax_admin.php?username=" + username, true);
          xmlhttp.send();
        }        
      }
    </script>
    
    <script>
      // Check Password
      function checkPassword(){
        var password = document.getElementById("password").value;
        var confirm = document.getElementById("password_confirm").value;

        if(document.getElementById("user_id").value !== ''){
          // Password Match
          if(password === confirm && password !== '' && confirm !== ''){
            document.getElementById("valid_pass").className = valid;
          }
          // Not Match
          else{
            document.getElementById("valid_pass").className = invalid;
          }
        }
        valid_admin(); // Check if Valid (Submit Button)
      }
    </script>
    
    <script>
      // Clear Input
      function clearInput(){
        document.getElementById("user_id").value = ''; 
        document.getElementById("first_name").value = '';
        document.getElementById("middle_name").value = '';
        document.getElementById("last_name").value = '';
        document.getElementById("username").value = ''; 
        document.getElementById("password").value = '';
        document.getElementById("password_confirm").value = ''; 
        document.getElementById("access_level_list").selectedIndex = "0";
        document.getElementById("valid_user").className = invalid;
        document.getElementById("valid_pass").className = invalid;
        document.getElementById("username").readOnly = false;
        document.getElementById("submit_admin").value = ("Add Admin");
        document.getElementById("delete_admin").style.display = "none";
      }
    </script>
  
    <script>
      // OnKeyRelease
      function checkUser(str){

        if(str.length == 0){
          clearInput(); // Clear Inputs 
        }else{
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              if(this.responseText !== ''){
                var data = this.responseText.split(",");

                document.getElementById("user_id").value = (data[0]) ? data[0] : '';
                document.getElementById("first_name").value = (data[1]) ? data[1] : '';
                document.getElementById("middle_name").value = (data[2]) ? data[2] : '';
                document.getElementById("last_name").value = (data[3]) ? data[3] : '';
                document.getElementById("username").value = (data[4]) ? data[4] : '';
                //document.getElementById("password").value = (data[5]) ? data[5] : '';
                //document.getElementById("password_confirm").value = (data[5]) ? data[5] : '';
                $("#access_level_list").val(data[6]); // Role
                checkPassword(); // Check Password

                // If Username Exist
                if(document.getElementById("username").value !== ''){
                  document.getElementById("username").readOnly = true;
                  document.getElementById("valid_user").className = valid;
                  document.getElementById("submit_admin").value = ("Update Admin");   

                  document.getElementById("submit_admin").disabled = true;  
                  document.getElementById("delete_admin").style.display = "block";
                              
                }else{
                  document.getElementById("username").readOnly = false;
                  document.getElementById("valid_user").className = invalid;
                  document.getElementById("submit_admin").value = ("Add Admin");

                  document.getElementById('access_level_list').selectedIndex = 1;
                  document.getElementById("delete_admin").style.display = "none";
                }

              }else{
                clearInput(); // Clear Inputs                  
              }                           
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_citizenProfile2.php?q="+str+"&brgy_id="+brgy_id, true);
          xmlhttp.send();
        }
      }
    </script>

    <script>
      // Load Access Level
      function loadAccessLevel(){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) { 

            var data = this.responseText.split(",");        

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#access_level_list').append("<option>"+data[i]+"</option>")
              }
            }
              update(); // Load Update
          }
        };
        
        xmlhttp.open("GET", "../ajax/loadAjax_access_level.php?", true); // Get access levels
        xmlhttp.send();
      }
    </script>

    <script>
      $( document ).ready(function() {
        loadAccessLevel(); // Load Access Level
        checkUser("<?php echo $name; ?>");
        document.getElementById("password").focus();
      });
    </script>

    <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>
    
    <div class="container container-profile"><!-- Profile Divider --> 
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
          <div class="panel panel-default">

            <div class="panel-heading">
              <h3 class="panel-title">Update Admin</h3>
            </div>

            <div class="panel-body">
              <form action="" method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
              
                <div class="input-group">
                  <span class="input-group-addon">First name</span>                  
                  <input type="text" name="first_name" id="first_name" class="form-control input-sm" required readonly>
                </div>
              <br>
                <div class="input-group">
                  <span class="input-group-addon">Middle name</span>                  
                  <input type="text" name="middle_name" id="middle_name" class="form-control input-sm" required readonly>
                </div>
              <br>
                <div class="input-group">
                  <span class="input-group-addon">Last name</span>                  
                  <input type="text" name="last_name" id="last_name" class="form-control input-sm" required readonly>
                </div>
              <input type="text" name="user_id" id="user_id" class="form-control" placeholder="User Id" style="display: none;">
              <br>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-star"></i> Access Level</span>
                    <select id="access_level_list" name="access_level" class="form-control input-sm">
                    </select>
                  </div>
              <br>
                <div class="input-group">
                  <span class="input-group-addon">Username</span>                  
                  <input type="text" onkeyup="check_username(this.value)" name="username" id="username" class="form-control input-sm" required>
                  <span class="input-group-addon"><i id="valid_user" class="glyphicon glyphicon-remove"/></i></span>
                </div>
              <br>
                <div class="input-group">
                  <span class="input-group-addon">Password</span>                  
                  <input type="password" onkeyup="checkPassword()" name="password" id="password" class="form-control input-sm">
                </div>
              <br>
                <div class="input-group">
                  <span class="input-group-addon">Confirm Password</span>                  
                  <input type="password" onkeyup="checkPassword()" name="password_confirm" id="password_confirm" class="form-control input-sm">
                  <span class="input-group-addon"><i id="valid_pass" class="glyphicon glyphicon-remove"/></i></span>
                </div>
              <br>
                
                <input id="submit_admin" name="submit" type="submit" onclick="return doCheckUpdate()" value="Add Admin" class="btn btn-primary" style="float: right; margin-left:10px; width: 125px;" disabled>
                <input id="delete_admin" name="submit" type="submit" onclick="return doCheckDelete()" value="Delete Admin" class="btn btn-danger" style="float: right; margin-left:10px; display: none; width: 125px;"> 
                <span onclick="window.open('admin_home.php', '_top')" class="btn btn-info" style="float: right; width: 125px;">Cancel</span>

              </form>
            </div>

          </div>
        </div>
      </div>
    </div>

  </body>

</html>