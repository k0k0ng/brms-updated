<?php
  require('../session/brgy_captain.php'); // Secure Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
?>
<?php
  // Update Password
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Save Changes"){ 
    
    if(!password_verify($_POST["old_password"],$_SESSION['brms_password'])){
      echo "<script type='text/javascript'>
                alert('Invalid Old Password!');
                location = 'change_password.php';
            </script>";
      return;
    }

    $password = $_POST["new_password"]; // Get Password
    $info_id = $_SESSION['brms_userId']; // Info ID

    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> update_admin_password($info_id,$password);

    if($result==0){
      echo "<script type='text/javascript'>
                alert('Error!');
            </script>";             
    }
    else{
      echo "<script type='text/javascript'>
                alert('Password Successfully Updated!');
                location = 'change_password.php';
            </script>";  
    }

  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  
  <title>BRMS - Barangay Record Management System</title>
  <!--Website Tab Icon-->
  <link rel="icon" type="image/png" href="../images/logo.png"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/custom2test.css">    

  <link rel="javascript" src="../js/jquery.js">
  <link rel="javascript" src="../js/jquery.min.js">

  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->
  
  <script>
    function verifyPassword(){
      if (confirm("Are you sure to change password?")) {
        return checkPassword(); // Check Password
      } else {
        return false;
      }  
    }
  </script>

  <script>
    function checkPassword(){
      var pass = document.getElementById("password").value;
      var conf = document.getElementById("confirm").value;

      if(pass === ''){
        alert("Invalid password");
        document.getElementById('password').requestFocus();
        return false;
      }

      if(conf === ''){
        alert("Invalid password");
        document.getElementById('confirm').requestFocus();
        return false;
      }

      if(pass === conf){
        return true;
      }else{
        alert("Password did not match!");
        return false;
      }

    }
  </script>

  <script>
    $.get("navigation.php", function(data){
      $("#nav-placeholder").replaceWith(data);
    });
  </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container container-profile"> <!-- Profile Divider -->  
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">         
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Change Password</h3>
            </div>

              <div class="panel-body">
                <div class="row">
                  <form method="post" role="form" autocomplete="off" action="">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="input-group">
                        <span class="input-group-addon">Old Password</i></span>                  
                        <input id="old_password" type="password" class="form-control" name="old_password"  required autofocus>
                      </div>
                      <br>
                      <div class="input-group">
                        <span class="input-group-addon">New Password</span>                  
                        <input id="new_password" type="password" class="form-control" name="new_password" required>
                      </div>
                      <br>
                      <div class="input-group">
                        <span class="input-group-addon">Confirm Password</span>                  
                        <input id="confirm" type="password" class="form-control" name="confirm" required>
                      </div>
                      <br>
                      <input onclick="return verifyPassword()" id="brgy_name" type="submit" name="submit" class="btn btn-2 btn-lg btn-primary btn-block btn-signin" value="Save Changes">
                    </div>
                  </form>                  
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>      

  </body>

</html>