<?php
  session_start(); 
  require_once('tcpdf_include.php'); // Add PDF api
  require('../../session/brgy_admin_brgy_image.php'); // Get Image Src
  require('../../database/brgy_admin_database_query.php'); // Database Query  

  // Image Location
  if(isset($_SESSION['brms_brgyImgName']) || !empty($_SESSION['brms_brgyImgName'])){
    $src = '../../brgy_image/'.$_SESSION['brms_brgyImgName'].'.'.$_SESSION['brms_brgyImgExtension'];      
  }else{
    $src = '../../brgy_image/logo.png';
  }

  // Insert Log
  function insert_log(){ 
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_log($_SESSION['brms_userId'],"Print Religion"); // Insert Log
  }

  if(isset($_SESSION['brms_brgyId']) || !empty($_SESSION['brms_brgyId'])){

    $davao = '../../images/DavaoCity.png'; // Davao City Logo
    
    insert_log(); // Insert Log
    
    $brgy_name = $_SESSION['brms_brgyName']; // Barangay Name
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $obj_pdf->SetCreator(PDF_CREATOR);  
    $obj_pdf->SetTitle('Barangay '.$brgy_name.' Religion');  
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $obj_pdf->SetDefaultMonospacedFont('helvetica');  
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
    $obj_pdf->setPrintHeader(false);  
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);  
    $obj_pdf->SetFont('times', '', 12);  
    $obj_pdf->AddPage('L');  

    date_default_timezone_set('America/Mexico_City'); 
    $curDate=date('F j, Y');

    $content ='<style type="text/css"> 
                  td{
                    font-size: 80%;
                  }      
                  tr{
                    text-align: center;
                  }              
              </style>'; 

    $content .= '<table style="text-align:center;">
                    <tr>
                      <td class="icon"><img src="'.$src.'" width="500px"></td>
                    </tr>
                  </table>';

    $content .= '<p style="text-align:center; font-weight: bold;">Religion</p>';

    $content .= '<table border="1" cellpadding="10">
                  <tr bgcolor="#800000" style="color: white;">
                    <thead>
                      <th colspan="2"><b>Purok Name</b></th>
                      <th><b>A</b></th>
                      <th><b>B</b></th>
                      <th><b>C</b></th>
                      <th><b>D</b></th>
                      <th><b>E</b></th>
                      <th><b>F</b></th>
                      <th><b>G</b></th>
                      <th><b>H</b></th>
                      <th><b>I</b></th>
                      <th><b>J</b></th>
                      <th><b>K</b></th>
                      <th><b>L</b></th>
                      <th><b>M</b></th>
                      <th><b>N</b></th>
                      <th><b>O</b></th>
                    </thead>
                  </tr>
                  '.getPurokTable().'            
                </table>';    

    $obj_pdf->writeHTML($content);

    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT); 
    $obj_pdf->AddPage();
    $content = legend();
    
    $obj_pdf->writeHTML($content);  
    ob_end_clean(); // Clear
    $obj_pdf->Output('brgy_'.$brgy_name.'_religion.pdf', 'I');

  }else{
    header("Location: ../../brgy_admin/admin_home.php");
  }

  // Get Purok
  function getPurokTable() {
      $database = new Database(); // Create Database Connection
      $conn = $database -> get_Connection(); // Get Database Connection
      
      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID

      $sql = "SELECT
                purok.name AS 'Purok Name',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Roman Catholic' AND purok_id = purok.id) AS 'A',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Islam' AND purok_id = purok.id) AS 'B',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Protestant' AND purok_id = purok.id) AS 'C',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Iglesia ni Cristo' AND purok_id = purok.id) AS 'D',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Jesus Miracle Crusade International Ministry' AND purok_id = purok.id) AS 'E',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Members Church of God International' AND purok_id = purok.id) AS 'F',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Most Holy Church of God in Christ Jesus' AND purok_id = purok.id) AS 'G',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Philippine Independent Church' AND purok_id = purok.id) AS 'H',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Apostolic Catholic Church' AND purok_id = purok.id) AS 'I',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Orthodoxy' AND purok_id = purok.id) AS 'J',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'The Kingdom of Jesus Christ the Name Above Every Name' AND purok_id = purok.id) AS 'K',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Judaism' AND purok_id = purok.id) AS 'L',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Hinduism' AND purok_id = purok.id) AS 'M',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Atheism' AND purok_id = purok.id) AS 'N',
                (SELECT COUNT(*) FROM user_info WHERE user_info.religion = 'Others' AND purok_id = purok.id) AS 'O'

              FROM user_info
              RIGHT OUTER JOIN purok
              ON purok.id = user_info.purok_id
              WHERE purok.brgy_id = $brgy_id
              GROUP BY purok.id
              ORDER BY purok.id, purok.name";              

      $result = $conn->query($sql);
      
      $return = "<tbody>";
      if ($result->num_rows > 0) {

        // output data of each row
        while($row = $result->fetch_assoc()) {
          $return .= "<tr class=\"tr\">
                                  <td colspan=\"2\">" . $row['Purok Name'] .
                                  "</td><td>" . $row['A'] . 
                                  "</td><td>" . $row['B'] . 
                                  "</td><td>" . $row['C'] . 
                                  "</td><td>" . $row['D'] . 
                                  "</td><td>" . $row['E'] .
                                  "</td><td>" . $row['F'] . 
                                  "</td><td>" . $row['G'] .
                                  "</td><td>" . $row['H'] . 
                                  "</td><td>" . $row['I'] .
                                  "</td><td>" . $row['J'] . 
                                  "</td><td>" . $row['K'] .
                                  "</td><td>" . $row['L'] . 
                                  "</td><td>" . $row['M'] .
                                  "</td><td>" . $row['N'] .   
                                  "</td><td>" . $row['O'] .                                
                      "</td></tr>";
        }
      }
      $return .= "</tbody>";
      mysqli_close($conn);

      return $return;
    }

    // Get Legend
    function legend(){
      $return = '<table>';
      $return .= '<tr><td><b>' . 'LEGEND' . '</b></td></tr>';
      $return .= '<tr><td><b>' . 'A</b> = Roman Catholic' . '</td></tr>';
      $return .= '<tr><td><b>' . 'B</b> = Islam' . '</td></tr>';
      $return .= '<tr><td><b>' . 'C</b> = Protestant' . '</td></tr>';
      $return .= '<tr><td><b>' . 'D</b> = Iglesia ni Cristo' . '</td></tr>';
      $return .= '<tr><td><b>' . 'E</b> = Jesus Miracle Crusade International Ministry' . '</td></tr>';
      $return .= '<tr><td><b>' . 'F</b> = Members Church of God International' . '</td></tr>';
      $return .= '<tr><td><b>' . 'G</b> = Most Holy Church of God in Christ Jesus' . '</td></tr>';
      $return .= '<tr><td><b>' . 'H</b> = Philippine Independent Church' . '</td></tr>';
      $return .= '<tr><td><b>' . 'I</b> = Apostolic Catholic Church' . '</td></tr>';
      $return .= '<tr><td><b>' . 'J</b> = Orthodoxy' . '</td></tr>';
      $return .= '<tr><td><b>' . 'K</b> = The Kingdom of Jesus Christ the Name Above Every Name' . '</td></tr>';
      $return .= '<tr><td><b>' . 'L</b> = Judaism' . '</td></tr>';
      $return .= '<tr><td><b>' . 'M</b> = Hinduism' . '</td></tr>';
      $return .= '<tr><td><b>' . 'N</b> = Atheism' . '</td></tr>';
      $return .= '<tr><td><b>' . 'O</b> = Others' . '</td></tr>';;
      $return .= '</table>';
      return $return;
    }
    
?>



