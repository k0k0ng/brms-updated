<?php
  session_start(); 
  require_once('tcpdf_include.php'); // Add PDF api
  require('../../database/brgy_admin_database_query.php'); // Database Query

  // Profile Variables
  $CITIZEN_ADDRESS;
  $PUROK;
  $RESIDENCE_TYPE;
  $HIS_HER;
  $HE_SHE;
  $MALE_FEMALE;
  $STATUS;
  $BLOOD_TYPE;
  $BIRTHDATE;
  $AGE;
  $EDUCATION;
  $OCCUPATION;
  $RELIGION;
  $CITIZEN_NAME;
  $BRGY_ID;

  // Replace String
  function replace_string($string){
    $string = str_replace('CITIZEN_ADDRESS','<b>'.$GLOBALS['CITIZEN_ADDRESS'].'</b>',$string);
    $string = str_replace('PUROK','<b>Purok '.$GLOBALS['PUROK'].'</b>',$string);
    $string = str_replace('RESIDENCE_TYPE','<b>'.$GLOBALS['RESIDENCE_TYPE'].'</b>',$string);
    $string = str_replace('HIS_HER','<b>'.$GLOBALS['HIS_HER'].'</b>',$string);
    $string = str_replace('MALE_FEMALE','<b>'.$GLOBALS['MALE_FEMALE'].'</b>',$string);
    $string = str_replace('STATUS','<b>'.$GLOBALS['STATUS'].'</b>',$string);
    $string = str_replace('BLOOD_TYPE','<b>'.$GLOBALS['BLOOD_TYPE'].'</b>',$string);
    $string = str_replace('BIRTHDATE','<b>'.$GLOBALS['BIRTHDATE'].'</b>',$string);
    $string = str_replace('AGE','<b>'.$GLOBALS['AGE'].'</b>',$string);
    $string = str_replace('EDUCATION','<b>'.$GLOBALS['EDUCATION'].'</b>',$string);
    $string = str_replace('OCCUPATION','<b>'.$GLOBALS['OCCUPATION'].'</b>',$string);
    $string = str_replace('RELIGION','<b>'.$GLOBALS['RELIGION'].'</b>',$string);
    $string = str_replace('BARANGAY','<b>Barangay '.$_SESSION['brms_brgyName'].'</b>',$string);
    $string = str_replace('CITY','<b>Davao City</b>',$string); 
    $string = str_replace('CITIZEN_NAME','<b>'.$GLOBALS['CITIZEN_NAME'].'</b>',$string);
    $string = str_replace('BRGY_ID','<b>'.$GLOBALS['BRGY_ID'].'</b>',$string);
    $string = str_replace("\n",'<br>',$string);
    $string = str_replace("[",'<b>',$string);
    $string = str_replace("]",'</b>',$string);
    $string = str_replace("{",'<i>',$string);
    $string = str_replace("}",'</i>',$string);
    $string = str_replace('HE_SHE','<b>'.$GLOBALS['HE_SHE'].'</b>',$string);

     // Indent
    $temp = (explode("\t",$string)); 
    $string = "";
    foreach ($temp as &$array_value) {
      $string .= '<span class="indent">' . $array_value . '</span>';
    }

    return $string;
  }

  // Get Information
  function get_information($info_id, $brgy_id){

    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection

    $sql = "SELECT user_info.*, purok.name, (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) AS AGE
            FROM user_info
            INNER JOIN purok ON user_info.purok_id = purok.id
            WHERE user_info.id = $info_id
            AND user_info.brgy_id = $brgy_id";

    $result = $conn->query($sql);
    
      if($result->num_rows > 0){
        // output data of each row
        while($row = $result->fetch_assoc()) {          
          $GLOBALS['CITIZEN_ADDRESS'] = $row['address'];
          $GLOBALS['PUROK'] = $row['name'];
          $GLOBALS['RESIDENCE_TYPE'] = $row['residence_type'];

          $GLOBALS['HE_SHE'] = ($row['gender'] === 'Male' ? 'He' : 'She');
          $GLOBALS['HIS_HER'] = ($row['gender'] === 'Male' ? 'His' : 'Her');
          $GLOBALS['MALE_FEMALE'] = $row['gender'];
          $GLOBALS['STATUS'] = $row['status'];

          $GLOBALS['BLOOD_TYPE'] = $row['blood_type'];

          $date = date_create($row['birthdate']);
          $GLOBALS['BIRTHDATE'] = date_format($date, 'F d, Y');

          $GLOBALS['AGE'] = $row['AGE'];

          $GLOBALS['EDUCATION'] = $row['education'];
          $GLOBALS['OCCUPATION'] = $row['occupation'];
          $GLOBALS['RELIGION'] = $row['religion'];

          $GLOBALS['CITIZEN_NAME'] = strtoupper($row['first_name'] . ' ' . $row['middle_name'][0] . '. ' . $row['last_name']);     
          $GLOBALS['BRGY_ID'] = $row['citizen_brgy_id'];
        }
      }
  }

  // Insert Transaction
  function insert_transaction($type,$receipt_no){
    $info_id = $_SESSION['brms_SESSION_userId']; // Info ID
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $admin_id = $_SESSION['brms_userId']; // Admin ID

    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_transaction($brgy_id,$info_id,$admin_id,$type,$receipt_no);
  }

  // Insert Log
  function insert_log($type){ 
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_log($_SESSION['brms_userId'],$type); // Insert Log
  }

  // Get Transcation ID
  function get_transaction_id(){

    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 

    $sql = "SELECT transaction.* 
            FROM transaction 
            INNER JOIN user_info ON user_info.id = transaction.info_id
            WHERE user_info.brgy_id = $brgy_id ORDER BY transaction.id DESC";
    $result = $conn->query($sql);
        
    if($result->num_rows > 0){
      // output data of each row
      while($row = $result->fetch_assoc()) {
        return $row['id'];
      }
    } return 0;

  }

  if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Image Location
    if(isset($_SESSION['brms_brgyImgName']) || !empty($_SESSION['brms_brgyImgName'])){
      $src = '../../brgy_image/'.$_SESSION['brms_brgyImgName'].'.'.$_SESSION['brms_brgyImgExtension'];      
    }else{
      $src = '../../brgy_image/logo.png';
    }
   
    // Check if Barangay Official is Set
    if(!isset($_POST['official_list'])){
      echo "<script type='text/javascript'>
                alert('Please setup Barangay Official!');
                location = '../../brgy_admin/brgy_official.php';
            </script>";
      return;       
    }   

    $davao = '../../images/DavaoCity.png'; // Davao City Logo
    
    $brgy_name = $_SESSION['brms_brgyName'];
    $brgy_address = $_SESSION['brms_brgyAddress'];

    $first_name = $_SESSION['brms_SESSION_userFname'];
    $last_name = $_SESSION['brms_SESSION_userLname'];
    $middle_name = $_SESSION['brms_SESSION_userMname'];
    
    date_default_timezone_set('America/Mexico_City');
    $cur_date = date('jS \d\a\y \o\f F Y');

    $official = $_POST['official_list'];

    $header_temp = trim(strstr($_POST['certification_list'], '(', true));
    $header = (strlen($header_temp)>0) ? $header_temp : $_POST['certification_list'];

    $receipt = $_POST['receipt'];
    $paragraph1 = $_POST['paragraph1'];
    $paragraph2 = $_POST['paragraph2'];

    /** GET INFORMATION **/
    get_information($_SESSION['brms_SESSION_userId'], $_SESSION['brms_brgyId']); 
    
    /** REPLACE VALUES **/
    $paragraph1  = replace_string($paragraph1);

    insert_transaction($header,$receipt); // Insert Transaction
    insert_log($header); // Insert Log

    if($official === $_SESSION['brms_cap_name']){
      $position = "Punong Barangay";
    }
    else if($official === $_SESSION['brms_wad1_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad2_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad3_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad4_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad5_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad6_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad7_name']){
      $position = "Kagawad";
    }

    $official = strtoupper($official); // UPPERCASE

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $obj_pdf->SetCreator(PDF_CREATOR);  
    $obj_pdf->SetTitle("Barangay ".$brgy_name." Certificate of Residency");  
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $obj_pdf->SetDefaultMonospacedFont('helvetica');  
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
    $obj_pdf->setPrintHeader(false);  
    $obj_pdf->setPrintFooter(false);  
    $obj_pdf->SetAutoPageBreak(TRUE, 10);  
    $obj_pdf->SetFont('times', '', 10);  
    $obj_pdf->AddPage();  

    $content ='<style type="text/css"> 
                    body {background-color: powderblue;} 
                    h1 {color: blue;}
                    .icon {
                      line-height: 150px;
                    }
                    .leftcontainer{
                      width: 50%;
                      float: left;
                    }
                    .rightcontainer{
                      width: 50%;
                      float: left;
                    }
                    .parag{
                      text-align: justify;
                    }
                    .indent{
                      text-indent: 30px;
                    }
                </style>'; 

    $content .= '<table style="text-align:center">
                    <tr>
                      <td class="icon"><img src="'.$src.'" width="500px"></td>
                    </tr>
                  </table>';

    $content .= '<h2 style="text-transform: uppercase; text-align: center;">'.$header.'<br></h2>';

    $content .= '<p class="parag">'.$paragraph1.'</p>';
    $content .= '<p class="parag indent">'.$paragraph2.'</p>';

    if($official === strtoupper($_SESSION['brms_cap_name'])){
      $content .= '<table>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><p style="text-align:center">
                        <br><br><br>
                        <b><u>'.$official.'</u></b><br>
                        '.$position.'
                      </p></td>
                    </tr>
                    <tr></tr>
                </table>';

      $content .= '<br><br><br>';
      $content .= '<table>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><p style="text-align:center">
                            <font color="red">"Not valid without official seal"</font>                          
                            </p>
                        </td>
                      </tr>
                  </table>';
    }else{
      $content .= '<table>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>
                        <p>
                          For and in behalf of:<br><br>
                          <b><u>'.strtoupper($_SESSION['brms_cap_name']).'</u></b><br>
                          Punong Barangay
                        </p>
                        <p>
                          By:<br><br>
                          <b><u>'.$official.'</u></b><br>
                          '.$position.'
                        </p>
                      </td>
                    </tr>
                </table>';
      
      $content .= '<br><br>';
      $content .= '<table>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td> 
                        <td><p>
                            <font color="red">"Not valid without official seal"</font>                          
                            </p>
                        </td>
                      </tr>
                  </table>';
    }


    $obj_pdf->writeHTML($content); 
    ob_end_clean(); // Clear
    //$obj_pdf->Output('brgy_'.$brgy_name.'_certificate_of_residency.pdf', 'I'); 
    $obj_pdf->Output(__DIR__.'/../../pdf_transaction/'.get_transaction_id().'.pdf', 'FI');

  }else{
    header("Location: ../../brgy_admin/form_residency.php");
  }
  
?>