<?php
  session_start(); 
  require_once('tcpdf_include.php'); // Add PDF api
  require('../../session/brgy_admin_brgy_image.php'); // Get Image Src
  require('../../database/brgy_admin_database_query.php'); // Database Query  

  // Image Location
  if(isset($_SESSION['brms_brgyImgName']) || !empty($_SESSION['brms_brgyImgName'])){
    $src = '../../brgy_image/'.$_SESSION['brms_brgyImgName'].'.'.$_SESSION['brms_brgyImgExtension'];      
  }else{
    $src = '../../brgy_image/logo.png';
  }

  // Insert Log
  function insert_log(){ 
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_log($_SESSION['brms_userId'],"Print Currently Employed"); // Insert Log
  }

  if(isset($_SESSION['brms_brgyId']) || !empty($_SESSION['brms_brgyId'])){

    $davao = '../../images/DavaoCity.png'; // Davao City Logo
    
    insert_log(); // Insert Log
    
    $brgy_name = $_SESSION['brms_brgyName']; // Barangay Name
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $obj_pdf->SetCreator(PDF_CREATOR);  
    $obj_pdf->SetTitle('Barangay '.$brgy_name.' Currently Employed');  
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $obj_pdf->SetDefaultMonospacedFont('helvetica');  
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
    $obj_pdf->setPrintHeader(false);  
    $obj_pdf->SetAutoPageBreak(TRUE, 10);  
    $obj_pdf->SetFont('times', '', 12);  
    $obj_pdf->AddPage();  

    date_default_timezone_set('America/Mexico_City'); 
    $curDate=date('F j, Y');

    $content ='<style type="text/css"> 
                  td{
                    font-size: 80%;
                  }          
                  tr{
                    text-align: center;
                  }          
              </style>'; 

    $content .= '<table style="text-align:center;">
                    <tr>
                      <td class="icon"><img src="'.$src.'" width="500px"></td>
                    </tr>
                  </table>';

    $content .= '<p style="text-align:center; font-weight: bold;">Currently Employed</p>';

    $content .= '<table border="1" cellpadding="10">
                  <tr bgcolor="#800000" style="color: white;">
                    <thead>
                      <th><b>Purok Name</b></th>
                      <th><b>Employed</b></th>                      
                      <th><b>Population</b></th>
                    </thead>
                  </tr>
                  '.getPurokTable().'            
                </table>';    

    $obj_pdf->writeHTML($content);  
    ob_end_clean(); // Clear
    $obj_pdf->Output('brgy_'.$brgy_name.'_currently_employed.pdf', 'I');

  }else{
    header("Location: ../../brgy_admin/admin_home.php");
  }

  // Get Purok
  function getPurokTable() {
      $database = new Database(); // Create Database Connection
      $conn = $database -> get_Connection(); // Get Database Connection
      
      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID

      $sql = "SELECT
                purok.name AS 'Purok Name',
                (SELECT COUNT(*) FROM user_info WHERE user_info.cur_employed = 'Yes' AND purok_id = purok.id) AS 'Employed',
                (SELECT COUNT(*) FROM user_info WHERE purok_id = purok.id) AS 'Population'
              FROM user_info
              RIGHT OUTER JOIN purok
              ON purok.id = user_info.purok_id
              WHERE purok.brgy_id = $brgy_id
              GROUP BY purok.id
              ORDER BY purok.id, purok.name";

      $result = $conn->query($sql);
      
      $return = "<tbody>";
      if ($result->num_rows > 0) {

        // output data of each row
        while($row = $result->fetch_assoc()) {
          $return .= "<tr class=\"tr\"><td>" . $row['Purok Name'] . "</td><td>" . $row['Employed']. "</td><td>" . $row['Population'] . "</td></tr>";
        }
      }
      $return .= "</tbody>";
      mysqli_close($conn);

      return $return;
    }
    
?>
