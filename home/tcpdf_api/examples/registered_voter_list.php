<?php
  session_start(); 
  require_once('tcpdf_include.php'); // Add PDF api
  require('../../session/brgy_admin_brgy_image.php'); // Get Image Src
  require('../../database/brgy_admin_database_query.php'); // Database Query 

  // Image Location
  if(isset($_SESSION['brms_brgyImgName']) || !empty($_SESSION['brms_brgyImgName'])){
    $src = '../../brgy_image/'.$_SESSION['brms_brgyImgName'].'.'.$_SESSION['brms_brgyImgExtension'];      
  }else{
    $src = '../../brgy_image/logo.png';
  }

  // Insert Log
  function insert_log($purok_name){ 
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_log($_SESSION['brms_userId'],"Print Registered Voter of Purok " . $purok_name); // Insert Log
  }

  if($_SERVER["REQUEST_METHOD"] == "GET"){
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $purok_name = $_REQUEST["purok_name"]; // Purok Name
    $brgy_name = $_SESSION['brms_brgyName']; // Barangay Name

    $davao = '../../images/DavaoCity.png'; // Davao City Logo
    
    insert_log($purok_name); // Insert Log

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $obj_pdf->SetCreator(PDF_CREATOR);  
    $obj_pdf->SetTitle('Barangay '.$brgy_name.' Purok ' .$purok_name.' Registered Voter');  
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $obj_pdf->SetDefaultMonospacedFont('helvetica');  
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
    $obj_pdf->setPrintHeader(false);  
    $obj_pdf->SetAutoPageBreak(TRUE, 10);  
    $obj_pdf->SetFont('times', '', 12);  
    $obj_pdf->AddPage();

    date_default_timezone_set('America/Mexico_City'); 
    $curDate=date('F j, Y');

    $content ='<style type="text/css"> 
                  td{
                    font-size: 80%;
                  }                  
              </style>'; 

    $content .= '<table style="text-align:center;">
                    <tr>
                      <td class="icon"><img src="'.$src.'" width="500px"></td>
                    </tr>
                  </table>';

    $content .= '<p style="text-align:center; font-weight: bold;">Registered Voter - Purok '.$purok_name.'</p>';

    $content .= '<table cellpadding="10">
                  <tr>
                    <thead>
                      <hr>
                        <th><b>Name</b></th>
                        <th><b>Address</b></th>
                      <hr>
                    </thead>
                  </tr>
                  '.getPurokTable($brgy_id, $purok_name).'         
                </table>';

    $obj_pdf->writeHTML($content);  
    ob_end_clean(); // Clear
    $obj_pdf->Output('brgy_'.$brgy_name.'_purok_' .$purok_name.'_registered_voter.pdf', 'I');

  }

  // Get Purok
  function getPurokTable($brgy_id, $purok_name) {
      $database = new Database(); // Create Database Connection
      $conn = $database -> get_Connection(); // Get Database Connection

      $sql = "SELECT 
                CONCAT(user_info.last_name, ', ', user_info.first_name, ' ', user_info.middle_name) AS 'Name',
                IF(user_info.address IS NULL OR user_info.address = '', 'Unknown', user_info.address ) AS 'Address'
              FROM user_info
              INNER JOIN purok ON user_info.purok_id = purok.id
              WHERE user_info.brgy_id = $brgy_id
              AND user_info.is_voter = 'Yes'
              AND purok.name = '$purok_name'
              ORDER BY user_info.last_name";

      $result = $conn->query($sql);
      
      $return = "<tbody>";
      if ($result->num_rows > 0) {
        // output data of each row
        $loop = 1;
        while($row = $result->fetch_assoc()) {
          $return .= "<tr><td>" . $loop++ . ".) " . $row['Name'] . "</td><td>" . $row['Address'] . "</td></tr>";
        }
      }
      $return .= "</tbody>";
      mysqli_close($conn);

      return $return;
    }
    
?>