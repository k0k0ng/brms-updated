<?php
  session_start(); 
  require_once('tcpdf_include.php'); // Add PDF api
  require('../../session/brgy_admin_brgy_image.php'); // Get Image Src
  require('../../database/brgy_admin_database_query.php'); // Database Query 

  // Image Location
  if(isset($_SESSION['brms_brgyImgName']) || !empty($_SESSION['brms_brgyImgName'])){
    $src = '../../brgy_image/'.$_SESSION['brms_brgyImgName'].'.'.$_SESSION['brms_brgyImgExtension'];      
  }else{
    $src = '../../brgy_image/logo.png';
  }

  // Insert Log
  function insert_log($type){ 
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_log($_SESSION['brms_userId'],$type); // Insert Log
  }

  if($_SERVER["REQUEST_METHOD"] == "POST"){

    insert_log("Filtered Print"); // Insert Log

    $age = [];
    $message_age = "";
    $has_previous = false;
    $age_range_count = 0;
    $max_age = "";
    

    if ( isset($_POST['age1'])) {
      $age[$age_range_count] = " AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) >= 1
                                 AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) <= 2 ";
      $message_age .= " - Age 1 - 2";
      $max_age = "2"; 
      $has_previous = true; 
      $age_range_count++;
    }

    if ( isset($_POST['age2'])) {
      $age[$age_range_count] = " AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) >= 3
                                 AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) <= 5 ";
      $message_age = $has_previous ? " - Age 1 - 5" : " - Age 3 - 5";
      $max_age = "5";
      $has_previous = true;
      $age_range_count++;
    }

    if ( isset($_POST['age3'])) {
      $age[$age_range_count] = " AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) >= 6
                                 AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) <= 11 ";
      if($has_previous){
        if($max_age == "5"){
          $message_age = substr($message_age, 0, -1);
          $message_age .= "11";
        }else{
          $message_age .= ", 6 - 11";
        }
      }else{
        $message_age = " - Age 6 - 11";
      }
      $max_age = "11";
      $has_previous = true;
      $age_range_count++;
    }

    if ( isset($_POST['age4'])) {
      $age[$age_range_count] = " AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) >= 12
              AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) <= 15 ";
      if($has_previous){
        if($max_age == "11"){
          $message_age = substr($message_age, 0, -2);
          $message_age .= "15";
        }else{
          $message_age .= ", 12 - 15";
        }
      }else{
        $message_age = " - Age 12 - 15";
      }
      $max_age = "15";
      $has_previous = true;
      $age_range_count++;
    }

    if ( isset($_POST['age5'])) {
      $age[$age_range_count] = " AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) >= 16
              AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) <= 17 ";
      if($has_previous){
        if($max_age == "15"){
          $message_age = substr($message_age, 0, -2);
          $message_age .= "17";
        }else{
          $message_age .= ", 16 - 17";
        }
      }else{
        $message_age = " - Age 16 - 17";
      }
      $max_age = "17";
      $has_previous = true;
      $age_range_count++;
    }

    if ( isset($_POST['age6'])) {
      $age[$age_range_count] = " AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) >= 18
              AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) <= 35 ";
      if($has_previous){
        if($max_age == "17"){
          $message_age = substr($message_age, 0, -2);
          $message_age .= "35";
        }else{
          $message_age .= ", 18 - 35";
        }
      }else{
        $message_age = " - Age 18 - 35";
      }
      $max_age = "35";
      $has_previous = true;
      $age_range_count++;
    }

    if ( isset($_POST['age7'])) {
      $age[$age_range_count] = " AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) >= 36
              AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) <= 64 ";
      if($has_previous){
        if($max_age == "35"){
          $message_age = substr($message_age, 0, -2);
          $message_age .= "64";
        }else{
          $message_age .= ", 36 - 64";
        }
      }else{
        $message_age = " - Age 36 - 64";
      }
      $max_age = "64";
      $has_previous = true;
      $age_range_count++;
    }

    if ( isset($_POST['age8'])) {
      $age[$age_range_count] = " AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) >= 65 ";
      if($has_previous){
        if($max_age == "64"){
          $message_age = substr($message_age, 0, -4);
          $message_age .= " and above";
        }else{
          $message_age .= ", 65 and above";
        }
      }else{
        $message_age = " - Age 65 and above";
      }
      $max_age = "above";
      $has_previous = true;
      $age_range_count++;
    }

    if ( isset($_POST['ageOther']) && !empty($_POST['age9'])) {
      $ageValue = $_POST['age9'];
      $age[$age_range_count] = " AND (DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),user_info.birthdate)), '%Y')+0) = $ageValue ";
      if($has_previous){
        $message_age .= ", and ". $ageValue."year(s) old";
      }else{
        $message_age = " - Age ". $ageValue;
      }
      $max_age = "$ageValue";
      $has_previous = true;
      $age_range_count++;
    }
    if (count($age) > 0) {
      $message_age .= " <br>";
    }
    

    /************************** END of AGE **************************/

    $message_bloodtpye  = " - Blood Type (";
    $message_bloodtpye .= isset($_POST['bloodtype1']) ? 'A+ , ' : '';
    $message_bloodtpye .= isset($_POST['bloodtype2']) ? 'A- , ' : '';
    $message_bloodtpye .= isset($_POST['bloodtype3']) ? 'B+ , ' : '';
    $message_bloodtpye .= isset($_POST['bloodtype4']) ? 'B- , ' : '';
    $message_bloodtpye .= isset($_POST['bloodtype5']) ? 'AB+ , ': '';
    $message_bloodtpye .= isset($_POST['bloodtype6']) ? 'AB- , ': '';
    $message_bloodtpye .= isset($_POST['bloodtype7']) ? 'O+ , ' : '';
    $message_bloodtpye .= isset($_POST['bloodtype8']) ? 'O- , ' : '';
    $message_bloodtpye .= isset($_POST['bloodtype9']) ? 'Unknown , ' : '';
    $message_bloodtpye  = substr($message_bloodtpye, 0, -3);
    $message_bloodtpye .= ") <br>";

    $bloodtype1 = isset($_POST['bloodtype1']) ? 'A+' : 'X';
    $bloodtype2 = isset($_POST['bloodtype2']) ? 'A-' : 'X';
    $bloodtype3 = isset($_POST['bloodtype3']) ? 'B+' : 'X';
    $bloodtype4 = isset($_POST['bloodtype4']) ? 'B-' : 'X';
    $bloodtype5 = isset($_POST['bloodtype5']) ? 'AB+ ': 'X';
    $bloodtype6 = isset($_POST['bloodtype6']) ? 'AB- ': 'X';
    $bloodtype7 = isset($_POST['bloodtype7']) ? 'O+' : 'X';
    $bloodtype8 = isset($_POST['bloodtype8']) ? 'O-' : 'X';
    $bloodtype9 = isset($_POST['bloodtype9']) ? "'Unknown',''": 'X';
    $bloodtype = "AND blood_type IN 
                  (
                    '$bloodtype1',
                    '$bloodtype2',
                    '$bloodtype3',
                    '$bloodtype4',
                    '$bloodtype5',
                    '$bloodtype6',
                    '$bloodtype7',
                    '$bloodtype8',
                    '$bloodtype9'
                  )";
    
    if( 
        !isset($_POST['bloodtype1']) && 
        !isset($_POST['bloodtype2']) &&
        !isset($_POST['bloodtype3']) &&
        !isset($_POST['bloodtype4']) &&
        !isset($_POST['bloodtype5']) &&
        !isset($_POST['bloodtype6']) &&
        !isset($_POST['bloodtype7']) &&
        !isset($_POST['bloodtype8']) &&
        !isset($_POST['bloodtype9'])
      ){
        $bloodtype = "";
        $message_bloodtpye = "";
    }

    /************************** END of BLOOD TYPE **************************/

    $message_enrolled  = " - Curretly Enrolled (";
    $message_enrolled .= isset($_POST['enrolled1'])? 'Yes , ': '';
    $message_enrolled .= isset($_POST['enrolled2'])? 'No , ': '';
    $message_enrolled  = substr($message_enrolled, 0, -3);
    $message_enrolled .= ") <br>";
    
    $enrolled1 = isset($_POST['enrolled1'])? 'Yes': 'X';
    $enrolled2 = isset($_POST['enrolled2'])? 'No': 'X';
    $enrolled = "AND cur_enrolled IN ('$enrolled1','$enrolled2')";

    if( !isset($_POST['enrolled1']) && !isset($_POST['enrolled2']) ){
      $enrolled = "";
      $message_enrolled = "";
    }

    /************************** END of ENROLLED **************************/ 

    $message_employed  = " - Curretly Employed (";
    $message_employed .= isset($_POST['employed1'])? 'Yes , ': '';
    $message_employed .= isset($_POST['employed2'])? 'No , ': '';
    $message_employed  = substr($message_employed, 0, -3);
    $message_employed .= ") <br>";   

    $employed1 = isset($_POST['employed1'])? 'Yes': 'X';
    $employed2 = isset($_POST['employed2'])? 'No': 'X';
    $employed = "AND cur_employed IN ('$employed1','$employed2')";

    if( !isset($_POST['employed1']) && !isset($_POST['employed2']) ){
      $employed = "";
      $message_employed = "";
    }

    /************************** END of EMPLOYED **************************/

    $message_gender  = " - Gender (";
    $message_gender .= isset($_POST['gender1'])? 'Male , ': '';
    $message_gender .= isset($_POST['gender2'])? 'Female , ': '';
    $message_gender  = substr($message_gender, 0, -3);
    $message_gender .= ") <br>";   

    $gender1 = isset($_POST['gender1'])? 'Male': 'X';
    $gender2 = isset($_POST['gender2'])? 'Female': 'X';
    $gender = "AND gender IN ('$gender1','$gender2')";

    if( !isset($_POST['gender1']) && !isset($_POST['gender2']) ){
      $gender = "";
      $message_gender = "";
    }

    /************************** END of GENDER **************************/ 

    $message_pwd  = " - Person With Disability (";
    $message_pwd .= isset($_POST['pwd1'])? 'Blind , ': '';
    $message_pwd .= isset($_POST['pwd2'])? 'Deaf , ': '';
    $message_pwd .= isset($_POST['pwd3'])? 'Mute , ': '';
    $message_pwd .= isset($_POST['pwd4'])? 'Others , ': '';
    $message_pwd  = substr($message_pwd, 0, -3);
    $message_pwd .= ") <br>";

    $pwd1 = isset($_POST['pwd1'])? 'Blind': 'X';
    $pwd2 = isset($_POST['pwd2'])? 'Deaf': 'X';
    $pwd3 = isset($_POST['pwd3'])? 'Mute': 'X';
    $pwd4 = isset($_POST['pwd4'])? 'Others': 'X';
    $pwd = "AND pwd IN ('$pwd1','$pwd2','$pwd3','$pwd4')";

    if( !isset($_POST['pwd1']) && 
        !isset($_POST['pwd2']) &&
        !isset($_POST['pwd3']) &&
        !isset($_POST['pwd4'])
      ){
        $pwd = "";
        $message_pwd = "";
    }

    /************************** END of PWD **************************/

    $message_voter  = " - Registered Voter (";
    $message_voter .= isset($_POST['voter1'])? 'Yes , ': '';
    $message_voter .= isset($_POST['voter2'])? 'No , ': '';
    $message_voter  = substr($message_voter, 0, -3);
    $message_voter .= ") <br>";

    $voter1 = isset($_POST['voter1'])? 'Yes': 'X';
    $voter2 = isset($_POST['voter2'])? 'No': 'X';
    $voter = "AND is_voter IN ('$voter1','$voter2')";

    if( !isset($_POST['voter1']) && !isset($_POST['voter2']) ){
      $voter = "";
      $message_voter = "";
    }

    /************************** END of VOTER **************************/

    $message_residence  = " - Residence Type (";
    $message_residence .= isset($_POST['residence1'])? 'Owned , ': '';
    $message_residence .= isset($_POST['residence2'])? 'Leased , ': '';
    $message_residence .= isset($_POST['residence3'])? 'Rented , ': '';
    $message_residence .= isset($_POST['residence4'])? 'Boarder , ': '';
    $message_residence .= isset($_POST['residence5'])? 'Otherwise , ': '';
    $message_residence  = substr($message_residence, 0, -3);
    $message_residence .= ") <br>";

    $residence1 = isset($_POST['residence1'])? 'Owned': 'X';
    $residence2 = isset($_POST['residence2'])? 'Leased': 'X';
    $residence3 = isset($_POST['residence3'])? 'Rented': 'X';
    $residence4 = isset($_POST['residence4'])? 'Boarder': 'X';
    $residence5 = isset($_POST['residence5'])? 'Otherwise': 'X';
    $residence = "AND residence_type IN ('$residence1','$residence2','$residence3','$residence4','$residence5')";

    if( !isset($_POST['residence1']) && 
        !isset($_POST['residence2']) && 
        !isset($_POST['residence3']) && 
        !isset($_POST['residence4']) &&
        !isset($_POST['residence5']) 
      ){
        $residence = "";
        $message_residence = "";
    }

    /************************** END of RESIDENCE **************************/

    $message_senior  = " - Senior Citizen (";
    $message_senior .= isset($_POST['senior1'])? 'Yes , ': '';
    $message_senior .= isset($_POST['senior2'])? 'No , ': '';
    $message_senior  = substr($message_senior, 0, -3);
    $message_senior .= ") <br>";

    $senior1 = isset($_POST['senior1'])? 'Yes': 'X';
    $senior2 = isset($_POST['senior2'])? 'No': 'X';
    $senior = "AND senior_citizen IN ('$senior1','$senior2')";

    if( !isset($_POST['senior1']) && !isset($_POST['senior2']) ){
      $senior = "";
      $message_senior = "";
    }

    /************************** END of SENIOR **************************/

    $message_religion  = " - Religion (";
    $message_religion .= isset($_POST['religion1'])? 'Roman Catholic , ': '';
    $message_religion .= isset($_POST['religion2'])? 'Islam , ': '';
    $message_religion .= isset($_POST['religion3'])? 'Protestant , ': '';
    $message_religion .= isset($_POST['religion4'])? 'Iglesia ni Cristo , ': '';
    $message_religion .= isset($_POST['religion5'])? 'Jesus Miracle Crusade International Ministry , ': '';
    $message_religion .= isset($_POST['religion6'])? 'Members Church of God International , ': '';
    $message_religion .= isset($_POST['religion7'])? 'Most Holy Church of God in Christ Jesus , ': '';
    $message_religion .= isset($_POST['religion8'])? 'Philippine Independent Church , ': '';
    $message_religion .= isset($_POST['religion9'])? 'Apostolic Catholic Church , ': '';
    $message_religion .= isset($_POST['religion10'])? 'Orthodoxy , ': '';
    $message_religion .= isset($_POST['religion11'])? 'The Kingdom of Jesus Christ the Name Above Every Name , ': '';
    $message_religion .= isset($_POST['religion12'])? 'Judaism , ': '';
    $message_religion .= isset($_POST['religion13'])? 'Hinduism , ': '';
    $message_religion .= isset($_POST['religion14'])? 'Atheism , ': '';
    $message_religion .= isset($_POST['religion15'])? 'Others , ': '';
    $message_religion  = substr($message_religion, 0, -3);
    $message_religion .= ") <br>";

    $religion1 = isset($_POST['religion1'])? 'Roman Catholic': 'X';
    $religion2 = isset($_POST['religion2'])? 'Islam': 'X';
    $religion3 = isset($_POST['religion3'])? 'Protestant': 'X';
    $religion4 = isset($_POST['religion4'])? 'Iglesia ni Cristo': 'X';
    $religion5 = isset($_POST['religion5'])? 'Jesus Miracle Crusade International Ministry': 'X';
    $religion6 = isset($_POST['religion6'])? 'Members Church of God International': 'X';
    $religion7 = isset($_POST['religion7'])? 'Most Holy Church of God in Christ Jesus': 'X';
    $religion8 = isset($_POST['religion8'])? 'Philippine Independent Church': 'X';
    $religion9 = isset($_POST['religion9'])? 'Apostolic Catholic Church': 'X';
    $religion10 = isset($_POST['religion10'])? 'Orthodoxy': 'X';
    $religion11 = isset($_POST['religion11'])? 'The Kingdom of Jesus Christ the Name Above Every Name': 'X';
    $religion12 = isset($_POST['religion12'])? 'Judaism': 'X';
    $religion13 = isset($_POST['religion13'])? 'Hinduism': 'X';
    $religion14 = isset($_POST['religion14'])? 'Atheism': 'X';
    $religion15 = isset($_POST['religion15'])? 'Others': 'X';

    $religion = "AND religion IN (
                  '$religion1',
                  '$religion2',
                  '$religion3',
                  '$religion4',
                  '$religion5',
                  '$religion6',
                  '$religion7',
                  '$religion8',
                  '$religion9',
                  '$religion10',
                  '$religion11',
                  '$religion12',
                  '$religion13',
                  '$religion14',
                  '$religion15'
                )";

    if( !isset($_POST['religion1']) && 
        !isset($_POST['religion2']) &&
        !isset($_POST['religion3']) && 
        !isset($_POST['religion4']) &&
        !isset($_POST['religion5']) && 
        !isset($_POST['religion6']) &&
        !isset($_POST['religion7']) && 
        !isset($_POST['religion8']) &&
        !isset($_POST['religion9']) && 
        !isset($_POST['religion10']) &&
        !isset($_POST['religion11']) && 
        !isset($_POST['religion12']) &&
        !isset($_POST['religion13']) && 
        !isset($_POST['religion14']) && 
        !isset($_POST['religion15'])
      ){
      $religion = "";
      $message_religion = "";
    }

    /************************** END of RELIGION **************************/

    $message  = "<b>Filtered by: </b><br>";
    $message .= $message_age;
    $message .= $message_bloodtpye;
    $message .= $message_enrolled;
    $message .= $message_employed;
    $message .= $message_gender;
    $message .= $message_pwd;
    $message .= $message_voter;
    $message .= $message_residence;
    $message .= $message_senior;
    $message .= $message_religion;
    $message .= ($message === "<b>Filtered by: </b><br>") ? " - None" : ""; 

    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $brgy_name = $_SESSION['brms_brgyName']; // Barangay Name

    $davao = '../../images/DavaoCity.png'; // Davao City Logo

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $obj_pdf->SetCreator(PDF_CREATOR);  
    $obj_pdf->SetTitle('Barangay '.$brgy_name);  
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $obj_pdf->SetDefaultMonospacedFont('helvetica');  
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
    $obj_pdf->setPrintHeader(false);  
    $obj_pdf->SetAutoPageBreak(TRUE, 10);  
    $obj_pdf->SetFont('times', '', 12);  
    $obj_pdf->AddPage();

    date_default_timezone_set('America/Mexico_City'); 
    $curDate=date('F j, Y');

    $content ='<style type="text/css"> 
                  td{
                    font-size: 80%;
                  }           
              </style>'; 

    $content .= '<table style="text-align:center;">
                    <tr>
                      <td class="icon"><img src="'.$src.'" width="500px"></td>
                    </tr>
                  </table>';

    $content .= '<p>'.$message.'</p>';

    $content .= '<table cellpadding="10">
                  <tr>
                    <thead>
                      <hr>
                        <th><b>Name</b></th>
                        <th><b>Address</b></th>
                        <th><b>Purok</b></th>
                      <hr>
                    </thead>
                  </tr>
                  '.getPurokTable (
                                    $brgy_id,
                                    $age,
                                    $bloodtype,
                                    $enrolled,
                                    $employed,
                                    $pwd,
                                    $gender,
                                    $voter,
                                    $residence,
                                    $senior,
                                    $religion
                                  ).'         
                </table>';

    $obj_pdf->writeHTML($content);  
    ob_end_clean(); // Clear
    $obj_pdf->Output('brgy_'.$brgy_name.'.pdf', 'I');

  }

  // Get Purok
  function getPurokTable( 
                          $brgy_id,
                          $age,
                          $bloodtype,
                          $enrolled,
                          $employed,
                          $pwd,
                          $gender,
                          $voter,
                          $residence,
                          $senior,
                          $religion
                        ) {
      $return = "";
      $return = "<tbody>";
      $added_row_count = 1;

      if (count($age) == 0) {
        $database = new Database(); // Create Database Connection
        $conn = $database -> get_Connection(); // Get Database Connection

        $sql = "SELECT 
                  CONCAT(user_info.last_name, ', ', user_info.first_name, ' ', user_info.middle_name) AS 'Name',
                  IF(user_info.address IS NULL OR user_info.address = '', 'Unknown', user_info.address ) AS 'Address',
                  IF(purok.name IS NULL OR purok.name = '', 'Unknown', purok.name ) AS 'Purok'
                FROM user_info
                INNER JOIN purok ON user_info.purok_id = purok.id
                WHERE user_info.brgy_id = $brgy_id
                
                $bloodtype
                $enrolled
                $employed
                $pwd
                $gender
                $voter
                $residence
                $senior
                $religion
                
                ORDER BY user_info.last_name, purok.name";

        $result = $conn->query($sql);

        $return = "<tbody>";

        $return .= mysqli_error($conn);

        if ($result->num_rows > 0) {
          // output data of each row
          $loop = 1;
          while($row = $result->fetch_assoc()) {
            $return .= "<tr><td>" . $loop++ . ".) " 
                    . $row['Name'] . "</td><td>" 
                    . $row['Address'] . "</td><td>" 
                    . $row['Purok'] . "</td></tr>";
          }
        }
      }else{
        for($index = 0; $index < count($age); $index++){
          $database = new Database(); // Create Database Connection
          $conn = $database -> get_Connection(); // Get Database Connection

          $sql = "SELECT 
                    CONCAT(user_info.last_name, ', ', user_info.first_name, ' ', user_info.middle_name) AS 'Name',
                    IF(user_info.address IS NULL OR user_info.address = '', 'Unknown', user_info.address ) AS 'Address',
                    IF(purok.name IS NULL OR purok.name = '', 'Unknown', purok.name ) AS 'Purok'
                  FROM user_info
                  INNER JOIN purok ON user_info.purok_id = purok.id
                  WHERE user_info.brgy_id = $brgy_id
                  
                  ".$age[$index]."
                  $bloodtype
                  $enrolled
                  $employed
                  $pwd
                  $gender
                  $voter
                  $residence
                  $senior
                  $religion
        
                  ORDER BY user_info.last_name, purok.name";

          $result = $conn->query($sql);
          $return .= mysqli_error($conn);

          if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
              $return .= "<tr><td>" . $added_row_count++ . ".) " 
                      . $row['Name'] . "</td><td>" 
                      . $row['Address'] . "</td><td>" 
                      . $row['Purok'] . "</td></tr>";
            }
          }
        }
      }

      $return .= "</tbody>";
      mysqli_close($conn);
      //exit();
      return $return;
    }
    
?>