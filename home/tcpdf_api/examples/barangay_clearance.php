<?php
  session_start(); 
  require_once('tcpdf_include.php'); // Add PDF api
  require('../../database/brgy_admin_database_query.php'); // Database Query

  // Insert Transaction
  function insert_transaction($type,$receipt_no){
    $info_id = $_SESSION['brms_SESSION_userId']; // Info ID
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $admin_id = $_SESSION['brms_userId']; // Admin ID

    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_transaction($brgy_id,$info_id,$admin_id,$type,$receipt_no);
  }

  // Insert Log
  function insert_log($type){ 
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_log($_SESSION['brms_userId'],$type); // Insert Log
  }

  // Get Transcation ID
  function get_transaction_id(){

    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 

    $sql = "SELECT transaction.* 
            FROM transaction 
            INNER JOIN user_info ON user_info.id = transaction.info_id
            WHERE user_info.brgy_id = $brgy_id ORDER BY transaction.id DESC";
    $result = $conn->query($sql);
        
    if($result->num_rows > 0){
      // output data of each row
      while($row = $result->fetch_assoc()) {
        return $row['id'];
      }
    } return 0;

  }

  if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Image Location
    if(isset($_SESSION['brms_brgyImgName']) || !empty($_SESSION['brms_brgyImgName'])){
      $src = '../../brgy_image/'.$_SESSION['brms_brgyImgName'].'.'.$_SESSION['brms_brgyImgExtension'];      
    }else{
      $src = '../../brgy_image/logo.png';
    }
   
    // Check if Barangay Official is Set
    if(!isset($_POST['official_list'])){
      echo "<script type='text/javascript'>
                alert('Please setup Barangay Official!');
                location = '../../brgy_admin/brgy_official.php';
            </script>";
      return;       
    }   

    $davao = '../../images/DavaoCity.png'; // Davao City Logo
    $thumb_mark = '../../images/thumb_mark.png'; // Davao City Logo
    
    $brgy_name = $_SESSION['brms_brgyName'];
    $brgy_address = $_SESSION['brms_brgyAddress'];

    $purok_name = $_SESSION['brms_SESSION_userPurokName'];

    $first_name = $_SESSION['brms_SESSION_userFname'];
    $last_name = $_SESSION['brms_SESSION_userLname'];
    $middle_name = $_SESSION['brms_SESSION_userMname'];

    $gender_he_she = $_SESSION['brms_SESSION_userGender'] ==='Male' ? 'he' : 'she';
    $gender_his_her = $_SESSION['brms_SESSION_userGender']==='Male' ? 'his' : 'her';
    
    date_default_timezone_set('America/Mexico_City');
    $cur_date = date('jS \d\a\y \o\f F Y');

    $official = $_POST['official_list'];

    $purpose = $_POST['purpose'];

    $ctcno = $_POST['ctcno'];
    $location = $_SESSION['brms_brgyAddress'];

    $date = date_create($_POST['issuedon']);
    $issuedon = date_format($date,"jS \d\a\y \of F Y");

    $header = 'Barangay Clearance';
    $receipt = $_POST['receipt'];

    insert_transaction($header,$receipt); // Insert Transaction
    insert_log($header); // Insert Log

    if($official === $_SESSION['brms_cap_name']){
      $position = "Punong Barangay";
    }
    else if($official === $_SESSION['brms_wad1_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad2_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad3_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad4_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad5_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad6_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad7_name']){
      $position = "Kagawad";
    }

    $official = strtoupper($official); // UPPERCASE

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $obj_pdf->SetCreator(PDF_CREATOR);  
    $obj_pdf->SetTitle("Barangay ".$brgy_name." Certificate of Residency");  
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $obj_pdf->SetDefaultMonospacedFont('helvetica');  
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
    $obj_pdf->setPrintHeader(false);  
    $obj_pdf->setPrintFooter(false);  
    $obj_pdf->SetAutoPageBreak(TRUE, 10);  
    $obj_pdf->SetFont('times', '', 10);  
    $obj_pdf->AddPage();  

    $content ='<style type="text/css"> 
                    body {background-color: powderblue;} 
                    h1 {color: blue;}
                    .icon {
                      line-height: 150px;
                    }
                    .leftcontainer{
                      width: 50%;
                      float: left;
                    }
                    .rightcontainer{
                      width: 50%;
                      float: left;
                    }
                    .parag{
                      text-indent: 30px;
                      text-align: justify;
                    }
                </style>'; 

    $content .= '<table style="text-align:center">
                    <tr>
                      <td class="icon"><img src="'.$src.'" width="500px"></td>
                    </tr>
                  </table>';

    $content .= '<h2 style="text-transform: uppercase; text-align: center;">'.strtoupper($header).'<br></h2>';

    $content .= '<p><b>TO WHOM IT MAY CONCERN:</b></p>';

    $content .= '<p class="parag">This is to certify that <b>'.strtoupper($first_name).' '.strtoupper($middle_name[0]).'. '.strtoupper($last_name).'</b>, legal age single/married/widower/separated, a resident of <b>Purok '.$purok_name.', Barangay '.$brgy_name.', Davao City</b>. Further, '.$gender_he_she.' neither has accountability with this office, nor pending case either civil or criminal as per record available on file in our office.</p>';

    $content .= '<p class="parag">This certification is being issued upon the request of the above-name person as supporting document for <b>'.$purpose.'</b>.</p>';

    $content .= '<p class="parag">Issued this <b>'.$cur_date.'</b> after an affiant has exhibited '.$gender_his_her.' Community Tax Certificate No. <b>'.$ctcno.'</b>, issued on <b>'.$issuedon.'</b> at <b>CTO - Davao City</b>.</p>';

    $content .= '<p></p><p></p>';
    $content .= '<table>
                    <tr>
                      <td><p style="text-align:center;">
                        <b><u>'.strtoupper($first_name).' '.strtoupper($middle_name[0]).'. '.strtoupper($last_name).'</u></b><br>
                        (Signature of Affiant)
                      </p></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr><br></tr>
                    <tr>
                      <td style="text-align:center;">
                        <img src="'.$thumb_mark.'" width="100px">
                      </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                </table>';

    if($official === strtoupper($_SESSION['brms_cap_name'])){
      $content .= '<table>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><p style="text-align:center">
                        <br><br><br>
                        <b><u>'.$official.'</u></b><br>
                        '.$position.'
                      </p></td>
                    </tr>
                    <tr></tr>
                </table>';

      $content .= '<br><br><br>';
      $content .= '<table>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><p style="text-align:center">
                            <font color="red">"Not valid without official seal"</font>                          
                            </p>
                        </td>
                      </tr>
                  </table>';
    }else{
      $content .= '<table>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>
                        <p>
                          For and in behalf of:<br><br>
                          <b><u>'.strtoupper($_SESSION['brms_cap_name']).'</u></b><br>
                          Punong Barangay
                        </p>
                        <p>
                          By:<br><br>
                          <b><u>'.$official.'</u></b><br>
                          '.$position.'
                        </p>
                      </td>
                    </tr>
                </table>';
      
      $content .= '<br><br>';
      $content .= '<table>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td> 
                        <td><p>
                            <font color="red">"Not valid without official seal"</font>                          
                            </p>
                        </td>
                      </tr>
                  </table>';
    } 

    $obj_pdf->writeHTML($content); 
    ob_end_clean(); // Clear
    //$obj_pdf->Output('brgy_'.$brgy_name.'_certificate_of_residency.pdf', 'I'); 
    $obj_pdf->Output(__DIR__.'/../../pdf_transaction/'.get_transaction_id().'.pdf', 'FI');

  }else{
    header("Location: ../../brgy_admin/form_residency.php");
  }
  
?>