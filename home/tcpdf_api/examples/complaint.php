<?php
  session_start(); 
  require_once('tcpdf_include.php'); // Add PDF api
  require('../../database/brgy_admin_database_query.php'); // Database Query

  // Insert Log
  function insert_log(){ 
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_log($_SESSION['brms_userId'],"Compaint"); // Insert Log
  }

  // Insert Transaction for Business
  function insert_complaint_note($complainee_id,$complainants,$action_taken){ 

    $info_id = $_SESSION['brms_SESSION_userId']; // Info ID
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $admin_id = $_SESSION['brms_userId']; // Admin ID

    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection
    $transaction_id = get_transaction_id(); // Get Transaction ID

    // Loop All Complainee
    foreach($complainee_id as $complain) {
      $compaint_id = get_complainee_id($complain); // Get Compainee ID

      $sql = "INSERT INTO complaint (admin_id,complainee_id, complainants, action_taken,date_file) 
              VALUES ($admin_id, $compaint_id, '$complainants',' $action_taken',NOW())";
      mysqli_query($conn, $sql);
    }

  }

  // Get Transcation ID
  function get_transaction_id(){

    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 

    $sql = "SELECT * FROM transaction WHERE brgy_id = $brgy_id ORDER BY id DESC";
    $result = $conn->query($sql);
        
    if($result->num_rows > 0){
      // output data of each row
      while($row = $result->fetch_assoc()) {
        return $row['id'];
      }
    } return 0;

  }

  // Get Complainee ID
  function get_complainee_id($name){
    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 

    $sql = "SELECT * FROM user_info 
            WHERE brgy_id = $brgy_id
            AND CONCAT(first_name, ' ', middle_name, ' ', last_name) = '$name'";
    $result = $conn->query($sql);

    if($result->num_rows > 0){
      // output data of each row
      while($row = $result->fetch_assoc()) {
        return $row['id'];
      }
    } return 0;

  }

  // Get Transcation ID
  function get_complaint_id(){

    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 

    $sql = "SELECT * FROM complaint ORDER BY id DESC";
    $result = $conn->query($sql);
        
    if($result->num_rows > 0){
      // output data of each row
      while($row = $result->fetch_assoc()) {
        return $row['id'];
      }
    } return 0;

  }

  if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Image Location
    if(isset($_SESSION['brms_brgyImgName']) || !empty($_SESSION['brms_brgyImgName'])){
      $src = '../../brgy_image/'.$_SESSION['brms_brgyImgName'].'.'.$_SESSION['brms_brgyImgExtension'];      
    }else{
      $src = '../../brgy_image/logo.png';
    }

    $davao = '../../images/DavaoCity.png'; // Davao City Logo
    
    $brgy_name = $_SESSION['brms_brgyName']; // Barangay Name

    $chairman = $_SESSION['brms_cap_name']; // Barangay Chairman
    $secretary = $_SESSION['brms_sec_name']; // Barangay Secretary

    // Check if Barangay Official is Set
    if($chairman === ''){
      echo "<script type='text/javascript'>
                alert('Please setup Punong Barangay!');
                location = '../../brgy_admin/brgy_official.php';
            </script>";
      return;       
    }

    // Check if Barangay Official is Set
    if($secretary === ''){
      echo "<script type='text/javascript'>
                alert('Please setup Pangkat Secretary!');
                location = '../../brgy_admin/brgy_official.php';
            </script>";
      return;       
    }

    $case_no = $_POST['brgy_case_no'];
    $complainant_list = $_POST['complainant_list'];
    $complainee_list = $_POST['complainee_list'];

    $complainee_split = explode("<br>", $complainee_list);
    $complainant_split = substr(str_replace("<br>",", ",$complainant_list),0,-2);

    $for = $_POST['for'];
    $action_taken = $_POST['action_taken'];

    insert_log(); // Insert Log
    
    insert_complaint_note($complainee_split,$complainant_split,$action_taken); // Insert Compaint Note      

    date_default_timezone_set('America/Mexico_City');

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $obj_pdf->SetCreator(PDF_CREATOR);  
    $obj_pdf->SetTitle("Barangay ".$brgy_name." Certificate of Complaint");  
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $obj_pdf->SetDefaultMonospacedFont('helvetica');  
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
    $obj_pdf->setPrintHeader(false);  
    $obj_pdf->setPrintFooter(false);
    $obj_pdf->SetAutoPageBreak(TRUE, 10);  
    $obj_pdf->SetFont('times', '', 10);  
    $obj_pdf->AddPage();  

    $content ='<style type="text/css"> 
                    .icon {
                      line-height: 150px;
                    }                    
                    .leftcontainer{
                      width: 50%;
                      float: left;
                    }
                    .rightcontainer{
                      width: 50%;
                      float: left;
                    }
                    .parag{
                      text-indent: 30px;
                      text-align: justify;
                    }
                </style>';

    $content .= '<table style="text-align:center">
                    <tr>
                      <td class="icon"><img src="'.$src.'" width="500px"></td>
                    </tr>
                  </table>';

    $content .= '<h3 style="text-transform: uppercase; text-align: center;">Office of The Lupong Tagapamayapa<br></h3>';

    $content .=   '<p style="text-align: right;">
                    Barangay Case No: <u>'.$case_no.'</u>
                  <br>
                    For: <u>'.$for.'</u>
                  </p>';
	
	$content .= '<table style="padding-top: -25px;">
                  <tr>                      
                      <td>
          						  <p style="text-align:center">
          							<b>'.strtoupper($complainant_list).'</b>
          							________________________<br>
          							Complainant/s<br>
          							<br>— against —<br><br>
          							<b>'.strtoupper($complainee_list).'</b>
          							________________________<br>
          							Respondent/s
          						  </p>
          					  </td>
          					  <td>&nbsp;</td>
                  </tr>
              </table>'; 

    $content .= '<br><h3 style="text-transform: uppercase; text-align: center;">Certification to File Action</h3>';

    $content .= '<p>This is to certify that:</p>';

    $content .= '<p class="parag">1. There has been a personal confrontation between the parties before the Punong Barangay but mediation failed;</p>';

    $content .= '<p class="parag">2. The Pangkat ng Tagapagkasundo was constituted by the personal confrontation before the Pangkat likewise did not result into a settlement;</p>';

    $content .= '<p class="parag">3. Therefore, the corresponding complaint for the dispute may now be filed in court/government office;</p>';

    $content .= '<p class="parag">This '.date("jS \d\a\y \of F\, Y").'.</p>';
				  
	$content .= '<table>
                    <tr>
					  <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><p style="text-align:center">
                        <b><u>'.strtoupper($secretary).'</u></b><br>
                        Pangkat Secretary
                      </p></td>

                    </tr>
                </table>'; 

	$content .= '<table>
                    <tr>                      
                      <td><p style="text-align:center">
                        <br>
                        <b><u>'.strtoupper($chairman).'</u></b><br>
                        Pangkat Chairman
                      </p></td>
					  <td>&nbsp;</td>
                    </tr>
                </table>'; 


    $obj_pdf->writeHTML($content); 
    ob_end_clean(); // Clear
    //$obj_pdf->Output('brgy_'.$brgy_name.'_certificate_of_complaint.pdf', 'I'); 
    $obj_pdf->Output(__DIR__.'/../../pdf_complaint/'.get_complaint_id().'.pdf', 'FI');

  }else{
    header("Location: ../../brgy_admin/form_complaint.php");
  }
  
?>