<?php
  session_start(); 
  require_once('tcpdf_include.php'); // Add PDF api
  require('../../database/brgy_admin_database_query.php'); // Database Query  
  require('../../session/brgy_admin_brgy_image.php'); // Get Image Src

  // BUsiness INFO
  $business_name = '';
  $business_address = '';
  $business_purok = '';

  // Insert Transaction
  function insert_transaction($type,$receipt){       
    $info_id = $_SESSION['brms_business_owner_id']; // Info ID
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $admin_id = $_SESSION['brms_userId']; // Admin ID
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_transaction($brgy_id,$info_id,$admin_id,$type,$receipt);
  }

  // Get Business Name, Purok, Address
  function get_business_info($business_id){

    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 

    $sql = "SELECT 
              business.name AS 'Name',
              business.address AS 'Address',
              purok.name AS 'Purok'
            FROM business
            INNER JOIN purok ON business.purok_id = purok.id
            WHERE business.id = $business_id
            AND purok.brgy_id = $brgy_id";

    $result = $conn->query($sql);
    
      if($result->num_rows > 0){
        // output data of each row
        while($row = $result->fetch_assoc()) {
          $GLOBALS['business_name'] = $row['Name'];
          $GLOBALS['business_address'] = $row['Address'];
          $GLOBALS['business_purok'] = $row['Purok'];
        }
      }
  }

  // Get Transcation ID
  function get_transaction_id(){

    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 

    $sql = "SELECT transaction.* 
            FROM transaction 
            INNER JOIN user_info ON user_info.id = transaction.info_id
            WHERE user_info.brgy_id = $brgy_id ORDER BY transaction.id DESC";
    $result = $conn->query($sql);
        
    if($result->num_rows > 0){
      // output data of each row
      while($row = $result->fetch_assoc()) {
        return $row['id'];
      }
    } return 0;

  }

  // Insert Transaction for Business
  function insert_transaction_business($business_id){ 

    $query = new database_query(); // Database Query (initialize connection)
    $conn = $query->get_Connection(); // Database Connection
    $transaction_id = get_transaction_id(); // Get Transaction ID

    $sql = "INSERT INTO transaction_business (business_id, transaction_id) 
            VALUES ($business_id,$transaction_id)";

    mysqli_query($conn, $sql);

  }

  // Insert Log
  function insert_log(){ 
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_log($_SESSION['brms_userId'],"Business Clearance"); // Insert Log
  }

  // Server Request POST
  if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Image Location
    if(isset($_SESSION['brms_brgyImgName']) || !empty($_SESSION['brms_brgyImgName'])){
      $src = '../../brgy_image/'.$_SESSION['brms_brgyImgName'].'.'.$_SESSION['brms_brgyImgExtension'];      
    }else{
      $src = '../../brgy_image/logo.png';
    }    
    
    // Check if Barangay Official is Set
    if(!isset($_POST['official_list'])){
      echo "<script type='text/javascript'>
                alert('Please setup Barangay Official!');
                location = '../../brgy_admin/brgy_official.php';
            </script>";
      return;       
    }    
    
    $davao = '../../images/DavaoCity.png'; // Davao City Logo

    $header = 'Barangay Business Clearance';

    $brgy_name = $_SESSION['brms_brgyName'];
    $brgy_address = $_SESSION['brms_brgyAddress'];
    $purpose = $_POST["purpose"];

    $ctcno = $_POST["ctcno"];
    $location = $_POST["location"];

    $receipt = $_POST["receipt"];

    date_default_timezone_set('America/Mexico_City'); 
    $cur_date = date('l, F j, Y');
    $date_issued = date("F d, Y", strtotime($_POST["issuedon"])); 
    
    insert_log(); // Insert Log
    insert_transaction($header,$receipt); // Add Transaction
    get_business_info($_POST["business_id"]); // Get Bussiness Info
    insert_transaction_business($_POST["business_id"]); // Insert Business Transcation

    $official = $_POST['official_list'];     

    if($official === $_SESSION['brms_cap_name']){
      $position = "Punong Barangay";
    }
    else if($official === $_SESSION['brms_wad1_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad2_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad3_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad4_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad5_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad6_name']){
      $position = "Kagawad";
    }
    else if($official === $_SESSION['brms_wad7_name']){
      $position = "Kagawad";
    }

    $official = strtoupper($official); // UPPERCASE

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $obj_pdf->SetCreator(PDF_CREATOR);  
    $obj_pdf->SetTitle("Barangay ".$brgy_name." Business Clearance");  
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $obj_pdf->SetDefaultMonospacedFont('helvetica');  
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
    $obj_pdf->setPrintHeader(false);  
    $obj_pdf->setPrintFooter(false);  
    $obj_pdf->SetAutoPageBreak(TRUE, 10);  
    $obj_pdf->SetFont('times', '', 10);  
    $obj_pdf->AddPage();  

    $content ='<style type="text/css"> 
                  body {background-color: powderblue;} 
                  h1 {color: blue;}
                  .icon {
                    line-height: 150px;
                  }
                  .leftcontainer{
                    width: 50%;
                    float: left;
                  }
                  .rightcontainer{
                    width: 50%;
                    float: left;
                  }
                  .parag{
                    text-indent: 30px;
                    text-align: justify;
                  }
              </style>'; 

    $content .= '<table style="text-align:center">
                    <tr>
                      <td class="icon"><img src="'.$src.'" width="500px"></td>
                    </tr>
                  </table>';

    $content .= '<h2 style="text-transform: uppercase; text-align: center;">'.$header.'<br></h2>';

    $content .= '<p style="text-transform: uppercase;"><b>To Whom It May Concern:</b></p>';

    $content .= '<p class="parag">This is to certify that <b>'.$GLOBALS['business_name'].'</b> located at <b>'.$GLOBALS['business_address'].'</b>, <b>Purok '.$GLOBALS['business_purok'].'</b>, Barangay <b>'.$brgy_name.'</b>, Davao City, is a bonafide establishment in this barangay.
      <br><br>This certifies further that the above named-establishment has not engaged in any crime involving moral turpitude.
      <br><br>This certification/clearance is hereby issued in connection with the subject\'s application for <b>'.$purpose.'</b> and for whatever legal purpose it may serve it best, and is valid for six (6) months from the date issued.</p>';

    $content .= '<p class="parag">Issued on '.$cur_date.', at the office of the Punong Barangay, '.$brgy_address.'.</p>';
      
    if($official === strtoupper($_SESSION['brms_cap_name'])){
      $content .= '<table>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><p style="text-align:center">
                        <br><br><br>
                        <b><u>'.$official.'</u></b><br>
                        '.$position.'
                      </p></td>
                    </tr>
                    <tr></tr>
                </table>';

      $content .= '<br><br><br>';
      $content .= '<table>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><p style="text-align:center">
                            <font color="red">"Not valid without official seal"</font>                          
                            </p>
                        </td>
                      </tr>
                  </table>';
    }else{
      $content .= '<table>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>
                        <p>
                          For and in behalf of:<br><br>
                          <b><u>'.strtoupper($_SESSION['brms_cap_name']).'</u></b><br>
                          Punong Barangay
                        </p>
                        <p>
                          By:<br><br>
                          <b><u>'.$official.'</u></b><br>
                          '.$position.'
                        </p>
                      </td>
                    </tr>
                </table>';
      
      $content .= '<br><br>';
      $content .= '<table>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td> 
                        <td><p>
                            <font color="red">"Not valid without official seal"</font>                          
                            </p>
                        </td>
                      </tr>
                  </table>';
    }

    $content .= '<br><br>';

    $content .= '<p>
                    CTC No. <b>'.$ctcno.'</b><br>
                    Issued at <b>'.$location.'</b><br>
                    Issued on <b>'.$date_issued.'</b>
                </p>';

    $obj_pdf->writeHTML($content);  
    ob_end_clean(); // Clear
    //$obj_pdf->Output('brgy_'.$brgy_name.'_business_clearance.pdf', 'I');
    $obj_pdf->Output(__DIR__.'/../../pdf_transaction/'.get_transaction_id().'.pdf', 'FI');

  }else{
    header("Location: ../../brgy_admin/form_businessclearance.php");
  }

?>