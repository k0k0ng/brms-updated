<?php
  session_start(); 
  require_once('tcpdf_include.php'); // Add PDF api
  require('../../session/brgy_admin_brgy_image.php'); // Get Image Src
  require('../../database/brgy_admin_database_query.php'); // Database Query  

  // Insert Log
  function insert_log(){ 
    $query = new database_query(); // Database Query (initialize connection)
    $query -> insert_log($_SESSION['brms_userId'],"Print Purok"); // Insert Log
  }

  if(isset($_SESSION['brms_VIEW_purok_name']) || !empty($_SESSION['brms_VIEW_purok_name'])){

    $davao = '../../images/DavaoCity.png'; // Davao City Logo
    
    insert_log(); // Insert Log

    // Image Location
    if(isset($_SESSION['brms_brgyImgName']) || !empty($_SESSION['brms_brgyImgName'])){
      $src = '../../brgy_image/'.$_SESSION['brms_brgyImgName'].'.'.$_SESSION['brms_brgyImgExtension'];      
    }else{
      $src = '../../brgy_image/logo.png';
    }

    $brgy_name = $_SESSION['brms_brgyName']; // Barangay Name    

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $obj_pdf->SetCreator(PDF_CREATOR);  
    $obj_pdf->SetTitle('Barangay '.$brgy_name.' Purok '.$_SESSION['purok_name']);  
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $obj_pdf->SetDefaultMonospacedFont('helvetica');  
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
    $obj_pdf->setPrintHeader(false);  
    $obj_pdf->SetAutoPageBreak(TRUE, 10);  
    $obj_pdf->SetFont('times', '', 12);  
    $obj_pdf->AddPage();  

    date_default_timezone_set('America/Mexico_City'); 
    $curDate=date('F j, Y');

    $content ='<style type="text/css"> 
                  td{
                    font-size: 80%;
                  }      
                  tr{
                    text-align: center;
                  }              
              </style>'; 

    $content .= '<table style="text-align:center;">
                    <tr>
                      <td class="icon"><img src="'.$src.'" width="500px"></td>
                    </tr>
                  </table>';

    $content .= '<p>Purok: '.$_SESSION['purok_name'];
    $content .= '<br>';
    $content .= 'Purok Leader: '.$_SESSION['purok_leader'];
    $content .= '<br>';
    $content .= 'Population: '.$_SESSION['purok_population'] . '</p>';

    $content .= '<table cellpadding="10">
                  <tr>
                    <thead>
                      <hr>
                      <th><b>Name</b></th>                  
                      <th><b>Address</b></th>
                      <hr>
                    </thead>
                  </tr>
                  '.getPurokTable().'            
                </table>';    

    $obj_pdf->writeHTML($content);  
    ob_end_clean(); // Clear
    $obj_pdf->Output('brgy_'.$brgy_name.'_purok_'.$_SESSION['purok_name'].'.pdf', 'I');

  }else{
    header("Location: ../../brgy_admin/admin_home.php");
  }

  // Get Purok
  function getPurokTable() {
      $database = new Database(); // Create Database Connection
      $conn = $database -> get_Connection(); // Get Database Connection
      
      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
      $purok_id = $_SESSION['brms_VIEW_purok_id']; // Purok ID   

      $sql = "SELECT * FROM user_info WHERE purok_id = '$purok_id' AND brgy_id = $brgy_id ORDER BY last_name";
      $result = $conn->query($sql);
      
      $return = "<tbody>";
      if ($result->num_rows > 0) {
        $loop = 1;
        // output data of each row
        while($row = $result->fetch_assoc()) {
          $return .= "<tr><td>". $loop++ . ".) " . $row['last_name'] . ", " . $row['middle_name']. " " . $row['first_name']. "</td><td>" . $row['address'] . "</td></tr>";
        }
      }
      $return .= "</tbody>";
      mysqli_close($conn);

      return $return;
    }
    
?>
