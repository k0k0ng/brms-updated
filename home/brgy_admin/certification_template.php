<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
?>

<?php
  // SAVE
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Save"){ 
    
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $header = $_POST["header"];
    $paragraph1 = $_POST["paragraph1"];
    $paragraph2 = $_POST["paragraph2"];
      
    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> insert_certification_template($brgy_id,$header,$paragraph1,$paragraph2); // Insert

    // Success
    if($result == 1){

      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Add certification " . $header); // Insert Log 

      echo "<script type='text/javascript'> 
                alert('Certification successfully added!');
                location = 'certification_template_settings.php';
            </script>";      
    }
    // Error
    else{
      echo "<script type='text/javascript'>
              alert('Certification Already Exist!');
              window.location.href = 'certification_template_settings.php';
            </script>";
    }    
  }
?>

<?php
  // UPDATE
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Update"){ 
    
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $id = $_SESSION['brms_template_id'];
    $header = $_POST["header"];
    $paragraph1 = $_POST["paragraph1"];
    $paragraph2 = $_POST["paragraph2"];
  
    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> update_certification_template($id,$brgy_id,$header,$paragraph1,$paragraph2); // Update

    // Success
    if($result == 1){

      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Update certification " . $header); // Insert Log 

      echo "<script type='text/javascript'> 
                alert('Certification successfully updated!');
                location = 'certification_template_settings.php';
            </script>";      
    }
    // Error
    else{
      echo "<script type='text/javascript'>alert('Error updating certification!');</script>";
    }    
  }
?>

<?php
  // Delete
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Delete"){ 
    
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $id = $_SESSION['brms_template_id'];
    $header = $_POST["header"];
      
    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> delete_certification_template($id,$brgy_id); // Update

    // Success
    if($result == 1){

      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Delete certification " . $header); // Insert Log 

      echo "<script type='text/javascript'> 
                alert('Certification successfully deleted!');
                location = 'certification_template_settings.php';
            </script>";      
    }
    // Error
    else{
      echo "<script type='text/javascript'>alert('Error deleting certification!');</script>";
    }    
  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>

        // On Submit

        function doDelete(){

          if (confirm("Are you sure to delete this certification template?")) {
            return true;
          } else {
            return false;
          }          
          
        }

        function doCheck(){

          var header = document.getElementById('header').value;
          var paragraph1 = document.getElementById('paragraph1').value;
          var paragraph2 = document.getElementById('paragraph2').value;
          var message = (document.getElementById("submit_save").value === "Update") ? "update" : "save"; 

          if(!header){
            alert("Invalid Header!");
            document.getElementById("header").focus();
            document.getElementById("header").value = "";
            return false;
          }
          else if(!paragraph1){
            alert("Invalid Paragraph 1!");
            document.getElementById("paragraph1").focus();
            document.getElementById("paragraph1").value = "";
            return false;
          }
          else if(!paragraph2){
            alert("Invalid Paragraph 2!");
            document.getElementById("paragraph2").focus();
            document.getElementById("paragraph2").value = "";
            return false;
          }

          if (confirm("Are you sure to " + message + " this certification template?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

    <script>
      // OnKeyRelease (Check if Header Exist)
      function checkTemplate(header){

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>";

        if(header.length == 0){           
          document.getElementById("submit_save").disabled = true;
        }else{          
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) { 

              // Barangay Found
              if(this.responseText == 1){
                document.getElementById("submit_save").disabled = true;               
              }
              else {                
                document.getElementById("submit_save").disabled = false;
              }                
            }
          };
          
          xmlhttp.open("GET", "../ajax/checkAjax_template.php?brgy_id=" + brgy_id + "&header=" + header, true);
          xmlhttp.send();
        }
      }
    </script>
  
    <script>
      // DocumentReady
      $( document ).ready(function() {
        var id = "<?php echo $_SESSION['brms_template_id']; ?>";
        var brgy_id = "<?php echo  $_SESSION['brms_template_brgyid']; ?>";
        var header = "<?php echo  $_SESSION['brms_template_header']; ?>";
        
        if(id !== ""){
          document.getElementById("header").value = header;
          setContent(header);
          document.getElementById("submit_save").value = "Update";
          $("#headerTitle").text("Update Certification Template");          

          document.getElementById("submit_save").disabled = false;
          document.getElementById("header").readOnly = true;
          document.getElementById("submit_delete").style.display = "block";
        }

        $('#paragraph1').click(function() { setParagraphActiveParagraph1(); });
        $('#paragraph2').click(function() { setParagraphActiveParagraph2(); });

        setParagraphActiveParagraph1(); // Set Paragraph1 Active 
        
        // Tab Key
        $(document).keyup(function(e) {
          if (e.keyCode == "9" ) {
            var x = document.activeElement.id;
            if(x === "paragraph2"){
              insertTag("\t");
            }
          }
        });

      });
    </script>

    <script>
      function loadCertificationContentParagraph(header,paragraph){
        var xmlhttp = new XMLHttpRequest();
        
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById(paragraph).value = this.responseText.trim();  
          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

        xmlhttp.open("GET", "../ajax/loadAjax_certificationContent.php?brgy_id="+brgy_id+"&paragraph="+paragraph+"&header="+header, true); 
        xmlhttp.send();
      }
    </script>

    <script>
      function setContent(header){
        loadCertificationContentParagraph(header,"paragraph1");
        loadCertificationContentParagraph(header,"paragraph2");
      }
    </script>

    <script>

      var paragraph; // Paragraph Active

      function setParagraphActiveParagraph1(){
        paragraph = true;
      }

      function setParagraphActiveParagraph2(){
        paragraph = false;
      }

      // Insert Tag
      function insertTag(text) {

        var paragraphID = (paragraph) ? "paragraph1":"paragraph2";     

        var textarea = document.getElementById(paragraphID);
        var currentPos = $('#'+paragraphID).prop("selectionStart");
        var strLeft = textarea.value.substring(0, currentPos);
        var strRight = textarea.value.substring(currentPos, textarea.value.length);
        textarea.value = strLeft + text + strRight; // Set TaxeArea Value
        setCaretToPos(document.getElementById(paragraphID), parseInt(currentPos) + text.length);    
      }

      // Set Range
      function setSelectionRange(input, selectionStart, selectionEnd) {
        if (input.setSelectionRange) {
          input.focus();
          input.setSelectionRange(selectionStart, selectionEnd);
        }
        else if (input.createTextRange) {
          var range = input.createTextRange();
          range.collapse(true);
          range.moveEnd('character', selectionEnd);
          range.moveStart('character', selectionStart);
          range.select();
        }
      }

      // Set Current Position
      function setCaretToPos (input, pos) {
        setSelectionRange(input, pos, pos);
      }
    </script>

    <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!-- Profile Divider --> 

    <div class="container container-profile">

      <div class="row">

        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">

          <div> 
            <h4 class="text-secondary"><b>Tags - Data from the user (click below to insert in the template)</b></h4>
            <table class="table table-condensed table-hover text-warning" style="cursor: pointer;">                  
              <tr>
                <td><b onclick="insertTag('CITIZEN_NAME')">CITIZEN_NAME</b></td>
                <td><b onclick="insertTag('CITIZEN_ADDRESS')">CITIZEN_ADDRESS</b></td>
                <td><b onclick="insertTag('PUROK')">PUROK</b></td>               
              </tr>
              <tr>
                <td><b onclick="insertTag('HIS_HER')">HIS_HER</b></td>
                <td><b onclick="insertTag('MALE_FEMALE')">MALE_FEMALE</b></td>
                <td><b onclick="insertTag('HE_SHE')">HE_SHE</b></td>                               
              </tr>
              <tr>                
                <td><b onclick="insertTag('STATUS')">STATUS</b></td>                 
                <td><b onclick="insertTag('BIRTHDATE')">BIRTHDATE</b></td>
                <td><b onclick="insertTag('AGE')">AGE</b></td>                
              </tr>
              <tr>          
                <td><b onclick="insertTag('EDUCATION')">EDUCATION</b></td>      
                <td><b onclick="insertTag('OCCUPATION')">OCCUPATION</b></td>
                <td><b onclick="insertTag('RELIGION')">RELIGION</b></td>
              </tr>     
              <tr>          
                <td><b onclick="insertTag('BLOOD_TYPE')">BLOOD_TYPE</b></td>
                <td><b onclick="insertTag('RESIDENCE_TYPE')">RESIDENCE_TYPE</b></td>                
                <td><b onclick="insertTag('OCCUPATION')">OCCUPATION</b></td>
              </tr>   
              <tr>          
                <td><b onclick="insertTag('BRGY_NAME')">BRGY_NAME</b></td> 
                <td><b onclick="insertTag('BRGY_ID')">BRGY_ID</b></td>
                <td><b onclick="insertTag('CITY')">CITY</b></td>
                <td></td>
              </tr> 
              <tr style="color: #000000; cursor: auto;">          
                <td>
                  <h4 class="text-secondary"><b>Settings</b></h4>
                </td>
              </tr>
              <tr style="color: #0000ff">          
                <td><b onclick="insertTag('[')">_START_BOLD_FONT</b></td>
                <td><b onclick="insertTag(']')">_END_BOLD_FONT</b></td>
              </tr>
              <tr style="color: #0000ff">          
                <td><b onclick="insertTag('{')">_START_ITALIC_FONT</b></td>
                <td><b onclick="insertTag('}')">_END_ITALIC_FONT</b></td>
              </tr>
            </table>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 id="headerTitle">Add Certification Template</h3>
            </div>
            <div class="panel-body">
              <form method="post" name="confirmationForm" id="confirmationForm" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?" target="_self">
                
                <!-- Header -->
                <div class="input-group">
                  <span class="input-group-addon">Header</span>
                  <input id="header" onkeyup="checkTemplate(this.value)" type="text" class="form-control" name="header" required>
                </div>                
                <br>

                <!-- TextArea -->
                <textarea form="confirmationForm" rows="5" id="paragraph1" type="textarea" class="form-control" name="paragraph1" required placeholder="Content"></textarea>                
                <br>

                <textarea form="confirmationForm" rows="2" id="paragraph2" type="textarea" class="form-control" name="paragraph2" readonly><?php 
                    $cur_date = date('jS \d\a\y \o\f F Y');
                    $brgy_address = $_SESSION['brms_brgyAddress'];
                    $text = 'Issued on '.$cur_date .' at the office of the Punong Barangay, '.$brgy_address.'.';
                    echo $text;
                ?>
                </textarea>
                <br>
                
                <input id="submit_save" name="submit" onclick="return doCheck()" type="submit" value="Save" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;" disabled>
                <input id="submit_delete" name="submit" onclick="return doDelete()" type="submit" value="Delete" class="btn btn-danger" style="float: right; margin-left:10px; display: none; width:100px;">
                <span onclick="window.open('certification_template_settings.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
