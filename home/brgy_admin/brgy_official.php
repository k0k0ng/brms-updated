<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
?>

<?php    
  // INSERT USER
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Save") {    
	
	$brgy_id = $_SESSION['brms_brgyId'];
    $info_cap  = $_POST["text0"];
    $info_wad1 = $_POST["text1"];
    $info_wad2 = $_POST["text2"];
    $info_wad3 = $_POST["text3"];
    $info_wad4 = $_POST["text4"];
    $info_wad5 = $_POST["text5"];
    $info_wad6 = $_POST["text6"];
    $info_wad7 = $_POST["text7"];
    $info_sec = $_POST["text8"];

    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID

    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> update_brgy_official($brgy_id,$info_cap,$info_wad1,$info_wad2,$info_wad3,$info_wad4,$info_wad5,$info_wad6,$info_wad7,$info_sec);

    if ($result == 1) {

      update_brgy_officials(); // Update Officials
      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Update Barangay Officials for Brgy. " . $_SESSION['brms_brgyName']); // Insert Log

      echo "<script type='text/javascript'>alert('Barangay officials successfully updated!'); location = 'brgy_official.php';</script>"; 
    }else{
      echo "<script type='text/javascript'>alert('Error!')</script>"; 
    }

  }
?>

<?php
  // Barangay OFFICIALS SESSION
  function update_brgy_officials(){    
    $database = new Database(); // New Conenction
    $conn = $database->get_Connection(); // Get Database Connection
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $sql = "SELECT * FROM barangay_info WHERE id = $brgy_id";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
       $_SESSION['brms_cap'] = $row['info_cap'];
       $_SESSION['brms_wad1'] = $row['info_wad1'];
       $_SESSION['brms_wad2'] = $row['info_wad2'];
       $_SESSION['brms_wad3'] = $row['info_wad3'];
       $_SESSION['brms_wad4'] = $row['info_wad4'];
       $_SESSION['brms_wad5'] = $row['info_wad5'];
       $_SESSION['brms_wad6'] = $row['info_wad6'];
       $_SESSION['brms_wad7'] = $row['info_wad7'];   
       $_SESSION['brms_sec'] = $row['info_sec'];   
     }
   }
   
    $_SESSION['brms_cap_name'] = update_brgy_officials_name($conn, $_SESSION['brms_cap']);
    $_SESSION['brms_wad1_name'] = update_brgy_officials_name($conn, $_SESSION['brms_wad1']);
    $_SESSION['brms_wad2_name'] = update_brgy_officials_name($conn, $_SESSION['brms_wad2']);
    $_SESSION['brms_wad3_name'] = update_brgy_officials_name($conn, $_SESSION['brms_wad3']);
    $_SESSION['brms_wad4_name'] = update_brgy_officials_name($conn, $_SESSION['brms_wad4']);
    $_SESSION['brms_wad5_name'] = update_brgy_officials_name($conn, $_SESSION['brms_wad5']);
    $_SESSION['brms_wad6_name'] = update_brgy_officials_name($conn, $_SESSION['brms_wad6']);
    $_SESSION['brms_wad7_name'] = update_brgy_officials_name($conn, $_SESSION['brms_wad7']);
    $_SESSION['brms_sec_name'] = update_brgy_officials_name($conn, $_SESSION['brms_sec']);

    mysqli_close($conn);
  }
?>

<?php
  // Barangay OFFICIALS  SESSION
  function update_brgy_officials_name($conn, $info_id){
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $sql = "SELECT * FROM user_info WHERE brgy_id = $brgy_id AND id = $info_id";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        return $row['first_name'] . ' ' . $row['middle_name'][0] . '. ' . $row['last_name'];
     }
   }
   return '';
  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">

    <link rel="stylesheet" href="../css/live_search.css"> <!-- LIVE SEARCH CSS -->    

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->
    <link rel="stylesheet" href="../css/table_Design.css"> <!-- COSTUMIZE TABLE COLOR -->

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>


    <script>
        // On Submit
        function doCheck(){

          if (confirm("Are you sure to update barangay officials?")) {
            return true;
          } else {
            return false;
          }          
          
        }
      </script>

    <script>
      var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>";
    </script>

    <script>
      // Hide Table
      function hide(){
        document.getElementById("responsecontainer1").innerHTML = '';
        document.getElementById("responsecontainer2").innerHTML = '';
        document.getElementById("responsecontainer3").innerHTML = '';
        document.getElementById("responsecontainer4").innerHTML = '';
        document.getElementById("responsecontainer5").innerHTML = '';
        document.getElementById("responsecontainer6").innerHTML = '';
        document.getElementById("responsecontainer7").innerHTML = '';
        document.getElementById("responsecontainer8").innerHTML = '';
        document.getElementById("responsecontainer9").innerHTML = '';
      }

      function check(id){        
        if(document.getElementById("text0").value == id)
          return 1;
        else if(document.getElementById("text1").value == id)
          return 1;
        else if(document.getElementById("text2").value == id)
          return 1;
        else if(document.getElementById("text3").value == id)
          return 1;
        else if(document.getElementById("text4").value == id)
          return 1;
        else if(document.getElementById("text5").value == id)
          return 1;
        else if(document.getElementById("text6").value == id)
          return 1;
        else if(document.getElementById("text7").value == id)
          return 1;
        else if(document.getElementById("text8").value == id)
          return 1;
        else        
          return 0;       
      }

      // Document Ready
      $(document).ready(function(){ 
        
        $(document).click(function(e){ hide(); }); // Document Click
        
        hide(); // Hide tables when load

        //  Table 1
        $("#table_Value0").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var name = currentRow.find("td:eq(1)").text(); // get current row 2nd TD value

          if(check(id)==0){
            document.getElementById("text0").value = id;
            document.getElementById("punong_barangay").value = name;
          }else{
            alert('Official already exist!');
            document.getElementById("punong_barangay").value = '';
          }
        });

        //  Table 1
        $("#table_Value1").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value          
          var name = currentRow.find("td:eq(1)").text(); // get current row 1st TD value
          
          if(check(id)==0){
            document.getElementById("text1").value = id;
            document.getElementById("kagawad1").value = name;
          }else{
            alert('Official already exist!');
            document.getElementById("kagawad1").value = '';
          }
        });

        //  Table 2
        $("#table_Value2").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");  
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value        
          var name = currentRow.find("td:eq(1)").text(); // get current row 1st TD value

          if(check(id)==0){            
            document.getElementById("text2").value = id;
            document.getElementById("kagawad2").value = name;
          }else{
            alert('Official already exist!');
            document.getElementById("kagawad2").value = '';
          }
        });

        //  Table 3
        $("#table_Value3").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");      
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value    
          var name = currentRow.find("td:eq(1)").text(); // get current row 1st TD value

          if(check(id)==0){
            document.getElementById("text3").value = id;
            document.getElementById("kagawad3").value = name;
          }else{
            alert('Official already exist!');
            document.getElementById("kagawad3").value = '';
          }
        });

        //  Table 4
        $("#table_Value4").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");   
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value       
          var name = currentRow.find("td:eq(1)").text(); // get current row 1st TD value

          if(check(id)==0){
            document.getElementById("text4").value = id;
            document.getElementById("kagawad4").value = name;
          }else{
            alert('Official already exist!');
            document.getElementById("kagawad4").value = '';
          }
        });

        //  Table 5
        $("#table_Value5").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");   
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value       
          var name = currentRow.find("td:eq(1)").text(); // get current row 1st TD value

          if(check(id)==0){
            document.getElementById("text5").value = id;
            document.getElementById("kagawad5").value = name;
          }else{
            alert('Official already exist!');
            document.getElementById("kagawad5").value = '';
          }
        });

        //  Table 6
        $("#table_Value6").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");    
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value      
          var name = currentRow.find("td:eq(1)").text(); // get current row 1st TD value

          if(check(id)==0){
            document.getElementById("text6").value = id;
            document.getElementById("kagawad6").value = name;
          }else{
            alert('Official already exist!');
            document.getElementById("kagawad6").value = '';
          }
        });

        //  Table 7
        $("#table_Value7").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");    
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value      
          var name = currentRow.find("td:eq(1)").text(); // get current row 1st TD value

          if(check(id)==0){
            document.getElementById("text7").value = id;
            document.getElementById("kagawad7").value = name;
          }else{
            alert('Official already exist!');
            document.getElementById("kagawad7").value = '';
          }
        });

        //  Table 8
        $("#table_Value8").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");    
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value      
          var name = currentRow.find("td:eq(1)").text(); // get current row 1st TD value

          if(check(id)==0){
            document.getElementById("text8").value = id;
            document.getElementById("secretary").value = name;
          }else{
            alert('Official already exist!');
            document.getElementById("secretary").value = '';
          }
        });

      });
    </script>

    <script>
      // Clear
      function clear_form(id){      

        switch(id){
          case 1:
            document.getElementById("text0").value = 0;
            document.getElementById("punong_barangay").value = '';
          break;

          case 2:
            document.getElementById("text1").value = 0;
            document.getElementById("kagawad1").value = '';
          break;

          case 3:
            document.getElementById("text2").value = 0;
            document.getElementById("kagawad2").value = '';
          break;

          case 4:
            document.getElementById("text3").value = 0;
            document.getElementById("kagawad3").value = '';
          break;

          case 5:
            document.getElementById("text4").value = 0;
            document.getElementById("kagawad4").value = '';
          break;

          case 6:
            document.getElementById("text5").value = 0;
            document.getElementById("kagawad5").value = '';
          break;

          case 7:
            document.getElementById("text6").value = 0;
            document.getElementById("kagawad6").value = '';
          break;

          case 8:
            document.getElementById("text7").value = 0;
            document.getElementById("kagawad7").value = '';
          break;

          case 9:
            document.getElementById("text8").value = 0;
            document.getElementById("secretary").value = '';
          break;
        }
      }

    </script>

    <script>
      // OnKeyRelease
      function checkUser(str,container,id){

        // Back Key Pressed
        var key = event.keyCode || event.charCode;
        if( key == 8 || key == 46 ){
          clear_form(id); // Clear Form          
        }

        if(str.length == 0){
          hide();
        }else{
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById(container).innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_brgy_official.php?q="+str+"&brgy_id="+brgy_id, true);
          xmlhttp.send();  
        }     
      }
    </script>

  <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!--SEARCH BAR-->
    <div class="container container-profile">    
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Barangay Officials (Signatory)</h3>
            </div>

            <div class="panel-body">

                <form id="this_form" method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">

                  <div class="form-group">
                    <input id="text0" name="text0" type="text" class="form-control text_val" placeholder="id 0" value="<?php echo $_SESSION['brms_cap']; ?>">
                    <div class="input-group">  
                      <span class="input-group-addon">Punong Barangay</span>
                      <input id="punong_barangay" onkeyup="checkUser(this.value,'responsecontainer1',1)" type="text" class="form-control input-sm" name="punong_barangay" value="<?php echo $_SESSION['brms_cap_name']; ?>">
                      <span onclick="clear_form(1)" class="input-group-addon btn btn-info">Clear</span>
                    </div>

                    <div id="table_Value0">          
                      <table class="table table-condensed table-hover">                                          
                        <tbody id="responsecontainer1" style="cursor: pointer;">
                          <tr><td>123</td></tr>
                          <tr><td>123</td></tr>
                        </tbody>
                      </table>
                    </div>   
                  </div>

                  <div class="form-group">
                    <input id="text1" name="text1" type="text" class="form-control text_val" placeholder="id 1" value="<?php echo $_SESSION['brms_wad1']; ?>">
                    <div class="input-group">
                      <span class="input-group-addon">Barangay Kagawad 1</span>  
                      <input id="kagawad1" onkeyup="checkUser(this.value,'responsecontainer2',2)" type="text" class="form-control input-sm" name="kagawad1" value="<?php echo $_SESSION['brms_wad1_name']; ?>">
                      <span onclick="clear_form(2)" class="input-group-addon btn btn-info">Clear</span>
                    </div>

                    <div id="table_Value1">          
                      <table class="table table-condensed table-hover">                                          
                        <tbody id="responsecontainer2" style="cursor: pointer;">
                          <tr><td>123</td></tr>
                          <tr><td>123</td></tr>
                        </tbody>
                      </table>
                    </div>  
                  </div>

                  <div class="form-group">
                    <input id="text2" name="text2" type="text" class="form-control text_val" placeholder="id 2" value="<?php echo $_SESSION['brms_wad2']; ?>">
                    <div class="input-group">
                      <span class="input-group-addon">Barangay Kagawad 2</span>   
                      <input id="kagawad2" onkeyup="checkUser(this.value,'responsecontainer3',3)" type="text" class="form-control input-sm" name="kagawad2" value="<?php echo $_SESSION['brms_wad2_name']; ?>">
                      <span onclick="clear_form(3)" class="input-group-addon btn btn-info">Clear</span>
                    </div>

                    <div id="table_Value2">          
                      <table class="table table-condensed table-hover">                                          
                        <tbody id="responsecontainer3" style="cursor: pointer;">
                          <tr><td>123</td></tr>
                          <tr><td>123</td></tr>
                        </tbody>
                      </table>
                    </div>  
                  </div>

                  <div class="form-group">
                    <input id="text3" name="text3" type="text" class="form-control text_val" placeholder="id 3" value="<?php echo $_SESSION['brms_wad3']; ?>">
                    <div class="input-group">
                      <span class="input-group-addon">Barangay Kagawad 3</span>  
                      <input id="kagawad3" onkeyup="checkUser(this.value,'responsecontainer4',4)" type="text" class="form-control input-sm" name="kagawad3" value="<?php echo $_SESSION['brms_wad3_name']; ?>">
                      <span onclick="clear_form(4)" class="input-group-addon btn btn-info">Clear</span>
                    </div>

                    <div id="table_Value3">          
                      <table class="table table-condensed table-hover">                                          
                        <tbody id="responsecontainer4" style="cursor: pointer;">
                          <tr><td>123</td></tr>
                          <tr><td>123</td></tr>
                        </tbody>
                      </table>
                    </div>  
                  </div>

                  <div class="form-group">
                    <input id="text4" name="text4" type="text" class="form-control text_val" placeholder="id 4" value="<?php echo $_SESSION['brms_wad4']; ?>">
                    <div class="input-group">
                      <span class="input-group-addon">Barangay Kagawad 4</span> 
                      <input id="kagawad4" onkeyup="checkUser(this.value,'responsecontainer5',5)" type="text" class="form-control input-sm" name="kagawad4" value="<?php echo $_SESSION['brms_wad4_name']; ?>">
                      <span onclick="clear_form(5)" class="input-group-addon btn btn-info">Clear</span>
                    </div>

                    <div id="table_Value4">          
                      <table class="table table-condensed table-hover">                                          
                        <tbody id="responsecontainer5" style="cursor: pointer;">
                          <tr><td>123</td></tr>
                          <tr><td>123</td></tr>
                        </tbody>
                      </table>
                    </div>  
                  </div>

                  <div class="form-group">
                    <input id="text5" name="text5" type="text" class="form-control text_val" placeholder="id 5" value="<?php echo $_SESSION['brms_wad5']; ?>">
                    <div class="input-group">
                      <span class="input-group-addon">Barangay Kagawad 5</span> 
                      <input id="kagawad5" onkeyup="checkUser(this.value,'responsecontainer6',6)" type="text" class="form-control input-sm" name="kagawad5" value="<?php echo $_SESSION['brms_wad5_name']; ?>">
                      <span onclick="clear_form(6)" class="input-group-addon btn btn-info">Clear</span>
                    </div>

                    <div id="table_Value5">          
                      <table class="table table-condensed table-hover">                                          
                        <tbody id="responsecontainer6" style="cursor: pointer;">
                          <tr><td>123</td></tr>
                          <tr><td>123</td></tr>
                        </tbody>
                      </table>
                    </div>  
                  </div>

                  <div class="form-group">
                    <input id="text6" name="text6" type="text" class="form-control text_val" placeholder="id 6" value="<?php echo $_SESSION['brms_wad6']; ?>">
                    <div class="input-group">
                      <span class="input-group-addon">Barangay Kagawad 6</span>
                      <input id="kagawad6" onkeyup="checkUser(this.value,'responsecontainer7',7)" type="text" class="form-control input-sm" name="kagawad6" value="<?php echo $_SESSION['brms_wad6_name']; ?>">
                      <span onclick="clear_form(7)" class="input-group-addon btn btn-info">Clear</span>
                    </div>

                    <div id="table_Value6">
                      <table class="table table-condensed table-hover">                                          
                        <tbody id="responsecontainer7" style="cursor: pointer;">
                          <tr><td>123</td></tr>
                          <tr><td>123</td></tr>
                        </tbody>
                      </table>
                    </div>  
                  </div>   

                  <div class="form-group">
                    <input id="text7" name="text7" type="text" class="form-control text_val" placeholder="id 7" value="<?php echo $_SESSION['brms_wad7']; ?>">
                    <div class="input-group">
                      <span class="input-group-addon">Barangay Kagawad 7</span>
                      <input id="kagawad7" onkeyup="checkUser(this.value,'responsecontainer8',8)" type="text" class="form-control input-sm" name="kagawad7" value="<?php echo $_SESSION['brms_wad7_name']; ?>">
                      <span onclick="clear_form(8)" class="input-group-addon btn btn-info">Clear</span>
                    </div>

                    <div id="table_Value7">
                      <table class="table table-condensed table-hover">                                          
                        <tbody id="responsecontainer8" style="cursor: pointer;">
                          <tr><td>123</td></tr>
                          <tr><td>123</td></tr>
                        </tbody>
                      </table>
                    </div>  
                  </div>

                  <div class="form-group">
                    <input id="text8" name="text8" type="text" class="form-control text_val" placeholder="id 8" value="<?php echo $_SESSION['brms_sec']; ?>">
                    <div class="input-group">
                      <span class="input-group-addon">Pangkat Secretary</span>
                      <input id="secretary" onkeyup="checkUser(this.value,'responsecontainer9',9)" type="text" class="form-control input-sm" name="secretary" value="<?php echo $_SESSION['brms_sec_name']; ?>">
                      <span onclick="clear_form(9)" class="input-group-addon btn btn-info">Clear</span>
                    </div>

                    <div id="table_Value8">
                      <table class="table table-condensed table-hover">                                          
                        <tbody id="responsecontainer9" style="cursor: pointer;">
                          <tr><td>123</td></tr>
                          <tr><td>123</td></tr>
                        </tbody>
                      </table>
                    </div>  
                  </div>
                  
                  <input name="submit" type="submit" onclick="return doCheck()"  value="Save" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                  <span onclick="window.open('admin_home.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>
                  
                </form>                  

            </div>            
          </div>

        </div>
      </div>
    </div>

  </body>

</html>