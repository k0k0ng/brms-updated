<?php
  require('../session/brgy_admin.php'); // Secure Connection
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">

    <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->
  
    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
      // OnKeyRelease
      function checkUser(str){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer").innerHTML = this.responseText;                                
          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID 

        xmlhttp.open("GET", "../ajax/loadAjax_purokTable.php?q="+str+"&brgy_id="+brgy_id, true);
        xmlhttp.send();        
      }
    </script>

    <script>
      // DocumentReady
      $( document ).ready(function() {
        checkUser('');  

        $("#table_Value").on('click','.btnView',function(){
          // get the current row
          var currentRow=$(this).closest("tr"); 

          var purok_id = currentRow.find("td:eq(1)").text(); // get current row 2nd TD
          var purok_name = currentRow.find("td:eq(2)").text(); // get current row 3rd TD
          var purok_leader = currentRow.find("td:eq(3)").text(); // get current row 4th TD

          window.location.href="../session/brgy_admin_update_session_view_purok.php?purok_id="+purok_id+"&purok_name=" + purok_name + "&purok_leader=" + purok_leader; // View PUROK
        });
        
        
        $("#table_Value").on('click','.btnUpdate',function(){
          // get the current row
          var currentRow=$(this).closest("tr"); 
      
          var user_id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var purok_id = currentRow.find("td:eq(1)").text(); // get current row 2nd TD
          var purok_name = currentRow.find("td:eq(2)").text(); // get current row 3rd TD
          var citizen_name = currentRow.find("td:eq(3)").text(); // get current row 4th TD

          window.location.href="../session/brgy_admin_update_session_update_purok.php?purok_name=" + purok_name + "&citizen_name=" + citizen_name + "&user_id=" + user_id + "&purok_id=" + purok_id; // Update PUROK
          
        });

      });
    </script>

  <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container-fluid"><!-- Profile Divider --> 
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
        
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Purok (Clusters)</h3>
            </div>

            <div class="panel-body">

              <!--Search-->
              <div class="input-group">
                <span class="input-group-addon">Search </span>                  
                <input type="text" onkeyup="checkUser(this.value)" class="form-control image-preview-filename" placeholder="Purok name">
              </div>                

              <br>

              <div id="table_Value" class="table-responsive">          
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Purok Leader</th>
                      <th>Population</th>
                      <th colspan="2">Action</th>                      
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer">
                  </tbody>
                </table>
              </div>

            </div>

          </div>

        </div>
      </div>
    </div>

  </body>

</html>