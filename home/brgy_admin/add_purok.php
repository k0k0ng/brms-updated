<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
?>

<?php
  // ADD | UPDATE PUROK
  if($_SERVER["REQUEST_METHOD"] == "POST" && ($_POST["submit"] === "Add" || $_POST["submit"] === "Update")){ 

    $purok_new = $_POST["purok_name"];
    $purok_old = (isset($_SESSION['brms_UPDATE_purok_name'])) ? $_SESSION['brms_UPDATE_purok_name'] : '';
    $info_id = ($_POST["user_id"] !== '') ? $_POST["user_id"] : 0;
    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 
    
    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> insert_purok($brgy_id,$purok_old,$purok_new,$info_id); // Insert or Update Purok

    // Success
    if($result == 1){
      // CLEAR CACHE
      header('Cache-Control: no-store, no-cache, must-revalidate'); 
      header('Cache-Control: post-check=0, pre-check=0',false); 
      header('Pragma: no-cache');

      if(isset($_SESSION['brms_UPDATE_purok_name'])){

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Update Information for Purok " . $purok_new); // Insert Log 
        
        echo "<script type='text/javascript'>
                alert('$purok_new successfully updated!');
                location = 'purok.php';
            </script>";
      }
      // Error
      else{

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Add New Purok " . $purok_new); // Insert Log 

        echo "<script type='text/javascript'>
                alert('$purok_new successfully added!');
                location = 'purok.php';
              </script>";
      } 
    }else{
      echo "<script type='text/javascript'>alert('Invalid Purok Name!')</script>";      
    }

  }
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <title>BRMS - Barangay Record Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/custom2.css">

  <link rel="javascript" src="../js/jquery.js">
  <link rel="javascript" src="../js/jquery.min.js">

  <!--Website Tab Icon-->
  <link rel="icon" type="image/png" href="../images/logo.png"/>
  
  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>

  <script>
    // On Submit
    function doCheck(){

      if (confirm("Are you sure to add changes to this purok?")) {
        return true;
      } else {
        return false;
      }          
          
    }
  </script>
  
  <script>
      var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID
      var valid = "glyphicon glyphicon-ok";
      var invalid = "glyphicon glyphicon-remove";
  </script>

  <script>
      // OnKeyRelease 
      function checkPurok(str){

        checkPurokName(str); // Check Purok name

        if(str.length == 0){           
          document.getElementById("valid_purok").className = invalid;
          document.getElementById("submit_purok").disabled = true;          

        }else{          
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              if(this.responseText == 1){
                document.getElementById("valid_purok").className = invalid;
                document.getElementById("submit_purok").disabled = true;
              }else{
                document.getElementById("valid_purok").className = valid;
                document.getElementById("submit_purok").disabled = false;
              }                
            }
          };

          xmlhttp.open("GET", "../ajax/checkAjax_purok.php?q="+str+"&brgy_id="+brgy_id, true);
          xmlhttp.send();
        }
      }
    </script>

    <script>
      // OnKeyRelease
      function checkUser(str){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer").innerHTML = this.responseText; 

            // If result is OK (Go to add citizen)
            if(this.responseText === ""){
              if (confirm("Citizen not found, would you like to add new?") == true) {
                location = 'add_citizen.php';
              }
            }

          }
        };

        if(str.length == 0){           
          document.getElementById("responsecontainer").innerHTML = "";
          
        }else{
          var purok_id = "<?php echo $_SESSION['brms_VIEW_purok_id']; ?>"; // Purok ID
          xmlhttp.open("GET", "../ajax/loadAjax_citizenTable2.php?q="+str+"&brgy_id="+brgy_id+"&purok_id="+purok_id, true);
          xmlhttp.send(); 

        } 
      }
    </script>

    <script>
      // OnKeyRelease
      function checkPurokName(str){
        if(str.length == 0){ 
          document.getElementById("responsecontainer2").innerHTML = "";

        }else{
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer2").innerHTML = this.responseText;                                
            }
          };

          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

          xmlhttp.open("GET", "../ajax/loadAjax_purokName.php?q="+str+"&brgy_id="+brgy_id, true);
          xmlhttp.send();
        }
      }
    </script>

    <script>
      // OnClick Table
      $(document).ready(function(){
        // code to read selected table row cell data (values).
        $("#table_Value").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var col1 = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var col2 = currentRow.find("td:eq(1)").text(); // get current row 2nd TD value
          var col3 = currentRow.find("td:eq(2)").text(); // get current row 3rd TD value
          var col4 = currentRow.find("td:eq(3)").text(); // get current row 4th TD value

          document.getElementById("user_id").value = col1;
          document.getElementById("first_name").value = col4 + ", " + col2 + " " + col3;

          document.getElementById("responsecontainer").innerHTML = "";
          document.getElementById("citizen_name").value = ""; 
        });

        // GET PUROK NAME
        var citizen_name = "<?php echo $_SESSION['brms_UPDATE_citizen_name']; ?>";
        var purok_name = "<?php echo $_SESSION['brms_UPDATE_purok_name']; ?>";
        var citizen_id = "<?php echo $_SESSION['brms_UPDATE_user_id']; ?>";
        
        if(purok_name !== ''){          
          showField(); // Update Purok
          hideField(); // Add Purok 

          $("h3").html("Update Purok (Cluster)");
          
          document.getElementById("user_id").value = citizen_id;
          document.getElementById("purok_name").value = purok_name;
          document.getElementById("first_name").value = citizen_name;

          document.getElementById("submit_purok").value = "Update";
          document.getElementById("submit_purok").disabled = false;
          document.getElementById("citizen_name").focus();

          document.getElementById("valid_purok").className = valid;
        }

      });
    </script>

    <script>
        // Update Purok
        function showField(){    
          $("#purok_leader").show();
          $("#citizen").show();
          $("#table_Value").show();
          $(".br").show();
        }

        // Add Purok
        function hideField(){    
          $("#table_Value2").hide();
          $(".br2").hide();
        }

    </script>
    <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container container-profile"> <!-- Profile Divider -->  
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
          <div class="panel panel-default">

            <div class="panel-heading">
              <h3 class="panel-title" id="form">Add Purok (Cluster)</h3>
            </div>

            <div class="panel-body">
              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="">

                <!--Purok Name-->
                <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>                  
                  <input id="purok_name" type="text" onkeyup="checkPurok(this.value)" class="form-control" name="purok_name" placeholder="Purok Name" required autofocus>
                  <input type="hidden" id="user_id" name="user_id" value="0">
                  <span class="input-group-addon"><i id="valid_purok" class="glyphicon glyphicon-remove"/></i></span>
                </div>

                <br class="br" style="display: none;">

                <!--Purok Leader Name-->
                <div class="input-group" id="purok_leader" style="display: none;">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input id="first_name" type="text" class="form-control" name="first_name" placeholder="Purok Leader" readonly>
                </div>
                
                <br class="br" style="display: none;">

                <!--Search-->
                <div class="input-group image-preview input-group-btn" id="citizen" style="display: none;">
                  <!-- don't give a name === doesn't send on POST/GET -->
                  <input id="citizen_name" type="text" onkeyup="checkUser(this.value)" class="form-control image-preview-filename" placeholder="Enter citizen's name">  
                </div><!-- /input-group image-preview [TO HERE]--> 

                <br class="br" style="display: none;">

                <div id="table_Value" class="table-responsive" style="display: none;">          
                  
                  <table class="table table-condensed table-hover">
                    <thead>
                      <tr>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Purok Name</th>
                      </tr>
                    </thead>                    
                    <tbody id="responsecontainer"></tbody>

                  </table>

                </div>
                
                <br>
                
                <input id="submit_purok" name="submit" type="submit" onclick="return doCheck()" value="Add" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;" disabled>
                <span onclick="window.open('purok.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>
                
                <br class="br2">
                <br class="br2">
                <br class="br2">

                <div id="table_Value2" class="table-responsive2">          
                  
                  <table class="table table-condensed table-hover">
                    <thead>
                      <tr>
                        <th>List of Registered Purok (Name)</th>
                      </tr>
                    </thead>                    
                    <tbody id="responsecontainer2">
                    </tbody>

                  </table>

                </div>
          
              </form>                        
            </div>

          </div>
        </div>
      </div>
    </div>

  </body>

</html>