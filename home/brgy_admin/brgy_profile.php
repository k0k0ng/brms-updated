<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
  require('../session/brgy_admin_brgy_image.php'); // Get Image Src
?>

<?php
  // SAVE CHANGES
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Save"){    

      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
      
      $address = $_POST["address"]; // Barangay Address
      $telefax = $_POST["telefax"]; // Barangay Telefax
      $email = $_POST["email"]; // Barangay Email
      $website = $_POST["website"]; // Barangay Website

      $query = new database_query(); // Database Query (initialize connection)
      $result = $query -> update_brgy_info($brgy_id,$address,$telefax,$email,$website); // Update Barangay Info

      // SUCCESS
      if($result == 1){

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Update Information for Brgy. " . $_SESSION['brms_brgyName']); // Insert Log

        // Update Session
        $_SESSION['brms_brgyAddress'] = $address;
        $_SESSION['brms_brgyTelefax'] = $telefax;
        $_SESSION['brms_brgyEmail'] = $email;
        $_SESSION['brms_brgyWebsite'] = $website;

        echo "<script type='text/javascript'>alert('Barangay profile successfully updated!');</script>";
      }
      // ERROR
      else{
        echo "<script type='text/javascript'>alert('Error updating barangay profile!');</script>";
      }          
  }
?>

<?php
  // UPLOAD IMAGE
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Upload"){ 
    if($_FILES['image']['error'] == 0){

      include '../image_api/lib/WideImage.php'; // API for IMAGE

      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
      
      $name = $brgy_id; // Image name (ID of USER)
      $filename = $_FILES["image"]["name"];
      $ext = strtolower(substr(strrchr($filename, '.'), 1)); // Get extension
      $image_name = $name . '.' . $ext; //New image name

      $image = WideImage::load($_FILES['image']['tmp_name']); // Get image
      $image->saveToFile('../brgy_image/' . $image_name); // Save image

      $query = new database_query(); // Database Query (initialize connection)
      $result = $query -> upload_image_brgy($brgy_id,$name,$ext); // Upload Image

      // Success
      if($result == 1){

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Upload Image for Brgy. " . $_SESSION['brms_brgyName']); // Insert Log

        $_SESSION['brms_brgyImgName'] = $name; // Update Image Name
        $_SESSION['brms_brgyImgExtension'] = $ext; // Update Image Extension
        
        echo "<script type='text/javascript'>alert('Image uploaded successfully!'); location = 'brgy_profile.php';</script>";
      }
      // Error
      else{
        echo "<script type='text/javascript'>alert('Error uploading image!');</script>";
      } 

    }else{      
      echo "<script type='text/javascript'>alert('No image selected!')</script>";
    }    
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
        // On Submit
        function doCheckUpdload(){

          if (confirm("Are you sure to updload this image?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

    <script>
        // On Submit
        function doCheckUpdate(){

          if (confirm("Are you sure to update this barangay profile?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

    <script>
      $(document).on('click', '#close-preview', function(){ 
        $('.image-preview').popover('hide');
            // Hover befor close the preview
            $('.image-preview').hover(
              function () {
                $('.image-preview').popover('show');
              }, 
              function () {
                $('.image-preview').popover('hide');
              }
              );    
        });

        $(function() {
            // Create the close button
            var closebtn = $('<button/>', {
              type:"button",
              text: 'x',
              id: 'close-preview',
              style: 'font-size: initial;',
            });

            closebtn.attr("class","close pull-right");

            // Set the popover default content
            $('.image-preview').popover({
              trigger:'manual',
              html:true,
              title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
              content: "There's no image",
              placement:'bottom'
            });

            // Clear event
            $('.image-preview-clear').click(function(){
              $('.image-preview').attr("data-content","").popover('hide');
              $('.image-preview-filename').val("");
              $('.image-preview-clear').hide();
              $('.image-preview-input input:file').val("");
              $(".image-preview-input-title").text("Browse"); 
            }); 

            // Create the preview image
            $(".image-preview-input input:file").change(function (){     
              var img = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
              });      

              var file = this.files[0];
              var reader = new FileReader();

            // Set preview image into the popover data-content
            reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }        

            reader.readAsDataURL(file);        
          });  
        });
    </script>
    
    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>
    
    <!--for navigation bar-->     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container container-profile">    
      <div class="row">
        <div class=" col-xs-12 col-sm-6 col-md-4 col-lg-4">
          <img src="<?php echo $src; ?>" alt="profile photo" style="width: 200px;margin: 0px auto;display: block;padding-top: 30px;" />
          <br><br>
          <form action="" method="post" enctype="multipart/form-data">
            <div class="row">  
              <div class="col-md-12" style="margin-left: 0px;">
                <!-- image-preview-filename input [CUT FROM HERE]-->
                <div class="input-group image-preview">
                  <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                  <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                      <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                      <span class="glyphicon glyphicon-folder-open"></span>
                      <span class="image-preview-input-title">Browse</span>
                      <input type="file" accept="image/png, image/jpeg, image/gif" name="image"/> <!-- rename it -->
                    </div>                      
                    <input type="submit" onclick="return doCheckUpdload()" name="submit" class="btn btn-default image-preview-input" value="Upload"/>
                  </span>
                </div><!-- /input-group image-preview [TO HERE]--> 
              </div>
            </div>
          </form>
          <br>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Barangay <?php echo $_SESSION['brms_brgyName']; ?> Profile</h3>
            </div>
            <div class="panel-body">
              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">                
                <!--Address-->

                <div class="form-group input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-road"></i> Address</span>
                  <input id="address" type="text" class="form-control" name="address" placeholder="Address" required value="<?php echo $_SESSION['brms_brgyAddress']; ?>">
                </div>

                <!--Mobile and Tel-->
                
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i> Telefax</span>
                      <input id="mobile" type="text" class="form-control" name="telefax" placeholder="(070) 412 - 3456" value="<?php echo $_SESSION['brms_brgyTelefax']; ?>">
                    </div>
                  
                  
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i> Email</span>
                      <input id="telephone" type="email" class="form-control" name="email" placeholder="email@example.com" value="<?php echo $_SESSION['brms_brgyEmail']; ?>">
                    </div>

                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i> Website</span>
                      <input id="telephone" type="text" class="form-control" name="website" placeholder=" www.example.com" value="<?php echo $_SESSION['brms_brgyWebsite']; ?>">
                    </div>    
                
                <input name="submit" type="submit" onclick="return doCheckUpdate()" value="Save" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                <span onclick="window.open('admin_home.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    </body>

  </html>
