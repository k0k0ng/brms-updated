<?php
  require('../session/brgy_admin.php'); // Secure Connection
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">
    <link rel="stylesheet" href="../css/live_search.css"> <!-- LIVE SEARCH CSS -->

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
        // On Submit
        function doCheck(){

          if (confirm("Are you sure to print this business clearance?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>
    
    <script>
      var cap_name = "<?php echo $_SESSION['brms_cap_name']; ?>";
      var wad1_name = "<?php echo $_SESSION['brms_wad1_name']; ?>";
      var wad2_name = "<?php echo $_SESSION['brms_wad2_name']; ?>";
      var wad3_name = "<?php echo $_SESSION['brms_wad3_name']; ?>";
      var wad4_name = "<?php echo $_SESSION['brms_wad4_name']; ?>";
      var wad5_name = "<?php echo $_SESSION['brms_wad5_name']; ?>";
      var wad6_name = "<?php echo $_SESSION['brms_wad6_name']; ?>";
      var wad7_name = "<?php echo $_SESSION['brms_wad7_name']; ?>";

      // LOAD PUROK
      $( document ).ready(function() {
        if(cap_name !== ''){
          $('#official_list').append("<option>"+cap_name+"</option>");
        }
        if(wad1_name !== ''){
          $('#official_list').append("<option>"+wad1_name+"</option>");
        }
        if(wad2_name !== ''){
          $('#official_list').append("<option>"+wad2_name+"</option>");
        }
        if(wad3_name !== ''){
          $('#official_list').append("<option>"+wad3_name+"</option>");
        }
        if(wad4_name !== ''){
          $('#official_list').append("<option>"+wad4_name+"</option>");
        }
        if(wad5_name !== ''){
          $('#official_list').append("<option>"+wad5_name+"</option>");
        }
        if(wad6_name !== ''){
          $('#official_list').append("<option>"+wad6_name+"</option>");
        }
        if(wad7_name !== ''){
          $('#official_list').append("<option>"+wad7_name+"</option>");
        }

      });
    </script>

  <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!-- Profile Divider --> 

    <div class="container container-profile">    
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Barangay Business Clearance</h3>
            </div>
            <div class="panel-body">
              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="../tcpdf_api/examples/business_clearance.php" target="_blank">
                <!--FML Name-->
                <div class="input-group" style="display: none">
                  <span class="input-group-addon">Business ID</span>
                  <input id="business_id" type="text" class="form-control" name="business_id" value="<?php echo $_SESSION['brms_business_id']; ?>">
                </div>
                <div class="input-group">
                  <span class="input-group-addon">Business Name</span>
                  <input id="business_name" type="text" class="form-control" name="business_name" readonly value="<?php echo $_SESSION['brms_business_name'].', '.$_SESSION['brms_business_address']; ?>">
                </div>
                <br>
                                
                <!--Purok, Gender and Civil Status  COMMUNITY TAX CERTIFICATE -->
                <div class="input-group">
                      <span class="input-group-addon">Cedula No.</span>
                      <input id="ctcno" type="text" class="form-control" name="ctcno" required>
                </div>
                <br>
                <!--Date and At-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Issued on</span>
                      <input id="issuedon" type="date" class="form-control" name="issuedon" required>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 input-group" style="padding-right: 15px">
                    <div class="input-group">
                      <span class="input-group-addon">At</span>
                      <input id="location" type="text" class="form-control" name="location" value="Davao City" required>
                    </div>   
                </div>
                </div>
                <div class="input-group">
                      <span class="input-group-addon">Purpose</span>
                      <input id="purpose" type="text" class="form-control" name="purpose" required>
                </div>
                <br>

                <!-- Receipt No -->
                <div class="input-group">
                  <span class="input-group-addon">Receipt No.</span>
                  <input id="receipt" type="text" class="form-control" name="receipt">
                </div>                
                <br>
                
                <div class="input-group">
                      <span class="input-group-addon">Barangay Official</span>
                      <select id="official_list" name="official_list" class="form-control"></select>
                </div>              

                <br>
                <input id="submit" name="submit" type="submit" onclick="return doCheck()" value="Print" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                <span onclick="window.open('profile_business.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
