<?php
  require('../session/brgy_admin.php'); // Secure Connection
?> 

<!DOCTYPE html>
<html lang="en">

<head>

  <title>BRMS - Barangay Record Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/custom2.css"> 
  <link rel="stylesheet" href="../css/side_nav.css">     

  <link rel="javascript" src="../js/jquery.js">
  <link rel="javascript" src="../js/jquery.min.js">

  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->
  <link rel="stylesheet" href="../css/table_Design.css"> <!-- COSTUMIZE TABLE COLOR -->
  <link rel="stylesheet" href="../css/table_minimal.css"> <!-- COSTUMIZE TABLE COLOR -->

  <!--Website Tab Icon-->
  <link rel="icon" type="image/png" href="../images/logo.png"/>

  <script>
      // OnKeyRelease
      function check_blood_type(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer1").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_bloodType.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_business_establishment(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer2").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_businessEstablishment.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_currently_employed(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer3").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_currentlyEmployed.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_currently_enrolled(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer4").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_currentlyEnrolled.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_gender(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer5").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_gender.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_minor(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer6").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_minor.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_pwd(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer7").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_pwd.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_voter(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer8").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_voter.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_religion(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer9").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_religion.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_residence_type(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer10").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_residenceType.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script>
      // OnKeyRelease
      function check_senior_citizen(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer11").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_seniorCitizen.php?purok_name="+str, true);
        xmlhttp.send();        
      }
  </script>

  <script> 

      // Document Ready
      $(document).ready(function(){
        check_blood_type('');
        check_business_establishment('');
        check_currently_employed('');
        check_currently_enrolled('');
        check_gender('');
        check_minor('');
        check_pwd('');
        check_voter(''); 
        check_religion('');
        check_residence_type('');  
        check_senior_citizen('');

        $('#table_Value1').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/blood_type_list.php?purok_name="+purok_name+"&column="+column);     
         }
        });

        $('#table_Value2').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/business_establishment_list.php?purok_name="+purok_name);     
         }
        });

        $('#table_Value3').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/currently_employed_list.php?purok_name="+purok_name);     
         }
        });

        $('#table_Value4').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/currently_enrolled_list.php?purok_name="+purok_name);     
         }
        });

        $('#table_Value5').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/gender_list.php?purok_name="+purok_name+"&column="+column);     
         }
        });

        $('#table_Value6').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/minor_list.php?purok_name="+purok_name);     
         }
        });

        $('#table_Value7').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/pwd_list.php?purok_name="+purok_name+"&column="+column);     
         }
        });

        $('#table_Value8').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/registered_voter_list.php?purok_name="+purok_name);     
         }
        });

        $('#table_Value9').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/religion_list.php?purok_name="+purok_name+"&column="+column);      
         }
        });

        $('#table_Value10').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/residence_type_list.php?purok_name="+purok_name+"&column="+column);     
         }
        });

        $('#table_Value11').on('click', 'td', function() { // td not tr         
         var currentRow=$(this).closest("tr");
         var purok_name = currentRow.find("td:eq(0)").text();
         var column = $(this).index();

         if(column != 0){
          window.open("../tcpdf_api/examples/senior_citizen_list.php?purok_name="+purok_name);     
         }
        });       

      });
  </script>

  <script>
    $(window).on("load resize ", function() {
      var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
      $('.tbl-header').css({'padding-right':scrollWidth});
    }).resize();      
  </script>

  <!--for navigation bar-->

  <script>
    $.get("navigation.php", function(data){
      $("#nav-placeholder").replaceWith(data);
    });
  </script>

  <style type="text/css">    
    td, th{
      text-align: center;
    }
    .td{
      text-align: left;
      vertical-align: text-top;
    }
  </style>

  </head>

  <body>

    <div id="nav-placeholder"></div>
    
    <div class="container">
      <div class="row">

        <!-- tabs left -->
        <div class="tabbable tabs-left">
          
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#all">All</a></li>
            <li><a data-toggle="tab" href="#blood_type">Blood Type</a></li>
            <li><a data-toggle="tab" href="#establishment">Establishment</a></li>
            <li><a data-toggle="tab" href="#employed">Employed</a></li>
            <li><a data-toggle="tab" href="#enrolled">Enrolled</a></li> 
            <li><a data-toggle="tab" href="#gender">Gender</a></li>   
            <li><a data-toggle="tab" href="#minor">Minor</a></li>   
            <li><a data-toggle="tab" href="#pwd">PWD</a></li>   
            <li><a data-toggle="tab" href="#voter">Voter</a></li>   
            <li><a data-toggle="tab" href="#religion">Religion</a></li>   
            <li><a data-toggle="tab" href="#residence">Residence Type</a></li>   
            <li><a data-toggle="tab" href="#senior">Senior Citizen</a></li>  
          </ul>
          
          <div class="tab-content">

            <div id="all" class="tab-pane fade in active">
              <form method="post" role="form" autocomplete="off" action="../tcpdf_api/examples/all.php" target="_blank">
                <div class="input-group" style="width: 80%">

                <br>
                <br>
                  
                  <input type="submit" name="print" class="btn btn-primary" style="width: 100px; float: right" value="Print">
                  <input type="button" onclick="window.open('reports.php', '_top')" class="btn btn-success" style="width: 100px; float: right; margin-right: 10px" value="Clear">
                  
                  <table style="width: 100%;" id="hor-minimalist-a">
                    <tr>
                      <td class="td">
                        <h3>Age</h3>
                        <input type="checkbox" name="age1" value="1" id="age1">
                        <label for="age1">1-2</label>                        
                        <br>
                        <input type="checkbox" name="age2" value="2" id="age2">
                        <label for="age2">3-5</label>
                        <br>
                        <input type="checkbox" name="age3" value="3" id="age3">
                        <label for="age3">6-11</label> 
                        <br>
                        <input type="checkbox" name="age4" value="4" id="age4">
                        <label for="age4">12-15</label> 
                        <br>
                        <input type="checkbox" name="age5" value="5" id="age5">
                        <label for="age5">16-17</label> 
                        <br>
                        <input type="checkbox" name="age6" value="6" id="age6">
                        <label for="age6">18-35</label>
                        <br>
                        <input type="checkbox" name="age7" value="7" id="age7">
                        <label for="age7">36-64</label>
                        <br>
                        <input type="checkbox" name="age8" value="8" id="age8">
                        <label for="age8">65 and Above</label>
                        <br>
                        <input type="checkbox" name="ageOther" value="9" onclick="if(this.checked){ document.getElementById('age9').focus();}" id="age99">
                        <input type="number" min="0" step="1" name="age9" onclick="document.getElementById('age99').checked = true;" id="age9">
                      </td>

                      <td class="td">
                        <h3>Blood Type</h3>
                        <input type="checkbox" name="bloodtype1" value="letter" id="bloodtype1">
                        <label for="bloodtype1">A+</label> 
                        <br> 
                        <input type="checkbox" name="bloodtype2" value="letter" id="bloodtype2">
                        <label for="bloodtype2">A-</label>  
                        <br>
                        <input type="checkbox" name="bloodtype3" value="letter" id="bloodtype3">
                        <label for="bloodtype3">B+</label>  
                        <br>
                        <input type="checkbox" name="bloodtype4" value="letter" id="bloodtype4">
                        <label for="bloodtype4">B-</label> 
                        <br>
                        <input type="checkbox" name="bloodtype5" value="letter" id="bloodtype5">
                        <label for="bloodtype5">AB+</label> 
                        <br>
                        <input type="checkbox" name="bloodtype6" value="letter" id="bloodtype6">
                        <label for="bloodtype6">AB-</label> 
                        <br>
                        <input type="checkbox" name="bloodtype7" value="letter" id="bloodtype7">
                        <label for="bloodtype7">O+</label> 
                        <br>
                        <input type="checkbox" name="bloodtype8" value="letter" id="bloodtype8">
                        <label for="bloodtype8">O-</label> 
                        <br>
                        <input type="checkbox" name="bloodtype9" value="letter" id="bloodtype9">
                        <label for="bloodtype9">Unknown</label> 
                        <br>
                      </td>

                      <td class="td">
                        <h3>Curretly Enrolled</h3>
                        <input type="checkbox" name="enrolled1" value="letter" id="enrolled1">
                        <label for="enrolled1">Yes</label>
                        <br>
                        <input type="checkbox" name="enrolled2" value="letter" id="enrolled2">
                        <label for="enrolled2">No</label>
                      </td>
                    </tr>

                    <tr class="td">
                      <td class="td">
                        <h3>Curretly Employed</h3>
                        <input type="checkbox" name="employed1" value="letter" id="employed1">
                        <label for="employed1">Yes</label>
                        <br>
                        <input type="checkbox" name="employed2" value="letter" id="employed2">
                        <label for="employed2">No</label>
                      </td>

                      <td class="td">
                        <h3>Gender</h3>
                        <input type="checkbox" name="gender1" value="letter" id="gender1">
                        <label for="gender1">Male</label>
                        <br>
                        <input type="checkbox" name="gender2" value="letter" id="gender2">
                        <label for="gender2">Female</label>
                      </td>

                      <td class="td">
                        <h3>PWD</h3>
                        <input type="checkbox" name="pwd1" value="letter" id="pwd1">
                        <label for="pwd1">Blind</label> 
                        <br>
                        <input type="checkbox" name="pwd2" value="letter" id="pwd2">
                        <label for="pwd2">Deaf</label>
                        <br>
                        <input type="checkbox" name="pwd3" value="letter" id="pwd3">  
                        <label for="pwd3">Mute</label>
                        <br>  
                        <input type="checkbox" name="pwd4" value="letter" id="pwd4">
                        <label for="pwd4">Others</label>
                      </td>
                    </tr>

                    <tr>
                      <td class="td">
                        <h3>Registered Voter</h3>
                        <input type="checkbox" name="voter1" value="letter" id="voter1">
                        <label for="voter1">Yes</label>
                        <br>
                        <input type="checkbox" name="voter2" value="letter" id="voter2">
                        <label for="voter2">No</label>
                      </td>                      

                      <td class="td">
                        <h3>Residence Type</h3>
                        <input type="checkbox" name="residence1" value="letter" id="residence1">
                        <label for="residence1">Owned</label>
                        <br>
                        <input type="checkbox" name="residence2" value="letter" id="residence2"> 
                        <label for="residence2">Leased</label>
                        <br>
                        <input type="checkbox" name="residence3" value="letter" id="residence3">
                        <label for="residence3">Rented</label>
                        <br>
                        <input type="checkbox" name="residence4" value="letter" id="residence4">
                        <label for="residence4">Boarder</label>
                        <br>
                        <input type="checkbox" name="residence5" value="letter" id="residence5">
                        <label for="residence5">Otherwise</label>
                      </td>

                      <td class="td">
                        <h3>Senior Citizen</h3>
                        <input type="checkbox" name="senior1" value="letter" id="senior1">
                        <label for="senior1">Yes</label>
                        <br>
                        <input type="checkbox" name="senior2" value="letter" id="senior2">
                        <label for="senior2">No</label>
                      </td>

                    </tr>

                    <tr>
                      
                      <td colspan="3" class="td">
                        <h3>Religion</h3>
                        <input type="checkbox" name="religion1" value="letter" id="religion1">
                        <label for="religion1">Roman Catholic</label>
                        <br>
                        <input type="checkbox" name="religion2" value="letter" id="religion2">
                        <label for="religion2">Islam</label>
                        <br>
                        <input type="checkbox" name="religion3" value="letter" id="religion3">
                        <label for="religion3">Protestant</label>
                        <br>
                        <input type="checkbox" name="religion4" value="letter" id="religion4">
                        <label for="religion4">Iglesia ni Cristo</label>
                        <br>
                        <input type="checkbox" name="religion5" value="letter" id="religion5">
                        <label for="religion5">Jesus Miracle Crusade International Ministry</label>
                        <br>
                        <input type="checkbox" name="religion6" value="letter" id="religion6">
                        <label for="religion6">Members Church of God International</label>
                        <br>
                        <input type="checkbox" name="religion7" value="letter" id="religion7">
                        <label for="religion7">Most Holy Church of God in Christ Jesus</label>
                        <br>
                        <input type="checkbox" name="religion8" value="letter" id="religion8">
                        <label for="religion8">Philippine Independent Church</label>
                        <br>
                        <input type="checkbox" name="religion9" value="letter" id="religion9">
                        <label for="religion9">Apostolic Catholic Church</label>
                        <br>
                        <input type="checkbox" name="religion10" value="letter" id="religion10">
                        <label for="religion10">Orthodoxy</label>
                        <br>
                        <input type="checkbox" name="religion11" value="letter" id="religion11">
                        <label for="religion11">The Kingdom of Jesus Christ the Name Above Every Name</label>
                        <br>
                        <input type="checkbox" name="religion12" value="letter" id="religion12">
                        <label for="religion12">Judaism</label>
                        <br>
                        <input type="checkbox" name="religion13" value="letter" id="religion13">
                        <label for="religion13">Hinduism</label>
                        <br>
                        <input type="checkbox" name="religion14" value="letter" id="religion14">
                        <label for="religion14">Atheism</label>
                        <br>
                        <input type="checkbox" name="religion15" value="letter" id="religion15">
                        <label for="religion15">Others</label>
                      </td>

                    </tr> 

                  </table>

                </div>
              </form>
            </div> <!-- /all -->
            
            <div id="blood_type" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">
              
              <br>
              <br>

              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_blood_type(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/blood_type.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>

              <div id="table_Value1" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th colspan="2">Purok Name</th>
                      <th>A+</th>
                      <th>A-</th>
                      <th>B+</th>
                      <th>B-</th>
                      <th>AB+</th>
                      <th>AB-</th>
                      <th>O+</th>
                      <th>O-</th>
                      <th>Unknown</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer1"></tbody>
                </table>
              </div> 

            </div> <!-- /blood type -->

            <div id="establishment" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_business_establishment(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/business_establishment.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>

              <div id="table_Value2" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Establishments</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer2"></tbody>
                </table>
              </div> 

            </div> <!-- /establishment -->
            
            <div id="employed" class="tab-pane fade in  col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_currently_employed(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/currently_employed.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>

              <div id="table_Value3" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Employed</th>
                      <th>Population</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer3"></tbody>
                </table>
              </div> 

            </div> <!-- /employed -->
            
            <div id="enrolled" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_currently_enrolled(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/currently_enrolled.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>

              <div id="table_Value4" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Enrolled</th>
                      <th>Population</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer4"></tbody>
                </table>
              </div> 

            </div> <!-- /enrolled -->

            <div id="gender" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_gender(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/gender.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>

              <div id="table_Value5" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Male</th>
                      <th>Female</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer5"></tbody>
                </table>
              </div> 

            </div> <!-- /gender -->

            <div id="minor" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_minor(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/minor.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>

              <div id="table_Value6" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Minor</th>
                      <th>Population</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer6"></tbody>
                </table>
              </div> 

            </div> <!-- /minor -->

            <div id="pwd" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_pwd(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/pwd.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>

              <div id="table_Value7" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Blind</th>
                      <th>Deaf</th>
                      <th>Mute</th>
                      <th>Others</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer7"></tbody>
                </table>
              </div> 

            </div> <!-- /pwd -->

            <div id="voter" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_voter(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/registered_voter.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>

              <div id="table_Value8" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Registered Voter</th>
                      <th>Population</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer8"></tbody>
                </table>
              </div> 

            </div> <!-- /voter -->

            <div id="religion" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <div class="table-responsive"> 

                <table>                  
                  <tr>
                    <td class="td"><b>A</b> = Roman Catholic</td>
                    <td class="td"><b>I</b> = Apostolic Catholic Church</td>
                  </tr>
                  <tr>
                    <td class="td"><b>B</b> = Islam</td>
                    <td class="td"><b>J</b> = Orthodoxy</td>
                  </tr>
                  <tr>
                    <td class="td"><b>C</b> = Protestant</td>
                    <td class="td"><b>J</b> = Orthodoxy</td>
                  </tr>
                  <tr>
                    <td class="td"><b>D</b> = Iglesia ni Cristo</td>
                    <td class="td"><b>L</b> = Judaism</td>
                  </tr>
                  <tr>
                    <td class="td"><b>E</b> = Jesus Miracle Crusade International Ministry</td>
                    <td class="td"><b>M</b> = Hinduism</td>
                  </tr>
                  <tr>
                    <td class="td"><b>F</b> = Members Church of God International</td>
                    <td class="td"><b>N</b> = Atheism</td>
                  </tr>
                  <tr>
                    <td class="td"><b>G</b> = Most Holy Church of God in Christ Jesus</td>
                    <td class="td"><b>O</b> = Others</td>
                  </tr>    
                  <tr>
                    <td class="td"><b>H</b> = Philippine Independent Church</td>
                  </tr>              
                </table>

              </div> 
              
              <br>

              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_religion(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/religion.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>            
              
              <div id="table_Value9" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th colspan="2">Purok Name</th>
                      <th>A</th>
                      <th>B</th>
                      <th>C</th>
                      <th>D</th>
                      <th>E</th>
                      <th>F</th>
                      <th>G</th>
                      <th>H</th>
                      <th>I</th>
                      <th>J</th>
                      <th>K</th>
                      <th>L</th>
                      <th>M</th>
                      <th>N</th>
                      <th>O</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer9"></tbody>
                </table>
              </div> 

            </div> <!-- /religion -->

            <div id="residence" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_residence_type(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/residence_type.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>            
              
              <div id="table_Value10" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Owned</th>
                      <th>Leased</th>
                      <th>Rented</th>
                      <th>Boarder</th>
                      <th>Otherwise</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer10"></tbody>
                </table>
              </div> 

            </div> <!-- /residence -->

            <div id="senior" class="tab-pane fade in col-xs-12 col-sm-12 col-md-10 col-lg-10">

            <br>
            <br>
              
              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="check_senior_citizen(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Purok Name">
                  <span class="input-group-addon">
                    <a target="_blank" href="../tcpdf_api/examples/senior_citizen.php?">(Print)</a>
                  </span> 
                </div>
              </form>
              
              <br>            
              
              <div id="table_Value11" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Purok Name</th>
                      <th>Member</th>
                      <th>Population</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer11"></tbody>
                </table>
              </div> 

            </div> <!-- /senior -->

          </div> <!-- /content -->
          
        </div> <!-- /tabs -->
        

      </div> <!-- /row -->  
    </div> <!-- /container -->

  </body>

</html>