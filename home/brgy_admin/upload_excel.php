<?php
  require('../session/brgy_admin.php'); // Secure Connection
?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <title>BRMS - Barangay Record Management System</title>

    <link rel="stylesheet" href="../css/loading.css"> <!-- LOADING -->
    <link rel="stylesheet" href="../css/animation.css"> <!-- ANIMATION -->   

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    
    <script>
      // On Submit
      function doCheck(){

        // Confirmation
        if (confirm("Are you sure to upload this excel file?") == true) {
          show_loading();
          return true;
        } else {
          return false;
        }
        
      }
    </script>

    <script>
      function show_loading(){
        $('#loading').addClass('w3-animate-opacity');
        document.getElementById('loading').style.display = 'block';
      }
    </script>

    <!--for navigation bar-->  

    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>    

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!--SEARCH BAR-->
    <div class="container container-profile">    
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Upload Excel File</h3>
            </div>

            <div class="panel-body">
              <form method="POST" action="excel-upload/excelUpload.php" enctype="multipart/form-data" target="_self">
                <div class="form-group">
                  <input type="file" name="file" class="form-control"  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                </div>

                <button onclick="return doCheck()" type="submit" name="Submit" class="btn btn-success" style="width: 200px; float: right;">Upload</button>                
              </form>
            </div>
            
          </div>          

        </div>
      </div>

      <div id="loading" class="row" style="display: none">
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-sm-offset-5 col-md-offset-5 col-lg-offset-5">
          <h3 align="center">Uploading</h3>
          <table class="table table">
            <tbody>
              <tr>
                <td align="center" style="border:none"><div class="loader"></div>
                </td>
              </tr>
            </tbody>
          </table>          
        </div>
      </div>

    </div>

  </body>

</html>