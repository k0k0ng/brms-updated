<?php
  require('../session/brgy_admin.php'); // Secure Connection
?>

  <!DOCTYPE html>
  <html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">    

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <link rel="stylesheet" href="../css/table_Color2.css"> <!-- COSTUMIZE TABLE COLOR -->

    <!--for navigation bar-->

    <script>
      // OnKeyRelease
      function checkTemplate(header){

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>";

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_template.php?header="+header+"&brgy_id="+brgy_id, true);
        xmlhttp.send();        
      }
    </script>

    <script> 
      // Document Ready
      $(document).ready(function(){
        checkTemplate('');
        // On Click Table
        $("#table_Value").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var header = currentRow.find("td:eq(1)").text(); // get current row 1st TD value
          var paragraph1 = currentRow.find("td:eq(2)").text(); // get current row 1st TD value
          var paragraph2 = currentRow.find("td:eq(3)").text(); // get current row 1st TD value
          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

          window.location.href="../session/brgy_admin_update_session_template.php?"+"id="+id+"&brgy_id="+brgy_id+"&header="+header+"&paragraph1="+paragraph1+"&paragraph2="+paragraph2; // Update Sesssion (CLIENT)

          //window.location.href="certification_template.php?"+"id="+id+"&brgy_id="+brgy_id+"&header="+header+"&paragraph1="+paragraph1+"&paragraph2="+paragraph2; // Update Sesssion (CLIENT
        });

      });
    </script>

    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>
    
    <div class="container">
      <div class="row">

        <form method="post" role="form" autocomplete="off" action="">
          <div class="input-group">
            <span class="input-group-addon">Search Certification Template</span>                  
            <input id="purok_name" type="text" onkeyup="checkTemplate(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Header">
          </div>
        </form>

        <br>

        <div id="table_Value" class="table-responsive"> 
          <table class="table table-condensed table-hover">
            <thead>
              <tr>
                <th>Header</th>
                <th>Paragraph 1</th>
                <th>Paragraph 2</th>
              </tr>
            </thead>                    
            <tbody id="responsecontainer"></tbody>
          </table>
        </div> 

      </div> <!-- /row -->  
    </div> <!-- /container -->

  </body>

</html>