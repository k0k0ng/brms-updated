<?php
  session_start();
?>

<div class="example3">
  <nav class="navbar navbar-inverse navbar-static-top" style="background-color: #7a0404;">
    <div class="container">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        
        <a href="admin_home.php"><img src="../brgy_image/brms_logo.png" style="height: 60px;margin-top: 10px;"></a>
        <h4 style="color: white;position: absolute;bottom: 20px;margin-left: 250px;">(<?php echo $_SESSION['brms_brgyName']; ?>)</h4>
      </div>

      <div id="navbar3" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
                  
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="background-color: #7a0404;">Barangay Staff (<?php echo $_SESSION['brms_userFname']; ?>)<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" style="padding: 0px; border: 0px;" >
              <li>
                <div class="container-fluid" style="padding: 0px;">
                  <div class="row">
                    <div class="col-md-12">   
                      <a href="../session/brgy_admin_update_session_admin.php" class="list-group-item list-group-item-action">View Profile</a>
                      <a href="change_password.php" class="list-group-item list-group-item-action">Change Password</a>
                      <a href="../session/brgy_logout.php" class="list-group-item list-group-item-action">Log out</a>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>

          <li><a href="admin_home.php">Home</a></li>
          <li><a href="form_complaint.php">Complaint</a></li>
          <li><a href="reports.php">Reports</a></li>
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="background-color: #7a0404;">Settings<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" style="padding: 0px; border: 0px;" >
              <li>
                <div class="container-fluid" style="padding: 0px;">
                  <div class="row">
                    <div class="col-md-12">   
                      <a href="brgy_official.php" class="list-group-item list-group-item-action" >Barangay Officials</a>
                      <a href="brgy_profile.php" class="list-group-item list-group-item-action">Barangay Profile</a>
                      <a href="certification_template_settings.php" class="list-group-item list-group-item-action">Certification Template</a>
                      <a href="purok.php" class="list-group-item list-group-item-action">Purok</a>                      
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>

          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="background-color: #7a0404;">Add<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" style="padding: 0px; border: 0px;" >
              <li>
                <div class="container-fluid" style="padding: 0px;">
                  <div class="row">
                    <div class="col-md-12">
                      <a href="add_business.php" class="list-group-item list-group-item-action">Business</a>  
                      <a href="../session/brgy_admin_update_session_template_add.php" class="list-group-item list-group-item-action">Certification Template</a>   
                      <a href="add_citizen.php" class="list-group-item list-group-item-action">Citizen</a>
                      <a href="add_feetype.php" class="list-group-item list-group-item-action">Fee Type</a>
                      <a href="../session/brgy_admin_update_session_add_purok.php" class="list-group-item list-group-item-action">Purok</a>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div><!--/.nav-collapse -->     
    </div><!--/.container-fluid -->
  </nav>
</div>