<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../session/brgy_admin_profile.php'); // Secure Profile Connection
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
        // On Submit
        function doCheck(){

          if (confirm("Are you sure to print this certification?")) {
            
            var header = $('#certification_list').find(":selected").text().trim();
           
            // Header is Empty
            if(!header.length){
              alert('Please Create Certification Template!');
              window.location.href = "../session/brgy_admin_update_session_template_add.php";
              return false;
            }            
            else{
              return true;
            }

          } else {
            return false;
          }          
          
        }
    </script>

    <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

    <script>
      var cap_name = "<?php echo $_SESSION['brms_cap_name']; ?>";
      var wad1_name = "<?php echo $_SESSION['brms_wad1_name']; ?>";
      var wad2_name = "<?php echo $_SESSION['brms_wad2_name']; ?>";
      var wad3_name = "<?php echo $_SESSION['brms_wad3_name']; ?>";
      var wad4_name = "<?php echo $_SESSION['brms_wad4_name']; ?>";
      var wad5_name = "<?php echo $_SESSION['brms_wad5_name']; ?>";
      var wad6_name = "<?php echo $_SESSION['brms_wad6_name']; ?>";
      var wad7_name = "<?php echo $_SESSION['brms_wad7_name']; ?>";

      // LOAD PUROK
      $( document ).ready(function() {
        if(cap_name !== ''){
          $('#official_list').append("<option>"+cap_name+"</option>");
        }
        if(wad1_name !== ''){
          $('#official_list').append("<option>"+wad1_name+"</option>");
        }
        if(wad2_name !== ''){
          $('#official_list').append("<option>"+wad2_name+"</option>");
        }
        if(wad3_name !== ''){
          $('#official_list').append("<option>"+wad3_name+"</option>");
        }
        if(wad4_name !== ''){
          $('#official_list').append("<option>"+wad4_name+"</option>");
        }
        if(wad5_name !== ''){
          $('#official_list').append("<option>"+wad5_name+"</option>");
        }
        if(wad6_name !== ''){
          $('#official_list').append("<option>"+wad6_name+"</option>");
        }
        if(wad7_name !== ''){
          $('#official_list').append("<option>"+wad7_name+"</option>");
        }      

        $('#paragraph1').click(function() { setParagraphActiveParagraph1(); });
        $('#paragraph2').click(function() { setParagraphActiveParagraph2(); });

        loadCertification();    
        setParagraphActiveParagraph1(); // Set Paragraph1 Active    

        // Tab Key
        $(document).keyup(function(e) {
          if (e.keyCode == "9" ) {
            var x = document.activeElement.id;
            if(x === "paragraph2"){
              insertTag("\t");
            }
          }
        });

      });
    </script>

    <script>
      function loadCertification(){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            var data = this.responseText.split(",");          

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#certification_list').append("<option>"+data[i]+"</option>");                
                if(i == 0){
                  setContent(data[i]);
                }                
              }
            }

          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

        xmlhttp.open("GET", "../ajax/loadAjax_header.php?brgy_id="+brgy_id, true); // Get purok names on (loadAjax_purok.php)
        xmlhttp.send();
      }
    </script>

    <script>
      function loadCertificationContentParagraph(header,paragraph){
        var xmlhttp = new XMLHttpRequest();
        
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById(paragraph).value = this.responseText.trim(); 
          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

        xmlhttp.open("GET", "../ajax/loadAjax_certificationContent.php?brgy_id="+brgy_id+"&paragraph="+paragraph+"&header="+header, true); 
        xmlhttp.send();
      }
    </script>

    <script>
      function setContent(header){
        loadCertificationContentParagraph(header,"paragraph1");
        // loadCertificationContentParagraph(header,"paragraph2");
      }
    </script>

    <script>

      var paragraph; // Paragraph Active

      function setParagraphActiveParagraph1(){
        paragraph = true;
      }

      function setParagraphActiveParagraph2(){
        paragraph = false;
      }

      // Insert Tag
      function insertTag(text) {

        var paragraphID = (paragraph) ? "paragraph1":"paragraph2";     

        var textarea = document.getElementById(paragraphID);
        var currentPos = $('#'+paragraphID).prop("selectionStart");
        var strLeft = textarea.value.substring(0, currentPos);
        var strRight = textarea.value.substring(currentPos, textarea.value.length);
        textarea.value = strLeft + text + strRight; // Set TaxeArea Value
        setCaretToPos(document.getElementById(paragraphID), parseInt(currentPos) + text.length);    
      }

      // Set Range
      function setSelectionRange(input, selectionStart, selectionEnd) {
        if (input.setSelectionRange) {
          input.focus();
          input.setSelectionRange(selectionStart, selectionEnd);
        }
        else if (input.createTextRange) {
          var range = input.createTextRange();
          range.collapse(true);
          range.moveEnd('character', selectionEnd);
          range.moveStart('character', selectionStart);
          range.select();
        }
      }

      // Set Current Position
      function setCaretToPos(input, pos) {
        setSelectionRange(input, pos, pos);
      }
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!-- Profile Divider --> 

    <div class="container container-profile">    
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">

          <div> 
            <h4 class="text-secondary"><b>Tags - Data from the user (click below to insert in the template)</b></h4>
            <table class="table table-condensed table-hover text-warning" style="cursor: pointer;">                  
              <tr>
                <td><b onclick="insertTag('CITIZEN_NAME')">CITIZEN_NAME</b></td>
                <td><b onclick="insertTag('CITIZEN_ADDRESS')">CITIZEN_ADDRESS</b></td>
                <td><b onclick="insertTag('PUROK')">PUROK</b></td>               
              </tr>
              <tr>
                <td><b onclick="insertTag('HIS_HER')">HIS_HER</b></td>
                <td><b onclick="insertTag('MALE_FEMALE')">MALE_FEMALE</b></td>
                <td><b onclick="insertTag('HE_SHE')">HE_SHE</b></td>                               
              </tr>
              <tr>                
                <td><b onclick="insertTag('STATUS')">STATUS</b></td>                 
                <td><b onclick="insertTag('BIRTHDATE')">BIRTHDATE</b></td>
                <td><b onclick="insertTag('AGE')">AGE</b></td>                
              </tr>
              <tr>          
                <td><b onclick="insertTag('EDUCATION')">EDUCATION</b></td>      
                <td><b onclick="insertTag('OCCUPATION')">OCCUPATION</b></td>
                <td><b onclick="insertTag('RELIGION')">RELIGION</b></td>
              </tr>     
              <tr>          
                <td><b onclick="insertTag('BLOOD_TYPE')">BLOOD_TYPE</b></td>
                <td><b onclick="insertTag('RESIDENCE_TYPE')">RESIDENCE_TYPE</b></td>                
                <td><b onclick="insertTag('OCCUPATION')">OCCUPATION</b></td>
              </tr>   
              <tr>          
                <td><b onclick="insertTag('BRGY_NAME')">BRGY_NAME</b></td> 
                <td><b onclick="insertTag('BRGY_ID')">BRGY_ID</b></td>
                <td><b onclick="insertTag('CITY')">CITY</b></td>
                <td></td>
              </tr> 
              <tr style="color: #000000; cursor: auto;">          
                <td>
                  <h4 class="text-secondary"><b>Settings</b></h4>
                </td>
              </tr>
              <tr style="color: #0000ff">          
                <td><b onclick="insertTag('[')">_START_BOLD_FONT</b></td>
                <td><b onclick="insertTag(']')">_END_BOLD_FONT</b></td>
              </tr>
              <tr style="color: #0000ff">          
                <td><b onclick="insertTag('{')">_START_ITALIC_FONT</b></td>
                <td><b onclick="insertTag('}')">_END_ITALIC_FONT</b></td>
              </tr>
            </table>
          </div> 

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3>Certification</h3>
            </div>
            <div class="panel-body">
              <form method="post" name="confirmationForm" id="confirmationForm" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="../tcpdf_api/examples/certification.php" target="_blank">
                
                <!-- Header -->
                <div class="input-group">
                  <span class="input-group-addon">Header</span>                
                  <select id="certification_list" onchange="(setContent(this.value))" name="certification_list" class="form-control"></select>
                </div>    
                
                <br>

                <!-- TextArea -->
                <textarea form="confirmationForm" rows="5" id="paragraph1" type="textarea" class="form-control" name="paragraph1" required placeholder="Content"></textarea>
                <br>

                <textarea form="confirmationForm" rows="2" id="paragraph2" type="textarea" class="form-control" name="paragraph2" readonly><?php 
                    // $cur_date = "date('jS \d\a\y \o\f F Y')";
                    $cur_day = date("dS");
                    $cur_month_year = date("F Y");
                    $brgy_address = $_SESSION['brms_brgyAddress'];
                    $texts = 'Issued on '.$cur_day . ' day of ' .$cur_month_year. ' at the office of the Punong Barangay, '.$brgy_address.'.';
                    echo $texts;
                ?>
                </textarea>
                <br>
                
                <!-- Receipt No -->
                <div class="input-group">
                  <span class="input-group-addon">Receipt No.</span>
                  <input id="receipt" type="text" class="form-control" name="receipt">
                </div>                
                <br>

                <div class="input-group">
                      <span class="input-group-addon">Signatory</span>
                      <select id="official_list" name="official_list" class="form-control"></select>
                </div>              

                <br>
                <input name="submit" type="submit" onclick="return doCheck()"  value="Print" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                <span onclick="window.open('profile.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
