<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
?>

<?php
  // INSERT BUSINESS
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Add") {

    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 
    
    $name = $_POST["business_name"];
    $info_id = $_POST["owner_id"];    
    $owner = $_POST["owner"];
    $establish = $_POST["establish"];
    $status = $_POST["status"];
    $address = $_POST["address"];
    $permit = $_POST["permit"];
    $contact = $_POST["contact"];
    $type = $_POST["business_type"];
    $form = $_POST["business_form"];    

    $query = new database_query(); // Database Query (initialize connection)
    $purok_id = $query -> select_purok_id($brgy_id,$_POST["purok_list"]); // Get Purok ID

    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> insert_business($brgy_id,$purok_id,$form,$type,$info_id,$name,$address,$establish,$status,$permit,$contact); // Get Result

    // Duplicate
    if($result == -1){

      $query = new database_query(); // Database Query (initialize connection)
      $business_id = $query -> select_business_id($brgy_id,$name,$address); // Get Business ID

      echo "<script> 
              alert('Duplicate Exist!');
              location = '../session/brgy_admin_update_session_business.php?business_id='+$business_id+'&brgy_id='+$brgy_id;
            </script>";
    }
    // Success
    else if($result == 1){

      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Add New Business " . $name ); // Insert Log 

      $query = new database_query(); // Database Query (initialize connection)
      $business_id = $query -> select_last_business_id($brgy_id); // Get Latest Business

      echo "<script> 
              alert('Business successfully added!');
              location = '../session/brgy_admin_update_session_business.php?business_id='+$business_id+'&brgy_id='+$brgy_id;
            </script>";

    }
    // Error
    else{
      echo "<script> alert('Error'); </script>";
    }

  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">
    <link rel="stylesheet" href="../css/live_search.css"> <!-- LIVE SEARCH CSS -->

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
        // On Submit
        function doCheck(){

          if (confirm("Are you sure to add this business?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

    <script>      
      $( document ).ready(function() {
        $(document).click(function(e){ hide_live_search(); }); // Document Click

        document.getElementById("submit").disabled = true; // Disable Submit

        load_purok();
        load_business_form();
        load_business_type();
        hide_live_search();

        $("#table_Value").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var name = currentRow.find("td:eq(1)").text(); // get current row 2nd TD value

          document.getElementById("owner").value = name;
          document.getElementById("owner_id").value = id;
          document.getElementById("submit").disabled = false; // Enable Submit                    
        });

      });
    </script>
    
    <script>
      // Hide Live Search
      function hide_live_search(){
        document.getElementById("responsecontainer").innerHTML = '';
      }
    </script>

    <script>
      // OnKeyRelease
      function checkUser(str){

        // Back Key Pressed
        var key = event.keyCode || event.charCode;
        if( key == 8 || key == 46 ){
          document.getElementById("owner").value = '';
          document.getElementById("owner_id").value = '0'; 
          document.getElementById("submit").disabled = true; // Disable Submit                           
        }

        if(str.length == 0){
          hide_live_search();          
        }else{
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById('responsecontainer').innerHTML = this.responseText; 
            }
          };
          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>";
          xmlhttp.open("GET", "../ajax/loadAjax_brgy_official.php?q="+str+"&brgy_id="+brgy_id, true);
          xmlhttp.send();  
        }     
      }
    </script>

    <script>
      // LOAD BUSINESS FORM
      function load_business_form(){
        //$('#purok_list').append("<option>BMW</option>")
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            var data = this.responseText.split(",");          

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#business_form').append("<option>"+data[i]+"</option>")
              }
            }
          }
        };

        xmlhttp.open("GET", "../ajax/loadAjax_businessForm.php?", true); // Get Business Forms
        xmlhttp.send();
      }
    </script>

    <script>
      // LOAD BUSINESS FORM
      function load_business_type(){
        //$('#purok_list').append("<option>BMW</option>")
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            var data = this.responseText.split(",");          

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#business_type').append("<option>"+data[i]+"</option>")
              }
            }
          }
        };

        xmlhttp.open("GET", "../ajax/loadAjax_businessType.php?", true); // Get Business Forms
        xmlhttp.send();
      }
    </script>

    <script>
      // LOAD PUROK
      function load_purok(){
        //$('#purok_list').append("<option>BMW</option>")
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            var data = this.responseText.split(",");          

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#purok_list').append("<option>"+data[i]+"</option>")
              }
            }
          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

        xmlhttp.open("GET", "../ajax/loadAjax_purok.php?brgy_id="+brgy_id, true); // Get purok names on (loadAjax_purok.php)
        xmlhttp.send();
      }
    </script>

    <!--for navigation bar-->
    
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container container-profile"> <!-- Profile Divider -->  
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Add Business</h3>
            </div>
            <div class="panel-body">
              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">
                <!--FML Name-->
                <div class="input-group">
                  <span class="input-group-addon">Business Name</span>
                  <input id="business_name" type="text" class="form-control" name="business_name" placeholder="Business Name" required autofocus>
                </div>
                <br>
                <div class="input-group" style="display: none">
                  <span class="input-group-addon">Owner ID</span>
                  <input id="owner_id" name="owner_id" value="0" type="text" class="form-control" placeholder="">
                </div>
                <div class="input-group">
                  <span class="input-group-addon">Owner</span>
                  <input id="owner" onkeyup="checkUser(this.value)" type="text" class="form-control" name="owner" placeholder="Owner's Name" required>
                </div>
                <div id="table_Value">          
                  <table class="table table-condensed table-hover">                                          
                    <tbody id="responsecontainer" style="cursor: pointer;">
                      <tr><td>123</td></tr>
                      <tr><td>123</td></tr>
                    </tbody>
                  </table>
                </div> 
                
                <!-- Date Established and Status -->
                <div class="row">
                  <div class="col-xs-8 col-sm-8 col-md-8">
                    <div class="form-group input-group">
                        <span class="input-group-addon">Date Established</span>
                        <input id="establish" type="date" class="form-control" name="establish" placeholder="Birthdate" required>
                      </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4 input-group" style="padding-right: 15px">
                    <div class="form-group input-group">
                        <span class="input-group-addon">Status</span>
                        <select id="status" name="status" class="form-control">
                          <option>Active</option>
                          <option>Inactive</option>
                        </select>
                      </div>
                  </div>
                </div>

                <!--Address and Purok-->
                <div class="row">
                  <div class="col-xs-8 col-sm-8 col-md-8">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Address</span>
                      <input id="address" type="text" class="form-control" name="address" placeholder="Address" required>
                    </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4 input-group" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Purok</span>
                      <select id="purok_list" name="purok_list" class="form-control"></select>
                    </div>
                  </div>
                </div>

                <!--Business Permit-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Business Permit No.</span>
                      <input id="permit" type="text" class="form-control" name="permit" placeholder="X 000000-0">
                    </div>
                  </div>

                <!--Contact Number-->
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Contact No.</span>
                      <input id="contact" type="text" class="form-control" name="contact" placeholder="Mobile / Telephone">
                    </div>
                  </div>
                </div>

                <!--Residence Type and Purok-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Business Organization Form</span>
                      <select id="business_form" name="business_form" class="form-control"></select>
                    </div>
                  </div>


                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Business Type</span>
                      <select id="business_type" name="business_type" class="form-control"></select>
                    </div>
                  </div>
                </div>
                
                <input id="submit" name="submit" type="submit" onclick="return doCheck()" value="Add" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                <span onclick="window.open('admin_home.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>
                
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>

  </body>

</html>