<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../session/brgy_admin_view_purok.php'); // Secure View Purok Connection
?>

<?php 
  // Purok Population
  function countPopulation($purok_name) {
    include 'database.php'; // Database
 
    $sql = "SELECT COUNT(*) AS purok_count FROM user_info WHERE purok_id = '$purok_name' GROUP BY purok_id";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        return $row['purok_count'];
      }
    }else{
      return 0;
    }
  }
?>

<?php
  $_SESSION['purok_id'] = $_SESSION['brms_VIEW_purok_id'];
  $_SESSION['purok_name'] = $purok_name;
  $_SESSION['purok_leader'] = $purok_leader;
  $_SESSION['purok_population'] = $purok_population;
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">

    <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
      // OnKeyRelease
      function checkUser(str){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer").innerHTML = this.responseText;                                
          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID 
        var purok_id = "<?php echo $_SESSION['brms_VIEW_purok_id']; ?>"; // Purok ID

        xmlhttp.open("GET", "../ajax/loadAjax_purokTableCluster.php?brgy_id="+brgy_id+"&purok_id=" + purok_id + "&q=" + str, true);
        xmlhttp.send();        
      }
    </script>

    <script>
      // DocumentReady
      $(document).ready(function(){
        checkUser("");
      });
    </script>

  <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!--SEARCH BAR-->
    <div class="container-fluid">    
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">

          <div class="panel panel-default">
            <div class="panel-heading">              
            <ul class="container-fluid details list-unstyled" style="margin-top: 20px;">
                <li><p><span class="glyphicon glyphicon-home" style="width:30px;"></span>Purok Name: <?php echo $purok_name; ?></p></li>
                <li><p><span class="glyphicon glyphicon-user" style="width:30px;"></span>Purok Leader: <?php echo $purok_leader; ?> </p></li>
                <li><p><span class="glyphicon glyphicon-globe" style="width:30px;"></span>Population: <?php echo $purok_population; ?></p></li>
                <li><p><span class="glyphicon glyphicon-print" style="width:30px;"></span>(<a target="_blank" href="../tcpdf_api/examples/purok.php?">Print</a>)</p></li>     
              </ul>
            </div>

            <div class="panel-body">

              <!--Search-->
              <div class="input-group">
                <span class="input-group-addon">Search </span>                  
                <input type="text" onkeyup="checkUser(this.value)" class="form-control image-preview-filename" placeholder="Citizen's name">
              </div> 

              <br>

              <div id="table_Value" class="table-responsive">          
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>First Name</th>
                      <th>Middle Name</th>
                      <th>Last Name</th>
                      <th>Address</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer"></tbody>
                </table>
              </div>       

            </div>      

          </div>

        </div>
      </div>
    </div>

  </body>

</html>