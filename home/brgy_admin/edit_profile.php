<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../session/brgy_admin_profile.php'); // Secure Profile Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
  require('../session/brgy_admin_user_image.php'); // Get Image Src
?>

<?php
  // UPDATE (user_info)
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Save"){

    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
    $info_id = $_SESSION['brms_SESSION_userId']; // Info ID

    $query = new database_query(); // Database Query (initialize connection)
    $purok_id = $query -> select_purok_id($brgy_id,$_POST["purok"]); // Get Purok ID

    $query = new database_query(); // Database Query (initialize connection)
    // Update User Info    
    $result = $query -> update_user_info(
        $brgy_id,
        $info_id,
        $_POST["purok"],
        $_POST["first_name"],
        $_POST["last_name"],
        $_POST["middle_name"],
        $purok_id,
        $_POST["address"],
        $_POST["birthdate"],
        $_POST["gender"],
        $_POST["civilstatus_list"],
        $_POST["bloodtype_list"],
        $_POST["occupation"],
        $_POST["education_list"],
        $_POST["mobile"],
        $_POST["telephone"],
        $_POST["is_voter"],
        $_POST["voter_id"],
        $_POST["barangay_id"],

        $_POST["residence_type"],
        $_POST["is_employed"],
        $_POST["pwd"],
        $_POST["is_enrolled"],
        $_POST["is_seniorCitizenMember"],
        $_POST["deceased"],
        $_POST["religion"]
     );

    // If Admin
    if($_SESSION['brms_userId'] == $info_id){
      $_SESSION['brms_userFname'] = $_POST["first_name"];
      $_SESSION['brms_userLname'] = $_POST["last_name"];
      $_SESSION['brms_userMname'] = $_POST["middle_name"];

      $_SESSION['brms_userPurokId'] = $purok_id;
      $_SESSION['brms_userPurokName'] = $_POST["purok"];

      $_SESSION['brms_userAddress'] = $_POST["address"];
      $_SESSION['brms_userBdate'] = $_POST["birthdate"];
      $_SESSION['brms_userGender'] = $_POST["gender"];

      $_SESSION['brms_userStatus'] = $_POST["civilstatus_list"];
      $_SESSION['brms_userBlood_type'] = $_POST["bloodtype_list"];
      $_SESSION['brms_userEducation'] = $_POST["education_list"];
      $_SESSION['brms_userOccupation'] = $_POST["occupation"];

      $_SESSION['brms_userCell'] = $_POST["mobile"];
      $_SESSION['brms_userTell'] = $_POST["telephone"];

      $_SESSION['brms_userIsVoter'] = $_POST["is_voter"];
      $_SESSION['brms_userVoterId'] = $_POST["voter_id"];
      $_SESSION['brms_userCitizenBrgyId'] = $_POST["barangay_id"];

      $_SESSION['brms_userResidenceType'] = $_POST["residence_type"];
      $_SESSION['brms_userCurEmployed'] = $_POST["is_employed"];
      $_SESSION['brms_userPwd'] = $_POST["pwd"];
      $_SESSION['brms_userCurEnrolled'] = $_POST["is_enrolled"];
      $_SESSION['brms_userSeniorCitizen'] = $_POST["is_seniorCitizenMember"];
      $_SESSION['brms_userDeceased'] = $_POST["deceased"];
      $_SESSION['brms_userReligion'] = $_POST["religion"];
    }    

    // Duplicate Exist
    if($result==-1){
      echo "<script type='text/javascript'>
                alert('Citizen duplicate exist!');
                location = '../session/brgy_admin_update_session_client_new.php';
            </script>";             
    }
    // Success
    else if($result == 1){

      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Update Information for " 
                  . $_SESSION['brms_userLname'] . ", " 
                  . $_SESSION['brms_userFname'] . " " 
                  . $_SESSION['brms_userMname']); // Insert Log

      // CLEAR CACHE
      header('Cache-Control: no-store, no-cache, must-revalidate'); 
      header('Cache-Control: post-check=0, pre-check=0',false); 
      header('Pragma: no-cache');
      header('Location: profile.php');
    }
    // Error
    else{
      echo "<script type='text/javascript'>alert('Error updating database!')</script>";
    } 

  }  
?>

<?php
  // UPLOAD IMAGE
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Upload"){ 
    if($_FILES['image']['error'] == 0){
      
      include '../image_api/lib/WideImage.php'; // API for IMAGE

      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
      $info_id = $_SESSION['brms_SESSION_userId']; // Info ID
      
      $name = $info_id; // Image name (ID of USER)
      $filename = $_FILES["image"]["name"];
      $ext = strtolower(substr(strrchr($filename, '.'), 1)); // Get extension
      $image_name = $name . '.' . $ext; //New image name

      $image = WideImage::load($_FILES['image']['tmp_name']); // Get image
      $image->saveToFile('../user_image/' . $image_name); // Save image

      $query = new database_query(); // Database Query (initialize connection)
      $result = $query -> upload_image($brgy_id,$info_id,$name,$ext); // Upload Image

      // Success
      if($result == 1){
        $_SESSION['brms_SESSION_userImgName'] = $name; // Update Image Name
        $_SESSION['brms_SESSION_userImgExtension'] = $ext; // Update Image Extension

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Upload image for " 
                  . $_SESSION['brms_userLname'] . ", " 
                  . $_SESSION['brms_userFname'] . " " 
                  . $_SESSION['brms_userMname']); // Insert Log

        // If Admin
        if($_SESSION['brms_ADMIN'] == 1){
            $_SESSION['brms_userImgName'] = $name; // Update Image Name
            $_SESSION['brms_userImgExtension'] = $ext; // Update Image Extension
        }
        // If Client
        else{            
            $_SESSION['brms_NEW_userImgName'] = $name; // Update Image Name
            $_SESSION['brms_NEW_userImgExtension'] = $ext; // Update Image Extension
        }

        // If Admin
        if($_SESSION['brms_userId'] == $info_id){
          $_SESSION['brms_userImgName'] = $name; // Update Image Name
          $_SESSION['brms_userImgExtension'] = $ext; // Update Image Extension
        }

        echo "<script type='text/javascript'>alert('Image uploaded successfully!'); location = 'edit_profile.php';</script>";
      }
      // Error
      else{
        echo "<script type='text/javascript'>alert('Error uploading image!');</script>";
      } 

    }else{      
      echo "<script type='text/javascript'>alert('No image selected!')</script>";
    }    
  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
        // On Submit
        function doCheckUpdload(){

          if (confirm("Are you sure to updload this image?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

    <script>
        // On Submit
        function doCheckUpdate(){

          if (confirm("Are you sure to update this profile?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

    <script>
      $(document).on('click', '#close-preview', function(){ 
        $('.image-preview').popover('hide');
            // Hover befor close the preview
            $('.image-preview').hover(
              function () {
                $('.image-preview').popover('show');
              }, 
              function () {
                $('.image-preview').popover('hide');
              }
              );    
        });

        $(function() {
            // Create the close button
            var closebtn = $('<button/>', {
              type:"button",
              text: 'x',
              id: 'close-preview',
              style: 'font-size: initial;',
            });

            closebtn.attr("class","close pull-right");

            // Set the popover default content
            $('.image-preview').popover({
              trigger:'manual',
              html:true,
              title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
              content: "There's no image",
              placement:'bottom'
            });

            // Clear event
            $('.image-preview-clear').click(function(){
              $('.image-preview').attr("data-content","").popover('hide');
              $('.image-preview-filename').val("");
              $('.image-preview-clear').hide();
              $('.image-preview-input input:file').val("");
              $(".image-preview-input-title").text("Browse"); 
            }); 

            // Create the preview image
            $(".image-preview-input input:file").change(function (){     
              var img = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
              });      

              var file = this.files[0];
              var reader = new FileReader();

            // Set preview image into the popover data-content
            reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }        

            reader.readAsDataURL(file);        
          });  
        });
    </script>

    <script>
        // LOAD PUROK
        $( document ).ready(function() {  

          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              var data = this.responseText.split(",");          

              for(var i = 0; i < data.length; i++){
                if(data[i] !== ""){
                  $('#purok_list').append("<option>"+data[i]+"</option>")
                }
              }

              $("#purok_list").val("<?php echo $_SESSION['brms_SESSION_userPurokName']; ?>");
              $("#gender_list").val("<?php echo $_SESSION['brms_SESSION_userGender']; ?>");

              $("#civilstatus_list").val("<?php echo $_SESSION['brms_SESSION_userStatus']; ?>");
              $("#bloodtype_list").val("<?php echo $_SESSION['brms_SESSION_userBlood_type']; ?>");
              $("#education_list").val("<?php echo $_SESSION['brms_SESSION_userEducation']; ?>");
              
              $("#residence_type").val("<?php echo $_SESSION['brms_SESSION_userResidenceType']; ?>");
              $("#is_employed").val("<?php echo $_SESSION['brms_SESSION_userCurEmployed']; ?>");
              $("#pwd").val("<?php echo $_SESSION['brms_SESSION_userPwd']; ?>");
              $("#is_enrolled").val("<?php echo $_SESSION['brms_SESSION_userCurEnrolled']; ?>");
              $("#is_seniorCitizenMember").val("<?php echo $_SESSION['brms_SESSION_userSeniorCitizen']; ?>");
              $("#deceased").val("<?php echo $_SESSION['brms_SESSION_userDeceased']; ?>");
              $("#religion").val("<?php echo $_SESSION['brms_SESSION_userReligion']; ?>");

              var is_voter = "<?php echo $_SESSION['brms_SESSION_userIsVoter'];?>";
              if(is_voter.length == 0 || is_voter === 'No'){
                is_voter = "No";
                $("#div_voter_id").hide(); 
                document.getElementById("voter_id").required = false;
              }else{
                document.getElementById("voter_id").required = true;
              }

              $("#is_voter").val(is_voter); 
            }
          };

          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

          xmlhttp.open("GET", "../ajax/loadAjax_purok.php?brgy_id="+brgy_id, true); // Get purok names on (loadAjax_purok.php)
          xmlhttp.send();
        });
    </script>

    <script>
      // Voters ID state
      function voterStatusChange(){
        // Hide Voters ID
        if(document.getElementById("is_voter").value === "No"){
          document.getElementById("voter_id").value = "";  
          $("#div_voter_id").hide();
          document.getElementById("voter_id").required = false;      
        }
        // Show Voters ID
        else{
          $("#div_voter_id").show();
          document.getElementById("voter_id").focus();
          document.getElementById("voter_id").required = true;
        }
      }
    </script>

    <!--for navigation bar-->
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!-- Profile Divider --> 

    <div class="container container-profile">    
      <div class="row">
        <div class=" col-xs-12 col-sm-6 col-md-4 col-lg-4">
          <img src="<?php echo $src; ?>" alt="profile photo" style="width: 200px;margin: 0px auto;display: block;padding-top: 30px;" />
          <br><br>
          <form action="" method="post" enctype="multipart/form-data">
            <div class="row">  
              <div class="col-md-12" style="margin-left: 0px;">
                <!-- image-preview-filename input [CUT FROM HERE]-->
                <div class="input-group image-preview">
                  <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                  <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                      <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                      <span class="glyphicon glyphicon-folder-open"></span>
                      <span class="image-preview-input-title">Browse</span>
                      <input type="file" accept="image/png, image/jpeg, image/gif" name="image"/> <!-- rename it -->
                    </div>                      
                    <input type="submit" onclick="return doCheckUpdload()" name="submit" class="btn btn-default image-preview-input" value="Upload"/>
                  </span>
                </div><!-- /input-group image-preview [TO HERE]--> 
              </div>
            </div>
          </form>
          <br>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Citizen Profile</h3>
            </div>
            <div class="panel-body">
              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">
                <!--FML Name-->
                <div class="input-group">
                  <span class="input-group-addon">First Name</span>
                  <input id="first_name" type="text" class="form-control" name="first_name" placeholder="First Name" required value="<?php echo $_SESSION['brms_SESSION_userFname']; ?>">
                </div>
                <br>
                <div class="input-group">
                  <span class="input-group-addon">Middle Name</span>
                  <input id="middle_name" type="text" class="form-control" name="middle_name" placeholder="Middle Name" required value="<?php echo $_SESSION['brms_SESSION_userMname']; ?>">
                </div>
                <br>
                <div class="input-group">
                  <span class="input-group-addon">Last Name</span>
                  <input id="last_name" type="text" class="form-control" name="last_name" placeholder="Last Name" required value="<?php echo $_SESSION['brms_SESSION_userLname']; ?>">
                </div>
                <br>
                <!--Address and Purok-->
                <div class="row">
                  <div class="col-xs-8 col-sm-8 col-md-8">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-road"></i> Address</span>
                      <input id="address" type="text" class="form-control" name="address" placeholder="Address" required value="<?php echo $_SESSION['brms_SESSION_userAddress']; ?>">
                    </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4 input-group" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i> Purok</span>
                      <select id="purok_list" name="purok" class="form-control">
                      </select>
                    </div>
                  </div>
                </div>

                <!--Residence Type and Purok-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i> Residence Type</span>
                      <select id="residence_type" name="residence_type" class="form-control">
                        <option>Owned</option>
                        <option>Leased</option>
                        <option>Rented</option>
                        <option>Boarder</option>
                        <option>Otherwise</option>
                      </select>
                    </div>
                  </div>


                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i> Sex</span>
                      <select id="gender_list" name="gender" class="form-control">
                        <option>Female</option>
                        <option>Male</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-heart"></i> Status</span>
                      <select id="civilstatus_list" name="civilstatus_list" class="form-control">
                        <option>Single</option>
                        <option>Married</option>
                        <option>Widowed</option>
						<option>Separated</option>
						</select>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i> Blood Type</span>
                      <select id="bloodtype_list" name="bloodtype_list" class="form-control">
                        <option>A+</option>
                        <option>A-</option>
                        <option>B+</option>
                        <option>B-</option>
                        <option>AB+</option>
                        <option>AB-</option>
                        <option>O+</option>
                        <option>O-</option>
                        <option>Unknown</option>
                      </select>
                    </div>
                  </div>
                </div>


                <!--Bdate and SC Member--> 
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"> </i> Birthdate</span>
                      <input id="birthdate" type="date" class="form-control" name="birthdate" placeholder="Birthdate" required value="<?php echo $_SESSION['brms_SESSION_userBdate']; ?>">
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i> Senior Citizen Member</span>
                      <select id="is_seniorCitizenMember" onchange="voterStatusChange()" name="is_seniorCitizenMember" class="form-control">
                        <option>No</option>
                        <option>Yes</option>
                      </select>
                    </div>
                  </div>
                </div>

                <!--Highest Educational Attainment and Degree Received-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-education"></i> Education</span>
                      <select id="education_list" name="education_list" class="form-control">
                        <option>Elementary</option>
                        <option>High School</option>
                        <option>Vocational</option>
                        <option>College</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i> Currently Enrolled</span>
                      <select id="is_enrolled" onchange="()" name="is_enrolled" class="form-control">
                        <option>Yes</option>
                        <option>No</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!--Occupation and Curr Employed--> 
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i> Occupation</span>
                      <input id="occupation" type="text" class="form-control" name="occupation" placeholder="Occupation" value="<?php echo $_SESSION['brms_SESSION_userOccupation']; ?>">
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 " style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i> Currently Employed</span>
                      <select id="is_employed" onchange="()" name="is_employed" class="form-control">
                        <option>Yes</option>
                        <option>No</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!--Mobile and Tel-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i> Mobile No.</span>
                      <input id="mobile" type="text" class="form-control" name="mobile" placeholder="Cell No." value="<?php echo $_SESSION['brms_SESSION_userCell']; ?>">
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i>  Tel. No.</span>
                      <input id="telephone" type="text" class="form-control" name="telephone" placeholder="Tel. No" value="<?php echo $_SESSION['brms_SESSION_userTell']; ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i> Religion</span>
                      <select id="religion" name="religion" class="form-control">
                        <option>Roman Catholic</option>
                        <option>Islam</option>
                        <option>Protestant</option>
                        <option>Iglesia ni Cristo</option>
                        <option>Jesus Miracle Crusade International Ministry</option>
                        <option>Members Church of God International</option>
                        <option>Most Holy Church of God in Christ Jesus</option>
                        <option>Philippine Independent Church</option>
                        <option>Apostolic Catholic Church</option>
                        <option>Orthodoxy</option>
                        <option>The Kingdom of Jesus Christ the Name Above Every Name</option>
                        <option>Judaism</option>
                        <option>Hinduism</option>
                        <option>Atheism</option>
                        <option>Others</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Deceased</span>
                      <select id="deceased" onchange="()" name="deceased" class="form-control">
                        <option>No</option>
                        <option>Yes</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon">PWD</span>
                      <select id="pwd" onchange="()" name="pwd" class="form-control">
                        <option>N/A</option>
                        <option>Blind</option>
                        <option>Deaf</option>
                        <option>Mute</option>
                        <option>Others</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!--Registered Voter, Voter's ID and Barangay ID -->
                <div class="row">                  
                  <div class="col-xs-3 col-sm-3 col-md-3" >
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i> Voter</span>
                      <select id="is_voter" onchange="voterStatusChange()" name="is_voter" class="form-control">
                        <option>Yes</option>
                        <option>No</option>
                      </select>
                    </div>
                  </div>
                  <div id="div_voter_id" class="col-xs-4 col-sm-4 col-md-4" style="padding-right: 15px;" >
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i>  Voter's ID</span>
                      <input id="voter_id" type="text" class="form-control" name="voter_id" placeholder="Voter's ID" value="<?php echo $_SESSION['brms_SESSION_userVoterId']; ?>">
                    </div>
                  </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i>  Barangay ID</span>
                      <input id="barangay_id" type="text" class="form-control" name="barangay_id" placeholder="Barangay ID" value="<?php echo $_SESSION['brms_SESSION_userCitizenBrgyId']; ?>">
                    </div>
                  </div>
                </div>      

                <input name="submit" type="submit" onclick="return doCheckUpdate()" value="Save" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                <span onclick="window.open('profile.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>