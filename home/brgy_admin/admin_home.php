<?php
  require('../session/brgy_admin.php'); // Secure Connection
?>

  <!DOCTYPE html>
  <html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">    

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->
    
    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script>
      var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID
    </script>

    <script>
      // OnKeyRelease
      function checkBusiness(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer2").innerHTML = this.responseText;

            // If result is OK (Go to add citizen)
            if(this.responseText === '' && str.length > 0){
              if (confirm("Business not found, would you like to add new?") == true) {
                location = 'add_business.php';
              }
            }

          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_businessTable.php?q="+str+"&brgy_id="+brgy_id, true);
        xmlhttp.send();        
      }
    </script>

    <script>
      // OnKeyRelease
      function checkUser(str){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer1").innerHTML = this.responseText;

            // If result is OK (Go to add citizen)
            if(this.responseText === '' && str.length > 0){
              if (confirm("Citizen not found, would you like to add new?") == true) {
                location = 'add_citizen.php';
              }
            }

          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_citizenTable.php?q="+str+"&brgy_id="+brgy_id, true);
        xmlhttp.send();        
      }
    </script>

    <script> 
      // Document Ready
      $(document).ready(function(){
        checkUser(''); // Load all citizen
        checkBusiness(''); // Load add business

        // On Click Table
        $("#table_Value1").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var info_id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

          window.location.href="../session/brgy_admin_update_session_client.php?info_id="+info_id+"&brgy_id="+brgy_id; // Update Sesssion (CLIENT)
        });

        // On Click Table
        $("#table_Value2").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");          
          var business_id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

          window.location.href="../session/brgy_admin_update_session_business.php?business_id="+business_id+"&brgy_id="+brgy_id; // Update Business Sesssion
        });
        
      });
    </script>

    <script>
      $(window).on("load resize ", function() {
        var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
        $('.tbl-header').css({'padding-right':scrollWidth});
      }).resize();      
    </script>

    <!--for navigation bar-->

    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>
    
    <div class="container">
      <div class="row">

        <!-- tabs left -->
        <div class="tabbable tabs-left">

          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#citizen">Citizen</a></li>
            <li><a data-toggle="tab" href="#business">Business</a></li>
          </ul>

          <div class="tab-content">
            
            <br>

            <div id="citizen" class="tab-pane fade in active">

              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="checkUser(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Citizen's name">
                </div>
              </form>
              
              <br>

              <div id="table_Value1" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>First Name</th>
                      <th>Middle Name</th>
                      <th>Last Name</th>
                      <th>Purok</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer1"></tbody>
                </table>
              </div> 

            </div> <!-- /citizen -->

            <div id="business" class="tab-pane fade in">

              <form method="post" role="form" autocomplete="off" action="">
                <div class="input-group">
                  <span class="input-group-addon">Search </span>                  
                  <input id="purok_name" type="text" onkeyup="checkBusiness(this.value)" class="form-control" name="purok_name" required autofocus placeholder="Business or Owner's Name">
                </div>
              </form>
              
              <br>

              <div id="table_Value2" class="table-responsive"> 
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Business Name</th>
                      <th>Owner</th>
                      <th>Address</th>
                      <th>Purok</th>
                    </tr>
                  </thead>                    
                  <tbody id="responsecontainer2"></tbody>
                </table>
              </div> 

            </div> <!-- /business -->
            
          </div> <!-- /content -->
          
        </div> <!-- /tabs -->

      </div> <!-- /row -->  
    </div> <!-- /container -->

  </body>

</html>