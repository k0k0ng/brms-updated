<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../session/brgy_admin_business.php'); // Secure Connection Business
  require('../database/brgy_admin_database_query.php'); // Database Query
  require('../session/brgy_admin_business_image.php'); // Get Image Src
?>

<?php
  // INSERT BUSINESS
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Save") {

    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 
    $business_id = $_SESSION['brms_business_id']; // Business ID

    $name = $_POST["business_name"];
    $info_id = $_POST["owner_id"];    
    $owner = $_POST["owner"];
    $establish = $_POST["establish"];
    $status = $_POST["status"];
    $address = $_POST["address"];
    $permit = $_POST["permit"];
    $contact = $_POST["contact"];
    $type = $_POST["business_type"];
    $form = $_POST["business_form"];

    $query = new database_query(); // Database Query (initialize connection)
    $purok_id = $query -> select_purok_id($brgy_id,$_POST["purok_list"]); // Get Purok ID

    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> update_business($business_id,$brgy_id,$purok_id,$form,$type,$info_id,$name,$address,$establish,$status,$permit,$contact); // Get Result

    // Success
    if($result == 1){
      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Update Information for " . $_SESSION['brms_business_name']); // Insert Log      
      header('Location: ../session/brgy_admin_update_session_business.php?business_id='.$business_id.'&brgy_id='.$brgy_id);
    }
    // Error
    else{
      echo "<script> alert('Error'); </script>";
    }
  }
?>

<?php
  // UPLOAD IMAGE
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Upload"){ 
    if($_FILES['image']['error'] == 0){

      include '../image_api/lib/WideImage.php'; // API for IMAGE

      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
      $business_id = $_SESSION['brms_business_id']; // Business ID
      
      $name = $business_id; // Image name (ID of USER)
      $filename = $_FILES["image"]["name"];
      $ext = strtolower(substr(strrchr($filename, '.'), 1)); // Get extension
      $image_name = $name . '.' . $ext; //New image name

      $image = WideImage::load($_FILES['image']['tmp_name']); // Get image
      $image->saveToFile('../bussiness_image/' . $image_name); // Save image

      $query = new database_query(); // Database Query (initialize connection)
      $result = $query -> upload_image_business($brgy_id,$business_id,$name,$ext); // Upload Image

      // Success
      if($result == 1){
        $_SESSION['brms_business_ImgName'] = $name; // Update Image Name
        $_SESSION['brms_business_Extension'] = $ext; // Update Image Extension

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Upload image for " . $_SESSION['brms_business_name']); // Insert Log
        
        echo "<script type='text/javascript'>
                  alert('Image uploaded successfully!'); 
                  location = 'edit_business.php';
              </script>";
      }
      // Error
      else{
        echo "<script type='text/javascript'>alert('Error uploading image!');</script>";
      } 

    }else{      
      echo "<script type='text/javascript'>alert('No image selected!')</script>";
    }    
  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
        // On Submit
        function doCheckUpdload(){

          if (confirm("Are you sure to updload this image?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

    <script>
        // On Submit
        function doCheckUpdate(){

          if (confirm("Are you sure to update this business profile?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

    <script>
      $(document).on('click', '#close-preview', function(){ 
        $('.image-preview').popover('hide');
            // Hover befor close the preview
            $('.image-preview').hover(
              function () {
                $('.image-preview').popover('show');
              }, 
              function () {
                $('.image-preview').popover('hide');
              }
              );    
        });

        $(function() {
            // Create the close button
            var closebtn = $('<button/>', {
              type:"button",
              text: 'x',
              id: 'close-preview',
              style: 'font-size: initial;',
            });

            closebtn.attr("class","close pull-right");

            // Set the popover default content
            $('.image-preview').popover({
              trigger:'manual',
              html:true,
              title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
              content: "There's no image",
              placement:'bottom'
            });

            // Clear event
            $('.image-preview-clear').click(function(){
              $('.image-preview').attr("data-content","").popover('hide');
              $('.image-preview-filename').val("");
              $('.image-preview-clear').hide();
              $('.image-preview-input input:file').val("");
              $(".image-preview-input-title").text("Browse"); 
            }); 

            // Create the preview image
            $(".image-preview-input input:file").change(function (){     
              var img = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
              });      

              var file = this.files[0];
              var reader = new FileReader();

            // Set preview image into the popover data-content
            reader.onload = function (e) {
              $(".image-preview-input-title").text("Change");
              $(".image-preview-clear").show();
              $(".image-preview-filename").val(file.name);            
              img.attr('src', e.target.result);
              $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }        

            reader.readAsDataURL(file);        
          });  
        });
    </script>

    <script>      
      $( document ).ready(function() {
        $(document).click(function(e){ hide_live_search(); }); // Document Click

        load_purok();
        load_business_form();
        load_business_type();
        hide_live_search();
        $("#status").val("<?php echo $_SESSION['brms_business_status']; ?>");

        $("#table_Value").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var name = currentRow.find("td:eq(1)").text(); // get current row 2nd TD value

          document.getElementById("owner").value = name;
          document.getElementById("owner_id").value = id;
          document.getElementById("submit").disabled = false; // Enable Submit                    
        });

      });
    </script>
    
    <script>
      // Hide Live Search
      function hide_live_search(){
        document.getElementById("responsecontainer").innerHTML = '';
      }
    </script>

    <script>
      // OnKeyRelease
      function checkUser(str){

        // Back Key Pressed
        var key = event.keyCode || event.charCode;
        if( key == 8 || key == 46 ){
          document.getElementById("owner").value = '';
          document.getElementById("owner_id").value = '0'; 
          document.getElementById("submit").disabled = true; // Disable Submit                           
        }

        if(str.length == 0){
          hide_live_search();          
        }else{
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById('responsecontainer').innerHTML = this.responseText; 
            }
          };
          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>";
          xmlhttp.open("GET", "../ajax/loadAjax_brgy_official.php?q="+str+"&brgy_id="+brgy_id, true);
          xmlhttp.send();  
        }     
      }
    </script>

    <script>
      // LOAD BUSINESS FORM
      function load_business_form(){
        //$('#purok_list').append("<option>BMW</option>")
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            var data = this.responseText.split(",");          

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#business_form').append("<option>"+data[i]+"</option>")
              }
            }
            $("#business_form").val("<?php echo $_SESSION['brms_business_form']; ?>");            
          }
        };

        xmlhttp.open("GET", "../ajax/loadAjax_businessForm.php?", true); // Get Business Forms
        xmlhttp.send();
      }
    </script>

    <script>
      // LOAD BUSINESS FORM
      function load_business_type(){
        //$('#purok_list').append("<option>BMW</option>")
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            var data = this.responseText.split(",");          

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#business_type').append("<option>"+data[i]+"</option>")
              }
            }
            $("#business_type").val("<?php echo $_SESSION['brms_business_type']; ?>");
          }
        };

        xmlhttp.open("GET", "../ajax/loadAjax_businessType.php?", true); // Get Business Forms
        xmlhttp.send();
      }
    </script>

    <script>
      // LOAD PUROK
      function load_purok(){
        //$('#purok_list').append("<option>BMW</option>")
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            var data = this.responseText.split(",");          

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#purok_list').append("<option>"+data[i]+"</option>")
              }
            }
            $("#purok_list").val("<?php echo $_SESSION['brms_business_purok']; ?>");
          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

        xmlhttp.open("GET", "../ajax/loadAjax_purok.php?brgy_id="+brgy_id, true); // Get purok names on (loadAjax_purok.php)
        xmlhttp.send();
      }
    </script>

    <!--for navigation bar-->
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!-- Profile Divider --> 

    <div class="container container-profile">    
      <div class="row">
        <div class=" col-xs-12 col-sm-6 col-md-4 col-lg-4">
          <img src="<?php echo $src; ?>" alt="profile photo" style="width: 200px;margin: 0px auto;display: block;padding-top: 30px;" />
          <br><br>
          <form action="" method="post" enctype="multipart/form-data">
            <div class="row">  
              <div class="col-md-12" style="margin-left: 0px;">
                <!-- image-preview-filename input [CUT FROM HERE]-->
                <div class="input-group image-preview">
                  <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                  <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                      <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                      <span class="glyphicon glyphicon-folder-open"></span>
                      <span class="image-preview-input-title">Browse</span>
                      <input type="file" accept="image/png, image/jpeg, image/gif" name="image"/> <!-- rename it -->
                    </div>                      
                    <input type="submit" onclick="return doCheckUpdload()" name="submit" class="btn btn-default image-preview-input" value="Upload"/>
                  </span>
                </div><!-- /input-group image-preview [TO HERE]--> 
              </div>
            </div>
          </form>
          <br>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Business Profile</h3>
            </div>
            <div class="panel-body">
              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">
                <!--FML Name-->
                <div class="input-group">
                  <span class="input-group-addon">Business Name</span>
                  <input id="business_name" type="text" class="form-control" name="business_name" placeholder="" required value="<?php echo $_SESSION['brms_business_name']; ?>">
                </div>
                <br>
                <div class="input-group" style="display: none">
                  <span class="input-group-addon">Owner ID</span>
                  <input id="owner_id" name="owner_id" value="<?php echo $_SESSION['brms_business_owner_id']; ?>" type="text" class="form-control" placeholder="Full Name">
                </div>
                <div class="input-group">
                  <span class="input-group-addon">Owner</span>
                  <input id="owner" onkeyup="checkUser(this.value)" type="text" class="form-control" name="owner" placeholder="Full Name" required value="<?php echo $_SESSION['brms_business_owner_name']; ?>">
                </div>
                <div id="table_Value">          
                  <table class="table table-condensed table-hover">                                          
                    <tbody id="responsecontainer" style="cursor: pointer;">
                      <tr><td>123</td></tr>
                      <tr><td>123</td></tr>
                    </tbody>
                  </table>
                </div>

                <!-- Date Established and Status -->
                <div class="row">
                  <div class="col-xs-8 col-sm-8 col-md-8">
                    <div class="form-group input-group">
                        <span class="input-group-addon">Date Established</span>
                        <input id="establish" type="date" class="form-control" name="establish" placeholder="Birthdate" required value="<?php echo $_SESSION['brms_business_establish']; ?>">
                      </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4 input-group" style="padding-right: 15px">
                    <div class="form-group input-group">
                        <span class="input-group-addon">Status</span>
                        <select id="status" name="status" class="form-control">
                          <option>Active</option>
                          <option>Inactive</option>
                        </select>
                      </div>
                  </div>
                </div>  
                
                <!--Address and Purok-->
                <div class="row">
                  <div class="col-xs-8 col-sm-8 col-md-8">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Address</span>
                      <input id="address" type="text" class="form-control" name="address" placeholder="Address" required value="<?php echo $_SESSION['brms_business_address']; ?>">
                    </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4 input-group" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Purok</span>
                      <select id="purok_list" name="purok_list" class="form-control"></select>
                    </div>
                  </div>
                </div>

                <!--Business Permit-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Business Permit No.</span>
                      <input id="permit" type="text" class="form-control" name="permit" placeholder="X 000000-0" value="<?php echo $_SESSION['brms_business_permit']; ?>">
                    </div>
                  </div>

                <!--Contact Number-->
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Contact No.</span>
                      <input id="contact" type="text" class="form-control" name="contact" placeholder="Mobile / Telephone" value="<?php echo $_SESSION['brms_business_contact']; ?>">
                    </div>
                  </div>
                </div>

                <!--Residence Type and Purok-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Business Organization Form</span>
                      <select id="business_form" name="business_form" class="form-control"></select>
                    </div>
                  </div>


                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Business Type</span>
                      <select id="business_type" name="business_type" class="form-control"></select>
                    </div>
                  </div>
                </div>
                
                <input id="submit" name="submit" type="submit" onclick="return doCheckUpdate()" value="Save" class="btn btn-primary" style="float: right; margin-left:10px">
                <span onclick="window.open('profile_business.php', '_top')" class="btn btn-info" style="float: right">Back</span>
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>