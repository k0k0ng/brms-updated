<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
?>

<?php
  // INSERT USER
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Add") {

    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $middle_name = $_POST["middle_name"];

    $query = new database_query(); // Database Query (initialize connection)
    $purok_id = $query -> select_purok_id($brgy_id,$_POST["purok"]); // Get Purok ID

    $query = new database_query(); // Database Query (initialize connection)
    $result = $query -> insert_user_info(
        $brgy_id,
        $_POST["first_name"],
        $_POST["last_name"],
        $_POST["middle_name"],
        $purok_id,
        $_POST["address"],
        $_POST["birthdate"],
        $_POST["gender"],
        $_POST["civilstatus_list"],
        $_POST["bloodtype_list"],
        $_POST["education_list"],
        $_POST["occupation"],
        $_POST["mobile"],
        $_POST["telephone"],
        $_POST["is_voter"],
        $_POST["voter_id"],
        $_POST["barangay_id"],

        $_POST["residence_type"],
        $_POST["is_employed"],
        $_POST["pwd"],
        $_POST["is_enrolled"],
        $_POST["is_seniorCitizenMember"],
        $_POST["deceased"],
        $_POST["religion"]
      );

    // Duplicate Exist
    if($result==-1){
      echo "<script type='text/javascript'>
                alert('Citizen already exist!');
                location = '../session/brgy_admin_update_session_client_new.php';
            </script>";             
    }
    // Success
    else if($result==1){

      $query = new database_query(); // Database Query (initialize connection)
      $query -> insert_log($_SESSION['brms_userId'],"Add New Citizen " 
                          . $_POST["last_name"] . ", "
                          . $_POST["first_name"] . " "
                          . $_POST["middle_name"]); // Insert Log 

      // CLEAR CACHE
      header('Cache-Control: no-store, no-cache, must-revalidate'); 
      header('Cache-Control: post-check=0, pre-check=0',false); 
      header('Pragma: no-cache'); 
      echo "<script type='text/javascript'> alert('Citizen successfully added!'); location = 'add_citizen2.php'; </script>";  
    }
    // Error
    else{
      echo "<script type='text/javascript'>alert('Error adding citizen!')</script>";      
    }

  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>
    
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    
    <!--for navigation bar-->
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

    <script>
      // LOAD PUROK
      $( document ).ready(function() {   
        //$('#purok_list').append("<option>BMW</option>")

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            var data = this.responseText.split(",");          

            for(var i = 0; i < data.length; i++){
              if(data[i] !== ""){
                $('#purok_list').append("<option>"+data[i]+"</option>")
              }
            }
          }
        };

        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

        xmlhttp.open("GET", "../ajax/loadAjax_purok.php?brgy_id="+brgy_id, true); // Get purok names on (loadAjax_purok.php)
        xmlhttp.send();

        document.getElementById("voter_id").required = true;
      });
    </script>

    <script>
      function voterStatusChange(){
        // Hide Voters ID
        if(document.getElementById("is_voter").value === "No"){
          document.getElementById("voter_id").value = "";  
          $("#div_voter_id").hide();
          document.getElementById("voter_id").required = false;      
        }
        // Show Voters ID
        else{
          $("#div_voter_id").show();
          document.getElementById("voter_id").focus();
          document.getElementById("voter_id").required = true;
        }
      }
    </script>

    <!--for navigation bar-->
    
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container container-profile"> <!-- Profile Divider -->  
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">

          <div class="panel panel-default">
            <div class="panel-heading">
              <a href="upload_excel.php" style="float: right;">(Upload Excel File)</a>
              <h3 class="panel-title">Add Citizen</h3>
            </div>
            <div class="panel-body">
              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">
                <!--FML Name-->
                <div class="input-group">
                  <span class="input-group-addon">First Name</span>
                  <input id="first_name" type="text" class="form-control" name="first_name" placeholder="First Name" required>
                </div>
                <br>
                <div class="input-group">
                  <span class="input-group-addon">Middle Name</span>
                  <input id="middle_name" type="text" class="form-control" name="middle_name" placeholder="Middle Name" required>
                </div>
                <br>
                <div class="input-group">
                  <span class="input-group-addon">Last Name</span>
                  <input id="last_name" type="text" class="form-control" name="last_name" placeholder="Last Name" required>
                </div>
                <br>
                <!--Address and Purok-->
                <div class="row">
                  <div class="col-xs-8 col-sm-8 col-md-8">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-road"></i> Address</span>
                      <input id="address" type="text" class="form-control" name="address" placeholder="Address" required>
                    </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4 input-group" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i> Purok</span>
                      <select id="purok_list" name="purok" class="form-control">
                      </select>
                    </div>
                  </div>
                </div>

                <!--Residence Type and Purok-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i> Residence Type</span>
                      <select id="residence_type" name="residence_type" class="form-control">
                        <option>Owned</option>
                        <option>Leased</option>
                        <option>Rented</option>
                        <option>Boarder</option>
                        <option>Otherwise</option>
                      </select>
                    </div>
                  </div>


                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i> Sex</span>
                      <select id="gender_list" name="gender" class="form-control">
                        <option>Female</option>
                        <option>Male</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-heart"></i> Status</span>
                      <select id="civilstatus_list" name="civilstatus_list" class="form-control">
                        <option>Single</option>
                        <option>Married</option>
                        <option>Widowed</option>
						<option>Separated</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i> Blood Type</span>
                      <select id="bloodtype_list" name="bloodtype_list" class="form-control">
                        <option>A+</option>
                        <option>A-</option>
                        <option>B+</option>
                        <option>B-</option>
                        <option>AB+</option>
                        <option>AB-</option>
                        <option>O+</option>
                        <option>O-</option>
                        <option>Unknown</option>
                      </select>
                    </div>
                  </div>
                </div>


                <!--Bdate and SC Member--> 
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"> </i> Birthdate</span>
                      <input id="birthdate" type="date" class="form-control" name="birthdate" placeholder="Birthdate" required>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i> Senior Citizen Member</span>
                      <select id="is_seniorCitizenMember" onchange="voterStatusChange()" name="is_seniorCitizenMember" class="form-control">
                        <option>No</option>
                        <option>Yes</option>
                      </select>
                    </div>
                  </div>
                </div>

                <!--Highest Educational Attainment and Degree Received-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-education"></i> Education</span>
                      <select id="education_list" name="education_list" class="form-control">
                        <option>Elementary</option>
                        <option>High School</option>
                        <option>Vocational</option>
                        <option>College</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i> Currently Enrolled</span>
                      <select id="is_enrolled" onchange="()" name="is_enrolled" class="form-control">
                        <option>Yes</option>
                        <option>No</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!--Occupation and Curr Employed--> 
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i> Occupation</span>
                      <input id="occupation" type="text" class="form-control" name="occupation" placeholder="Occupation">
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 " style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i> Currently Employed</span>
                      <select id="is_employed" onchange="()" name="is_employed" class="form-control">
                        <option>Yes</option>
                        <option>No</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!--Mobile and Tel-->
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i> Mobile No.</span>
                      <input id="mobile" type="text" class="form-control" name="mobile" placeholder="Cell No.">
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i>  Tel. No.</span>
                      <input id="telephone" type="text" class="form-control" name="telephone" placeholder="Tel. No">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-tint"></i> Religion</span>
                      <select id="religion" name="religion" class="form-control">
                        <option>Roman Catholic</option>
                        <option>Islam</option>
                        <option>Protestant</option>
                        <option>Iglesia ni Cristo</option>
                        <option>Jesus Miracle Crusade International Ministry</option>
                        <option>Members Church of God International</option>
                        <option>Most Holy Church of God in Christ Jesus</option>
                        <option>Philippine Independent Church</option>
                        <option>Apostolic Catholic Church</option>
                        <option>Orthodoxy</option>
                        <option>The Kingdom of Jesus Christ the Name Above Every Name</option>
                        <option>Judaism</option>
                        <option>Hinduism</option>
                        <option>Atheism</option>
                        <option>Others</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group input-group">
                      <span class="input-group-addon">Deceased</span>
                      <select id="deceased" onchange="()" name="deceased" class="form-control">
                        <option>No</option>
                        <option>Yes</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-4" style="padding-right: 15px">
                    <div class="form-group input-group">
                      <span class="input-group-addon">PWD</span>
                      <select id="pwd" onchange="()" name="pwd" class="form-control">
                        <option>N/A</option>
                        <option>Blind</option>
                        <option>Deaf</option>
                        <option>Mute</option>
                        <option>Others</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!--Registered Voter, Voter's ID and Barangay ID -->
                <div class="row">                  
                  <div class="col-xs-3 col-sm-3 col-md-3" >
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i> Voter</span>
                      <select id="is_voter" onchange="voterStatusChange()" name="is_voter" class="form-control">
                        <option>Yes</option>
                        <option>No</option>
                      </select>
                    </div>
                  </div>
                  <div id="div_voter_id" class="col-xs-4 col-sm-4 col-md-4" style="padding-right: 15px;" >
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i>  Voter's ID</span>
                      <input id="voter_id" type="text" class="form-control" name="voter_id" placeholder="Voter's ID">
                    </div>
                  </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i>  Barangay ID</span>
                      <input id="barangay_id" type="text" class="form-control" name="barangay_id" placeholder="Barangay ID">
                    </div>
                  </div>
                </div>      
                
                <input name="submit" type="submit" value="Add" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                <span onclick="window.open('admin_home.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>
                

              </form>
            </div>
          </div>

        </div>
      </div>
    </div>

  </body>

</html>