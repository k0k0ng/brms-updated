<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../session/brgy_admin_business.php'); // Secure Connection Business
  require('../session/brgy_admin_business_image.php'); // Get Image Src
  require('../database/brgy_admin_database_query.php'); // Database Query
?>

<?php 
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Pay"){
      //$info_id = $_SESSION['brms_business_owner_id']; // Info ID

      $info_id = $_SESSION['brms_business_id'];
      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
      $admin_id = $_SESSION['brms_userId']; // Admin ID

      $fee_list = $_POST['fee_list']; // Fee
      $balance = $_POST['balance']; // Balance
      $amount = $_POST['amount']; // Amount Paid
      $receipt = $_POST['receipt']; //Receipt

      // Check Payment
      if($amount > $balance){
        $overpay = $amount - $balance;
        echo "<script type='text/javascript'>
                alert('You overpay $overpay, please check your Amount Paid!');
                location = 'profile_business.php';
              </script>";
        return;
      }

      $query = new database_query(); // Database Query (initialize connection)
      $result = $query -> insert_fee_collection($brgy_id,$info_id,$admin_id,$fee_list,$amount,$receipt); // Insert Fee Collection

      if($amount > $balance){
        $overpay = $amount - $balance;
        echo "<script type='text/javascript'>
                alert('You overpay $overpay, please check your Amount Paid!');
                location = 'profile_business.php';
              </script>";
      }
      // Success      
      if($result == 1){

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Payment Collection for " . $fee_list); // Insert Log

        echo "<script type='text/javascript'>
                alert('$fee_list successfully added!');
                location = 'profile_business.php';
              </script>";
      }
      // Error
      else{
        echo "<script type='text/javascript'> 
                alert('Error!'); 
                
              </script>";
      }    
    }
?>

<?php 
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Submit"){
      $info_id = $_SESSION['brms_business_id']; // Info ID
      //$brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
      $admin_id = $_SESSION['brms_userId']; // Admin ID
      $type = 'Business';
      $note = $_POST['note_text']; // Fee
      
      $query = new database_query(); // Database Query (initialize connection)
      $result = $query -> insert_note($info_id,$admin_id,$type,$note); // Insert Note

      // Success      
      if($result == 1){

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Note for " . $_SESSION['brms_business_name']); // Insert Log

        echo "<script type='text/javascript'>
                alert('Note successfully added!');
                location = 'profile_business.php';
              </script>";
      }
      // Error
      else{
        echo "<script type='text/javascript'> 
                alert('Error!'); 
                location = 'profile_business.php';
              </script>";
      } 
    }
?>

<!DOCTYPE html>
<html lang="en">

  <head>
  
      <title>BRMS - Barangay Record Management System</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="stylesheet" href="../css/bootstrap.min.css">
      <link rel="stylesheet" href="../css/custom2test.css">
      <link rel="stylesheet" href="../css/profile.css"> <!-- PROFILE -->
      <link rel="javascript" src="../js/jquery.js">
      <link rel="javascript" src="../js/jquery.min.js">

      <!--Website Tab Icon-->
      <link rel="icon" type="image/png" href="../images/logo.png"/>

      <script src="../js/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>

      <script>
        // On Submit
        function doCheckNote(){

          var text = document.getElementById('note_text').value.trim();

          if(!text){
            alert("Invalid note!");
            document.getElementById("note_text").focus();
            return false;
          }

          if (confirm("Are you sure to add this note?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

      <script>
        // On Submit
        function doCheckFee(){

          if (confirm("Are you sure to add this fee?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

      <script src="../js/moment.min.js"></script> <!-- DATE TIME -->
      
      <script>
        //var info_id = "<?php echo $_SESSION['brms_business_owner_id']; ?>";
        var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>";
        var bus_id = "<?php echo $_SESSION['brms_business_id']; ?>";
        var type = "Business";
      </script>

      <script>
      // Load Note
      function load_note(str){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {                  
            document.getElementById("responsecontainer3").innerHTML = this.responseText; 
          }
        };
        xmlhttp.open("GET", "../ajax/loadAjax_note.php?info_id="+bus_id+"&str="+str+"&type="+type, true);
        xmlhttp.send();    
      }      
      </script>

      <script>
        // Load Transaction
        function load_transaction(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer1").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_transaction_business_table.php?brgy_id="+brgy_id+"&bus_id="+bus_id+"&str="+str, true);
          xmlhttp.send();    
        }      
      </script>

      <script>
        // Load Fee Type
        function fee_type(){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              var data = this.responseText.split(",");        

              for(var i = 0; i < data.length; i++){
                if(data[i] !== ""){
                  $('#fee_list').append("<option>"+data[i]+"</option>")
                }
              }

              if(data.length > 1){
                document.getElementById("Pay").disabled = false;
              }

              balance(); // Get Balance

            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_feeType.php?&brgy_id="+brgy_id+"&collection_id="+1, true);
          xmlhttp.send();    
        }      
      </script>

      <script>
        // Load Balance
        function balance(){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {              
              var balance = this.responseText; // Balance
              document.getElementById("balance").value = parseFloat(balance,10).toFixed(2); // Round Off Two Decimal Places

              // If Zero Balance
              if(balance<=0){                
                document.getElementById("Pay").disabled = true;
              }else{
                document.getElementById("Pay").disabled = false;
                check_amount_paid(document.getElementById("amount").value); // Check amount
              }
            }
          };

          var sel = document.getElementById("fee_list");
          var type = sel.options[sel.selectedIndex].value; // or sel.value

          xmlhttp.open("GET", "../ajax/loadAjax_balance.php?&brgy_id="+brgy_id+"&type="+type+"&info_id="+bus_id, true);
          xmlhttp.send();    
        }      
      </script>

      <script>
        // Load Fee
        function load_fee(str){
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById("responsecontainer2").innerHTML = this.responseText; 
            }
          };
          xmlhttp.open("GET", "../ajax/loadAjax_fee_table.php?info_id="+bus_id+"&brgy_id="+brgy_id+"&str="+str+"&collection_id="+1, true);
          xmlhttp.send();    
        }      
      </script>

      <script> 
        // Document Ready
        $(document).ready(function(){
          load_transaction('');
          load_fee('');
          load_note('');
          fee_type(); 
          fee_list_stateChanged();     

          // On Click Table
          $("#table_value_transaction").on('click','.selectedRow',function(){
            // get the current row
            var currentRow=$(this).closest("tr");          
            var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value

            window.open("../pdf_transaction/"+id+".pdf");
          });

        });
      </script>

      <script>
        // Fee List State Changed
        function fee_list_stateChanged(){          
          $("#fee_list").on('change', function() {
            balance();
          });
        }
      </script>

      <!--for navigation bar-->

      <script>
        $.get("navigation.php", function(data){
          $("#nav-placeholder").replaceWith(data);
        });
      </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
          <div class="user-image">          
            <img src="<?php echo $src; ?>" alt="profile photo" style="width: 100px;margin: 0px auto;display: block;padding-top: 30px;" />          
            <br>
            <h3><?php echo $_SESSION['brms_business_name'];?></h3>
            <a href="edit_business.php" style="font-size: 12px; display: inline; vertical-align: bottom;">Edit profile</a>
            <br><br>
          </div>
        </div>

        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
          <ul class="nav nav-tabs">
            <li class="active">
              <a data-toggle="tab" href="#profile">
                <p>Profile</p>
              </a>
            </li>
            <li>
              <a data-toggle="tab" href="#transaction">
                <p>Transactions</p>
              </a>
            </li>
            <li>
              <a data-toggle="tab" href="#fee">
                <p>Fees</p>
              </a>
            </li>
            <li>
              <a data-toggle="tab" href="#note">
                <p>Notes</p>
              </a>
            </li>            
          </ul>
          <div class="tab-content">
            <div id="profile" class="tab-pane fade in active">
              <div class="user-body">
                <ul class="container-fluid details list-unstyled" >
                  <br>
                  <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                      <li><p><span class="glyphicon glyphicon-user" style="width:40px;"></span><b>Owner: </b><?php echo $_SESSION['brms_business_owner_name']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-home" style="width:40px;"></span><b>Purok: </b><?php echo $_SESSION['brms_business_purok']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-road" style="width:40px;"></span><b>Address: </b><?php echo $_SESSION['brms_business_address']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-calendar" style="width:40px;"></span><b>Date Established : </b><?php echo date("F d, Y", strtotime($_SESSION['brms_business_establish'])); ?></p></li>
                      <li><p><span class="glyphicon glyphicon-ok" style="width:40px;"></span><b>Status: </b><?php echo $_SESSION['brms_business_status']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-briefcase" style="width:40px;"></span><b>Business Form: </b><?php echo $_SESSION['brms_business_form']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-home" style="width:40px;"></span><b>Business Type: </b><?php echo $_SESSION['brms_business_type']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-barcode" style="width:40px;"></span><b>Business Permit No: </b><?php echo $_SESSION['brms_business_permit']; ?></p></li>
                      <li><p><span class="glyphicon glyphicon-earphone" style="width:40px;"></span><b>Contact No: </b><?php echo $_SESSION['brms_business_contact']; ?></p></li>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                      
                    </div>
                  </div>
                </ul>
              </div>
            </div>

            <div id="transaction" class="tab-pane fade ">
              <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                  
                  <br>
                  <div class="input-group">
                    <span class="input-group-addon">Search 10 Most Recent Transactions</span>
                    <input id="search" type="text" onkeyup="load_transaction(this.value)" class="form-control" name="search" placeholder="Purpose" required>
                  </div>
                  <br>

                  <div id="table_value_transaction" class="table-responsive">         
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Purpose</th>
                          <th>Receipt</th>                    
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer1">
                      </tbody>
                    </table>
                  </div>
                </div>
                <br>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                  <div id="table_Value" class="table-responsive">          
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th colspan="2">Request Forms</th>                      
                        </tr>
                      </thead>                    
                      <tbody>                        
                        <tr>
                          <td><a href="form_businessclearance.php" target="_self">Business Clearance</a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <br>
            </div>
            
            <div id="fee" class="tab-pane fade ">
              <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                  
                  <br>
                  <!--Date-->
                  <div class="input-group">
                    <span class="input-group-addon">Search 10 Most Recent Fees</span>
                    <input id="search" onkeyup="load_fee(this.value)" type="text" class="form-control" name="search" placeholder="Fee Type" required>
                  </div>
                  <br>

                  <div id="" class="">          
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Amount</th>
                          <th>Type</th>
                          <th>Receipt</th>
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer2">
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                  <br>            
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4>
                          <script>
                            document.write("Fees (As of " + moment().format('MMMM Do, YYYY') + ")");
                          </script>
                      </h4>
                    </div>
                    <div class="panel-body">
                      <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">

                        <!--Fee Type-->
                        <div class="input-group">
                          <span class="input-group-addon">Fee Type</span>
                          <select id="fee_list" name="fee_list" class="form-control">
                          </select>
                        </div>                       
                        <br>

                        <!--Amount-->
                        <div class="input-group">
                          <span class="input-group-addon">Remaining Balance</span>
                          <input id="balance" type="text" class="form-control" name="balance" placeholder="" readonly>
                        </div>                        
                        <br>

                        <!--Amount-->
                        <div class="input-group">
                          <span class="input-group-addon">Amount Paid</span>
                          <input id="amount" onkeyup="check_amount_paid(this.value)" type="number" min="0.01" step="0.01" class="form-control" name="amount" placeholder="" required>
                        </div>                        
                        <br>

                        <!--Receipt No-->
                        <div class="input-group">
                          <span class="input-group-addon">Receipt No.</span>
                          <input id="receipt" type="text" class="form-control" name="receipt" placeholder="" required>
                        </div>
                        <br>
                        
                        <input id="Pay" disabled ="true" name="submit" type="submit" onclick="return doCheckFee()" value="Pay" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                        <input name="clear" type="" onclick="" type="button" value="Clear" class="btn btn-success" style="float: right; width:100px;">
                        
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div id="note" class="tab-pane fade ">
              <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                  
                  <br>
                  <!--Date-->
                  <div class="input-group">
                    <span class="input-group-addon">Search 10 Most Recent Notes</span>
                    <input id="search" onkeyup="load_note(this.value)" type="text" class="form-control" name="search" placeholder="Note" required>
                  </div>
                  <br>

                  <div id="" class="">          
                    <table class="table table-condensed table-hover">
                      <thead>
                        <tr>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Note</th>
                        </tr>
                      </thead>                    
                      <tbody id="responsecontainer3">
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                  <br>            
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4>Add Note</h4>
                    </div>
                    <div class="panel-body">
                      <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">

                        <textarea id="note_text" name="note_text" class="form-control" rows="10" required></textarea>
                        <br>
                        
                        <input id="Pay" name="submit" type="submit" onclick="return doCheckNote()" onclick="" value="Submit" class="btn btn-primary" style="float: right; width:100px;">
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <br>
        </div>

      </div>
    </div>

  </body>
</html>