<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../session/brgy_admin_add_citizen.php'); // Secure Add Citizen Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
?>

<?php
  // UPLOAD IMAGE
  if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Upload"){ 
    if($_FILES['image']['error'] == 0){
      
      include '../image_api/lib/WideImage.php'; // API for IMAGE

      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
      $info_id = $_SESSION['brms_newUser_id']; // Info ID
      
      $name = $info_id; // Image name (ID of USER)
      $filename = $_FILES["image"]["name"];
      $ext = strtolower(substr(strrchr($filename, '.'), 1)); // Get extension
      $image_name = $name . '.' . $ext; //New image name

      $image = WideImage::load($_FILES['image']['tmp_name']); // Get image
      $image->saveToFile('../user_image/' . $image_name); // Save image

      $query = new database_query(); // Database Query (initialize connection)
      $result = $query -> upload_image($brgy_id,$info_id,$name,$ext); // Upload Image

      // Success
      if($result == 1){

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Upload image for " 
                        . $_SESSION['brms_NEW_userLname'] . ", "
                        . $_SESSION['brms_NEW_userFname'] . " "
                        . $_SESSION['brms_NEW_userMname']); // Insert Log 

        echo "<script type='text/javascript'> alert('Image uploaded successfully!'); location = '../session/brgy_admin_update_session_client_new.php'; </script>";
      }
      // Error
      else{
        echo "<script type='text/javascript'>alert('Error uploading image!');</script>";
      } 

    }else{      
      echo "<script type='text/javascript'>alert('No image selected!')</script>";
    }    
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>
    
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
      $(document).on('click', '#close-preview', function(){ 
        $('.image-preview').popover('hide');
          // Hover befor close the preview
          $('.image-preview').hover(
            function () {
              $('.image-preview').popover('show');
            }, 
          function () {
              $('.image-preview').popover('hide');
            }
          );    
        });

      $(function() {
          // Create the close button
          var closebtn = $('<button/>', {
            type:"button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
          });
              
          closebtn.attr("class","close pull-right");
              
          // Set the popover default content
          $('.image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
            content: "There's no image",
            placement:'bottom'
          });
              
          // Clear event
          $('.image-preview-clear').click(function(){
            $('.image-preview').attr("data-content","").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("Browse"); 
          }); 
              
          // Create the preview image
          $(".image-preview-input input:file").change(function (){     
            var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
          });      
                
          var file = this.files[0];
          var reader = new FileReader();
                  
          // Set preview image into the popover data-content
          reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
          }        
                  
          reader.readAsDataURL(file);        
        });  
      });
    </script>

  <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container container-profile"> <!-- Profile Divider -->  
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
          <div class="panel panel-default">

            <div class="panel-heading">
              <h3 class="panel-title">Add Citizen (2 of 2)</h3>
            </div>

            <div class="panel-body">

              <img src="../images/avatar.png" alt="profile photo" style="width: 200px;margin: 0px auto;display: block;padding-top: 30px;" />'              

              <br><br>

              <form action="" method="post" enctype="multipart/form-data">
                <div class="row">  
                  <div class="col-md-12" style="margin-left: 0px;">

                    <!-- image-preview-filename input [CUT FROM HERE]-->
                    <div class="input-group image-preview">
                      <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                      <span class="input-group-btn">
                        <!-- image-preview-clear button -->
                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                          <span class="glyphicon glyphicon-remove"></span> Clear
                        </button>
                        <!-- image-preview-input -->
                        <div class="btn btn-default image-preview-input">
                          <span class="glyphicon glyphicon-folder-open"></span>
                          <span class="image-preview-input-title">Browse</span>
                          <input type="file" accept="image/png, image/jpeg, image/gif" name="image"/> <!-- rename it -->
                        </div>                      

                        <input type="submit" name="submit" class="btn btn-default image-preview-input" value="Upload"/>
                        <input type="button" class="btn btn-primary" value="Skip" onclick="window.open('../session/brgy_admin_update_session_client_new.php', '_top')" />
                      </span>
                    </div><!-- /input-group image-preview [TO HERE]--> 

                  </div>
                </div>                
              </form>

              <br>

            </div>

          </div>
        </div>
      </div>
    </div>

  </body>

</html>
