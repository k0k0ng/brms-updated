<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../database/brgy_admin_database_query.php'); // Database Query
?>

<!DOCTYPE html>
<html lang="en">

<?php
    // Add Fee Type
    if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Add"){
      $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID 
      $amount = $_POST["amount"]; // Minimum Amount
      $type = $_POST["fee_type"]; // Fee Type
      $collection_period = $_POST["collection_period"]; // Collection Period
      $starting_date = $_POST["starting_date"]; // Starting Date
      $collection_type = $_POST["collection_type"]; // Collection Type

      $query = new database_query(); // Database Query (initialize connection)
      $result = $query -> insert_fee_type($brgy_id,$collection_type,$type,$amount,$collection_period,$starting_date); // Insert Fee Type

      // Duplicate
      if($result == -1){
        echo "<script type='text/javascript'>
                alert('$type already exist!');
                location = 'add_feetype.php';
              </script>";
      }
      // Success      
      else if($result == 1){

        $query = new database_query(); // Database Query (initialize connection)
        $query -> insert_log($_SESSION['brms_userId'],"Add New Fee Type " . $type ); // Insert Log 

        echo "<script type='text/javascript'>
                alert('$type successfully added!');
                location = 'add_feetype.php';
              </script>";
      }
      // Error
      else{
        echo "<script type='text/javascript'> 
                alert('Error!'); 
                location = 'add_feetype.php';
              </script>";
      }

    }
?>

<head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>
    
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
      // On Submit
      function doCheck(){

        if (confirm("Are you sure to add this fee?")) {
          return true;
        } else {
          return false;
        }          

      }
    </script>
    
    <script>
        // LOAD COLLECTION TYPE
        $( document ).ready(function() {  

          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              var data = this.responseText.split(",");          

              for(var i = 0; i < data.length; i++){
                if(data[i] !== ""){
                  $('#collection_type').append("<option>"+data[i]+"</option>")
                }
              }

            }
          };

          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>"; // Barangay ID

          xmlhttp.open("GET", "../ajax/loadAjax_collectionType.php?brgy_id="+brgy_id, true); // Get purok names on (loadAjax_purok.php)
          xmlhttp.send();
        });
    </script>

    <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>
    
    <div class="container container-profile"><!-- Profile Divider --> 
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Add Fee Type</h3>
            </div>
            <div class="panel-body">
              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="?">

                <!--Fee Type-->
                <div class="input-group">
                  <span class="input-group-addon">Fee Type</span>
                  <input id="fee_type" type="text" class="form-control" name="fee_type" placeholder="" required>
                </div>
                <br>

                <!--Amount-->
                <div class="input-group">
                  <span class="input-group-addon">Amount</span>
                  <input id="amount" type="number" class="form-control" name="amount" placeholder="" required>
                </div>
                <br>
                
                <div class="input-group">
                  <span class="input-group-addon">Collection Type</span>
                  <select id="collection_type" name="collection_type" class="form-control"></select>
                </div>
                <br>

                <!--Collection Period-->
                <div class="input-group">
                  <span class="input-group-addon">Collection Period</span>
                  <select id="collection_period" name="collection_period" class="form-control">
                    <option>Monthly</option>
                    <option>Yearly</option>
                  </select>
                </div>
                <br>

                <!--Amount-->
                <div class="input-group">
                  <span class="input-group-addon">Collection Starting Date</span>
                  <input id="starting_date" type="date" class="form-control" name="starting_date" required>
                </div>

                <br>
                <input name="submit" type="submit" onclick="return doCheck()" value="Add" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                <a href="admin_home.php" class="btn btn-info" style="float: right; width:100px;">Back</a>
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </body>

</html>