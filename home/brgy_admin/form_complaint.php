<?php
  require('../session/brgy_admin.php'); // Secure Connection
  ?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2.css">

    <link rel="stylesheet" href="../css/table_Color.css"> <!-- COSTUMIZE TABLE COLOR -->

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
        // On Submit
        function doCheck(){

          if (confirm("Are you sure to print this complaint?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>
    
    <script>
      // Validate Submit
      function toSubmit(){        
        // Complainant is Empty
        if(document.getElementById("complainant_list").value === ''){
          alert('Walay Nagreklamo!');
          document.getElementById("complainant").focus(); // Focus
          return false;
        }
        // Complainee is Empty
        else if(document.getElementById("complainee_list").value === ''){
          alert('Walay Gireklamo!');
          document.getElementById("complainee").focus(); // Focus
          return false;
        }
        else{
          return doCheck();
        }
      }
    </script>
    
    <script>
      // Hide Live Search
      function hide_live_search(){
        document.getElementById("responsecontainer").innerHTML = '';
      }
    </script>

    <script>      
      $( document ).ready(function() {
        $(document).click(function(e){ hide_live_search(); }); // Document Click

        hide_live_search();

        $("#table_complainee_livesearch").on('click','.selectedRow',function(){
          // get the current row
          var currentRow=$(this).closest("tr");
          var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
          var name = currentRow.find("td:eq(1)").text(); // get current row 2nd TD value

          document.getElementById("complainee").value = name;
          document.getElementById("complainee_id").value = id;                  
        });       

      });
    </script>

    <style type="text/css">
      td{
        width: 95%; 
      }
    </style>
    
    <script>
      // Get Complainees      
      function get_complainee() {
        var table = document.getElementById('table_complainee');
        var result = '';
        for (var r = 0, n = table.rows.length; r < n; r++) {
          result += table.rows[r].cells[0].innerHTML + "<br>";
        }
        document.getElementById("complainee_list").value = $.trim(result);
              
      }
    </script>

    <script>
      // Get Complainants      
      function get_complainant() {
        var table = document.getElementById('table_complainant');
        var result = '';
        for (var r = 0, n = table.rows.length; r < n; r++) {
          result += table.rows[r].cells[0].innerHTML + "<br>";
        }
        document.getElementById("complainant_list").value = $.trim(result);
      }
    </script>

    <script>
      // OnKeyRelease
      function checkUser(str){

        // Back Key Pressed
        var key = event.keyCode || event.charCode;
        if( key == 8 || key == 46 ){
          document.getElementById("complainee").value = '';
          document.getElementById("complainee_id").value = '0';                        
        }

        if(str.length == 0){
          hide_live_search();          
        }else{
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {                  
              document.getElementById('responsecontainer').innerHTML = this.responseText; 
            }
          };
          var brgy_id = "<?php echo $_SESSION['brms_brgyId']; ?>";
          xmlhttp.open("GET", "../ajax/loadAjax_brgy_official.php?q="+str+"&brgy_id="+brgy_id, true);
          xmlhttp.send();  
        }     
      }
    </script>

    <script>
      // Delete Table Row
      function table_complainee_deleteRow(r) {
        var i = r.parentNode.parentNode.rowIndex;
        document.getElementById("table_complainee").deleteRow(i);
        get_complainee(); // Get Complainees on Table
      }
    </script>   

    <script>
      // Insert Complainant in Table
      function add_complainanee(){
        var str = document.getElementById("complainee").value;

        if(document.getElementById("complainee_id").value != 0){
          var table = document.getElementById("table_complainee");
          var row = table.insertRow(0);
          var cell1 = row.insertCell(0);
          var cell2 = row.insertCell(1);

          cell1.innerHTML = str;
          cell2.innerHTML = '<button class="btnUpdate" onclick="table_complainee_deleteRow(this)">Delete</button>';
          document.getElementById("complainee").value = ''; // Clear
          document.getElementById("complainee_id").value = 0; // Clear

          get_complainee(); // Get Complainees on Table
        }

        document.getElementById("complainee").focus(); // Focus
      }
    </script>
    
    <script>
      // Delete Table Row
      function table_complainant_deleteRow(r) {
        var i = r.parentNode.parentNode.rowIndex;
        document.getElementById("table_complainant").deleteRow(i);
        get_complainant(); // Get Complainants on Table
      }
    </script>   

    <script>
      // Insert Complainant in Table
      function add_complainant(){
        var str = document.getElementById("complainant").value;

        if(str.length>0){
          var table = document.getElementById("table_complainant");
          var row = table.insertRow(0);
          var cell1 = row.insertCell(0);
          var cell2 = row.insertCell(1);
          cell1.innerHTML = str;
          cell2.innerHTML = '<button class="btnUpdate" onclick="table_complainant_deleteRow(this)">Delete</button>';
          document.getElementById("complainant").value = ''; // Clear

          get_complainant(); // Get Complainants on Table
        }

        document.getElementById("complainant").focus(); // Focus
      }
    </script>    

    <!--for navigation bar-->

    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <div class="container container-profile"> <!-- Profile Divider -->  
      <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Certification to File Action (Complaint)</h3>
            </div>
            <div class="panel-body">

              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="../tcpdf_api/examples/complaint.php" target="_blank">
                <div class="input-group">
                  <span class="input-group-addon">Barangay Case No.</span>
                  <input type="text" id="brgy_case_no" name="brgy_case_no" class="form-control" placeholder="" required>
                </div>
                <br>
                <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                  <label for="person_concerned">Complainant: <small class="text-muted">(Nagreklamo)</small></label>
                  <div class="input-group" style="display: none">
                    <span class="input-group-addon">Complainants</span>
                    <textarea  id="complainant_list" type="textarea" class="form-control" name="complainant_list"></textarea>
                  </div>

                  <div class="input-group">
                    <span class="input-group-addon">Full Name</span>
                    <input id="complainant" type="text" class="form-control" name="complainant" placeholder="">
                    <span onclick="add_complainant()" class="input-group-addon btn btn-info">Add</span>
                  </div>

                  <div id="table_value_compainant">          
                    <table id="table_complainant" class="table table-condensed table-hover"></table>
                  </div>

                </div>

                <div class="input-group" style="display: none">
                  <span class="input-group-addon">Complainee ID</span>
                  <input id="complainee_id" name="complainee_id" value="0" type="text" class="form-control" placeholder="">
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  <label for="person_concerned">Complainee: <small class="text-muted">(Gireklamo)</small></label>
                  
                  <div class="input-group" style="display: none">
                    <span class="input-group-addon">Complainees</span>
                    <textarea  id="complainee_list" type="textarea" class="form-control" name="complainee_list"></textarea>
                  </div>

                  <div class="input-group">
                    <span class="input-group-addon">Name (autofill)</span>
                    <input id="complainee" onkeyup="checkUser(this.value)" type="text" class="form-control" name="complainee" placeholder="">
                    <span onclick="add_complainanee()" class="input-group-addon btn btn-info">Add</span>  
                  </div>

                  <div id="table_complainee_livesearch" style="background: #f2f2f2; border-radius: 0 0 5px 5px;">         
                    <table class="table-hover">                                          
                      <tbody id="responsecontainer" style="cursor: pointer;"></tbody>
                    </table>
                  </div>

                  <div id="table_value_compainant">          
                    <table id="table_complainee" class="table table-condensed table-hover"></table>
                  </div>

                </div>
              </div> <!-- end of first row -->
              <br>
              <div class="input-group">
                <span class="input-group-addon">For</span>
                <input id="for" name="for" type="text" class="form-control" placeholder="" required>
              </div>
              <br>
        
              <div class="form-group input-group">
                <span class="input-group-addon">Action Taken</span>
                <input id="action_taken" name="action_taken" type="text" class="form-control"  required>
              </div>                   
              
              <input name="submit" type="submit" onclick="return toSubmit()" value="Submit" class="btn btn-primary" style="float: right; margin-right:10px; width:100px;">
              <input name="clear" type="button" onclick="window.open('admin_home.php', '_top')" value="Back" class="btn btn-info" style="float: right; margin-right:10px; width:100px;">
              
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>

</html>
