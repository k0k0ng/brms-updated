<?php
  require('../session/brgy_admin.php'); // Secure Connection
  require('../session/brgy_admin_profile.php'); // Secure Profile Connection
?>

<?php 
  $gender_he_she = $_SESSION['brms_SESSION_userGender']==='Male' ? 'he' : 'she';
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom2test.css">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <!--Website Tab Icon-->
    <link rel="icon" type="image/png" href="../images/logo.png"/>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script>
        // On Submit
        function doCheck(){

          if (confirm("Are you sure to print this barangay clearance?")) {
            return true;
          } else {
            return false;
          }          
          
        }
    </script>

    <script>
      var cap_name = "<?php echo $_SESSION['brms_cap_name']; ?>";
      var wad1_name = "<?php echo $_SESSION['brms_wad1_name']; ?>";
      var wad2_name = "<?php echo $_SESSION['brms_wad2_name']; ?>";
      var wad3_name = "<?php echo $_SESSION['brms_wad3_name']; ?>";
      var wad4_name = "<?php echo $_SESSION['brms_wad4_name']; ?>";
      var wad5_name = "<?php echo $_SESSION['brms_wad5_name']; ?>";
      var wad6_name = "<?php echo $_SESSION['brms_wad6_name']; ?>";
      var wad7_name = "<?php echo $_SESSION['brms_wad7_name']; ?>";

      // LOAD PUROK
      $( document ).ready(function() {
        if(cap_name !== ''){
          $('#official_list').append("<option>"+cap_name+"</option>");
        }
        if(wad1_name !== ''){
          $('#official_list').append("<option>"+wad1_name+"</option>");
        }
        if(wad2_name !== ''){
          $('#official_list').append("<option>"+wad2_name+"</option>");
        }
        if(wad3_name !== ''){
          $('#official_list').append("<option>"+wad3_name+"</option>");
        }
        if(wad4_name !== ''){
          $('#official_list').append("<option>"+wad4_name+"</option>");
        }
        if(wad5_name !== ''){
          $('#official_list').append("<option>"+wad5_name+"</option>");
        }
        if(wad6_name !== ''){
          $('#official_list').append("<option>"+wad6_name+"</option>");
        }
        if(wad7_name !== ''){
          $('#official_list').append("<option>"+wad7_name+"</option>");
        }      
      });
    </script>

  <!--for navigation bar-->
     
    <script>
      $.get("navigation.php", function(data){
        $("#nav-placeholder").replaceWith(data);
      });
    </script>

  </head>

  <body>

    <div id="nav-placeholder"></div>

    <!-- Profile Divider --> 

    <div class="container container-profile">    
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Barangay Clearance</h3>
            </div>
            <div class="panel-body">
              <form method="post" role="form" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" action="../tcpdf_api/examples/barangay_clearance.php" target="_blank">
                
                <!--  COMMUNITY TAX CERTIFICATE -->
                <div class="input-group">
                      <span class="input-group-addon">Purpose</span>
                      <input id="purpose" type="text" class="form-control" name="purpose" required>
                </div>
                <br>

                <!--  COMMUNITY TAX CERTIFICATE -->
                <div class="input-group">
                      <span class="input-group-addon">Cedula No.</span>
                      <input id="ctcno" type="text" class="form-control" name="ctcno" required>
                </div>
                <br>

                <!--DATE-->
                <div class="form-group input-group">
                      <span class="input-group-addon">Issued on</span>
                      <input id="issuedon" type="date" class="form-control" name="issuedon" required>
                    </div>

                <div class="input-group">
                      <span class="input-group-addon">Receipt No.</span>
                      <input id="receipt" type="text" class="form-control" name="receipt">
                </div>
                <br>
                
                <div class="input-group">
                      <span class="input-group-addon">Barangay Official</span>
                      <select id="official_list" name="official_list" class="form-control"></select>
                </div>              

                <br>
                <input name="submit" type="submit" onclick="return doCheck()" value="Print" class="btn btn-primary" style="float: right; margin-left:10px; width:100px;">
                <span onclick="window.open('profile.php', '_top')" class="btn btn-info" style="float: right; width:100px;">Back</span>
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
