<?php

  	include 'database.php'; // Database Connection
  
  	class database_query{       

  		private $database; // Database
  		
	    // Constructor
	    public function __construct () {      
	      	$this -> database = new Database(); // New Database Connection
	  	}

	  	// Update Admin Password
	  	function update_admin_password($info_id,$password){
	  		$conn = $this->database->get_Connection(); // Get Database Connection 		

			$encrypt = password_hash($password, PASSWORD_DEFAULT); // Password Encryption
			$sql = "UPDATE account SET password = '$encrypt' WHERE info_id = $info_id";
			
			if(mysqli_query($conn, $sql)){
				$this -> insert_log($info_id,"Update Password");
      			return 1;

			}else{ 
				mysqli_close($conn);
				return 0;
			}
	  	}

	  	// Delete Certification Template
	  	function delete_certification_template($id,$brgy_id){

			$conn = $this->database->get_Connection(); // Get Database Connection 		
		
			$sql = "DELETE FROM certification 
					WHERE id = $id AND brgy_id = $brgy_id";
			
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;

			}else{ 
				mysqli_close($conn);
				return 0;
			}
		}

	  	// Update Certification Template
	  	function update_certification_template($id,$brgy_id,$header,$paragraph1,$paragraph2){

			$conn = $this->database->get_Connection(); // Get Database Connection 		
		
			$sql = "UPDATE certification SET
						   header = '$header',
						   paragraph1 = '$paragraph1',
						   paragraph2 = '$paragraph2'
					WHERE id = $id AND brgy_id = $brgy_id";
			
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;

			}else{ 
				mysqli_close($conn);
				return 0;
			}
		}

	  	// Insert Certification Template
	    function insert_certification_template($brgy_id,$header,$paragraph1,$paragraph2){
	
	    	$conn = $this->database->get_Connection(); // Get Database Connection

			$sql = "INSERT INTO certification (brgy_id,header,paragraph1,paragraph2) 
					VALUES ($brgy_id,'$header','$paragraph1','$paragraph2')";
			
			// Success
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;
			}
			// Error
			else{
				mysqli_close($conn);
				return 0;
			}
		}


	  	// Insert Log
	    function insert_log($info_id,$history){
	
	    	$conn = $this->database->get_Connection(); // Get Database Connection

			$sql = "INSERT INTO log (info_id,history,date_time) 
					VALUES ($info_id,'$history',NOW())";
			
			// Success
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;
			}
			// Error
			else{
				mysqli_close($conn);
				return 0;
			}
		}

	  	// Get Latest Business
		function select_last_business_id($brgy_id){

			$conn = $this->database->get_Connection(); // Get Database Connection

		    $sql = "SELECT business.* 
		    		FROM business 
		    		INNER JOIN purok ON business.purok_id = purok.id
		    		WHERE purok.brgy_id=$brgy_id ORDER BY business.id DESC";
		    $result = $conn->query($sql);

		    // Found
			if($result->num_rows > 0){			
				// output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		mysqli_close($conn);
		    		return $row['id'];
		    	}			
			}
      		// Not Found
			else{
				mysqli_close($conn);
				return 0;
			}
		}

		// Get Connection
		public function get_Connection(){
			return $this -> database -> get_Connection();
		}

		// Update Business
	    function update_business($business_id,$brgy_id,$purok_id,$form,$type,$info_id,$name,$address,$establish,$status,$permit,$contact){	

	    	$conn = $this->database->get_Connection(); // Get Database Connection

	    	$type_id = $this -> select_business_type_id($conn,$type); // Get Business Type ID
	    	$form_id = $this -> select_business_form_id($conn,$form); // Get Business Form ID	    	

			$sql = "UPDATE 
						business SET
						purok_id = $purok_id,
						form_id = $form_id,
						type_id = $type_id,
						info_id = $info_id,
						name = '$name',
						address = '$address',
						establish = '$establish',
						status = '$status',
						bus_permit = '$permit',
						contact_no = '$contact'
					WHERE id = $business_id";
			
			// Success
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;
			}
			// Error
			else{
				echo mysqli_error($conn) . "<br>";
				mysqli_close($conn);
				return 0;
			}
		}

		// Insert Note
	    function insert_note($info_id,$admin_id,$type,$note){
	
	    	$conn = $this->database->get_Connection(); // Get Database Connection

			$sql = "INSERT INTO note (info_id,admin_id,type,note,date_note) 
					VALUES ($info_id,$admin_id,'$type','$note',NOW())";
			
			// Success
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;
			}
			// Error
			else{
				mysqli_close($conn);
				return 0;
			}
		}

	  	// Insert Business
	    function insert_business($brgy_id,$purok_id,$form,$type,$info_id,$name,$address,$establish,$status,$permit,$contact){	

	    	$conn = $this->database->get_Connection(); // Get Database Connection

	    	// Duplicate
	    	if($this->select_business($conn,$brgy_id,$name,$address)==1){
	    		mysqli_close($conn);
	    		return -1;
	    	}	    	

	    	$type_id = $this -> select_business_type_id($conn,$type); // Get Business Type ID
	    	$form_id = $this -> select_business_form_id($conn,$form); // Get Business Form ID	    	

			$sql = "INSERT INTO business (purok_id,form_id,type_id,info_id,name,address,establish,status,bus_permit,contact_no) 
					VALUES ($purok_id,$form_id,$type_id,$info_id,'$name','$address','$establish','$status','$permit','$contact')";
			
			// Success
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;
			}
			// Error
			else{
				echo mysqli_error($conn) . "<br>";
				mysqli_close($conn);
				return 0;
			}
		}

		// Check Business
		function select_business($conn,$brgy_id,$name,$address){

		    $sql = "SELECT business.* 
		    		FROM business
		    		INNER JOIN purok ON business.purok_id = purok.id 
		    		WHERE purok.brgy_id=$brgy_id AND business.address='$address' AND business.name='$name'";

		    $result = $conn->query($sql);

		    // Found
			if($result->num_rows > 0){			
				return 1;			
			}
      		// Not Found
			else{
				return 0;
			}
		}

		// Get Business ID
		function select_business_id($brgy_id,$name,$address){
			
			$conn = $this->database->get_Connection(); // Get Database Connection 
		    $sql = "SELECT business.* 
		    		FROM business 
		    		INNER JOIN purok ON business.purok_id = purok.id
		    		WHERE purok.brgy_id=$brgy_id AND business.address='$address' AND business.name='$name'";
		    		
		    $result = $conn->query($sql);

		    // Found
			if($result->num_rows > 0){			
				// output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		mysqli_close($conn);
		    		return $row['id'];
		    	}			
			}
      		// Not Found
			else{
				mysqli_close($conn);
				return 0;
			}
		}

		// GET Business Form ID
		function select_business_form_id($conn,$form){

		    $sql = "SELECT * FROM business_form WHERE form = '$form'";
		    $result = $conn->query($sql);

		    // Found
			if($result->num_rows > 0){			
				// output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		return $row['id'];
		    	}				
			}
      		// Not Found
			else{
				return 0;
			}
		}	

		// GET Business Type ID
		function select_business_type_id($conn,$type){

		    $sql = "SELECT * FROM business_type WHERE type = '$type'";
		    $result = $conn->query($sql);

		    // Found
			if($result->num_rows > 0){			
				// output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		return $row['id'];
		    	}				
			}
      		// Not Found
			else{
				return 0;
			}
		}	

	    // Insert Fee Collection
	    function insert_fee_collection($brgy_id,$info_id,$admin_id,$fee_type,$amount_paid,$receipt_no){	

	    	$conn = $this->database->get_Connection(); // Get Database Connection 
	    	$fee_type_id = $this->select_fee_id($conn,$brgy_id,$fee_type); // Get Fee Type ID

			$sql = "INSERT INTO fee_collection (info_id, admin_id, fee_type_id, amount_paid, receipt_no, date_paid) 
					VALUES ($info_id,$admin_id,$fee_type_id,$amount_paid,'$receipt_no', NOW())";
			
			// Success
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;
			}
			// Error
			else{
				echo mysqli_error($conn) . "<br>";
				mysqli_close($conn);
				return 0;
			}
		}

		// Get Fee Type ID
		function select_fee_id($conn,$brgy_id,$fee_type){
			$sql = "SELECT * FROM fee_type 
			WHERE type='$fee_type' AND brgy_id = $brgy_id";

			$result = $conn->query($sql);

      		// Found
			if($result->num_rows > 0){			
				// output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		return $row['id'];
		    	}				
			}
      		// Not Found
			else{
				return 0;
			}
		}

		// Get Collection ID
	  	public function select_collection_id($conn,$collection_type) {

	  		$conn = $this->database->get_Connection(); // Get Database Connection

		    $sql = "SELECT * FROM collection_type WHERE type = '$collection_type'";
		    $result = $conn->query($sql);

		    if ($result->num_rows > 0) {
		      // output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		return $row['id'];
		    	}
		    }else{
		    	return 0;
		    }
		}	

	    // Insert Fee Type
	    function insert_fee_type($brgy_id,$collection_type,$type,$amount,$collection_period,$starting_date){	

	    	$conn = $this->database->get_Connection(); // Get Database Connection 
	    	$collection_id = $this->select_collection_id($conn,$collection_type); // Get Collection ID

			$sql = "INSERT INTO fee_type (brgy_id,collection_id, type, amount, period, start) 
					VALUES ($brgy_id,$collection_id,'$type',$amount,'$collection_period','$starting_date')";
			
			// Duplicate
			if($this->check_fee_type($conn, $brgy_id,$type,$collection_id)==1){
				mysqli_close($conn);
				return -1;
			}
			// Success
			else if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;
			}
			// Error
			else{
				mysqli_close($conn);
				return 0;
			}
		}

		// Check if citizen exist
		private function check_fee_type($conn, $brgy_id,$type,$collection_id){

			$sql = "SELECT * FROM fee_type 
			WHERE type='$type' AND brgy_id = $brgy_id AND collection_id = $collection_id";

			$result = $conn->query($sql);

      		// Found
			if($result->num_rows > 0){			
				// output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		return 1;
		    	}				
			}
      		// Not Found
			else{
				return 0;
			}
		}

	    // Insert Transaction
	    function insert_transaction($brgy_id,$info_id,$admin_id,$type,$receipt_no){	

	    	$conn = $this->database->get_Connection(); // Get Database Connection
			$sql = "INSERT INTO transaction (info_id, admin_id, type, receipt_no, date_transact) 
					VALUES ($info_id,$admin_id,'$type','$receipt_no',NOW())";
			
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;
			}else{
				mysqli_close($conn);
				return 0;
			}
		}
	    

		// Insert Purok
		function insert_purok($brgy_id,$name_old,$name_new,$info_id){

			$conn = $this->database->get_Connection(); // Get Database Connection 		
			$name_new = trim($name_new," ");

			// If Empty Purok Name
			if($name_new === ''){
				return 0;
			}

			// If Update
			if(isset($_SESSION['brms_UPDATE_purok_name'])){
				$sql = "UPDATE purok SET purok_leader = $info_id, name = '$name_new' WHERE name = '$name_old' AND brgy_id = $brgy_id";
			}
			// If Add
			else{
				$sql = "INSERT INTO purok (brgy_id, name, purok_leader) VALUES ($brgy_id,'$name_new',$info_id)";
			}

			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;

			}else{
				mysqli_close($conn);
				return 0;
			}
		}

	  	// Insert Unser Info
	  	public function insert_user_info(
	  		$brgy_id,
			$first_name,
			$last_name,
			$middle_name,
			$purok_id,
			$address,
			$birthdate,
			$gender,
			$status,
			$blood_type,
			$education,
			$occupation,
			$mobile,
			$telephone,
			$is_voter,
			$voter_id,
			$barangay_id,

			$residence_type,
			$cur_employed,
			$pwd,
			$cur_enrolled,
			$senior_citizen,
			$deceased,
			$religion
		){
		
		$conn = $this->database->get_Connection(); // Get Database Connection

		    $sql = "INSERT INTO user_info 
	  		
		    ( 
				    brgy_id,
				    first_name, 
				    last_name, 
				    middle_name, 
				    purok_id, 
				    address, 
				    birthdate, 
				    gender, 
				    status, 
				    blood_type, 
				    education, 
				    occupation, 
				    cell_no, 
				    tell_no,
				    is_voter,
				    voter_id,
				    citizen_brgy_id,				    

				    residence_type,
				 	cur_employed,
				 	pwd,
				 	cur_enrolled,
				 	senior_citizen,
				 	deceased,
				 	religion
		    ) 
		    VALUES
		    (		
		    		 $brgy_id,
				    '$first_name', 
				    '$last_name', 
				    '$middle_name', 
				     $purok_id, 
				    '$address', 
				    '$birthdate', 
				    '$gender', 
				    '$status', 
				    '$blood_type' , 
				    '$education' , 
				    '$occupation' ,  
				    '$mobile',  
				    '$telephone',
				    '$is_voter',
				    '$voter_id',
				    '$barangay_id',				    
					
					'$residence_type',
				 	'$cur_employed',
				 	'$pwd',
				 	'$cur_enrolled',
				 	'$senior_citizen',
				 	'$deceased',
				 	'$religion'

		    )";

		    // If account exist
		    if($this->check_citizen($conn, $brgy_id,$first_name,$middle_name,$last_name)==1){
		    	mysqli_close($conn);
		    	return -1;
		    }

		    if(mysqli_query($conn, $sql)){		    	
		    	$_SESSION['brms_newUser_id'] = $this->last_user(); // Get last user info added
		    	
		    	mysqli_close($conn);
		        return 1; // Success

		    }else{
		    	die(mysqli_error($conn));
		    	mysqli_close($conn);
		    	return 0; // Error
		    }
	  	}

	  	// Check if citizen exist
		private function check_citizen($conn, $brgy_id,$first_name,$middle_name,$last_name){

			$sql = "SELECT * FROM user_info 
			WHERE first_name='$first_name' AND last_name='$last_name' AND middle_name='$middle_name'
			AND brgy_id = $brgy_id";

			$result = $conn->query($sql);

      		// Found
			if($result->num_rows > 0){			
				// output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		$_SESSION['brms_newUser_id'] = $row['id']; // Get Info ID
		    		return 1;
		    	}				
			}
      		// Not Found
			else{
				return 0;
			}
		}

	  	// Get Last User
	  	public function last_user() {

	  		$conn = $this->database->get_Connection(); // Get Database Connection

		    $sql = "SELECT * FROM user_info ORDER BY id DESC";
		    $result = $conn->query($sql);

		    if ($result->num_rows > 0) {
		      // output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		return $row['id'];
		    	}
		    }else{
		    	return 0;
		    }
		}		

		// Get purok ID
	    public function select_purok_id($brgy_id,$name){

	    	$conn = $this->database->get_Connection(); // Get Database Connection

		  	$sql = "SELECT * FROM purok WHERE brgy_id = $brgy_id AND name = '$name'";

	      	$result = $conn->query($sql);
	      
	      	if($result->num_rows > 0){
		        // output data of each row
		        while($row = $result->fetch_assoc()) {
			        mysqli_close($conn);   
			        return $row['id'];
		        }

	      	}else{
		        mysqli_close($conn);   
		        return 0;
	      	}      
	    }

	    // Update Barangay Profile
		function update_brgy_info($brgy_id,$address,$telefax,$email,$website){

			$conn = $this->database->get_Connection(); // Get Database Connection 		
		
			$sql = "UPDATE barangay_info SET
						   address = '$address',
						   telefax = '$telefax',
						   email = '$email',
						   website = '$website'
					WHERE id = $brgy_id";
			
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;

			}else{ 
				mysqli_close($conn);
				return 0;
			}
		}

	    // Update Barangay Officials
		function update_brgy_official($brgy_id,$info_cap,$info_wad1,$info_wad2,$info_wad3,$info_wad4,$info_wad5,$info_wad6,$info_wad7,$info_sec){

			$conn = $this->database->get_Connection(); // Get Database Connection 		
		
			$info_cap = ($info_cap === '') ? 0 : $info_cap;
			$info_wad1 = ($info_wad1 === '') ? 0 : $info_wad1;
			$info_wad2 = ($info_wad2 === '') ? 0 : $info_wad2;
			$info_wad3 = ($info_wad3 === '') ? 0 : $info_wad3;
			$info_wad4 = ($info_wad4 === '') ? 0 : $info_wad4;
			$info_wad5 = ($info_wad5 === '') ? 0 : $info_wad5;
			$info_wad6 = ($info_wad6 === '') ? 0 : $info_wad6;
			$info_wad7 = ($info_wad7 === '') ? 0 : $info_wad7;
			$info_sec = ($info_sec === '') ? 0 : $info_sec;
			
			$sql = "UPDATE barangay_info SET
						info_cap = $info_cap,
						info_wad1 = $info_wad1,
						info_wad2 = $info_wad2,
						info_wad3 = $info_wad3,
						info_wad4 = $info_wad4,
						info_wad5 = $info_wad5,
						info_wad6 = $info_wad6,
						info_wad7 = $info_wad7,
						info_sec = $info_sec
					WHERE id = $brgy_id";
			
			if(mysqli_query($conn, $sql)){
				mysqli_close($conn);
      			return 1;

			}else{
				echo mysqli_error($conn) . "<br>";
				mysqli_close($conn);
				return 0;
			}
		}

		 // Update User Info
		 public function update_user_info(
		 	$brgy_id,
		 	$info_id,
		 	$purok_name,
		 	$first_name,
		 	$last_name,
		 	$middle_name,
		 	$purok_id,
		 	$address,
		 	$birthdate,
		 	$gender,
		 	$status,
		 	$blood_type,
		 	$occupation,
		 	$education,
		 	$cell_no,
		 	$tell_no,
		 	$is_voter,
		 	$voter_id,
		 	$citizen_brgy_id,

		 	$residence_type,
		 	$cur_employed,
		 	$pwd,
		 	$cur_enrolled,
		 	$senior_citizen,
		 	$deceased,
		 	$religion

		 ){
		  	$conn = $this->database->get_Connection(); // Get Database Connection

		  	if( //Ignore if same user
		  		$_SESSION['brms_SESSION_userFname'] !== $first_name AND
		  		$_SESSION['brms_SESSION_userMname'] !== $middle_name AND
		  		$_SESSION['brms_SESSION_userLname'] !== $last_name
		  		){
        		// If account exist
		  		if($this->check_citizen($conn, $brgy_id,$first_name,$middle_name,$last_name)==1){
		  			mysqli_close($conn);
		  			return -1;
		  		}
		  	}

		  	$sql = "UPDATE user_info SET " . 
		              "first_name = '" . $first_name . "', " .
		              "last_name = '" . $last_name . "', " .
		              "middle_name = '" . $middle_name . "', " .
		              "purok_id = " . $purok_id . ", " .
		              "address = '" . $address . "', " .
		              "birthdate = '" . $birthdate . "', " .
		              "gender = '" . $gender . "', " .

		              "status = '" . $status . "', " .
		              "blood_type = '" . $blood_type . "', " .
		              "occupation = '" . $occupation . "', " .
		              "education = '" . $education . "', " .

		              "cell_no = '" . $cell_no . "', " .
		              "tell_no = '" . $tell_no . "', " .

		              "is_voter = '" . $is_voter . "', " .
		              "voter_id = '" . $voter_id . "', " .
		              "citizen_brgy_id = '" . $citizen_brgy_id . "', " .

		              "residence_type = '" . $residence_type . "', " .
		              "cur_employed = '" . $cur_employed . "', " .
		              "pwd = '" . $pwd . "', " .
		              "cur_enrolled = '" . $cur_enrolled . "', " .
		              "senior_citizen = '" . $senior_citizen . "', " .
		              "deceased = '" . $deceased . "', " .
		              "religion = '" . $religion . "' " .

	          		"WHERE id = $info_id AND brgy_id = $brgy_id";

	    	if($conn->query($sql) === TRUE){
	    		if($_SESSION['brms_ADMIN'] == 1){
	    			// Set Admin SESSION
	    			$_SESSION['brms_userFname'] = $first_name;
	    			$_SESSION['brms_userLname'] = $last_name;
	    			$_SESSION['brms_userMname'] = $middle_name;
	    			$_SESSION['brms_userPurokId'] = $purok_id;
	    			$_SESSION['brms_userPurokName'] = $purok_name;

	    			$_SESSION['brms_userAddress'] = $address;
	    			$_SESSION['brms_userBdate'] = $birthdate;
	    			$_SESSION['brms_userGender'] = $gender;

	    			$_SESSION['brms_userStatus'] = $status;
	    			$_SESSION['brms_userBlood_type'] = $blood_type;    			
	    			$_SESSION['brms_userOccupation'] = $occupation;
	    			$_SESSION['brms_userEducation'] = $education;
	    			
	    			$_SESSION['brms_userCell'] =  $cell_no;
	    			$_SESSION['brms_userTell'] = $tell_no;

	    			$_SESSION['brms_userIsVoter'] = $is_voter;
	    			$_SESSION['brms_userVoterId'] = $voter_id;
	    			$_SESSION['brms_userCitizenBrgyId'] = $citizen_brgy_id;

	    			$_SESSION['brms_userResidenceType'] = $residence_type;
    				$_SESSION['brms_userCurEmployed'] = $cur_employed;
				    $_SESSION['brms_userPwd'] = $pwd;
				    $_SESSION['brms_userCurEnrolled'] = $cur_enrolled;
				    $_SESSION['brms_userSeniorCitizen'] = $senior_citizen;
				    $_SESSION['brms_userDeceased'] = $deceased;
				    $_SESSION['brms_userReligion'] = $religion;
	    		}      		
	    		else{
	    			// Set Client SESSION
	    			$_SESSION['brms_NEW_userFname'] = $first_name;
	    			$_SESSION['brms_NEW_userLname'] = $last_name;
	    			$_SESSION['brms_NEW_userMname'] = $middle_name;
	    			$_SESSION['brms_NEW_userPurokId'] = $purok_id;
	    			$_SESSION['brms_NEW_userPurokName'] = $purok_name;

	    			$_SESSION['brms_NEW_userAddress'] = $address;
	    			$_SESSION['brms_NEW_userBdate'] = $birthdate;
	    			$_SESSION['brms_NEW_userGender'] = $gender;

	    			$_SESSION['brms_NEW_userStatus'] = $status;
	    			$_SESSION['brms_NEW_userBlood_type'] = $blood_type;    			
	    			$_SESSION['brms_NEW_userOccupation'] = $occupation;
	    			$_SESSION['brms_NEW_userEducation'] = $education;

	    			$_SESSION['brms_NEW_userCell'] = $cell_no;
	    			$_SESSION['brms_NEW_userTell'] = $tell_no;

	    			$_SESSION['brms_NEW_userIsVoter'] = $is_voter;
	    			$_SESSION['brms_NEW_userVoterId'] = $voter_id;
	    			$_SESSION['brms_NEW_userCitizenBrgyId'] = $citizen_brgy_id;

	    			$_SESSION['brms_NEW_userResidenceType'] = $residence_type;
				    $_SESSION['brms_NEW_userCurEmployed'] = $cur_employed;
				    $_SESSION['brms_NEW_userPwd'] = $pwd;
				    $_SESSION['brms_NEW_userCurEnrolled'] = $cur_enrolled;
				    $_SESSION['brms_NEW_userSeniorCitizen'] = $senior_citizen;
				    $_SESSION['brms_NEW_userDeceased'] = $deceased;
				    $_SESSION['brms_NEW_userReligion'] = $religion;
	    		}
	    		mysqli_close($conn); 
	    		return 1; // Return Success
		    }else{ 
			    mysqli_close($conn); 
			    return 0; // Return Error
		    } 
		}

		// Upload image - Business
	    public function upload_image_business($brgy_id,$business_id,$img_name,$img_extension){

	      $conn = $this->database->get_Connection(); // Get Database Connection      
	      $sql = "UPDATE business 
	      		  SET img_name = '$img_name', img_extension = '$img_extension'
	      		  WHERE id = $business_id";

	      if(mysqli_query($conn, $sql)){	      	
	      	mysqli_close($conn);
	      	return 1; // Return Success

	      }else{
			echo mysqli_error($conn) . "<br>";
	      	mysqli_close($conn);
	      	return 0; // Return Error
	      }

	    }

		// Upload image - Brgy
	    public function upload_image_brgy($brgy_id,$img_name,$img_extension){

	      $conn = $this->database->get_Connection(); // Get Database Connection      

	      $sql = "UPDATE barangay_info SET img_name = '$img_name', img_extension = '$img_extension' 
	      		  WHERE id = $brgy_id";

	      if(mysqli_query($conn, $sql)){	      	
	      	mysqli_close($conn);
	      	return 1; // Return Success

	      }else{
	      	mysqli_close($conn);
	      	return 0; // Return Error
	      }

	    }

		// Upload image - USER
	    public function upload_image($brgy_id,$info_id,$img_name,$img_extension){

	      $conn = $this->database->get_Connection(); // Get Database Connection      

	      $sql = "UPDATE user_info SET img_name = '$img_name', img_extension = '$img_extension' 
	      		  WHERE id = $info_id AND brgy_id = $brgy_id";

	      if(mysqli_query($conn, $sql)){	      	
	      	mysqli_close($conn);
	      	return 1; // Return Success

	      }else{
	      	mysqli_close($conn);
	      	echo mysqli_error($conn) . "<br>";
	      	return 0; // Return Error
	      }

	    }
	    
		// GET Purok Count
		function select_purok_count($brgy_id,$purok_id){

			$conn = $this->database->get_Connection(); // Get Database Connection 

		    $sql = "SELECT count(*) AS count FROM user_info WHERE brgy_id = $brgy_id AND purok_id = $purok_id";
		    $result = $conn->query($sql);

		    while($row = $result->fetch_assoc()){
		    	mysqli_close($conn);
		    	return $row["count"];       
		    }
		    mysqli_close($conn);
		    return 0;
		}		

	}
?>