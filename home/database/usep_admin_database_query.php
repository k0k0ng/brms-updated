<?php
  
  include 'database.php'; // Database Connection

  class database_query{       

    private $database; // Database

    // Constructor
    public function __construct () {      
      $this -> database = new Database(); // New Database Connection
    }

    // Insert Log
    public function insert_log($history){
  
        $conn = $this->database->get_Connection(); // Get Database Connection

        $sql = "INSERT INTO log(info_id,history,date_time) 
                VALUES (224,'$history',NOW())";
        
        // Success
        mysqli_query($conn, $sql);
    }

    // Insert Usep Admin For Testing (done once)
    function insert_usep_admin(){

      $conn = $this->database->get_Connection(); // Get Database Connection 
      $user = "admin";
      $password = "usepmyadmin";

      $encrypt = password_hash($password, PASSWORD_DEFAULT); // Password Encryption

      $sql = "INSERT INTO usep_admin (username, password) VALUES ('$user','$encrypt')";

      mysqli_query($conn, $sql); // Execute Query
      mysqli_close($conn);
    }

    // Get Access Level
    public function select_access_level($conn, $access_level){

      $sql = "SELECT * FROM access_level WHERE id IN (2,3,5)";
      $result = $conn->query($sql);
        
      if($result->num_rows > 0){
          // output data of each row
        while($row = $result->fetch_assoc()) {
          if($access_level === $row['role']){
            return $row['id'];
          }
        }
      }   
    }

    // Check if admin exist
    private function check_admin($conn, $brgy_id,$first_name,$middle_name,$last_name){

      $sql = "SELECT user_info.*, account.access_id FROM user_info 
              INNER JOIN account ON user_info.id = account.info_id
              WHERE first_name='$first_name' AND last_name='$last_name' AND middle_name='$middle_name' 
              AND account.access_id < 4
              AND user_info.brgy_id = $brgy_id";
              
      $result = $conn->query($sql);

      // Found
      if($result->num_rows > 0){
        return 1;
      }
      // Not Found
      else{
        return 0;
      }
    }

    // Insert Admin Info
    public function insert_admin_info($brgy_id,$first_name,$middle_name,$last_name,$username,$password,$access_role){

      $conn = $this->database->get_Connection(); // Get Database Connection 
      $access_level = $this->select_access_level($conn, $access_role); // Get Access Level

      // If account exist
      if($this->check_admin($conn, $brgy_id,$first_name,$middle_name,$last_name)==1){
        mysqli_close($conn);
        return -1;
      }

      $sql = "INSERT INTO user_info (brgy_id, first_name, middle_name, last_name) 
              VALUES ($brgy_id,'$first_name','$middle_name','$last_name')";
      
      // Success
      if(mysqli_query($conn, $sql)){

        $encrypt = password_hash($password, PASSWORD_DEFAULT); // Password Encryption
        $this->insert_account($username,$encrypt,$access_level);        
        mysqli_close($conn);
        return 1;
      // Error
      }else{
        echo mysqli_error($conn);
        mysqli_close($conn);
        return 0;
      }      
    }

    // Insert Barangay
    public function insert_barangay($brgy_name,$city){

      $conn = $this->database->get_Connection(); // Get Database Connection
      $sql = "INSERT INTO barangay_info (brgy_name, city) VALUES ('$brgy_name','$city')";
      
      // Success
      if(mysqli_query($conn, $sql)){
        $this -> insert_log("Barangay " . $brgy_name . " added");       
        mysqli_close($conn);
        return 1;

      // Error
      }else{
		echo mysqli_error($conn);
        mysqli_close($conn);
        return 0;
      }
    }

    // Update Admin Info
    public function update_admin_info($brgy_id,$info_id,$first_name,$middle_name,$last_name,$username,$password,$access_role){

      $conn = $this->database->get_Connection(); // Get Database Connection 
      $access_level = $this->select_access_level($conn, $access_role); // Get Access Level


      if( //Ignore if same user
        $_SESSION['brms_usep_admin_add_first_name'] !== $first_name AND
        $_SESSION['brms_usep_admin_add_middle_name'] !== $middle_name AND
        $_SESSION['brms_usep_admin_add_last_name'] !== $last_name
      ){
        // If account exist
        if($this->check_admin($conn, $brgy_id,$first_name,$middle_name,$last_name)==1){
          mysqli_close($conn);
          return -1;
        }
      }
     
      $sql = "UPDATE user_info 
              SET first_name = '$first_name',
                  middle_name = '$middle_name',
                  last_name = '$last_name'                 
              WHERE brgy_id = $brgy_id AND id = $info_id";            
      
      if(mysqli_query($conn, $sql)){ 

        $encrypt = password_hash($password, PASSWORD_DEFAULT); // Password Encryption
        $this->update_account($info_id,$username,$encrypt,$access_level);      
        return 1; // Return Success

      }else{
        echo mysqli_error($conn);
        mysqli_close($conn);
        return 0; // Return Error
      }    
    } 

    // Update Account
    public function update_account($info_id,$username,$password,$access_level){

      $conn = $this->database->get_Connection(); // Get Database Connection      
      $sql = "UPDATE account SET username = '$username', password = '$password', access_id = '$access_level'
              WHERE info_id = $info_id";

      if(mysqli_query($conn, $sql)){
        $this -> insert_log("Admin updated");       
        mysqli_close($conn);
        return 1; // Return Success

      }else{
        mysqli_close($conn);
        return 0; // Return Error
      }
    }

    // Upload Barangay Name
    public function update_brgy_name($brgy_id,$brgy_name){

      $conn = $this->database->get_Connection(); // Get Database Connection      
      $sql = "UPDATE barangay_info SET brgy_name = '$brgy_name' WHERE id = $brgy_id";

      if(mysqli_query($conn, $sql)){       
        $this -> insert_log("Barangay " . $brgy_name . " updated");
        mysqli_close($conn);
        return 1; // Return Success

      }else{
        return 0; // Return Error
      }
    }

    // Insert Account
    public function insert_account($username,$password,$access_id){

      $info_id = $this->last_user();

      $conn = $this->database->get_Connection(); // Get Database Connection
      $sql = "INSERT INTO account (info_id, access_id, username, password) 
              VALUES ($info_id,$access_id,'$username','$password')";
      
      mysqli_query($conn, $sql);
      echo mysqli_error($conn);
      $this -> insert_log("New admin added");
    }

    // Get Last User
    public function last_user() {

      $conn = $this->database->get_Connection(); // Get Database Connection

      $sql = "SELECT * FROM user_info ORDER BY id DESC";
      $result = $conn->query($sql);

      if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
          return $row['id'];
        }
      }else{
        return 0;
      }
    }

    // Delete Account
    public function delete_account($info_id){

      $conn = $this->database->get_Connection(); // Get Database Connection      
      $sql = "DELETE FROM account WHERE info_id = $info_id";

      if(mysqli_query($conn, $sql)){       
        $this -> insert_log("Admin deleted");
        mysqli_close($conn);
        return 1; // Return Success

      }else{
        echo mysqli_error($conn);
        mysqli_close($conn);
        return 0; // Return Error
      }
    }

  }

?>