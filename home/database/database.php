<?php
	// Database Class
	class Database{

		private $servername;
		private $username;
		private $password;
		private $dbname;
		private $conn;

		// Constructor
		public function __construct () {
			$this -> servername = "localhost";
			$this -> username = "root";
			$this -> password = "";
			$this -> dbname = "brms";
			
			// Create connection
			$this -> conn = mysqli_connect($this->servername, $this->username, $this->password, $this->dbname);

			// Check connection
			if ($this->conn->connect_error) {
				die("Connection failed!");
			}
		}

		// Get Connection
		public function get_Connection(){
			return $this -> conn;
		}

		// Close Connection
		public function close_Connection(){
			mysqli_close($this -> conn);
		}

		// Mysql Dump - Using Api
		public function dump_sql(){
			$dbhost = $this -> servername;
			$dbuser = $this -> username;
			$dbpass = $this -> password;
			$dbname = $this -> dbname;
			
			$current_dir =  str_replace("\\","/",getcwd()); // Current Directory

			// Backup Database
			include_once($current_dir. '/mysqldump_api/src/Ifsnop/Mysqldump/Mysqldump.php');
			$dump = new Ifsnop\Mysqldump\Mysqldump('mysql:host='.$dbhost.';dbname='.$dbname, $dbuser, $dbpass);
			$dump->start($current_dir.'/backup/'.$dbname.'.sql');
		}

		/*
		// Mysql Dump - Traditional Approach
		public function dump_sql(){
			$dbhost = $this -> servername;
			$dbuser = $this -> username;
			$dbpass = $this -> password;
			$dbname = $this -> dbname;

			// Current Directory
			$current_dir =  str_replace("\\","/",getcwd());
			
			// Execute Backup
			exec ("mysqldump -u $dbuser --p$dbpass $dbname > ".$current_dir."/backup/$dbname.sql");

			// mysqldump -u root -pUsep2k16 brms > D:\test.sql
		}
		*/
	}
?>	