<?php
  
  include 'database.php'; // Database Connection

  class database_query{       

    private $database; // Database

    // Constructor
    public function __construct () {      
      $this -> database = new Database(); // New Database Connection
    }

    // Get Access Level
    public function select_access_level($conn, $access_level){

      $sql = "SELECT * FROM access_level WHERE id IN (2,3,5)";
      $result = $conn->query($sql);
        
      if($result->num_rows > 0){
          // output data of each row
        while($row = $result->fetch_assoc()) {
          if($access_level === $row['role']){
            return $row['id'];
          }
        }
      }   
    }

    // Insert Log
    function insert_log($info_id,$history){

        $conn = $this->database->get_Connection(); // Get Database Connection

        $sql = "INSERT INTO log (info_id,history,date_time) 
        VALUES ($info_id,'$history',NOW())";

        // Success
        if(mysqli_query($conn, $sql)){
          mysqli_close($conn);
          return 1;
        }
        // Error
        else{
          mysqli_close($conn);
          return 0;
        }
    }

    // Add Admin
    public function add_admin($brgy_id,$user_id,$access_level,$username,$password){    

      $conn = $this->database->get_Connection(); // Get Database Connection
      $access_level_id = $this->select_access_level($conn,$access_level); // Get Access Level ID  

      $encrypt = password_hash($password, PASSWORD_DEFAULT); // Password Encryption

      $sql = "INSERT INTO account (info_id, access_id, username, password)
              VALUES ($user_id, $access_level_id, '$username', '$encrypt')";

      if(mysqli_query($conn, $sql)){ 
        mysqli_close($conn);      
        return 1; // Return Success

      }else{
        die(mysqli_close($conn));
        return 0; // Return Error
      }
    }

    // Update Admin
    public function update_admin($user_id,$access_level,$password){     

      $conn = $this->database->get_Connection(); // Get Database Connection
      $access_level_id = $this->select_access_level($conn,$access_level); // Get Access Level ID  

      $encrypt = password_hash($password, PASSWORD_DEFAULT); // Password Encryption

      $sql = "UPDATE account SET access_id = $access_level_id, password='$encrypt'
              WHERE info_id = $user_id";

      if(mysqli_query($conn, $sql)){ 
        mysqli_close($conn);      
        return 1; // Return Success

      }else{
        die(mysqli_close($conn));
        mysqli_close($conn);
        return 0; // Return Error
      }
    }

    // Delete Account
    public function delete_account($info_id){

      $conn = $this->database->get_Connection(); // Get Database Connection      
      $sql = "DELETE FROM account WHERE info_id = $info_id";

      if(mysqli_query($conn, $sql)){       
        $this -> insert_log($info_id,"Admin deleted");
        return 1; // Return Success

      }else{
        echo mysqli_error($conn);
        mysqli_close($conn);
        return 0; // Return Error
      }
    }
    
  }
?>