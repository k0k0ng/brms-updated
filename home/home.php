<?php
  require('session/check_session.php'); // Check Session 
?>

<?php
  // LOGIN
  if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] === "Login") {
    require('session/secure_login.php'); // Secure Login
    $Secure_Login = new Secure_Login($_POST["user"], $_POST["pass"]); // Secure Login
  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>
  
    <title>BRMS - Barangay Record Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom2.css">

    <link rel="stylesheet" href="css/footer.css">

    <link rel="javascript" src="js/jquery.js">
    <link rel="javascript" src="js/jquery.min.js">

    <link rel="javascript" src="../js/jquery.js">
    <link rel="javascript" src="../js/jquery.min.js">

    <link rel="icon" type="image/png" href="images/logo.png"/>

  </head>

  <body>

    <div class="example3">
      <nav class="navbar navbar-inverse navbar-static-top" style="background-color: #7a0404;">
        <div class="container">
          <img src="brgy_image/brms_logo.png" style="height: 60px;margin: 10px;">
        </div><!--/.nav-collapse -->
      </nav><!--/.container-fluid -->
    </div>

    <div class="container">
      <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="images/avatar.png" />
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin" method="post" autocomplete="off" action="?">
          <span id="reauth-email" class="reauth-email"></span>
          <input type="text" name="user" id="inputUser" class="form-control" placeholder="Username" required autofocus>
          <input type="password" name="pass" id="inputPassword" class="form-control" placeholder="Password" required>
          <input type="submit" name="submit" value="Login" class="btn btn-2 btn-lg btn-primary btn-block btn-signin"> <!-- CHANGE THE btn to btn-2-->
        </form><!-- /form -->
      </div><!-- /card-container -->
    </div>

    <div class="footer-bottom">
      <div class="container">
        <div class="row">

          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="copyright">
              © 2018, Institute of Computing, All rights reserved
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="design">
             <a href="#">EYE-SEE Enabled Barangay Program</a>
           </div>
         </div>

       </div>
     </div>
   </div>

  </body>

</html>