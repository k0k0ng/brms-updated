<?php		
	// Update session for usep admin barangay purok is_set
	if (!empty($_GET)) {

		session_start();

	    // UPDATE SESSION		

		$info_id = $_GET["info_id"];
		$brgy_id = $_SESSION['brms_usep_admin_barangay_purok_brgy_id']; // Barangay ID

		include '../database/database.php'; // Database
      	$query = new Database(); // Database Query
      	$conn = $query -> get_Connection(); // Get Database Connection

      	$sql = "SELECT user_info.*, account.access_id, account.username, account.password, access_level.role
                    FROM user_info 
                    INNER JOIN account ON user_info.id = account.info_id
                    INNER JOIN access_level ON account.access_id = access_level.id
                    WHERE user_info.id = $info_id AND user_info.brgy_id = $brgy_id";
      	$result = $conn->query($sql);

      	echo mysqli_error($conn);

      	if($result->num_rows > 0){
          // output data of each row
      		while($row = $result->fetch_assoc()) {

      			$_SESSION['brms_usep_admin_add_info_id'] = $row['id'];
      			$_SESSION['brms_usep_admin_add_first_name'] = $row['first_name'];
      			$_SESSION['brms_usep_admin_add_middle_name'] = $row['middle_name'];
      			$_SESSION['brms_usep_admin_add_last_name'] = $row['last_name'];
      			$_SESSION['brms_usep_admin_add_role'] = $row['role'];

      			$_SESSION['brms_usep_admin_add_username'] = $row['username'];
      			$_SESSION['brms_usep_admin_add_password'] = $row['password'];

      			mysqli_close($conn); // Close Connection
      			header('Location: ../usep_admin/add_admin.php');
      		}
      	}  
	}else{
		header('Location: ../usep_admin/admin_home.php');
	}
?>

