<?php
  session_start();

  // Set SESSION to client
    
  if (!empty($_GET)) {
      
      $_SESSION['brms_template_id'] = $_GET["id"];
      $_SESSION['brms_template_brgyid'] = $_GET["brgy_id"];
      $_SESSION['brms_template_header'] = $_GET["header"];
      $_SESSION['brms_template_paragraph1'] = $_GET["paragraph1"];
      $_SESSION['brms_template_paragraph2'] = $_GET["paragraph2"];

      header('Location: ../brgy_admin/certification_template.php');
    
  }else{
    header('Location: ../brgy_admin/admin_home.php');
  }
?>