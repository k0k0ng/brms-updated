<?php
  // If admin profile is viewed (SESSIONS for Admin)
  if($_SESSION['brms_ADMIN'] == 1){
    $_SESSION['brms_SESSION_userId'] = $_SESSION['brms_userId'];
    $_SESSION['brms_SESSION_userFname'] = $_SESSION['brms_userFname'];
    $_SESSION['brms_SESSION_userLname'] = $_SESSION['brms_userLname'];
    $_SESSION['brms_SESSION_userMname'] = $_SESSION['brms_userMname'];

    $_SESSION['brms_SESSION_userImgName'] = $_SESSION['brms_userImgName'];
    $_SESSION['brms_SESSION_userImgExtension'] = $_SESSION['brms_userImgExtension'];

    $_SESSION['brms_SESSION_userPurokId'] = $_SESSION['brms_userPurokId'];
    $_SESSION['brms_SESSION_userPurokName'] = $_SESSION['brms_userPurokName'];

    $_SESSION['brms_SESSION_userAddress'] = $_SESSION['brms_userAddress'];
    $_SESSION['brms_SESSION_userBdate'] = $_SESSION['brms_userBdate'];
    $_SESSION['brms_SESSION_userGender'] = $_SESSION['brms_userGender'];

    $_SESSION['brms_SESSION_userStatus'] = $_SESSION['brms_userStatus'];
    $_SESSION['brms_SESSION_userBlood_type'] = $_SESSION['brms_userBlood_type'];
    $_SESSION['brms_SESSION_userEducation'] = $_SESSION['brms_userEducation'];
    $_SESSION['brms_SESSION_userOccupation'] = $_SESSION['brms_userOccupation'];

    $_SESSION['brms_SESSION_userCell'] = $_SESSION['brms_userCell'];
    $_SESSION['brms_SESSION_userTell'] = $_SESSION['brms_userTell'];

    $_SESSION['brms_SESSION_userIsVoter'] = $_SESSION['brms_userIsVoter'];

    $_SESSION['brms_SESSION_userVoterId'] = $_SESSION['brms_userVoterId'];
    $_SESSION['brms_SESSION_userCitizenBrgyId'] = $_SESSION['brms_userCitizenBrgyId'];

    $_SESSION['brms_SESSION_userResidenceType'] = $_SESSION['brms_userResidenceType'];
    $_SESSION['brms_SESSION_userCurEmployed'] = $_SESSION['brms_userCurEmployed'];
    $_SESSION['brms_SESSION_userPwd'] = $_SESSION['brms_userPwd'];
    $_SESSION['brms_SESSION_userCurEnrolled'] = $_SESSION['brms_userCurEnrolled'];
    $_SESSION['brms_SESSION_userSeniorCitizen'] = $_SESSION['brms_userSeniorCitizen'];
    $_SESSION['brms_SESSION_userDeceased'] = $_SESSION['brms_userDeceased'];
    $_SESSION['brms_SESSION_userReligion'] = $_SESSION['brms_userReligion'];


  }
  // Else client user profile is viewed (SESSIONS for Client)
  else{
    $_SESSION['brms_SESSION_userId'] = $_SESSION['brms_NEW_userId'];
    $_SESSION['brms_SESSION_userFname'] = $_SESSION['brms_NEW_userFname'];
    $_SESSION['brms_SESSION_userLname'] = $_SESSION['brms_NEW_userLname'];
    $_SESSION['brms_SESSION_userMname'] = $_SESSION['brms_NEW_userMname'];

    $_SESSION['brms_SESSION_userImgName'] = $_SESSION['brms_NEW_userImgName'];
    $_SESSION['brms_SESSION_userImgExtension'] = $_SESSION['brms_NEW_userImgExtension'];

    $_SESSION['brms_SESSION_userPurokId'] = $_SESSION['brms_NEW_userPurokId'];
    $_SESSION['brms_SESSION_userPurokName'] = $_SESSION['brms_NEW_userPurokName'];

    $_SESSION['brms_SESSION_userAddress'] = $_SESSION['brms_NEW_userAddress'];
    $_SESSION['brms_SESSION_userBdate'] = $_SESSION['brms_NEW_userBdate'];
    $_SESSION['brms_SESSION_userGender'] = $_SESSION['brms_NEW_userGender'];

    $_SESSION['brms_SESSION_userStatus'] = $_SESSION['brms_NEW_userStatus'];
    $_SESSION['brms_SESSION_userBlood_type'] = $_SESSION['brms_NEW_userBlood_type'];
    $_SESSION['brms_SESSION_userEducation'] = $_SESSION['brms_NEW_userEducation'];
    $_SESSION['brms_SESSION_userOccupation'] = $_SESSION['brms_NEW_userOccupation'];

    $_SESSION['brms_SESSION_userCell'] = $_SESSION['brms_NEW_userCell'];
    $_SESSION['brms_SESSION_userTell'] = $_SESSION['brms_NEW_userTell'];

    $_SESSION['brms_SESSION_userIsVoter'] = $_SESSION['brms_NEW_userIsVoter'];

    $_SESSION['brms_SESSION_userVoterId'] = $_SESSION['brms_NEW_userVoterId'];
    $_SESSION['brms_SESSION_userCitizenBrgyId'] = $_SESSION['brms_NEW_userCitizenBrgyId'];

    $_SESSION['brms_SESSION_userResidenceType'] = $_SESSION['brms_NEW_userResidenceType'];
    $_SESSION['brms_SESSION_userCurEmployed'] = $_SESSION['brms_NEW_userCurEmployed'];
    $_SESSION['brms_SESSION_userPwd'] = $_SESSION['brms_NEW_userPwd'];
    $_SESSION['brms_SESSION_userCurEnrolled'] = $_SESSION['brms_NEW_userCurEnrolled'];
    $_SESSION['brms_SESSION_userSeniorCitizen'] = $_SESSION['brms_NEW_userSeniorCitizen'];
    $_SESSION['brms_SESSION_userDeceased'] = $_SESSION['brms_NEW_userDeceased'];
    $_SESSION['brms_SESSION_userReligion'] = $_SESSION['brms_NEW_userReligion'];
  }  
?>