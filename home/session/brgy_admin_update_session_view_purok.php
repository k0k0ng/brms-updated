<?php	
	session_start();

	// Session for Viewing purok (with Purok Leader)

	if (!empty($_GET)) {

	    // UPDATE SESSION
		function updateSession() {

			$_SESSION['brms_VIEW_purok_id']   = $_GET["purok_id"];
			$_SESSION['brms_VIEW_purok_name']   = $_GET["purok_name"];
			$_SESSION['brms_VIEW_purok_leader'] = $_GET["purok_leader"];

	      	header('Location: ../brgy_admin/view_purok.php');
	   	}

	   updateSession(); // START UPDATE
	    
	}else{
		header('Location: ../brgy_admin/admin_home.php');
	}
?>