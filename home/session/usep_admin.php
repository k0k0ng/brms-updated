<?php
	session_start();
	if($_SESSION['brms_security'] == 2 || $_SESSION['brms_security'] == 3 || $_SESSION['brms_security'] == 5 || !isset($_SESSION['brms_security'])){
		header('Location: ../home.php');
	}		
?>

<?php 
	class Usep_Admin{
		
		// Constructor
	    public function __construct () {      
	      $this -> unset_session(); // Unset Sessions
	    }

	    // Unset Session
		private function unset_session(){
			$helper = array_keys($_SESSION);
			foreach ($helper as $key){
				unset($_SESSION[$key]);
			}
		}

	    // Login
	    public function login(){
	    	$_SESSION['brms_security'] = 1;
	    	header('Location: usep_admin/admin_home.php');  // Usep Admin	
	    }

	    // Logout
	    public function logout(){
	    	$_SESSION['brms_security'] = 5;
	    	header('Location: ../home.php');  // Home	
	    }

	}	
?>
