<?php
	require('brgy_admin.php');
	require('../database/database.php'); // Database Connection

	$info_id = $_SESSION['brms_userId'];
	$brgy_id = $_SESSION['brms_brgyId'];

	$Brgy_Admin = new Brgy_Admin($info_id,$brgy_id);
	$Brgy_Admin -> logout($info_id);
?>