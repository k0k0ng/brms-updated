<?php	
	session_start();

	// Update purok

	if (!empty($_GET)) {

		function updateSession() {
			include '../database.php'; // Database

			$_SESSION['brms_VIEW_purok_id']   = $_GET["purok_id"];
			$_SESSION['brms_UPDATE_user_id'] =  $_GET["user_id"];
			$_SESSION['brms_UPDATE_purok_name'] = $_GET["purok_name"];
			$_SESSION['brms_UPDATE_citizen_name'] = $_GET["citizen_name"];

	      	header('Location: ../brgy_admin/add_purok.php');
	   	}
	   	
	   updateSession(); // START UPDATE
	    
	}else{
		header('Location: ../brgy_admin/admin_home.php');
	}
?>