<?php  
  require('database/database.php'); // Database Connection
?>

<?php
  // Database Class
  class Secure_Login{

    private $username; // Username
    private $password; // Password

    private $info_id; // Info ID
    private $brgy_id; // Barangay ID

    // Constructor
    public function __construct ($username, $password) {      
      $this -> username = $username; // Set username
      $this -> password = $password; // Set Password
      $this -> login(); // Login
    }

    // Login
    private function login(){
      $database = new Database(); // Get Database Connection
      $conn = $database -> get_Connection(); // Get Connection

      $sql = "SELECT 
                user_info.id,
                user_info.brgy_id,
                account.username,
                account.password,
                account.access_id,
                account.password
              FROM user_info
              INNER JOIN account ON user_info.id = account.info_id"; // Query
      $result = $conn -> query($sql); //  Execute Query

      if ($result->num_rows > 0) {
      // output data of each row
        while($row = $result->fetch_assoc()) {
          // Check Username and Password

          // Decryption
          if($row["username"] === $this->username && password_verify($this->password, $row["password"])){
            $this -> info_id = $row["id"];
            $this -> brgy_id = $row["brgy_id"];
            $this -> access_level($row["access_id"]); // Get Access Level
            $_SESSION['brms_password'] = $row["password"];
          }
        }
      } 

      echo "<script type='text/javascript'>
                alert('Invalid Username or Password!');
            </script>";      
    }

    // Access Levels
    private function access_level($level){
      switch ($level) {
        case 1: 
          $this -> usep_admin(); // Usep Admin
        break;
        
        case 2: 
          $this -> brgy_captain(); // Barangay Captain         
        break;

        case 3: 
          $this -> brgy_admin(); // Barangay Staff          
        break;

        case 4: 
          $this -> brgy_citizen(); // Barangay Citizen         
        break;
      }
    }

    // Usep Admin ($_SESSION['brms_security'] = 1)
    private function usep_admin(){
      require('usep_admin.php');
      $Usep_Admin = new Usep_Admin();
      $Usep_Admin -> login();
    }

    // Barangay Captain ($_SESSION['brms_security'] = 2)
    private function brgy_captain(){
      require('brgy_captain.php');
      $Brgy_Captain = new Brgy_Captain($this->info_id,$this->brgy_id);
      $Brgy_Captain -> login();
    }

    // Barangay Staff ($_SESSION['brms_security'] = 3)
    private function brgy_admin(){
      require('brgy_admin.php');
      $Brgy_Admin = new Brgy_Admin($this->info_id,$this->brgy_id);
      $Brgy_Admin -> login();
    }

    // Barangay Citizen ($_SESSION['brms_security'] = 4)
    private function brgy_citizen(){
    }

  }
?>