<?php
  // Check if usep admin barangay purok is_set
  if(!isset($_SESSION['brms_usep_admin_barangay_purok_brgy_id']) || empty($_SESSION['brms_usep_admin_barangay_purok_brgy_id'])) {
    header('Location: ../usep_admin/admin_home.php');
  }
?>