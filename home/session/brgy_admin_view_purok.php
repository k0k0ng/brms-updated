<?php
  // View Purok
  if(isset($_SESSION['brms_VIEW_purok_name']) || !empty($_SESSION['brms_VIEW_purok_name'])) {

    include '../database/brgy_admin_database_query.php'; // Database Query
    $query = new database_query(); // Database Query

    // SET PUROK VALUE
    $purok_name = $_SESSION['brms_VIEW_purok_name'];
    $purok_leader = $_SESSION['brms_VIEW_purok_leader'];
    $purok_population = $query -> select_purok_count($_SESSION['brms_brgyId'],$_SESSION['brms_VIEW_purok_id']); // Count citizen in purok

    if($purok_leader === ''){
      $purok_leader = 'Unknown';
    }
    
  }else{
    header('Location: ../brgy_admin/admin_home.php');
  }
?>