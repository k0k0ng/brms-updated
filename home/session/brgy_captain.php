<?php
	session_start();
	if($_SESSION['brms_security'] == 1 || $_SESSION['brms_security'] == 3 || $_SESSION['brms_security'] == 5 || !isset($_SESSION['brms_security'])){
		header('Location: ../home.php');
	}
?>

<?php 
	class Brgy_Captain{
		
		private $info_id; // Info ID
		private $brgy_id; // Barangay ID

		// Constructor
	    public function __construct ($info_id,$brgy_id) {      
	      $this -> unset_session(); // Unset Sessions
	      $this -> info_id = $info_id;
	      $this -> brgy_id = $brgy_id;
	    }

	    // Unset Session
		private function unset_session(){
			$helper = array_keys($_SESSION);
			foreach ($helper as $key){
				unset($_SESSION[$key]);
			}
		}

	    // Login
	    public function login(){
	    	$this -> update_brgy_session_info($this->brgy_id, $this->info_id); // Update Session
	    	$this -> update_brgy_officials(); // Update Barangay Officials
	    	$this -> insert_log($_SESSION['brms_userId'],"Login"); // Insert Log
	    	$_SESSION['brms_security'] = 2;
	    	header('Location: brgy_captain/admin_home.php');  // Brgy Admin	
	    }

	    // Logout
	    public function logout(){
	    	$this -> insert_log($_SESSION['brms_userId'],"Logout"); // Insert Log
	    	$_SESSION['brms_security'] = 5;
	    	header('Location: ../home.php');  // Home
	    }

	    // Barangay Admin Info
		function update_brgy_session_info($brgy_id, $info_id) {
		    $database = new Database(); // Create Database

		    $sql = "SELECT
		    			barangay_info.brgy_name AS 'brgy_name',
					    barangay_info.img_name AS 'brgy_img_name',
					    barangay_info.img_extension AS 'brgy_img_extension',

					    barangay_info.address AS 'brgy_address',
					    barangay_info.telefax AS 'brgy_telefax',
					    barangay_info.email AS 'brgy_email',
					    barangay_info.website AS 'brgy_website',

					    user_info.id AS 'id',
					    user_info.first_name AS 'fname',
					    user_info.last_name AS 'lname',
					    user_info.middle_name AS 'mname',

					    user_info.img_name AS 'user_img_name',
					    user_info.img_extension AS 'user_img_extension',

					    user_info.purok_id AS 'purok_id',
					    purok.name AS 'purok_name',

					    user_info.address AS 'address',
					    user_info.birthdate AS 'bdate',
					    user_info.gender AS 'gender',

					    user_info.status AS 'status',
					    user_info.blood_type AS 'blood_type',
					    user_info.education AS 'education',
					    user_info.occupation AS 'occupation',

					    user_info.cell_no AS 'cell',
					    user_info.tell_no AS 'tell',

					    user_info.is_voter AS 'is_voter',
					    user_info.voter_id AS 'voter_id',
					    user_info.citizen_brgy_id AS 'citizen_brgy_id',

						user_info.residence_type AS 'residence_type',
						user_info.cur_employed AS 'cur_employed',
						user_info.pwd AS 'pwd',
						user_info.cur_enrolled AS 'cur_enrolled',
						user_info.senior_citizen AS 'senior_citizen',
						user_info.deceased AS 'deceased',
						user_info.religion AS 'religion'

					    FROM user_info 
					    LEFT OUTER JOIN purok ON user_info.purok_id = purok.id
					    INNER JOIN barangay_info ON user_info.brgy_id = barangay_info.id
					    WHERE user_info.brgy_id = $brgy_id AND user_info.id = $info_id";

		    $result = $database -> get_Connection() -> query($sql);

		    if ($result->num_rows > 0) {

		      	// output data of each row
		    	while($row = $result->fetch_assoc()) {    

		        	// SET SESSION
		    		$_SESSION['brms_brgyId'] =  $brgy_id;
		    		$_SESSION['brms_brgyName'] = $row["brgy_name"];
		    		$_SESSION['brms_brgyImgName'] =  $row["brgy_img_name"];
		    		$_SESSION['brms_brgyImgExtension'] =  $row["brgy_img_extension"];

		    		$_SESSION['brms_brgyAddress'] = $row["brgy_address"];
		    		$_SESSION['brms_brgyTelefax'] = $row["brgy_telefax"];
		    		$_SESSION['brms_brgyEmail'] = $row["brgy_email"];
		    		$_SESSION['brms_brgyWebsite'] = $row["brgy_website"];
		    		
		    		$_SESSION['brms_userId'] = $row["id"];
		    		$_SESSION['brms_userFname'] = $row["fname"];
		    		$_SESSION['brms_userLname'] = $row["lname"];
		    		$_SESSION['brms_userMname'] = $row["mname"];

		    		$_SESSION['brms_userImgName'] = $row["user_img_name"];
		    		$_SESSION['brms_userImgExtension'] = $row["user_img_extension"];

		    		$_SESSION['brms_userPurokId'] = $row["purok_id"];
		    		$_SESSION['brms_userPurokName'] = $row["purok_name"];

		    		$_SESSION['brms_userAddress'] = $row["address"];
		    		$_SESSION['brms_userBdate'] = $row["bdate"];
		    		$_SESSION['brms_userGender'] = $row["gender"];

		    		$_SESSION['brms_userStatus'] = $row["status"];
		    		$_SESSION['brms_userBlood_type'] = $row["blood_type"];
		    		$_SESSION['brms_userEducation'] = $row["education"];
		    		$_SESSION['brms_userOccupation'] = $row["occupation"];

		    		$_SESSION['brms_userCell'] = $row["cell"];
		    		$_SESSION['brms_userTell'] = $row["tell"];

		    		$_SESSION['brms_userIsVoter'] = $row["is_voter"];
		    		$_SESSION['brms_userVoterId'] = $row["voter_id"];
		    		$_SESSION['brms_userCitizenBrgyId'] = $row["citizen_brgy_id"];

		    		$_SESSION['brms_userResidenceType'] = $row["residence_type"];
		    		$_SESSION['brms_userCurEmployed'] = $row["cur_employed"];
		    		$_SESSION['brms_userPwd'] = $row["pwd"];
		    		$_SESSION['brms_userCurEnrolled'] = $row["cur_enrolled"];
		    		$_SESSION['brms_userSeniorCitizen'] = $row["senior_citizen"];
		    		$_SESSION['brms_userDeceased'] = $row["deceased"];
		    		$_SESSION['brms_userReligion'] = $row["religion"];

			        $database -> close_Connection(); // Close Database Connection
			        return;		        
		   		}
			}
		    $database -> close_Connection(); // Close Database Connection
		    return 0;
		}

		// Insert Log
		private function insert_log($info_id,$history){
			$database = new Database(); // New Conenction
		    $conn = $database->get_Connection(); // Get Database Connection

			$sql = "INSERT INTO log (info_id,history,date_time) 
					VALUES ($info_id,'$history',NOW())";

			$result = $conn->query($sql);
			mysqli_close($conn);
		}

		// Barangay OFFICIALS
		private function update_brgy_officials(){
		    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
		    $database = new Database(); // New Conenction
		    $conn = $database->get_Connection(); // Get Database Connection

		    $sql = "SELECT * FROM barangay_info WHERE brgy_id = $brgy_id";
		    $result = $conn->query($sql);

		    if ($result->num_rows > 0) {
		      // output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		$_SESSION['brms_cap'] = $row['info_cap'];
		    		$_SESSION['brms_wad1'] = $row['info_wad1'];
		    		$_SESSION['brms_wad2'] = $row['info_wad2'];
		    		$_SESSION['brms_wad3'] = $row['info_wad3'];
		    		$_SESSION['brms_wad4'] = $row['info_wad4'];
		    		$_SESSION['brms_wad5'] = $row['info_wad5'];
		    		$_SESSION['brms_wad6'] = $row['info_wad6'];
		    		$_SESSION['brms_wad7'] = $row['info_wad7'];    
		    	}
		    }

		    $_SESSION['brms_cap_name']  = $this -> update_brgy_officials_name($conn, $_SESSION['brms_cap']);
		    $_SESSION['brms_wad1_name'] = $this -> update_brgy_officials_name($conn, $_SESSION['brms_wad1']);
		    $_SESSION['brms_wad2_name'] = $this -> update_brgy_officials_name($conn, $_SESSION['brms_wad2']);
		    $_SESSION['brms_wad3_name'] = $this -> update_brgy_officials_name($conn, $_SESSION['brms_wad3']);
		    $_SESSION['brms_wad4_name'] = $this -> update_brgy_officials_name($conn, $_SESSION['brms_wad4']);
		    $_SESSION['brms_wad5_name'] = $this -> update_brgy_officials_name($conn, $_SESSION['brms_wad5']);
		    $_SESSION['brms_wad6_name'] = $this -> update_brgy_officials_name($conn, $_SESSION['brms_wad6']);
		    $_SESSION['brms_wad7_name'] = $this -> update_brgy_officials_name($conn, $_SESSION['brms_wad7']);

		    mysqli_close($conn);
		}

		// Barangay OFFICIALS
		private function update_brgy_officials_name($conn, $info_id){
		    $brgy_id = $_SESSION['brms_brgyId']; // Barangay ID
		    $sql = "SELECT * FROM user_info WHERE brgy_id = $brgy_id AND id = $info_id";
		    $result = $conn->query($sql);

		    if ($result->num_rows > 0) {
		      // output data of each row
		    	while($row = $result->fetch_assoc()) {
		    		return $row['first_name'] . ' ' . $row['middle_name'][0] . '. ' . $row['last_name'];
		    	}
		    }
		    return '';
		}
	}	
?>