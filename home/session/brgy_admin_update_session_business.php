<?php
  session_start();

  // Set SESSION to client
    
  if (!empty($_GET)) {
      
      include '../database/database.php'; // Database
      $query = new Database(); // Database Query
      $conn = $query -> get_Connection(); // Get Database Connection
      
      $business_id = $_GET["business_id"];
      $brgy_id = $_GET["brgy_id"];
      
      $sql = "SELECT
              business.id AS 'Business ID',
              business.name AS 'Business Name',
              business.img_name AS 'Img Name',
              business.img_extension AS 'Img Extension',
              business.status AS 'Business Status',
              business.establish AS 'Business Establish',
              business.bus_permit AS 'Business Permit',
              business.contact_no AS 'Contact No',

              user_info.id AS 'Owner ID',
              CONCAT(user_info.first_name, ' ', user_info.middle_name, ' ', user_info.last_name) AS 'Owner Name',
              business.address AS 'Address',
              purok.name AS 'Purok Name',
              business_form.form AS 'Business Form',
              business_type.type AS 'Business Type'

              FROM business
              INNER JOIN user_info ON business.info_id = user_info.id
              INNER JOIN purok ON purok.id = business.purok_id
              INNER JOIN business_form ON business_form.id = business.form_id
              INNER JOIN business_type ON business_type.id = business.type_id
              WHERE business.id = $business_id AND purok.brgy_id = $brgy_id
              ORDER BY business.id DESC";

      $result = $conn->query($sql);  

      if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {

          $_SESSION['brms_business_ImgName'] =  $row["Img Name"];
          $_SESSION['brms_business_Extension'] =  $row["Img Extension"];
          $_SESSION['brms_business_status'] =  $row["Business Status"];          
          $_SESSION['brms_business_establish'] =  $row["Business Establish"];

          $_SESSION['brms_business_permit'] =  $row["Business Permit"];
          $_SESSION['brms_business_contact'] =  $row["Contact No"];

          $_SESSION['brms_business_id'] = $row["Business ID"];
          $_SESSION['brms_business_name'] = $row["Business Name"];
          $_SESSION['brms_business_owner_id'] = $row["Owner ID"];
          $_SESSION['brms_business_owner_name'] = $row["Owner Name"];
          $_SESSION['brms_business_address'] = $row["Address"];
          $_SESSION['brms_business_purok'] = $row["Purok Name"];
          $_SESSION['brms_business_form'] = $row["Business Form"];
          $_SESSION['brms_business_type'] = $row["Business Type"];
        }
        header('Location: ../brgy_admin/profile_business.php');
      }else{
        header('Location: ../brgy_admin/admin_home.php');
      }
    
  }else{
    header('Location: ../brgy_admin/admin_home.php');
  }
?>