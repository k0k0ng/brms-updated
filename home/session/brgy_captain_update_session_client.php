<?php
  session_start();

  // Set SESSION to client
    
  if (!empty($_GET)) {

    function updateSession() {
      
      include '../database/database.php'; // Database
      $query = new Database(); // Database Query
      $conn = $query -> get_Connection(); // Get Database Connection
      
      $info_id = $_GET["info_id"];
      $brgy_id = $_GET["brgy_id"];

      $sql = "SELECT 
              user_info.id AS 'id',
              user_info.first_name AS 'fname',
              user_info.last_name AS 'lname',
              user_info.middle_name AS 'mname',

              user_info.img_name AS 'img_name',
              user_info.img_extension AS 'img_extension',

              user_info.purok_id AS 'purok_id',
              purok.name AS 'purok_name',

              user_info.address AS 'address',
              user_info.birthdate AS 'bdate',
              user_info.gender AS 'gender',

              user_info.status AS 'status',
              user_info.blood_type AS 'blood_type',
              user_info.education AS 'education',
              user_info.occupation AS 'occupation',

              user_info.cell_no AS 'cell',
              user_info.tell_no AS 'tell',

              user_info.is_voter AS 'is_voter',
              user_info.voter_id AS 'voter_id',
              user_info.citizen_brgy_id AS 'citizen_brgy_id',

              user_info.residence_type AS 'residence_type',
              user_info.cur_employed AS 'cur_employed',
              user_info.pwd AS 'pwd',
              user_info.cur_enrolled AS 'cur_enrolled',
              user_info.senior_citizen AS 'senior_citizen',
              user_info.deceased AS 'deceased',
              user_info.religion AS 'religion'             

            FROM user_info 
            LEFT OUTER JOIN purok ON user_info.purok_id = purok.id
            WHERE user_info.brgy_id = $brgy_id AND user_info.id = $info_id";

      $result = $conn->query($sql);

      if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {  

          $_SESSION['brms_NEW_userId'] = $row["id"];
          $_SESSION['brms_NEW_userFname'] = $row["fname"];
          $_SESSION['brms_NEW_userLname'] = $row["lname"];
          $_SESSION['brms_NEW_userMname'] = $row["mname"];

          $_SESSION['brms_NEW_userImgName'] = $row["img_name"];
          $_SESSION['brms_NEW_userImgExtension'] = $row["img_extension"];

          $_SESSION['brms_NEW_userPurokId'] = $row["purok_id"];
          $_SESSION['brms_NEW_userPurokName'] = $row["purok_name"];

          $_SESSION['brms_NEW_userAddress'] = $row["address"];
          $_SESSION['brms_NEW_userBdate'] = $row["bdate"];
          $_SESSION['brms_NEW_userGender'] = $row["gender"];

          $_SESSION['brms_NEW_userStatus'] = $row["status"];
          $_SESSION['brms_NEW_userBlood_type'] = $row["blood_type"];
          $_SESSION['brms_NEW_userEducation'] = $row["education"];
          $_SESSION['brms_NEW_userOccupation'] = $row["occupation"];

          $_SESSION['brms_NEW_userCell'] = $row["cell"];
          $_SESSION['brms_NEW_userTell'] = $row["tell"];

          $_SESSION['brms_NEW_userIsVoter'] = $row["is_voter"];

          $_SESSION['brms_NEW_userVoterId'] = $row["voter_id"];
          $_SESSION['brms_NEW_userCitizenBrgyId'] = $row["citizen_brgy_id"];

          $_SESSION['brms_NEW_userResidenceType'] = $row["residence_type"];
          $_SESSION['brms_NEW_userCurEmployed'] = $row["cur_employed"];
          $_SESSION['brms_NEW_userPwd'] = $row["pwd"];
          $_SESSION['brms_NEW_userCurEnrolled'] = $row["cur_enrolled"];
          $_SESSION['brms_NEW_userSeniorCitizen'] = $row["senior_citizen"];
          $_SESSION['brms_NEW_userDeceased'] = $row["deceased"];
          $_SESSION['brms_NEW_userReligion'] = $row["religion"];
        }   
      }      
      $_SESSION['brms_ADMIN'] = 0; // UPDATE ADMIN SESSION TO CLIENT
      mysqli_close($conn);
      header('Location: ../brgy_captain/profile.php'); 
    }
    
    updateSession(); // START UPDATE
    
  }else{
    header('Location: ../brgy_captain/admin_home.php');
  }
?>